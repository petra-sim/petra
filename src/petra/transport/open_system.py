from __future__ import annotations
from typing import Collection, Optional, TYPE_CHECKING

import numpy as np
import scipy.linalg as la

from petra import constants as c
from petra import linalg as pla
from petra.transport.statespace import StateSpace
from petra.util import log
from petra.util.indexing import Indexer
from petra.util.sparse import MatrixBuilder
from petra.util.progress import progress

if TYPE_CHECKING:
    from petra.transport.structure import Structure

logger = log.getLogger(__name__)


def solve_E(structure, E):
    return H(structure, E).inject()


def solve_Es(structure: Structure,
             E: Collection[float],
             weights: Optional[Collection[float]] = None):
    if weights is None:
        weights = np.ones(len(E))

    states = []
    sweights = []
    for Ei, wi in progress(list(zip(E, weights)), name='Injecting waves'):
        statesi = solve_E(structure, Ei)
        states += statesi
        sweights += [wi] * len(statesi)

    occupations = None
    if structure.occupation is not None:
        occupations = [structure.occupation(state) for state in states]

    return StateSpace(structure, sweights, states, occupations)


def solve_k(structure, contact, k_a,
            Emin=-np.inf, Emax=np.inf, nmin=0, nmax=np.inf):
    icontact = structure.elements.contacts.index(contact)
    bs = contact.bandstructure()
    E_s, psi_is = bs.solve_k(k_a)
    n = np.r_[:E_s.size]
    mask = (Emin <= E_s) & (E_s <= Emax) & (nmin <= n) & (n <= nmax)
    E_s, psi_is = E_s[mask], psi_is[:, mask]
    v_s = bs.velocity(psi_is, k=k_a)
    mask = (v_s > 0)
    E_s, psi_is, v_s = E_s[mask], psi_is[:, mask], v_s[mask]

    states = []
    for E, psi_i, v in zip(E_s, psi_is.T, v_s):
        dos = contact.dk
        Hi = H(structure, E)
        mode = BoundaryMode(icontact, E, psi_i, v, dos)
        states += Hi.inject([mode])
    return states


class QTBMHamiltonian:
    """Open system Hamiltonian based on QTBM.

    Attributes:
        N (int): the rank
        E (float): the energy
        boundaries: the injecting boundaries
    """
    def __init__(self, structure, E):
        """Initialize the energy independent parts of the Hamiltonian.

        Args:
            structure (:obj:`petra.structure.Structure`)
            E (float): The total energy of evaluation.
        """
        self.structure = structure

        self.N = self.structure.nodes.idx.size
        self.E = E

        self.boundaries = [DirectContactBoundary(icontact, contact, self.E)
                           for icontact, contact
                           in enumerate(self.structure.elements.contacts)]

    def inject(self, modes=None, calc_transmission=True):

        if modes is None:
            modes = self.injectable

        if not any(modes):
            return []

        A_II = self.A()
        b_Is = self.b(modes)
        c_Is = pla.solve(A_II, b_Is)

        T_sc = None
        if calc_transmission:
            T_sc = self.calculate_transmission(modes, c_Is)

        return [ExtendedState(mode, c_Is[:, imode], T_sc[imode])
                for imode, mode in enumerate(modes)]

    def calculate_transmission(self, modes, c_Is):
        T_sc = np.empty((len(modes), len(self.boundaries)))
        for i_in, mode_in in enumerate(modes):
            J_in = mode_in.J
            for i_out, boundary_out in enumerate(self.boundaries):
                nodes = boundary_out.element.nodes.parents
                nodes_idx = self.structure.nodes.idx[nodes]
                psi = c_Is[nodes_idx, i_in]
                if mode_in.icontact == i_out:
                    # calculating reflection
                    boundary_in = self.boundaries[mode_in.icontact]
                    psi = psi - boundary_in.psi_I(mode_in.c_i)
                J_out = boundary_out.probJ_out(psi)
                T_sc[i_in, i_out] = np.abs(J_out / J_in)
        return T_sc

    @property
    def has_running_solutions(self):
        return any(boundary.modes for boundary in self.boundaries)

    def Sigma(self):
        """The boundary self energies.
        """
        build = MatrixBuilder((self.N, self.N))
        for boundary in self.boundaries:
            sel = self.structure.nodes.idx[boundary.element.nodes.parents]
            build.add(sel, sel, boundary.sigma)
        return build.matrix.tocsc()

    def A(self):
        """The lefthand side of the system of equations.
        """
        return self.E * self.structure.M - self.structure.H - self.Sigma()

    def b_spectral(self):
        """The rhs to calculate the spectral function.
        """
        all_modes_idx = Indexer((boundary, boundary.sigma.shape[1])
                                for boundary in self.boundaries)

        N_modes = all_modes_idx.size
        b = np.zeros((self.N, N_modes), dtype=complex)
        for ii, boundary in enumerate(self.boundaries):
            node_idx = self.structure.nodes.idx[boundary.element.nodes.parents]
            mode_idx = all_modes_idx[boundary]
            b[node_idx, mode_idx] = np.identity(boundary.sigma.shape[1])
        return b

    @property
    def injectable(self):
        """The rhs injection term for all running states in the system.
        """
        return sum((boundary.modes for boundary in self.boundaries), [])

    def b(self, modes):
        """The righthand side of the system of equation.
        """
        b_Is = np.zeros((self.N, len(modes)), dtype=complex)
        for imode, mode in enumerate(modes):
            boundary = self.boundaries[mode.icontact]
            node_idx = self.structure.nodes.idx[boundary.element.nodes.parents]
            b_Is[node_idx, imode] = boundary.b_Is(mode)
        return b_Is


H = QTBMHamiltonian


class BoundaryMode:
    """An injectable mode"""
    __slots__ = {
        'icontact': 'The injecting contact',
        'E': 'State energy',
        'c_i': 'The wavefunction coefficients',
        'J': 'The injecting probability current',
        'dos': 'The density of states of each mode',
    }

    def __init__(self, icontact, E, c_i, J, dos):
        self.icontact = icontact
        self.E = E
        self.c_i = c_i
        self.J = J
        self.dos = dos

    def __repr__(self):
        return '{}(E={:.2f} eV, contact={}, J={:.2g})'.format(
            type(self).__name__,
            self.E / c.eV,
            self.icontact,
            self.J)


class ExtendedState:
    """Solution of the open system."""
    __slots__ = ('E', 'c_I', 'T_c', 'J', 'dos', 'icontact')

    def __init__(self, mode, c_I, T_c):
        self.E = mode.E
        self.J = mode.J
        self.dos = mode.dos
        self.icontact = mode.icontact

        self.c_I = c_I
        self.T_c = T_c

    def __repr__(self):
        Tstr = ', '.join('{:.2f}'.format(T) for T in self.T_c)
        return '{}(E={:.2f} eV, contact={}, J={:.2g}, T=[{}])'.format(
            type(self).__name__,
            self.E / c.eV,
            self.icontact,
            self.J,
            Tstr)


class DirectContactBoundary:
    """The boundary terms of a contact at a given energy.
    """

    def __init__(self, icontact, element, E, alpha=None):
        """
        Arguments
        ---------
        element : petra.element.Element
            The boundary element
        E : float
            The energy
        alpha : (3) array_like of complex
            Phase factors
        """

        self.icontact = icontact
        self.element = element
        self.E = E
        self.sense = self.element.sense_v[self.element.direction]

        cmodes = element.bandstructure().solve_E(E, alpha_a=alpha)

        self.inv_psi_out = la.inv(cmodes.psi_out)
        alpha_out = cmodes.alpha_out**(-self.sense)
        B_out = self.bloch_matrix(cmodes.psi_out, alpha_out, self.inv_psi_out)

        self.inv_psi_in = la.inv(cmodes.psi_in)
        alpha_in = cmodes.alpha_in**(-self.sense)
        B_in = self.bloch_matrix(cmodes.psi_in, alpha_in, self.inv_psi_in)

        self.HEM = self.contact_HEM()
        self.sigma = self.HEM @ B_out
        self.sigma_dual = self.HEM @ B_in

        self.v_out_s = cmodes.velocity[cmodes.all_out]

        # calculate the modes
        psi_is = cmodes.psi[:, cmodes.running_in]

        v_in_s = cmodes.velocity[cmodes.running_in]
        dos_s = np.abs(1. / v_in_s)

        self.modes = [BoundaryMode(self.icontact, self.E, psi_i, J, dos)
                      for psi_i, J, dos in zip(psi_is.T, v_in_s, dos_s)]

    def bloch_matrix(self, c_ni, alpha_i, c_inv_in=None):
        """Calculates the Bloch matrix that propagates solutions in the contact.

        Arguments
        ---------
        c_ni : (N, N) np.ndarray of complex
            Wavefunction coefficients spanning subspace of modes
        alpha_i : (N) np.ndarray of complex
            Phase factors
        c_inv_in : (N, N) np.ndarray of complex
            Inverse mapping of modes subspace

        Returns
        -------
        (M, M) np.ndarray of complex
            Bloch matrix
        """
        if c_inv_in is None:
            c_inv_in = la.inv(c_ni)
        idx = self.element.nodes.idx
        real_idx = self.element.nodes.interior.idx
        Alpha_ii = np.diag(alpha_i)
        B = np.eye(idx.size, dtype=complex)
        for node in self.element.nodes.periodic:
            ifull, ibs = idx[node], real_idx[node.origin]
            B[ifull, ifull] = c_ni[ibs, ibs] @ Alpha_ii @ c_inv_in[ibs, ibs]
        return B

    def contact_HEM(self):
        '''Calculates (H'- E M')

        Returns
        -------
        (N, N) np.ndarray of complex
            H'- E M'
        '''
        idx = self.element.nodes.idx
        shape = (2, idx.size, idx.size)
        H, M = np.zeros(shape, dtype=complex)

        for n0, n1, h, m, _ in self.element.HM_contact:
            i0, i1 = idx[n0], idx[n1]
            H[i0, i1] += h
            M[i0, i1] += m

        return H - self.E * M

    def psi_I(self, psi_i_):
        idx = self.element.nodes.idx
        psi_I_ = np.zeros((idx.size,) + psi_i_.shape[1:], dtype=complex)
        for node in self.element.nodes.periodic:
            ifull = idx[node]
            ibs = self.element.nodes.interior.idx[node.origin]
            psi_I_[ifull] = psi_i_[ibs]
        return psi_I_

    def b_Is(self, mode):
        return (self.sigma_dual - self.sigma) @ self.psi_I(mode.c_i)

    def probJ_out(self, psis):
        """The outflowing probability current of given wavefunctions"""
        J_out = self.v_out_s
        idx = self.element.nodes.interior.idx
        shape = (idx.size,) + psis.shape[1:]
        psis_bs = np.zeros(shape, dtype=complex)
        for node in self.element.nodes.periodic:
            ifull = self.element.nodes.idx[node]
            ibs = self.element.nodes.interior.idx[node.origin]
            psis_bs[ibs] = psis[ifull]
        prob = np.abs(self.inv_psi_out @ psis_bs)**2
        return J_out @ prob
