import numpy as np

from petra.io.vasp.constants import Ang
from petra import constants as c
from petra.io.vasp.util import CommonFile


class DOSCAR(CommonFile):

    def _read(self, f):
        line = f.readline().strip()
        nion_all, self.nion, self.partial, ncdij = map(int, line.split())
        line = f.readline().strip()
        volume, a1, a2, a3, self.potim = map(float, line.split())
        self.volume = volume * Ang**3
        self.absa = np.array([a1, a2, a3]) * Ang
        line = f.readline().strip()
        self.T_begin = float(line)
        line = f.readline().strip()
        assert(line == 'CAR')
        line = f.readline().strip()
        self.name = line
        line = f.readline().strip()
        Emin, Emax, n, Efermi, one = line.split()
        assert(float(one) == 1.)
        self.Emin = float(Emin) * c.eV
        self.Emax = float(Emax) * c.eV
        self.Efermi = float(Efermi) * c.eV
        self.n = int(n)

        self.E = np.empty(self.n)
        self.dos = np.empty(self.n)
        self.idos = np.empty(self.n)
        for ii in range(self.n):
            line = f.readline().strip()
            self.E[ii], self.dos[ii], self.idos[ii] = map(float,
                                                          line.split())
        self.E *= c.eV
        self.dos /= c.eV
