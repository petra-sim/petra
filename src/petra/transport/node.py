'''
Provides Node and NodeList classes.
'''
from collections import Iterable
from functools import cached_property
from typing import List, MutableSequence

import numpy as np

from petra.util.indexing import Indexer
from petra.util.lazy import invalidate_cached_property


class Node(object):
    '''
    A Node is the most basic building block of the structure's PUM mesh.
    '''
    __slots__ = ('owner', 'position', 'children', 'parent', '_dof')

    def __init__(self, owner, position, children=None):
        self.owner = owner
        self.position = position
        self.children = NodeList()
        if children is not None:
            for child in children:
                self.children.append(child)
        self.parent = None
        self._dof = None

    @property
    def dof(self):
        if self._dof is not None:
            return self._dof
        if len(self.children) > 0:
            dof = self.children[0].dof
            if all(child.dof == dof for child in self.children[1:]):
                return dof
            raise AttributeError("degrees of freedom (dof) not defined for "
                                 "this node and children have different dof.")
        try:
            return self.owner.dof
        except AttributeError:
            raise AttributeError("degrees of freedom (dof) not defined for "
                                 "this node and its owner.")

    def distance(self, node):
        return np.sqrt(np.sum((self.position - node.position)**2))

    def __repr__(self):
        return "{}({:7.2f}, {:7.2f}, {:7.2f})".format(type(self).__name__,
                                                      *self.position)

    def make_parent(self, owner):
        if self.parent is None:
            self.parent = Node(owner, self.position, children=[self])
            return self.parent
        raise ValueError('This node already has a parent')


class EdgeNode(Node):
    pass


class PeriodicNode(EdgeNode):
    __slots__ = ('origin')

    def __init__(self, owner, position, children=None, origin=None):
        super().__init__(owner, position, children)
        self.origin = origin

    @property
    def dof(self):
        return self.origin.dof

    @property
    def period(self):
        return self.origin.position - self.position


class NodeList(MutableSequence[Node]):
    '''
    A list of Nodes with advanced filtering and bulk property access.
    '''

    def __init__(self, *args, **kwargs):
        self._list: List[Node] = list(*args, **kwargs)

    def __getitem__(self, key):
        result = self._list.__getitem__(key)
        if isinstance(result, Iterable):
            return NodeList(result)
        return result

    def __setitem__(self, key, item):
        self._list.__setitem__(key, item)
        self.invalidate()

    def __delitem__(self, key):
        self._list.__delitem__(key)
        self.invalidate()

    def __len__(self):
        return self._list.__len__()

    def insert(self, index, item):
        self._list.insert(index, item)
        self.invalidate()

    def invalidate(self):
        for attr in ('idx', 'position', 'interior', 'parents', 'edge', 'periodic'):
            invalidate_cached_property(self, attr)

    @cached_property
    def idx(self):
        return Indexer([(node, node.dof) for node in self._list])

    @cached_property
    def position(self):
        '''Returns positions of all nodes in this collection.'''
        return np.array([node.position for node in self._list])

    @cached_property
    def interior(self):
        '''Returns all nodes that are not on the edge.'''
        return NodeList(node for node in self
                        if not isinstance(node, PeriodicNode))

    @cached_property
    def parents(self):
        return NodeList(node.parent for node in self._list)

    @cached_property
    def edge(self):
        return NodeList(node for node in self._list
                        if isinstance(node, EdgeNode))

    @cached_property
    def periodic(self):
        return NodeList(node for node in self._list
                        if isinstance(node, PeriodicNode))

    def near(self, refnode, distance=1e-6):
        # by default: a milionth of a Bohr radius (~0.05 fm)
        return NodeList(node for node in self._list
                        if node.distance(refnode) < 1e-6)

    def __repr__(self):
        title = 'NodeList(['
        space = ' ' * len(title)
        nodes = ('\n' + space).join([repr(node) for node in self._list])
        return title + nodes + '])'
