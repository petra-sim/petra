import itertools
import collections

import numpy as np

from petra.util import log
from petra import constants as c
from petra.densitymesh import DensityMesh

logger = log.getLogger(__name__)


class Device:

    def __init__(self, structure, mesh=None, r_boundary_rv=None):
        self.structure = structure

        self.density_mesh = DensityMesh([structure], mesh=mesh, r_boundary_rv=r_boundary_rv)

        self.gates = collections.OrderedDict()
        self.dopings = collections.OrderedDict()
        self.dielectrics = collections.OrderedDict()

    def _evaluate(self, where, value):
        r_rv = self.density_mesh.r_rv
        if callable(where):
            where = where(r_rv)
        if callable(value):
            value = value(r_rv[where])
        if np.all(~where):
            raise ValueError('No mesh points in geometry')
        return where, value

    def define_gate(self, where, value=None, name=None):
        '''Define a gate.'''
        if value is None and name is None:
            raise ValueError('For an unnamed gate, a value has to be '
                             'specified.')

        if name is None:
            for i in itertools.count():
                name = 'unnamed_gate_{}'.format(i)
                if name not in self.gates:
                    break

        logger.begin('adding gate {}'.format(name))
        self.gates[name] = self._evaluate(where, value)
        logger.end('adding gate {}'.format(name))
        return self

    def remove_gate(self, name):
        '''Remove a gate.'''
        del self.gates[name]
        return self

    def set_gate(self, name, value):
        '''Set the value of a gate.'''
        where, _ = self.gates[name]
        logger.begin('setting gate {}'.format(name))
        self.gates[name] = self._evaluate(where, value)
        logger.end('setting gate {}'.format(name))
        return self

    def define_dielectric(self, where, value=None, name=None):
        '''Define the dielec constant.'''
        if value is None and name is None:
            raise ValueError('For an unnamed dielectric, a value has to be '
                             'specified.')
        if name is None:
            for i in itertools.count():
                name = 'unnamed_dielectric_{}'.format(i)
                if name not in self.dielectrics:
                    break

        logger.begin('adding dielectric {}'.format(name))
        self.dielectrics[name] = self._evaluate(where, value)
        logger.end('adding dielectric {}'.format(name))
        return self

    def remove_dielectric(self, name):
        '''Remove a dielectric.'''
        del self.dielectrics[name]
        return self

    def set_dielectric(self, name, value):
        '''Set the value of a dielectric.'''
        where, _ = self.dielectrics[name]
        logger.begin('setting gate {}'.format(name))
        self.dielectrics[name] = self._evaluate(where, value)
        logger.end('setting gate {}'.format(name))
        return self

    def define_doping(self, where, value=None, name=None):
        '''Define a doping.'''
        if value is None and name is None:
            raise ValueError('For an unnamed doping, a value has to be '
                             'specified.')
        if name is None:
            for i in itertools.count():
                name = 'unnamed_doping_{}'.format(i)
                if name not in self.dopings:
                    break

        logger.begin('adding doping {}'.format(name))
        self.dopings[name] = self._evaluate(where, value)
        logger.end('adding doping {}'.format(name))
        return self

    def remove_doping(self, name):
        '''Remove a doping.'''
        del self.dopings[name]
        return self

    def set_doping(self, name, value):
        '''Set the value of a doping.'''
        where, _ = self.dopings[name]
        logger.begin('setting doping {}'.format(name))
        self.dopings[name] = self._evaluate(where, value)
        logger.end('setting doping {}'.format(name))
        return self

    def gate(self):
        gate = np.ones(self.density_mesh.size) * np.nan
        for idx, value in self.gates.values():
            gate[idx] = value
        return gate

    def doping(self):
        doping = np.zeros(self.density_mesh.size)
        for idx, value in self.dopings.values():
            doping[idx] += value
        return doping

    def dielectric(self):
        '''The dielectric tensor (xx, yy, zz, yz, xz, xy) at each position'''
        dielectric = np.zeros((self.density_mesh.size, 6))
        dielectric[:, :3] = 1
        for idx, value in self.dielectrics.values():
            value = np.atleast_1d(value)
            if value.size not in (1, 3, 6):
                raise ValueError('dielectric constant has to be given as a '
                                 'constant (xx=yy=zz), '
                                 'a 3-vector (xx, yy, zz), '
                                 'or a 6-vector (xx, yy, zz, yz, xz, zy).')
            if value.size == 1:
                value = np.ones(3) * value
            dielectric[idx, :value.shape[0]] = value
        return dielectric

    def write_vtk(self, fname, **data):
        '''Write the device structure to a VTK file.'''
        gate_region = np.zeros(self.density_mesh.size, dtype=int)
        for ii, (name, (idx, value)) in enumerate(self.gates.items()):
            gate_region[idx] = ii + 1

        doping_region = np.zeros(self.density_mesh.size, dtype=int)
        for ii, (name, (idx, value)) in enumerate(self.dopings.items()):
            doping_region[idx] = ii + 1

        dielectric_region = np.zeros(self.density_mesh.size, dtype=int)
        for ii, (name, (idx, value)) in enumerate(self.dielectrics.items()):
            dielectric_region[idx] = ii + 1

        properties = dict(
            gate_region=gate_region,
            gate=self.gate() / c.eV,
            dielectric_region=dielectric_region,
            dielectric=self.dielectric(),
            doping_region=doping_region,
            doping=self.doping() * c.cm3,
        )
        properties.update(data)

        self.density_mesh.write_vtk(fname, **properties)

    def set_potential(self, V_r, name='default'):
        self.density_mesh.set_potential(V_r, name)

    def clear_potential(self, name):
        self.structure.clear_potential(name)
