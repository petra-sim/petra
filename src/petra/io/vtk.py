import numpy as np
import meshio

from petra.mesh import Mesh
from petra.util import mpi
from petra import constants as c

simplex_types = {4: 'tetra'}
r_simplex_types = {'tetra': 4}


@mpi.only_root
def save_mesh(fname, mesh, point_data=None, cell_data=None, field_data=None):
    '''Save the mesh and associated data to a .vtu file.
    '''
    simplex_type = simplex_types[mesh.simplices.shape[1]]

    if cell_data is None:
        cell_data = {}
    else:
        cell_data = {key: [value] for key, value in cell_data.items()}
    cell_data['neighbors'] = [mesh.neighbors]

    meshio.write_points_cells(fname,
                              mesh.points / c.nm,
                              [(simplex_type, mesh.simplices)],
                              point_data=point_data,
                              cell_data=cell_data,
                              field_data=field_data)


def load_mesh(fname, return_data=True, which=0):
    '''Load a mesh and associated data from a .vtu file.
    '''
    meshdata = meshio.read(fname)
    simplex_type, simplices = meshdata.cells[which]

    points = meshdata.points * c.nm
    neighbors = meshdata.cell_data['neighbors'][which]
    mesh = Mesh(points, simplices, neighbors)

    if not return_data:
        return mesh

    return mesh, (meshdata.point_data, meshdata.cell_data, meshdata.field_data)


def write_points(fname, points, **data):
    vertex = np.r_[:points.shape[0]].reshape((-1, 1))
    cells = [('vertex', vertex)]
    cell_data = {key: [value] for key, value in data.items()}

    meshio.write_points_cells(fname, points, cells, cell_data=cell_data)


def write_atoms(fname, atoms):
    '''Write an ase.atoms object to a .vtu file.
    '''
    points = atoms.get_positions() * c.Ang / c.nm
    numbers = atoms.numbers
    masses = atoms.get_masses()

    write_points(fname, points, Z=numbers, mass=masses)


def write_bonds(fname, atoms, bonds):
    '''Write bonds between atoms in an ase.atoms object to a .vtu file.
    '''
    points = atoms.get_positions() * c.Ang / c.nm
    cells = [('line', bonds)]
    meshio.write_points_cells(fname, points, cells)


@mpi.only_root
def write_points_set(fname, **points_set):
    '''Convenience function to write simple points to a .vtu file.
    '''
    points = []
    vertex_data = []
    field_data = {}

    for ii, key in enumerate(points_set):
        points.append(points_set[key])
        vertex_data.append(ii * np.ones(points_set[key].shape[0], dtype=int))
        field_data[key] = np.array(ii)

    points = np.concatenate(points)
    vertex_data = np.concatenate(vertex_data)
    vertex = np.r_[:points.shape[0]].reshape((-1, 1))
    cells = [('vertex', vertex)]
    cell_data = {'physical': [vertex_data]}

    meshio.write_points_cells(fname,
                              points,
                              cells,
                              cell_data=cell_data,
                              field_data=field_data)
