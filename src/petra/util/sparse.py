import numpy as np
import scipy.sparse as sparse


def wipe_csr_rows(matrix, rows):
    ''' Clears multiple rows of a csr matrix and sets the diagonal entries
    to one. This method does not change the sparsity.
    '''
    assert isinstance(matrix, sparse.csr_matrix)

    # clears rows
    for i in rows:
        matrix.data[matrix.indptr[i]:matrix.indptr[i + 1]] = 0.0


class MatrixBuilder:
    def __init__(self, shape):
        self.shape = shape
        self.iall = np.r_[0:shape[0]]
        self.jall = np.r_[0:shape[1]]
        self.idx = []
        self.data = []

    def add(self, sel0, sel1, data):
        i, j = self.iall[sel0], self.jall[sel1]
        ii, jj = np.meshgrid(i, j, indexing='ij')
        self.idx.append(np.array([ii.ravel(), jj.ravel()]))
        self.data.append(data.ravel())

    @property
    def matrix(self):
        data = np.concatenate(self.data)
        idx = np.concatenate(self.idx, axis=1)
        return sparse.coo_matrix((data, idx), shape=self.shape)
