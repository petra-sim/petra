import numpy as np
from collections import namedtuple

from petra import constants as c


Bandstructure = namedtuple('Bandstructure', ('k_ka', 'E_kn', 'psi_knn'))


def calculate(element, k_ka, retwave=False):
    bs = element.bandstructure()
    nk = k_ka.shape[0]
    E_kn = np.empty((nk, element.nbasis))

    psi_knn = None
    if retwave:
        psi_knn = np.empty((nk, element.nbasis, element.nbasis),
                           dtype=complex)

    for i, k_a in enumerate(k_ka):
        result = bs.solve_k(k_a, retwave=retwave)
        if retwave:
            E_kn[i], psi_knn[i] = result
        else:
            E_kn[i] = result

    return Bandstructure(k_ka=k_ka, E_kn=E_kn, psi_knn=psi_knn)


def sort(bandstructure):
    ''' Stable sort of the bandstructure (possibly sub-optimal)
    '''
    from numpy import linalg as la
    from petra.util.matching import stable_marriages
    if bandstructure.psi_knn is None:
        raise ValueError('psifunctions missing, but needed for sorting')
    sorted_E_kn = np.copy(bandstructure.E_kn)
    sorted_psi_knn = np.copy(bandstructure.psi_knn)
    for i, k_a in enumerate(bandstructure.k_ka):
        if not i:
            continue
        psi_nn = sorted_psi_knn[i - 1]
        invpsi_nn = la.inv(sorted_psi_knn[i])
        pref0_nn = np.abs(invpsi_nn @ psi_nn)
        psi_nn = sorted_psi_knn[i]
        invpsi_nn = la.inv(sorted_psi_knn[i - 1])
        pref1_nn = np.abs(invpsi_nn @ psi_nn)
        idx = stable_marriages(pref0_nn, pref1_nn)
        sorted_E_kn[i:, :] = sorted_E_kn[i:, idx]
        sorted_psi_knn[i:, :, :] = sorted_psi_knn[i:, :, idx]
    return Bandstructure(k_ka=bandstructure.k_ka,
                         E_kn=sorted_E_kn,
                         psi_knn=sorted_psi_knn)


def plot(bandstructure, element=None, direction=2, offset=0.,
         ax=None, **style):
    import matplotlib.pyplot as plt

    if ax is None:
        ax = plt.gca()

    ax.plot(bandstructure.k_ka[:, direction],
            (bandstructure.E_kn - offset) / c.eV,
            **style)

    if element is not None:
        ax.plot(element.k_na[:, direction],
                (element.E_n - offset) / c.eV,
                'kx')
        ax.set_ylim((element.E_n.min() - offset) / c.eV,
                    (element.E_n.max() - offset) / c.eV)
    ax.set_xlim(bandstructure.k_ka.min(),
                bandstructure.k_ka.max())


def calculate_complex(element, E, direction=2):
    bs = element.bandstructure()
    nbasis = element.nbasis
    k_nk = np.empty((len(E), 2 * nbasis), dtype=float)
    kind_nk = np.empty((len(E), 2 * nbasis), dtype=int)
    psi_nk = np.empty((len(E), 2 * nbasis, nbasis), dtype=complex)

    for i, Ei in enumerate(E):
        sol = bs.solve_E(Ei)
        k_nk[i, :] = sol.k_na
        kind_nk[i, :] = 0
        kind_nk[i, sol.running_in] = 1
        kind_nk[i, sol.running_out] = -1
        kind_nk[i, sol.decaying_in] = 2
        kind_nk[i, sol.decaying_out] = -2
        psi_nk[i, :, :] = sol.v
    return k_nk, kind_nk, psi_nk
