import itertools

import numpy as np
from scipy.spatial import cKDTree
from ase.data import covalent_radii, atomic_numbers


def find_bonds(atoms, bond_length=None, between=None, periodic=True):

    if bond_length is None and between is None:
        raise ValueError('The atom types and/or bond_length have to be specified')

    if between is not None and bond_length is None:
        atom0, atom1 = atomic_numbers[between[0]], atomic_numbers[between[1]]
        bond_length = covalent_radii[atom0] + covalent_radii[atom1]

    RR = np.eye(3, dtype=int)

    def R_helper(idxs):
        R = np.zeros((idxs.shape[0], 2, 3), dtype=int)
        R[:, 1] = 1
        return R

    tree = cKDTree(atoms.positions)
    idxs = np.array(sorted(tree.query_pairs(bond_length)))
    R = 0 * R_helper(idxs)

    def find_offset(Ri):
        nonlocal idxs
        nonlocal R
        tree_p = cKDTree(atoms.positions + Ri @ atoms.cell)
        idxs_p = tree.query_ball_tree(tree_p, bond_length)
        idxs_p = np.array(sorted((idx0, idx1)
                                 for idx0, idxs in enumerate(idxs_p)
                                 for idx1 in idxs))
        R_p = R_helper(idxs_p) * Ri
        if idxs_p.size > 0:
            idxs = np.concatenate((idxs, idxs_p), axis=0)
            R = np.concatenate((R, R_p), axis=0)

    if periodic:
        for Ri in RR[atoms.pbc]:
            find_offset(Ri)
            find_offset(-Ri)
        for R1, R2 in itertools.combinations(RR[atoms.pbc], 2):
            for Ri in (R1 + R2, R1 - R2, -R1 + R2, -R1 - R2):
                find_offset(Ri)

    if between is not None:
        allowed0 = np.array([atom.symbol == between[0] for atom in atoms])
        allowed1 = np.array([atom.symbol == between[1] for atom in atoms])
        mask = allowed0[idxs[:, 0]] & allowed1[idxs[:, 1]]
        mask |= allowed1[idxs[:, 0]] & allowed0[idxs[:, 1]]
        idxs = idxs[mask]
        R = R[mask]

    return idxs, R


def find_bond_positions(atoms, bond_length=None, between=None, periodic=True):
    idxs, R = find_bonds(atoms, bond_length,
                         between=between, periodic=periodic)
    return atoms.positions[idxs] + R @ atoms.cell


def valence_electrons(atom):
    if atom.number <= 2:
        return atom.number
    elif atom.number <= 18:
        return (atom.number - 2) % 8
    elif atom.number <= 54:
        vale = (atom.number - 18) % 18
        if vale < 3:
            return vale
        elif vale < 13:
            return vale - 2
        else:
            return vale - 10
    else:
        return (atom.number - 54) % 32
