from scipy.sparse.linalg import LinearOperator


def good_inv(Jinv, dx, df):
    Jinvdf = Jinv @ df
    alpha = (dx - Jinvdf) / (dx @ Jinvdf)

    def matvec(x):
        Jinvx = Jinv @ x
        return Jinvx + (dx @ Jinvx) * alpha

    return LinearOperator(Jinv.shape, matvec=matvec)
