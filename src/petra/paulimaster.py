from functools import partial

import numpy as np

from scipy import sparse
from scipy.sparse import linalg as spla

from petra import constants as c
from petra.distributions import fermi_dirac


class PauliMasterEquation:
    def __init__(self, device, statespace, M_ss):
        self.device = device
        self.statespace = statespace
        self.M_ss = M_ss

        self.dos_ss = sparse.diags(statespace.dos_s)
        self.w_ss = sparse.diags(statespace.weights)
        self.injection_ss = sparse.identity(len(statespace.states)) / c.h

    def ballistic_s(self):
        return fermi_dirac(self.statespace.E_s,
                           self.device.mu[self.statespace.icontact_s],
                           self.device.T)

    def V_ss(self, rho_s=None):
        if rho_s is None:
            rho_s = np.zeros(len(self.statespace.states))
        pauli_ss = sparse.diags(1. - rho_s)

        return 2 * np.pi / c.hbar * (pauli_ss @ self.dos_ss @ self.M_ss @ self.dos_ss)

    def scattering_rate_per_energy(self, rho_s=None):
        V_ss = self.V_ss(rho_s)
        srate_s = (self.w_ss @ V_ss).sum(0)
        return np.squeeze(np.asarray(srate_s))

    def scattering_rate_per_electron(self, rho_s=None):
        srate_s = self.scattering_rate_per_energy(rho_s)

        Leff_s = np.array([np.real(state.c_I.conj() @ self.device.structure.M0 @ state.c_I)
                           for state in self.statespace.states])
        dE_s = 1. / (Leff_s * self.statespace.dos_s)
        return srate_s * dE_s

    def solve(self, atol=1e-6, max_iter=100, pauli=True):
        rho0_s = self.ballistic_s()
        rhs_s = - self.injection_ss @ rho0_s

        rhoi_s = rho0_s
        for i in range(max_iter):

            rho_pauli = rhoi_s if pauli else None

            V_ss = self.V_ss(rho_pauli)

            srate_s = self.scattering_rate_per_energy(rho_pauli)
            srate_ss = sparse.diags(srate_s)

            lhs_ss = V_ss @ self.w_ss - srate_ss - self.injection_ss

            rho_s = spla.spsolve(lhs_ss, rhs_s)

            R = rhoi_s - rho_s
            error = np.sqrt((R**2).mean())
            if error < atol:
                break

            rhoi_s = rho_s

        else:
            print('WARNING: Not Converged! error = {}'.format(error))

        return rho_s


def I_noq(structure, istate, fstate, confined=''):
    confined = tuple('xyz'.index(c) for c in confined)
    overlap = 0.0
    for el, icoef, fcoef in structure.per_element(istate.c_I, fstate.c_I):
        dV_confined = np.prod(el.dr_v[confined])
        dV_free = np.prod(el.dr_v) / dV_confined
        if not confined:
            ipsi_r = el.psi_r(icoef)
            fpsi_r = el.psi_r(fcoef)
            psipsi = ipsi_r.conj() * fpsi_r
            overlap += np.sum(np.abs(psipsi)**2) * dV_free
        else:
            ipsi_xyz = el.psi_xyz(icoef)
            fpsi_xyz = el.psi_xyz(fcoef)
            psipsi = ipsi_xyz.conj() * fpsi_xyz
            overlap += np.sum(np.abs(np.sum(psipsi, axis=confined) * dV_confined)**2) * dV_free
    return overlap


I_noq_xyz = I_noq
I_noq_xy = partial(I_noq, confined='z')
I_noq_xz = partial(I_noq, confined='y')
I_noq_yz = partial(I_noq, confined='x')
I_noq_x = partial(I_noq, confined='yz')
I_noq_y = partial(I_noq, confined='xz')
I_noq_z = partial(I_noq, confined='xy')
