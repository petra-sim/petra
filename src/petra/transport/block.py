from warnings import warn
from functools import cached_property

import numpy as np
from numpy import linalg as la

from ase.data.vdw_alvarez import vdw_radii

from petra.mesh import pum
from petra.pwepp import PWEPP
from petra.util import log
from petra import constants as c
from petra.periodiccell import PeriodicCell
from petra.geometry import inside, union
from petra.distributions import fermi_dirac

logger = log.getLogger(__name__)


def from_calc(calc, ik=None, nband=None, masked_density=None):
    if isinstance(calc, str):
        calc = PWEPP(restart=calc)
        calc.states.basis = calc.basis.similar(antialias=0)

    pw = calc.states.basis
    grid = pw.to_grid()

    atoms = calc.atoms.copy()

    directions = calc.atoms.pbc.nonzero()[0]
    if len(directions) != 1:
        raise ValueError(
            "The atoms object in the calculator must have one and only one periodic direction."
        )
    direction = directions[0]

    k_ka = calc.states.k_ka
    E_kn = calc.states.E_kn

    ik_m, iband_m, direction = find_basis(k_ka, E_kn, direction, ik=ik, nband=nband)

    k_ma = k_ka[ik_m]
    E_m = E_kn[ik_m, iband_m]

    # meta data
    Efermi = calc.get_fermi_level()
    n_valence = calc.pseudopotentials.Nv
    g_spin = 2 // calc.get_number_of_spins()

    # grid
    r_xyza = grid.r_xyza
    offset_a = (r_xyza[1, 1, 1] - r_xyza[0, 0, 0]) / 2
    r_xyza = r_xyza + offset_a

    dV = calc.cell.volume / grid.size

    # get wavefunction and shift it with the offset
    U_mxyz = np.empty(ik_m.shape + grid.shape, dtype='complex')
    D_vmxyz = np.empty((3,) + ik_m.shape + grid.shape, dtype='complex')

    for ii, (ik, iband, k_a) in enumerate(zip(ik_m, iband_m, k_ma)):
        eikr_xyz = np.exp(2j * np.pi * (r_xyza @ k_a))

        U_mxyz[ii] = calc.states.psi(ik, iband, real=True, shift_a=offset_a) * eikr_xyz

        P_v = calc.hamiltonian.p(k_a)
        for d, P in enumerate(P_v):
            D_vmxyz[d, ii] = 1j * calc.states.psi(ik, iband, real=True, shift_a=offset_a,
                                                  operator=P) * eikr_xyz

    # normalization so that \int_{cell} d^3r |\psi(r)|^2 = V_{cell}
    U_mxyz *= np.sqrt(grid.size)
    D_vmxyz *= np.sqrt(grid.size)

    if masked_density is None:
        masked_density = bool(len(atoms))

    return Block(atoms, k_ma, E_m, U_mxyz, D_vmxyz, r_xyza, direction,
                 Efermi, n_valence, g_spin, dV, masked_density=masked_density)


class Block:
    """A reusable block that contains the PUM matrix elements

    Attributes
    ----------

    """

    def __init__(self, atoms, k_ma, E_m, U_mxyz, D_vmxyz, r_xyza, direction,
                 Efermi, n_valence, g_spin, dV, masked_density=True,
                 density_cutoff=0.01, calculate_matel=True):
        '''
        Parameters
        ----------
        source : object
            A source onject
        density_cutoff: float, optional
            The cutoff of the density grid (default: 1e-3).
        calculate_matel: bool, optional
            Immediately calculate matrix elements? (default: True).
        '''

        self.atoms = atoms
        self.k_na = k_ma
        self.E_n = E_m

        self.U_nxyz = U_mxyz
        self.D_vnxyz = D_vmxyz
        self.r_xyza = r_xyza
        self.direction = direction

        self.Efermi = Efermi
        self.n_valence = n_valence
        self.g_spin = g_spin

        self.density_cutoff = density_cutoff
        a = self.atoms.cell * c.Ang
        self.cell = PeriodicCell(a)

        self.r_xyzv = self.cell.to_cartesian(self.r_xyza)
        self.masked_density = masked_density

        if calculate_matel:
            self.calculate()

    @classmethod
    def from_calc(cls, calc, ik=None, nband=None):
        return from_calc(calc, ik, nband)

    def periodic_pairs(self, sense):
        return [(0, 1)] if sense < 0 else [(1, 0)]

    def calculate(self):
        self.calculate_basis()
        self.calculate_matrix_elements()
        self.calculate_density_basis()

    @logger.block('calculating Bloch-basis functions')
    def calculate_basis(self):
        '''Computes the matrix elements for this block from a WAVECAR

        Note
        ----
        The basis functions used in the calculation of the matrix
        elements are defined as $$ u_{ink}(r) \\exp(i•k(r-r_i)) $$,
        i.e. with the phase of their envelope aligned with the node r_i.
        Since the Bloch waves in the basis are returned as
        $$ u_{ink}(r) \\exp(ik•r) $$, we add a factor $$ \\exp(-ik•r_i) $$
        '''

        # generate nodes and PUM
        self.rnodes_na = np.zeros((2, 3))
        self.rnodes_na[1, self.direction] = 1.
        self.node_pos = self.cell.to_cartesian(self.rnodes_na)

        self.f_ixyz = np.array([pum.psi2(self.r_xyza[..., self.direction] - node)
                                for node in (0, 1)])
        self.df_ixyz = np.array([pum.dpsi2(self.r_xyza[..., self.direction] - node)
                                 for node in (0, 1)])
        self.df_ixyz /= self.cell.a_len[self.direction]

        self.phase_in = np.exp(-2j * np.pi * self.rnodes_na @ self.k_na.T)

        # calculate the basis functions
        self.phi_inxyz = np.einsum('nxyz,ixyz,in->inxyz', self.U_nxyz, self.f_ixyz, self.phase_in)

        # calculate the gradient of the basis functions
        self.dphi_vinxyz = np.einsum('vnxyz,ixyz,in->vinxyz',
                                     self.D_vnxyz, self.f_ixyz, self.phase_in)
        self.dphi_vinxyz[self.direction] += np.einsum('nxyz,ixyz,in->inxyz',
                                                      self.U_nxyz, self.df_ixyz, self.phase_in)

    @logger.block('calculating matrix elements')
    def calculate_matrix_elements(self):

        f_phase_inxyz = np.einsum('ixyz,in->inxyz', self.f_ixyz, self.phase_in)
        df_phase_inxyz = np.einsum('ixyz,in->inxyz', self.df_ixyz, self.phase_in)

        self.M_iinn = matel_iinn(self.phi_inxyz, self.phi_inxyz, self.dV)

        dfU_inxyz = df_phase_inxyz * self.U_nxyz

        T_iinn = matel_iinn(dfU_inxyz, dfU_inxyz, self.dV)

        fD_inxyz = f_phase_inxyz * self.D_vnxyz[self.direction]
        dfD_inxyz = df_phase_inxyz * self.D_vnxyz[self.direction]

        P_iinn = (matel_plus_hc(fD_inxyz, dfU_inxyz, self.dV) -
                  matel_plus_hc(dfD_inxyz, self.phi_inxyz, self.dV))

        E_nn = np.diag(self.E_n)
        self.E_iinn = ((E_nn @ self.M_iinn) + (self.M_iinn  @ E_nn)) / 2

        self.H_iinn = T_iinn / 2 + P_iinn / 4 + self.E_iinn

    @logger.block('calculating density basis')
    def calculate_density_basis(self):
        if not self.masked_density:
            self.density_mask_xyz = np.ones(self.U_nxyz.shape[1:], dtype=bool)
            return

        # include all points where the valence electron density is significant
        occ_n = fermi_dirac(self.E_n, self.Efermi, 300 * c.K)
        rho_nxyz = np.abs(self.U_nxyz * occ_n[:, None, None, None])
        maxrho_n = rho_nxyz.max(axis=(1, 2, 3)) * self.density_cutoff
        self.density_mask_xyz = np.any(rho_nxyz > maxrho_n[:, None, None, None], axis=0)

        # include all points within the van der Waals radii
        atoms = self.atoms.repeat(1 + (3 - 1) * self.atoms.pbc)
        atoms.translate(-(self.atoms.pbc @ self.atoms.cell))
        Z_i = atoms.get_atomic_numbers()
        filt = union(*(inside(atoms[Z_i == Z], vdw_radii[Z] * c.Ang) for Z in set(Z_i)))
        r_xyzv = self.cell.to_cartesian(self.r_xyza)
        self.density_mask_xyz |= filt(r_xyzv)

    def scalar_field_matrix(self, field):
        return matel_field_iinn(self.phi_inxyz, field, self.dV)

    @property
    def r_density_rv(self):
        return self.r_xyzv[self.density_mask_xyz]

    @cached_property
    def phi_density_inr(self):
        return self.phi_inxyz[..., self.density_mask_xyz]

    @property
    def dphi_density_vinr(self):
        return self.dphi_vinxyz[..., self.density_mask_xyz]

    @property
    def ndensity(self):
        return np.count_nonzero(self.density_mask_xyz)

    @property
    def dk(self):
        return (2 * np.pi) / la.norm(self.cell.a[self.direction])

    @property
    def nbasis(self):
        return self.k_na.shape[0]

    @property
    def dr_v(self):
        return self.r_xyzv[1, 1, 1] - self.r_xyzv[0, 0, 0]

    @property
    def dV(self):
        return np.prod(self.dr_v)


def find_basis(k_ka, E_kn, direction=None, ik=None, nband=None):
    logger = log.getLogger(Block.__name__)

    # find the direction of the wavevectors
    if direction is None:
        delta_k_a = np.ptp(k_ka, axis=0)
        directions = np.flatnonzero(delta_k_a)
        if directions.size != 1:
            raise ValueError('Could not determine a unique direction from '
                             'the provided wavevectors, please specify a direction or'
                             'check the wavevectors.')
        direction = directions[0]
        logger.info('Detected the {} direction.'.format(['x', 'y,', 'z'][direction]))

    k_k = np.abs(k_ka[:, direction])
    if ik is None:
        ik = (k_k.argmin(), k_k.argmax())
    ik = sorted(ik)
    logger.info('Using the k-points at {}.'.format(', '.join(map(str, ik))))
    if nband is None:
        nband = E_kn.shape[1]
    iband = range(nband)

    ik_m, iband_m = np.array([(i, j) for i in ik for j in iband]).T

    return ik_m, iband_m, direction


# Kernels to calculate the matrix elements

def _matel_fast_iinn(bra_inxyz, ket_inxyz, dV):
    '''Fastest matrix-element kernel

    May cause excessive memory consumption on old numpy versions.
    '''
    M_inin = np.tensordot(bra_inxyz.conj(), ket_inxyz, ([2, 3, 4], [2, 3, 4]))
    return np.transpose(M_inin, (0, 2, 1, 3)) * dV


def _matel_lowmem_iinn(bra_inxyz, ket_inxyz, dV):
    '''Low memory, but slow matrix-element kernel

    Should only be used as a fallback when matel_fast_iinn fails
    '''
    return np.einsum('inxyz,jmxyz->ijnm', bra_inxyz.conj(), ket_inxyz)


class MemoryWarning(UserWarning):
    pass


def matel_iinn(bra_inxyz, ket_inxyz, dV):
    try:
        return _matel_fast_iinn(bra_inxyz, ket_inxyz, dV)
    except MemoryError:
        warn('Out-of-memory: Falling back to slower method', MemoryWarning)

    return _matel_lowmem_iinn(bra_inxyz, ket_inxyz, dV)


# convenience functions

def matel_field_iinn(ket_inxyz, V_xyz, dV):
    return matel_iinn(ket_inxyz, V_xyz * ket_inxyz, dV)


def matel_plus_hc(bra_inxyz, ket_inxyz, dV):
    M_iinn = matel_iinn(bra_inxyz, ket_inxyz, dV)
    return M_iinn + np.transpose(M_iinn, (1, 0, 3, 2)).conj()
