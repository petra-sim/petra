'''PETRA

Usage:
    petra env
    petra parallel

Options:
    -h --help   Show this screen.
    --version   Show the version.
'''
import sys
from petra import __version__
from docopt import docopt


def environment():
    print(f'PETRA {__version__}')
    print('dependencies:')

    python_version = ' '.join(sys.version.split())
    print(f' ✓  python: {python_version}')

    import numpy
    print(f' ✓  numpy {numpy.__version__}')

    import scipy
    print(f' ✓  scipy {scipy.__version__}')

    import ase
    print(f' ✓  ase {ase.__version__}')

    print('optional:')
    from petra.util import mpi
    if mpi.is_enabled():
        import mpi4py
        print(f' ✓  mpi4py {mpi4py.__version__} with {mpi.vendor()}')
    else:
        print(' x  mpi4py not found')

    try:
        import mkl_fft
        print(f' ✓  mkl_fft {mkl_fft.__version__}')
    except ModuleNotFoundError:
        print(' x  mkl_fft not found')

    try:
        import pyfftw
        print(f' ✓  pyfftw {pyfftw.__version__}')
    except ModuleNotFoundError:
        print(' x  pyfftw not found')

    from petra.linalg import pardiso
    if pardiso.libmkl is not None:
        print(f' ✓  libmkl found at {pardiso.libmkl_path}, using MKL pardiso')
    else:
        print(' x  libmkl not found')

    try:
        import scikits.umfpack
        from scipy.sparse.linalg.dsolve import linsolve
        if linsolve.useUmfpack:
            print(f' ✓  scikit-umfpack {scikits.umfpack.__version__}, detected by scipy')
        else:
            print(f' x  scikit-umfpack {scikits.umfpack.__version__}, not detected by scipy')
    except ModuleNotFoundError:
        print(' x  scikit-umfpack not found')


def parallel():
    from petra.util import mpi, threading

    if mpi.is_enabled():
        import mpi4py
        if mpi.is_root():
            print(f'Found mpi4py {mpi4py.__version__} with {mpi.vendor()}')
        workers = mpi.comm.gather(
            (mpi.comm.rank, mpi.comm.size, mpi.node_name(), threading.optimal_threads())
        )
        if mpi.is_root():
            nodes = {}
            for rank, size, node, threads in workers:
                nodes[node] = nodes.get(node, 0) + threads
            print('Nodes:', flush=True)
            for node, threads in nodes.items():
                print(f' - {node} has {threads} cores')
            print('Workers:', flush=True)
            for rank, size, node, threads in workers:
                print(f' - worker {rank} of {size} on {node} can use {threads} threads')
    else:
        print('Could not find MPI/mpi4py')
        print(f'Threading can use {threading.optimal_threads()} threads')


def main():
    args = docopt(__doc__, version=f'PETRA {__version__}')

    if args['env']:
        environment()

    if args['parallel']:
        parallel()


if __name__ == "__main__":
    sys.exit(main())
