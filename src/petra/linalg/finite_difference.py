import numpy as np
from scipy import sparse

stencils = {
    'central': {
        1: {
            1: ([-1, 1],
                [-1 / 2, 1 / 2]),
            2: ([-2, -1, 1, 2],
                [1 / 12, -2 / 3, 2 / 3, -1 / 12]),
            3: ([-3, -2, -1, 1, 2, 3],
                [-1 / 60, 3 / 20, -3 / 4, 3 / 4, -3 / 20, 1 / 60]),
            4: ([-4, -3, -2, -1, 1, 2, 3, 4],
                [1 / 280, -4 / 105, 1 / 5, -4 / 5, 4 / 5, -1 / 5, 4 / 105, -1 / 280]),
        },
        2: {
            1: ([-1, 0, 1],
                [1, -2, 1]),
            2: ([-2, -1, 0, 1, 2],
                [-1 / 12, 4 / 3, -5 / 2, 4 / 3, -1 / 12]),
            3: ([-3, -2, -1, 0, 1, 2, 3],
                [1 / 90, -3 / 20, 3 / 2, -49 / 18, 3 / 2, -3 / 20, 1 / 90]),
            4: ([-4, -3, -2, -1, 0, 1, 2, 3, 4],
                [-1 / 560, 8 / 315, -1 / 5, 8 / 5, -205 / 72, 8 / 5, -1 / 5, 8 / 315, -1 / 560]),
        },
    },
    'forward': {
        1: {
            1: ([0, 1],
                [-1, 1]),
            2: ([0, 1, 2],
                [-3 / 2, 2, -1 / 2]),
            3: ([0, 1, 2, 3],
                [-11 / 6, 3, -3 / 2, 1 / 3]),
            4: ([0, 1, 2, 3, 4],
                [-25 / 12, 4, -3, 4 / 3, -1 / 4]),
            5: ([0, 1, 2, 3, 4, 5],
                [-137 / 60, 5, -5, 10 / 3, -5 / 4, 1 / 5]),
            6: ([0, 1, 2, 3, 4, 5, 6],
                [-49 / 20, 6, -15 / 2, 20 / 3, -15 / 4, 6 / 5, -1 / 6]),
        },
    }
}


def stencil(direction, derivative, order):
    _direction = direction
    if direction == 'backward':
        _direction = 'forward'
    offsets, values = map(np.array, stencils[_direction][derivative][order])
    if direction == 'backward':
        offsets *= -1
    return offsets, values


def D(N, derivative=1, direction='central', order=1, bc='dirichlet', phase=1.):
    offsets, values = stencil(direction, derivative, order)
    D = sparse.diags(values, offsets, shape=(N, N))
    if bc == 'dirichlet':
        return D

    D_left = sparse.diags(values[offsets < 0], offsets[offsets < 0] + N, shape=(N, N))
    D_right = sparse.diags(values[offsets > 0], offsets[offsets > 0] - N, shape=(N, N))
    if bc == 'neumann':
        return D + (D_left + D_right).tocsc()[:, ::-1]

    if bc == 'periodic':
        P = sparse.identity(N) * phase
        Pinv = sparse.identity(N) * phase**-1
        return D + D_left @ Pinv + D_right @ P

    raise ValueError('bc needs to be one of \'dirichlet\', \'neumann\', or \'periodic\'.')


def kron(*Xs, order='C'):
    if len(Xs) == 1:
        Xs = Xs[0]
    if order == 'F':
        Xs = Xs[::-1]
    product = Xs[0]
    for X in Xs[1:]:
        product = sparse.kron(product, X)
    return product


def kronsum(*Xs, order='C'):
    if len(Xs) == 1:
        Xs = Xs[0]
    Is = [sparse.identity(X.shape[0]) for X in Xs]

    return sum(
        kron([(X if i == j else I) for j, I in enumerate(Is)], order=order)
        for i, X in enumerate(Xs)
    )
