Installation
============

**Important**: If you want to develop PETRA, see :doc:`/develop` for installation instructions.

Using conda
-----------

Installation of PETRA on workstations is easiest using the conda package manager.

1. Install the miniconda_ package. [#fanaconda]_
2. Download PETRA's environment file: :source:`environment.yml`.
3. Create and activate the conda environment:

   .. code-block:: console

      conda env create
      conda activate petra

4. Install PETRA using pip:

   .. code-block:: console

      pip install https://gitlab.com/petra-sim/petra.git


To use, always activate the petra environment

   .. code-block:: console

      conda activate petra

To check which dependencies are installed, you can run

   .. code-block:: console

      petra env


Using Containers
----------------

PETRA provides Singularity_ container specifications for certain HPC environments. Make sure you
select the container with a matching MPI implementation to the host for compatibility and
efficiency.

Local using Intel MPI
^^^^^^^^^^^^^^^^^^^^^

When using Intel MPI, one can use the :code:`hpc-tacc-centos7-impi19.0.7-common` container optimized
for TACC_ with a small trick when running.

To build the container:

.. code-block:: console

   make containers/hpc-tacc-centos7-impi19.0.7-common.sif

To run/test the container on 4 cores:

.. code-block:: console

   UCX_TLS=sm mpirun -n 4 containers/hpc-tacc-centos7-impi19.0.7-common.sif python -m petra parallel

Note that we specify :code:`UCX_TLS` because you are unlikely to run the TACC_ transports. For
single-host use, :code:`UCX_TLS=sm` selects the shared memory transports, and should provide good
performance for most testing environments. For different use cases, you might consider
:code:`UCX_TLS=all`.

Texas Advanced Computing Center (TACC_)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This container works well on the Stampede2_ and Frontera_ systems at TACC_.

To build the container:

.. code-block:: console

   make containers/hpc-tacc-centos7-impi19.0.7-common.sif

To run/test the container:

1. copy the container to high-performance storage on TACC, i.e., $HOME or $SCRATCH.
2. Create a submission script for SLURM or ask for an interactive session, e.g., :code:`idev -N 2`
   for 2 nodes
3. To run the Singularity_ container:

   .. code-block:: console

      module load tacc-singularity
      ibrun containers/hpc-tacc-centos7-impi19.0.7-common.sif python -m petra parallel

.. Outside links

.. _miniconda: https://docs.conda.io/en/latest/miniconda.html
.. _Docker: https://www.docker.com/
.. _Singularity: https://singularity.lbl.gov/
.. _TACC: https://www.tacc.utexas.edu/
.. _Frontera: https://frontera-portal.tacc.utexas.edu/
.. _Stampede2: https://www.tacc.utexas.edu/systems/stampede2

.. rubric:: Footnotes

.. [#fanaconda] Miniconda_ is the minimal installation that provides the conda package manager.
                Alternatively, you may install the full `Anaconda <https://www.anaconda.com/>`_
                distribution.
