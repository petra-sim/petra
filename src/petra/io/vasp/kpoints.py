import numpy as np
from ase.dft.kpoints import get_special_points

from petra import util
from petra.io.vasp.util import CommonFile


class KPOINTS(CommonFile):

    def __init__(self, path='', read=True):
        self.comment = ''
        self.mode = None
        self.k = None
        self.weights = None
        self.cartesian = False
        self._tetra = None
        CommonFile.__init__(self, path, read)

    def _read(self, f):
        self.comment = f.readline().strip()
        nk = int(f.readline().strip().split()[0])
        mode = f.readline().strip().lower()

        if mode.startswith('a'):
            size = int(f.readline().strip().lower())
            self.automatic(size)

        if mode.startswith('g') or mode.startswith('m'):
            size = np.array(list(map(int, f.readline().strip().split())))
            soffset = f.readline().strip()
            offset = None
            if soffset:
                offset = np.array(list(map(float, soffset.split())))
            self.automatic(size, offset,
                           gamma_centered=mode.startswith('g'))

        elif mode.startswith('l'):
            cartesian = f.readline().strip().lower().startswith('c')
            lines = []
            start = None
            for line in f:
                line = line.strip()
                if not line:
                    continue
                comment = None
                if '!' in line:
                    line, comment = line.split('!')
                    line = line.strip()
                data = (np.array(list(map(float, line.split()[:3]))), comment)
                if start is None:
                    start = data
                else:
                    lines.append((start, data))
                    start = None
            if start is not None:
                raise ValueError(
                    'line-mode requires an even number of entries')
            self.line(lines, nk, cartesian=cartesian)

        else:
            cartesian = mode.startswith('c')
            kpoints = np.zeros((nk, 3))
            weights = np.zeros(nk)
            tetra = None
            for i in range(nk):
                data = list(map(float, f.readline().strip().split()))
                kpoints[i] = data[:3]
                weights[i] = data[3]
            if f.readline().strip().lower().startswith('t'):
                ntetra, vweight = f.readline().strip().split()
                ntetra, vweight = int(ntetra), float(vweight)
                tetra = np.zeros((ntetra, 4), dtype=int)
                tweights = np.zeros((ntetra))
                for i in range(ntetra):
                    data = f.readline().strip().split()
                    tweights[i] = float(data[0])
                    tetra[i] = list(map(int, data[1:]))
                tetra = {
                    'n': ntetra,
                    'volume_weight': vweight,
                    'weights': tweights,
                    'indices': tetra - 1
                }
            self.explicit(kpoints, weights,
                          cartesian=cartesian, tetra=tetra)

    def explicit(self, kpoints, weights, cartesian=False, tetra=None):
        self.mode = 'explicit'
        self.k = kpoints
        self.size = self.k.shape[0]
        self.weights = weights
        self.cartesian = cartesian
        self._tetra = tetra
        return self

    def automatic(self, size, offset=None, gamma_centered=True):
        self.mode = 'automatic'
        self.cartesian = False
        size = np.asarray(size)
        if offset is None:
            offset = np.zeros(3)
        self._offset = np.asarray(offset)
        self._gamma_centered = gamma_centered
        self.size = size
        if size.size == 1:
            raise NotImplementedError('Single size automatic grids')
        else:
            idx = np.indices(self.size).reshape(3, -1).T
            ik = idx + offset + 0.5 * (not gamma_centered)
            self.k = ik / self.size
        return self

    def kpath(self, cell, path, n):
        kpts = get_special_points(cell)
        lines = [
            [(kpts[point0], point0), (kpts[point1], point1)]
            for point0, point1 in zip(path[:-1], path[1:])
            if ',' not in (point0, point1)
        ]
        n = int(np.ceil(n / len(lines)))
        return self.line(lines, n, cartesian=False)

    def line(self, lines, n, cartesian=False):
        self.mode = 'line'
        self.lines = lines
        self.size = n
        self.cartesian = cartesian
        k = [util.linspace(np.asarray(start), np.asarray(stop), n)
             for (start, _), (stop, _) in lines]
        self.k = np.concatenate(k, axis=0)
        self.weights = np.ones(self.k.shape[0])
        return self

    def title(self, title):
        self.comment = title
        return self

    def make_explicit(self):
        self.mode = 'explicit'
        return self

    def _write(self, f):
        mode = self.mode

        print(self.comment, file=f)
        if mode == 'automatic':
            print(0, file=f)
        elif mode == 'explicit':
            print(self.k.shape[0], file=f)
        else:
            print(self.size, file=f)

        if mode == 'automatic':
            if self.size.size == 1:
                print('Automatic', file=f)
                print(self.size, file=f)
            else:
                if self._gamma_centered:
                    print('Gamma centered', file=f)
                else:
                    print('Monkhorst-Pack', file=f)
            print('{:d} {:d} {:d}'.format(*self.size), file=f)
            print('{:f} {:f} {:f}'.format(*self._offset), file=f)

        elif mode == 'line':
            print('Line', file=f)
            print('Cartesian' if self.cartesian else 'Reciprocal',
                  file=f)
            for ((k0, label0), (k1, label1)) in self.lines:
                if label0 is None:
                    print('{:f} {:f} {:f}'.format(*k0), file=f)
                else:
                    print('{:f} {:f} {:f} ! {}'.format(*k0, label0), file=f)
                if label1 is None:
                    print('{:f} {:f} {:f}'.format(*k1), file=f)
                else:
                    print('{:f} {:f} {:f} ! {}'.format(*k1, label1), file=f)
                print('', file=f)

        else:
            print('Cartesian' if self.cartesian else 'Reciprocal',
                  file=f)
            for kpoint, weight in zip(self.k, self.weights):
                print('{:f} {:f} {:f} {:f}'.format(*kpoint, weight),
                      file=f)

            if self._tetra:
                print('Tetrahedra', file=f)
                print('{:d} {:f}'.format(self._tetra['n'],
                                         self._tetra['volume_weight']))
                for weight, idx in zip(self._tetra['weights'],
                                       self._tetra['indices']):
                    print('{:f} {:d} {:d} {:d} {:d}'.format(weight, *idx))
