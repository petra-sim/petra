import numpy as np
import numpy.linalg as la
from scipy.linalg import block_diag
from scipy.spatial import cKDTree

import ase
import ase.neighborlist
import ase.build
import ase.io.vasp

from petra.util import lcm, gcd


def convert_miller(miller, origin, dest):
    direction = np.array([v for v in miller], dtype=int)

    B_origin = la.inv(origin.cell)
    B_dest = la.inv(dest.cell)

    dest_direction = direction @ B_origin @ la.inv(B_dest)

    fraction, _ = np.modf(dest_direction)
    subspace = ~np.isclose(fraction, 0)
    inv_fraction = 1. / fraction[subspace]
    inv_fraction_int = np.round(inv_fraction).astype(int)

    if not np.allclose(inv_fraction, inv_fraction_int):
        raise ValueError('Could not convert Miller indices, '
                         'lattices may be incompatible.')
    dest_direction *= lcm(inv_fraction_int)

    dest_miller = np.round(dest_direction).astype(int)
    if not np.allclose(dest_direction, dest_miller):
        raise ValueError('Could not convert Miller indices, '
                         'lattices may be incompatible.')

    return dest_miller // gcd(dest_miller)


def minimize_linear_dependence(vector, basis):
    '''Minimize the linear dependence of a vector to set of basis vectors.
    '''
    components = (basis @ vector).astype(float)
    components /= la.norm(basis, axis=1)**2
    return vector - (np.round(components).astype(int) @ basis)


def find_smallest_oriented_basis(intercepts):
    '''Finds the smallest integer basis for given intercepts of the hyperplane.

    Note
    ----
    This method must be called with finite, integer intercepts.
    The returned basis is not guaranteed to be oriented correctly, e.g., its
    volume might be negative.

    Returns
    -------
    orientation : int
        The index of the out-of-plane vector. (see also `basis`)
    reduction : (N,) array of int
        The multiplicity of each direction of the intercepts.
    basis : (N, M) array of int
        The integer basis of the in-plane basis and out-of-plane vector.
    '''
    orientations = np.r_[:intercepts.shape[0]]

    gain = 0

    for out_of_plane in orientations:

        in_plane = orientations != out_of_plane

        trial_basis = intercepts.copy()
        trial_basis[in_plane] -= intercepts[out_of_plane]

        trial_reduction = gcd(trial_basis, axis=1)
        trial_gain = np.prod(trial_reduction)

        if gain < trial_gain:
            gain = trial_gain
            reduction = trial_reduction
            basis = trial_basis // reduction[:, None]
            orientation = out_of_plane

    out_of_plane = orientation
    in_plane = orientations[orientations != out_of_plane]

    # reorient the in-plane vectors to minimize their linear dependence
    for target in in_plane:
        reference = in_plane[in_plane != target]
        basis[target] = minimize_linear_dependence(basis[target],
                                                   basis[reference])

    # reorient the out-of-plane vector to minimize linear dependence on the
    # in-plane space
    basis[out_of_plane] = minimize_linear_dependence(basis[out_of_plane],
                                                     basis[in_plane])

    return orientation, reduction, basis


def orient_to_plane(bulk, miller):
    '''Orients a crystal to a plane given by Miller indices (hkl).
    '''

    fdirection = np.array([v for v in miller], dtype=float)
    direction = fdirection.astype(int)
    assert(np.all(direction == fdirection))

    basis = np.eye(3, dtype=int)

    # find the subspace that has intercepts of the plane
    subspace = np.r_[:3][direction != 0]

    # find the common multiple of the direction
    mult_v = np.ones(3, dtype=int)
    intercepts_v = lcm(direction[subspace]) // direction[subspace]
    mult_v[subspace] = np.abs(intercepts_v)

    intercepts = np.zeros((subspace.size, direction.size), dtype=int)
    intercepts[np.r_[:subspace.size], subspace] = intercepts_v

    # define the plane using the intercepts
    smallest_basis = find_smallest_oriented_basis(intercepts)
    out_of_plane, reduction, subspace_basis = smallest_basis
    out_of_plane = subspace[out_of_plane]

    # the cell that makes the intercepts of the plane be integer
    # multiples of the lattice vectors
    assert(np.all((mult_v[subspace] % reduction) == 0))
    mult_v[subspace] //= reduction

    basis[subspace] = subspace_basis

    cell = basis @ bulk.cell

    # create the supercell and wrap all atoms inside
    sc = bulk.repeat(mult_v)
    sc.set_cell(cell, scale_atoms=False)
    sc.wrap()

    # make the given direction be 'c'
    swap = np.roll(np.r_[:3], 2 - out_of_plane)

    volume = la.det(cell)
    if volume < 0:  # fix negative volume cell
        swap[0], swap[1] = swap[1], swap[0]
    positions_a = sc.get_scaled_positions()[:, swap]
    sc.set_cell(cell[swap], scale_atoms=False)
    sc.set_scaled_positions(positions_a)
    sc.wrap()

    return sc


def slab(bulk,
         offset=0,
         thickness=10,
         vacuum=10,
         adatom='H',
         adatom_bond=1.487,
         rotate=True):

    unit = np.cross(bulk.cell[0], bulk.cell[1])
    unit /= la.norm(unit)

    # make the supercell big enough
    Lc = la.norm(bulk.cell[2] @ unit)
    N = int(np.ceil(2 * thickness / Lc))
    sc = bulk.repeat((1, 1, N))

    # cut atoms to size
    sc.wrap(0.)
    pos = sc.get_positions()
    idx, = np.where(2 * np.abs(pos @ unit) > thickness)

    if adatom is None:
        del sc[idx]
    else:
        sc = remove_and_passivate(sc, idx, adatom, adatom_bond)

    sc.cell[2] = unit * (thickness + vacuum)
    sc.center(vacuum=vacuum / 2, axis=2)

    if rotate:
        sc.rotate(sc.cell[2], 'z', rotate_cell=True)
        sc.rotate(sc.cell[0], 'x', rotate_cell=True)

    sc.wrap()

    return sc


def find_minimal_bond(atoms):
    tree = cKDTree(atoms.positions)
    L, _ = tree.query(atoms.positions, k=2)
    return np.min(L[:, 1])


def nanowire(name='Si',
             size=5,
             geometry='square',
             lattice='diamond',
             offset=0.,
             rotate=0,
             adatom='H',
             a=5.431,
             adatom_bond=1.487,
             vacuum=10,
             remove_single=True):

    bulk = ase.build.bulk(name, lattice, a=a, cubic=True)

    N = int(np.ceil((size + vacuum) / a) + 1)

    atoms = bulk.repeat((N, N, 1))
    atoms.pbc = [False, False, True]
    atoms.rotate(rotate, 'z', 'COU')

    center = np.sum(atoms.cell / 2, axis=1)
    center += np.sum(bulk.cell, axis=1) * offset
    pos = atoms.positions - center[None, :]
    if geometry == 'circle':
        r = np.sqrt(np.sum(pos[:, :2]**2, axis=1))
    elif geometry == 'square':
        r = np.max(np.abs(pos[:, :2]), axis=1)
    mask = r > size / 2
    idx, = np.where(mask)

    if remove_single:
        ratoms = atoms.copy()
        del ratoms[idx]
        single_idx = find_single(ratoms)
        retained, = np.where(~mask)
        single_idx = retained[single_idx]
        idx = np.concatenate([idx, single_idx])

    if adatom is None:
        del atoms[idx]
    else:
        atoms = remove_and_passivate(atoms, idx, adatom, adatom_bond)

    L = atoms.positions[:, 0].max() - atoms.positions[:, 0].min() + vacuum
    factor = L / atoms.cell[0, 0]
    atoms.cell[:2, :] *= factor
    atoms.center()
    return atoms


def find_single(atoms):
    ratoms = atoms.repeat(np.ones(3, dtype=int) + 2 * np.array(atoms.pbc))
    ratoms.positions -= np.array(atoms.pbc) @ np.array(atoms.cell)

    tree = cKDTree(atoms.positions)
    rtree = cKDTree(ratoms.positions)

    L = find_minimal_bond(atoms) * 1.01

    idx, = np.where([len(pairs) <= 2
                     for pairs in tree.query_ball_tree(rtree, L)])
    return idx


def remove_and_passivate(atoms, remove, adatom='H', bondlen=1.487):

    removed = atoms[remove].copy()
    removed = removed.repeat(np.ones(3, dtype=int) + 2 * np.array(atoms.pbc))
    removed.positions -= np.array(atoms.pbc) @ np.array(atoms.cell)

    del atoms[remove]

    tree = cKDTree(atoms.positions)
    rtree = cKDTree(removed.positions)

    L = find_minimal_bond(atoms) * 1.01

    for iatom, idxs in enumerate(tree.query_ball_tree(rtree, L)):
        for iremoved in idxs:
            refpos = atoms[iatom].position
            atom_bond = removed[iremoved].position - refpos
            direction = atom_bond / la.norm(atom_bond)
            pos = refpos + bondlen * direction
            atoms.append(ase.Atom(adatom, position=pos))

    return atoms


def twist_hex(m, r):
    costheta = ((3 * m**2 + 3 * m * r + r**2 / 2) /
                (3 * m**2 + 3 * m * r + r**2))
    if r % 3 == 0:
        transform = np.array([[m + r // 3, r // 3], [-r // 3, m + 2 * r // 3]])
        V = m**2 + m * r + r**2 // 3
    else:
        transform = np.array([[m, m + r], [-(m + r), 2 * m + r]])
        V = 3 * m**2 + 3 * m * r + r**2
    return np.arccos(costheta), transform, V


def bilayer_AB_hex(atoms, twist=None, t=3.4, separate=False):
    """
    The atomic structure of twisted AB Bilayer Graphene.

    Ref: Graphene Bilayer with a Twist: Electronic Structure
         J. M. B. Lopes dos Santos, N. M. R. Peres, and A. H. Castro Neto
         PRL 99, 256802 (2007)
         DOI: 10.1103/PhysRevLett.99.256802
    """

    atoms0 = atoms.copy()
    atoms0.set_pbc((1, 1, 0))
    cell = atoms0.get_cell()
    cell[2, 2] += t
    atoms0.set_cell(cell)

    atoms1 = atoms0.copy()
    atoms1.translate(-atoms1.positions[1] + np.array([0, 0, t]))
    atoms1.wrap()

    if twist is None:
        bilayer = atoms0 + atoms1
        bilayer.center()
        bilayer.info['twist'] = 0.0
        bilayer.info['sc_size'] = 1
        return bilayer

    try:
        m, r = twist
    except TypeError:
        m, r = twist, 1

    assert(m > 0 or r > 0)

    # Rotate top graphene layer

    phi, transform, volume = twist_hex(m, r)

    phi_deg = phi / np.pi * 180

    atoms1.rotate(phi_deg, 'z', rotate_cell=True)

    cell_T = block_diag(transform, 1)

    cell = cell_T.T @ atoms1.cell

    bilayer = ase.Atoms(cell=cell, pbc=(1, 1, 0))

    shape = (2 * m + r, 2 * m + r, 1)
    bilayer += atoms0.repeat(shape)
    bilayer += atoms1.repeat(shape)

    # Move all atoms inside the supercell

    bilayer.wrap()

    # Remove duplicate atoms

    eps = la.norm(bilayer.cell, axis=1).min() / len(bilayer)
    while True:
        tree = cKDTree(bilayer.positions)
        rem = [j for _, j in tree.query_pairs(eps)]
        if not rem:
            break
        del bilayer[rem]

    bilayer.center()

    # Rotate the cell to align bilayer.cell[0] to the x-axis

    costheta, sintheta = bilayer.cell[0, :2] / la.norm(bilayer.cell[0])
    R = np.array([[costheta, -sintheta, 0],
                  [sintheta, costheta, 0],
                  [0, 0, 1]])
    bilayer.set_cell(bilayer.cell @ R, scale_atoms=True)

    bilayer.info['twist'] = phi
    bilayer.info['sc_size'] = volume

    if not separate:
        return bilayer

    layer1 = bilayer.copy()
    del layer1[layer1.get_scaled_positions()[:, 2] > .5]

    layer2 = bilayer.copy()
    del layer2[layer2.get_scaled_positions()[:, 2] < .5]

    return layer1, layer2
