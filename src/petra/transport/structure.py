import math
from functools import cached_property
from typing import TYPE_CHECKING, Optional

import numpy as np
from ase.atoms import Atoms

from petra import constants as c
from petra.transport.node import NodeList
from petra.transport.element import Element, ElementList
from petra.util import log, mpi
from petra.util.lazy import invalidate_cached_property
from petra.util.sparse import MatrixBuilder
from petra.util.indexing import Indexer

if TYPE_CHECKING:
    from petra.transport.occupation import Occupation

logger = log.getLogger(__name__)


class Structure:

    def __init__(self, nodes: NodeList, elements: ElementList):
        self.nodes = nodes
        self.elements = elements

        self.points_of_element = Indexer((element, element.ndensity) for element in self.elements)
        self.ndensity = self.points_of_element.size

        self.occupation: Optional[Occupation] = None

    def set_occupation(self, occupation: 'Occupation'):
        assert(occupation.structure == self)
        self.occupation = occupation
        return self

    @property
    def atoms(self) -> Atoms:
        return sum((el.atoms for el in self.elements), Atoms())

    @property
    def r_rv(self):
        r_rv = [element.r_rv for element in self.elements]
        return np.concatenate(r_rv, axis=0)

    @property
    def r_density_rv(self):
        return np.concatenate([el.r_density_rv
                               for el in self.elements], axis=0)

    @property
    def r_boundary_rv(self):
        r_rv = np.concatenate([np.array(el.r_boundary_rv)
                               for el in self.elements])
        _, idx = np.unique(r_rv.round(decimals=8), axis=0, return_index=True)
        return r_rv[idx]

    @property
    def M(self):
        return self.HM[1]

    @property
    def H(self):
        return self.HM[0]

    @cached_property
    def HM(self):
        logger.begin('generating Hamiltonian')
        N = self.nodes.idx.size
        shape = (N, N)

        build_H = MatrixBuilder(shape)
        build_M = MatrixBuilder(shape)

        for element in self.elements:
            for n0, n1, H, M in element.HM:
                ig0 = self.nodes.idx[n0.parent]
                ig1 = self.nodes.idx[n1.parent]
                build_H.add(ig0, ig1, H)
                build_M.add(ig0, ig1, M)

        logger.end('generating Hamiltonian')
        return build_H.matrix.tocsc(), build_M.matrix.tocsc()

    @property
    def M0(self):
        return self.HM0[1]

    @property
    def H0(self):
        return self.HM0[0]

    @cached_property
    def HM0(self):
        logger.begin('generating Hamiltonian')
        N = self.nodes.idx.size
        shape = (N, N)

        build_H = MatrixBuilder(shape)
        build_M = MatrixBuilder(shape)

        for element in self.elements:
            for n0, n1, H, M in element.HM0:
                ig0 = self.nodes.idx[n0.parent]
                ig1 = self.nodes.idx[n1.parent]
                build_H.add(ig0, ig1, H)
                build_M.add(ig0, ig1, M)

        logger.end('generating Hamiltonian')
        return build_H.matrix.tocsc(), build_M.matrix.tocsc()

    def V(self):
        N = self.nodes.idx.size
        build_V = MatrixBuilder((N, N))
        for element in self.elements:
            for n0, n1, V in element.iter_V():
                ig0 = self.nodes.idx[n0.parent]
                ig1 = self.nodes.idx[n1.parent]
                build_V.add(ig0, ig1, V)
        return build_V.matrix.tocsc()

    @logger.block('setting potential from function')
    def set_potential(self, function, name='default'):
        for element in self.elements:
            element.set_potential(function, name)
        self.invalidate_potential()

    def invalidate_potential(self):
        invalidate_cached_property(self, 'HM', 'HM0')

    def clear_potential(self, name):
        for element in self.elements:
            self.element.clear_potential(name)
        self.invalidate_potential()

    def iter_overlaps(self):
        for n0_global, node0 in enumerate(self.nodes):
            for child0 in node0.children:
                el = child0.owner
                n0_local = el.nodes.index(child0)
                for n1_local, child1 in enumerate(el.nodes):
                    try:
                        n1_global = self.nodes.index(child1.parent)
                    except ValueError:
                        continue
                    yield n0_global, n1_global, n0_local, n1_local, el

    def per_element(self, *coefs):
        nodeidx = self.nodes.idx
        for element in self.elements:
            elcoefs = tuple(np.array([coef[nodeidx[node.parent]] for node in element.nodes])
                            for coef in coefs)
            yield (element,) + elcoefs

    @property
    def node_V(self):
        return np.real(
            self.nodes.idx.reduce(self.V().diagonal()) /
            self.nodes.idx.reduce(self.M.diagonal())
        )

    def V_r(self):
        V_r = [element.V_xyz().flatten() for element in self.elements]
        return np.concatenate(V_r)

    def node_psi2(self, state):
        """Calculates the node density of a state.
        """
        return self.nodes.idx.reduce(np.abs(state.c_I)**2)

    def node_density(self, state):
        """Calculates the node density of a state.
        """
        return state.dos * self.node_psi2(state)

    def psi(self, c_in):
        """Calculates the wavefunction from the PUM coefficients.
        """
        psi_r = np.zeros(self.ndensity, dtype=complex)
        for element, c_el_in in self.structure.per_element(c_in):
            points = self.points_of_element[element]
            psi_r[points] = element.psi_r(c_el_in, full=False)
        return psi_r

    def psi2(self, c_in):
        """Calculates the probability density from the PUM coefficients.
        """
        psi2_r = np.zeros(self.ndensity)
        for element, c_el_in in self.per_element(c_in):
            points = self.points_of_element[element]
            psi2_r[points] = element.psi2_r(c_el_in)
        return psi2_r

    def probability_current(self, c_in):
        """Calculates the probability current from the PUM coefficients.
        """
        J = np.zeros((3, self.ndensity))
        for element, c_el_in in self.structure.per_element(c_in):
            points = self.points_of_element[element]
            J[:, points] = element.probability_current_vr(c_el_in)
        return J

    def density(self, state):
        """Calculates the density of a state.
        """
        return state.dos * self.psi2(state)

    def current_density(self, state):
        """Calculates the current density of a state.
        """
        return state.dos * self.probability_current(state)

    def current(self, state):
        """Calculates the current of a state.
        """
        N = len(self.elements.contacts)
        I_cc = np.zeros((N, N))
        j_c = - state.dos * np.abs(state.J) * state.T_c / c.h
        I_cc[state.icontact, :] += j_c
        I_cc[:, state.icontact] -= j_c
        return I_cc

    def T(self, state):
        """Calculates the transmission coefficients of a state.
        """
        N = len(self.elements.contacts)
        T_cc = np.zeros((N, N))
        T_cc[state.icontact] = state.T_c
        return T_cc

    @logger.block('calculating density from density matrices')
    def dmats_to_density(self, dmats, current=False):
        density = np.zeros(self.ndensity)
        for element, dmat in mpi.split(zip(self.elements, dmats)):
            points = self.points_of_element[element]
            density[points] = element.dmat_to_density(dmat)
        return mpi.allreduce(density)

    @logger.block('calculating current density from density matrices')
    def dmats_to_current_density(self, dmats):
        density = np.zeros((self.ndensity, 3))
        for element, dmat in mpi.split(zip(self.elements, dmats)):
            points = self.points_of_element[element]
            density[points, :] = element.dmat_to_current_density(dmat).T
        return mpi.allreduce(density)

    def write_atoms(self, fname):
        from petra.io.vtk import write_atoms
        write_atoms(fname, self.atoms)

    def write_data(self, fname, triangulate=False, **data):
        '''Write 3D data to a VTK file.'''
        from petra.io import vtk
        if triangulate:
            from scipy.spatial import Delaunay
            mesh = Delaunay(self.r_density_rv)
            vtk.save_mesh(fname, mesh, point_data=data)
        else:
            vtk.write_points(fname, self.r_density_rv / c.nm, **data)


class StructureBuilder:
    def __init__(self):
        self.nodes = NodeList()
        self.elements = ElementList()

    def build(self):
        for element in self.elements:
            for node in element.nodes:
                if node.parent not in self.nodes:
                    raise ValueError("The structure could not be built.")
        for node in self.nodes:
            if not all(any(child in element.nodes
                           for element in self.elements)
                       for child in node.children):
                raise ValueError("The structure could not be built.")
        return Structure(self.nodes, self.elements)

    def add_element(self, element, position):
        # attach the element in the correct place
        element.position = position
        self.elements.append(element)
        # connect the element's nodes
        for node in element.nodes:
            try:
                parent = self.nodes.near(node)[0]
                node.parent = parent
                parent.children.append(node)
            except IndexError:
                parent = node.make_parent(self)
                self.nodes.append(parent)
        return self


def SimpleHomoStructure(block, N=None, L=None):
    '''A simple Homostructure with two contacts, at either ends'''
    direction = block.direction
    if N is None and L is None:
        raise ValueError('Neither N or L has been provided.')
    if L is not None:
        N_L = math.ceil(L / block.cell.a_len[direction])
        if N is not None and N != N_L:
            raise ValueError('Conflicting N and L have been provided.')
        N = N_L

    positions = np.r_[0:N][:, None] * block.cell.a[None, direction, :]
    contacts = {0: 1, N - 1: -1}

    sb = StructureBuilder()
    for ii, position in enumerate(positions):
        contacted = contacts.get(ii, None)
        element = Element(block, contacted=contacted)
        sb.add_element(element, position)
    return sb.build()
