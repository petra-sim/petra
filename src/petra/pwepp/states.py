from abc import ABC, abstractmethod
from typing import Optional

import numpy as np
from scipy import fft

from petra.pwepp.basis import PlaneWaves, Grid
from petra.util import mpi


def create(basis, k_ka, nbands, nspin=1, weight_k=None, allocate=True):
    if isinstance(basis, PWStates.supported_basis):
        return PWStates(basis, k_ka, nbands, nspin, weight_k, allocate)
    if isinstance(basis, GridStates.supported_basis):
        return GridStates(basis, k_ka, nbands, nspin, weight_k, allocate)
    return NotImplementedError()


def read(basis, name):
    if isinstance(basis, PWStates.supported_basis):
        return PWStates.read(basis, name)
    if isinstance(basis, GridStates.supported_basis):
        return GridStates.read(basis, name)
    return NotImplementedError()


class States(ABC):
    supported_basis: Optional[type] = None

    def __init__(self, basis, k_ka, nbands, nspin=1, weight_k=None, allocate=True):
        assert(isinstance(basis, self.supported_basis))
        self.basis = basis

        self.k_ka = k_ka
        self.nbands = nbands
        self.nspin = nspin
        if weight_k is None:
            weight_k = np.zeros(self.k_ka.shape[0])
        self.weight_k = weight_k

        self.nk = self.k_ka.shape[0]

        self.dist = mpi.dist(self.nk)
        self.local_ik_k = self.dist.iglobal()

        self.local_k_ka = self.k_ka[self.local_ik_k]
        self.local_nk = self.local_ik_k.size

        self.local_E_kn = np.zeros((self.local_nk, self.nbands))

        if allocate:
            self._allocate_psi()

    def owner(self, ik):
        return self.dist.owner[ik]

    def ik_local(self, ik):
        assert(self.dist.is_owner(ik))
        return self.dist.ilocal[ik]

    @property
    def gspin(self):
        return 2 // self.nspin

    @abstractmethod
    def set(self, ik, E_n, psi_no):
        pass

    @abstractmethod
    def get(self, ik):
        pass

    def write(self, name):
        if mpi.is_root():
            np.savez('{}-states.npz'.format(name),
                     k_ka=self.k_ka,
                     weight_k=self.weight_k,
                     nbands=self.nbands)

        for ik in self.local_ik_k:
            E, psi = self.get(ik)
            np.savez('{}-k-{:05d}.npz'.format(name, ik), E=E, psi=psi)

    @classmethod
    def read(cls, basis, name):
        data = np.load('{}-states.npz'.format(name))
        states = cls(basis, k_ka=data['k_ka'], weight_k=data['weight_k'], nbands=data['nbands'])

        for ik in states.local_ik_k:
            data = np.load('{}-k-{:05d}.npz'.format(name, ik))
            states.set(ik, E_n=data['E'], psi_ng=data['psi'])

        return states

    @abstractmethod
    def _allocate_psi(self):
        pass

    def psi(self, ik=0, n=slice(None), target=None, operator=None, **kwargs):
        '''
        Send the requested wavefunctions to the target rank(s).
        target is either:
        - int: send to single rank
        - list of int: send to multiple ranks
        - None (default): broadcast to all ranks
        '''

        # find owner and local index
        owner = self.owner(ik)

        psi = None
        if mpi.comm.rank == owner:
            ik_local = self.ik_local(ik)
            psi = self.local_psi(ik_local, n, operator=operator, **kwargs)

        if target is None:
            return mpi.bcast(psi, root=owner)
        else:
            return mpi.p2p(psi, orig=owner, dest=target)

    @property
    def E_kn(self):
        return self.dist.allgather(self.local_E_kn)

    @abstractmethod
    def local_psi(self, ik, n):
        pass

    def solve(self, hamiltonian, tol=1e-6, maxiter=20):
        assert(hamiltonian.basis == self.basis)

        for ik, k_a in zip(self.local_ik_k, self.local_k_ka):
            E_n, psi_no = hamiltonian.solve(k_a, self.nbands, tol=tol, maxiter=maxiter)
            self.set(ik, E_n, psi_no)

    def expectation(self, operator):
        results = []
        for ik_local, k_a in enumerate(self.local_k_ka):
            psi_no = self.local_psi(ik_local)
            op = operator(k_a)
            if isinstance(op, tuple):
                results.append(np.stack([np.sum(psi_no.conj() * (opi @ psi_no.T).T, axis=-1)
                                         for opi in op], axis=-1))
            else:
                results.append(np.sum(psi_no.conj() * (op @ psi_no.T).T, axis=-1))

        return self.dist.allgather(np.stack(results))

    def overlaps(self, operator):
        results = []

        for ik_local, k_a in enumerate(self.local_k_ka):
            psi_no = self.local_psi(ik_local)
            op = operator(k_a)
            if isinstance(op, tuple):
                results.append(np.stack([psi_no.conj() @ (opi @ psi_no.T)
                                         for opi in op], axis=-1))
            else:
                results.append(psi_no.conj() @ (op @ psi_no.T))

        return self.dist.allgather(np.stack(results))

    def sorted_bands(self):
        ''' Stable sort of a bandstructure (possibly sub-optimal)
        '''
        from numpy import linalg as la
        from petra.util.matching import stable_marriages

        sorted_E = np.copy(self.E_kn)
        nk, nbands = sorted_E.shape

        k_idx, n_idx = np.mgrid[:nk, :nbands]
        shape = (nbands, -1)

        for ik in range(len(sorted_E)):
            psi_1 = self.psi(ik, pad=True).reshape(shape)[n_idx[ik]]

            if ik == 0:
                psi_0 = psi_1
                continue

            pref0 = np.abs(la.pinv(psi_1.T) @ psi_0.T)
            pref1 = np.abs(la.pinv(psi_0.T) @ psi_1.T)
            idx = stable_marriages(pref0, pref1)
            sorted_E[ik:] = sorted_E[ik:, idx]
            n_idx[ik:] = n_idx[ik:, idx]

            psi_0 = psi_1[idx]

        return sorted_E, (k_idx, n_idx)


class PWStates(States):
    supported_basis = PlaneWaves

    def _allocate_psi(self):

        # determine nG for storage
        self.nG_k = np.array([self.basis.cutoff(k_a).nG for k_a in self.k_ka])
        self.nG_local_k = self.nG_k[self.local_ik_k]
        self.nG_stored = max(self.nG_k)

        self.local_psi_kng = np.zeros((self.local_nk, self.nbands, self.nG_stored), dtype=complex)

    def set(self, ik, E_n, psi_ng):
        ik_local = self.ik_local(ik)

        self.local_E_kn[ik_local] = E_n
        self.local_psi_kng[ik_local, :, :self.nG_local_k[ik_local]] = psi_ng

    def get(self, ik):
        ik_local = self.ik_local(ik)

        return (self.local_E_kn[ik_local],
                self.local_psi_kng[ik_local, :, :self.nG_local_k[ik_local]])

    def local_psi(self, ik_local=0, n=slice(None), operator=None,
                  pad=False, real=False, basis=None, shift_a=None):
        assert(np.isscalar(ik_local))

        pad = pad or real

        nG = self.nG_local_k[ik_local]
        psi_ng = self.local_psi_kng[ik_local, n, :nG]

        if operator is not None:
            psi_ng = (operator @ psi_ng.T).T

        if not pad:
            return psi_ng

        psi_nxyz = self._padded(psi_ng, self.local_k_ka[ik_local], basis)
        if shift_a is not None:
            psi_nxyz *= np.exp(2j * np.pi * (self.basis.G_xyza @ shift_a))

        if not real:
            return psi_nxyz

        return self._real(psi_nxyz)

    def _padded(self, psi_ng, k_a, basis=None):
        if basis is None:
            basis = self.basis

        if not self.basis.is_compatible(basis):
            raise ValueError('provided basis is not compatible with basis of states')

        return basis.cutoff(k_a).uncompress(psi_ng)

    def _real(self, psi_nxyz):
        norm = np.sqrt(np.prod(psi_nxyz.shape[-3:]))
        return norm * fft.ifftn(psi_nxyz, axes=[-3, -2, -1])

    @property
    def grid(self):
        return self.basis.to_grid()


class GridStates(States):
    supported_basis = Grid

    def _allocate_psi(self):
        self.local_psi_knxyz = np.zeros((self.local_nk, self.nbands) + self.basis.shape,
                                        dtype=complex)

    def set(self, ik, E_n, psi_nr):
        ik_local = self.ik_local(ik)

        self.local_E_kn[ik_local] = E_n
        self.local_psi_knxyz[ik_local] = self.basis.expand(psi_nr)

    def get(self, ik):
        ik_local = self.ik_local(ik)

        return (self.local_E_kn[ik_local],
                self.basis.flatten(self.local_psi_knxyz[ik_local]))

    def local_psi(self, ik=None, n=None, expanded=False, real=True):
        assert(real)
        psi_knr = self.local_psi_knr[ik, n]
        if expanded:
            self.expand(psi_knr)
        return psi_knr

    @property
    def grid(self):
        return self.basis
