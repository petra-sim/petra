import os
from contextlib import contextmanager
from functools import wraps
from itertools import chain

import numpy as np

try:
    from mpi4py import MPI
    COMM_WORLD = MPI.COMM_WORLD
except ModuleNotFoundError:
    MPI = None  # type:ignore

    class FakeCOMM:
        rank = 0
        size = 1
    COMM_WORLD = FakeCOMM()  # type:ignore

comm = COMM_WORLD

if MPI is not None:
    operations = {
        op.lower(): getattr(MPI, op) for op in dir(MPI)
        if isinstance(getattr(MPI, op), MPI.Op)
    }


def is_enabled():
    return MPI is not None


def is_root():
    return comm.rank == 0


def is_parallel():
    return comm.size > 1


def vendor():
    if not is_enabled():
        return 'not found'
    vendor, vendor_version = MPI.get_vendor()
    return vendor + " " + ".".join(map(str, vendor_version))


def node_name():
    if is_enabled():
        return MPI.Get_processor_name()
    import socket
    print(socket.gethostname())


def barrier():
    if is_parallel():
        comm.Barrier()


sync = barrier


def node_allocations():
    if not is_parallel():
        return {node_name(): [comm.rank]}
    info = {node_name(): comm.rank}

    raw = comm.allgather(info)
    info = {}
    for item in raw:
        for node, rank in item.items():
            info.setdefault(node, []).append(rank)
    return {node: sorted(ranks) for node, ranks in info.items()}


def node_allocation():
    return node_allocations()[node_name()]


def node_group():
    if not is_parallel():
        return None
    return comm.group.Incl(node_allocation())


def node_comm():
    if not is_parallel():
        return comm
    return comm.Create_group(node_group())


def node_sizes():
    return {node: len(rank) for node, rank in node_allocations()}


def node_size():
    return node_sizes()[node_name()]


@contextmanager
def communicator(newcomm):
    global comm
    oldcomm = comm
    comm = newcomm
    yield
    comm = oldcomm


def only_root(f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        if comm.rank == 0:
            return f(*args, **kwargs)
    return wrapper


class Distributor:
    def __init__(self, size):
        self.size = size

        self.avg, self.last = divmod(size, comm.size)

        self.owner = np.concatenate([np.full(self.local_size(rank), rank)
                                     for rank in range(comm.size)])
        self.ilocal = np.concatenate([np.r_[:self.local_size(rank)]
                                      for rank in range(comm.size)])

    def bounds(self, rank=None):
        if rank is None:
            rank = comm.rank
        begin = comm.rank * self.avg + min(rank, self.last)
        end = (comm.rank + 1) * self.avg + min(rank + 1, self.last)
        return (begin, end)

    def local_size(self, rank=None):
        begin, end = self.bounds(rank)
        return end - begin

    def slice(self, rank=None):
        return slice(*self.bounds(rank))

    def is_owner(self, iglobal, rank=None):
        if rank is None:
            rank = comm.rank
        return self.owner[iglobal] == rank

    def iglobal(self, rank=None):
        return np.r_[self.slice(rank)]

    def distribute(self, data):
        assert(self.size == len(data))
        if not is_parallel():
            return data
        return data[self.slice()]

    def iterate(self, local_data, dest=0):
        for iglobal in range(self.size):
            if self.is_owner(iglobal):
                data = local_data[self.ilocal[iglobal]]
                if comm.rank == dest:
                    yield data
                else:
                    comm.Send(local_data[self.ilocal[iglobal]], dest=dest)
            elif comm.rank == dest:
                data = np.empty(local_data.shape[1:], dtype=local_data.dtype)
                comm.Recv(data, source=self.owner[iglobal])
                yield data

    def alliterate(self, local_data):
        for iglobal in range(self.size):
            if self.is_owner(iglobal):
                data = local_data[self.ilocal[iglobal]]
            else:
                data = np.empty(local_data.shape[1:], dtype=local_data.dtype)
            comm.Bcast(data, root=self.owner[iglobal])
            yield data

    def gather(self, local_data, root=0):
        '''Equivalent to np.concatenate with axis=0 over mpi.'''
        if not is_parallel():
            return local_data

        shapes = [(self.local_size(rank),) + local_data.shape[1:]
                  for rank in range(comm.size)]

        recv = recvbuf = None
        if comm.rank == root:
            recv_shape = (sum(shape[0] for shape in shapes), ) + local_data.shape[1:]

            size_per_node, offset_per_node = _distributed_sizes_and_offset(shapes)

            recvbuf = np.empty(recv_shape, dtype=local_data.dtype)
            recv = [recvbuf, (size_per_node, offset_per_node)]

        comm.Gatherv(sendbuf=local_data, recvbuf=recv, root=root)
        return recvbuf

    def allgather(self, local_data, dest=0):
        if not is_parallel():
            return local_data
        recvbuf = np.empty((self.size,) + local_data.shape[1:], dtype=local_data.dtype)

        shapes = [(self.local_size(rank),) + local_data.shape[1:]
                  for rank in range(comm.size)]

        size_per_node, offset_per_node = _distributed_sizes_and_offset(shapes)
        comm.Allgatherv(sendbuf=local_data,
                        recvbuf=[recvbuf, (size_per_node,
                                           offset_per_node)])
        return recvbuf


dist = Distributor


def split(data):
    try:
        return Distributor(len(data)).distribute(data)
    except TypeError:
        return split(list(data))


def join(data):
    if not is_parallel():
        return data
    return list(chain.from_iterable(allgather(data)))


def pmap(func, data):
    if not is_parallel():
        return map(func, data)
    return join(list(map(func, split(data))))


def allreduce(data, op='sum'):
    if not is_parallel():
        return data
    comm.Allreduce(MPI.IN_PLACE, data, op=operations[op.lower()])
    return data


def reduce(data, op='sum'):
    if not is_parallel():
        return data
    comm.Reduce(MPI.IN_PLACE, data, op=operations[op.lower()])
    return data


def _validate_distributed(data):
    data = np.ascontiguousarray(data)
    shapes = comm.allgather(data.shape)

    if not all(shape[1:] == data.shape[1:] for shape in shapes):
        raise ValueError('The shapes of the arrays are not compatible.')

    shape = (sum(shape[0] for shape in shapes), ) + data.shape[1:]
    return data, shapes, shape


def _distributed_sizes_and_offset(shapes):
    size_per_node = [np.prod(shape) for shape in shapes]
    offset_per_node = np.zeros(comm.size + 1, dtype=int)
    offset_per_node[1:] = np.cumsum(size_per_node)
    offset_per_node = offset_per_node[:-1]
    return size_per_node, offset_per_node


def p2p(data, orig=0, dest=0):
    '''Broadcast array to all nodes'''
    if comm.rank != orig and comm.dest != dest:
        return

    if orig == dest:
        return data

    meta = None
    if comm.rank == orig:
        meta = (data.shape, data.dtype)
        comm.send(meta)

        comm.Send(data)
    else:
        shape, dtype = comm.recv(meta)
        data = np.empty(shape, dtype=dtype)

        comm.Recv(data)

    return data


def bcast(data, root=0):
    '''Broadcast array to all nodes'''
    if comm.size == 1:
        return data

    meta = None
    if comm.rank == root:
        meta = (data.shape, data.dtype)

    shape, dtype = comm.bcast(meta, root=root)

    if comm.rank != root:
        data = np.empty(shape, dtype=dtype)

    comm.Bcast(data, root=root)

    return data


def gather(data, root=0):
    '''Equivalent to np.concatenate with axis=0 over mpi.'''
    if comm.size == 1:
        return data

    data, shapes, shape = _validate_distributed(data)

    recv = recvbuf = None
    if comm.rank == root:
        shape = (sum(shape[0] for shape in shapes), ) + data.shape[1:]

        size_per_node, offset_per_node = _distributed_sizes_and_offset(shapes)

        recvbuf = np.empty(shape, dtype=data.dtype)
        recv = [recvbuf, (size_per_node, offset_per_node)]

    comm.Gatherv(sendbuf=data, recvbuf=recv, root=root)

    return recvbuf


def allgather(data):
    '''Equivalent to np.concatenate with axis=0 over mpi.'''
    if comm.size == 1:
        return data

    data, shapes, shape = _validate_distributed(data)

    size_per_node, offset_per_node = _distributed_sizes_and_offset(shapes)

    sendbuf = np.concatenate(data, axis=None)
    recvbuf = np.empty(shape, dtype=sendbuf.dtype)

    comm.Allgatherv(sendbuf=sendbuf,
                    recvbuf=[recvbuf, (size_per_node,
                                       offset_per_node)])
    return recvbuf


concatenate = allgather


class File:
    def __init__(self, filename, mode='w'):
        self.fd = None

        if 'w' in mode:
            amode = MPI.MODE_CREATE
            if '+' in mode:
                amode |= MPI.MODE_RDWR
            else:
                amode |= MPI.MODE_WRONLY
        elif 'r' in mode:
            if '+' in mode:
                amode = MPI.MODE_RDWR
            else:
                amode = MPI.MODE_RDONLY
        else:
            raise ValueError()

        self.binary = 'b' in mode

        if not self.binary:
            raise NotImplementedError()

        self.fd = MPI.File.Open(comm, filename, amode)

    def __enter__(self):
        return self

    def __del__(self):
        if self.fd is not None:
            self.fd.Close()
        self.fd = None

    def __exit__(self, exception_type, exception_value, traceback):
        self.__del__()

    def write(self, data):
        self.fd.Write(data)

    def read(self, size):
        data = bytearray(size)
        self.fd.Read(data)
        return bytes(data)

    def tell(self):
        return self.fd.Get_position()

    def seek(self, offset, whence=0):
        whence = [MPI.SEEK_SET, MPI.SEEK_CUR, MPI.SEEK_END][whence]
        self.fd.Seek(offset, whence)


def npy_header(shape, dtype):
    """ Generates the header metadata for a npy file
    """
    from numpy.lib.format import dtype_to_descr
    return {
        'shape': shape,
        'fortran_order': False,
        'descr': dtype_to_descr(dtype),
    }


def save(fname, arr):
    from numpy.lib.format import _write_array_header

    fname = os.fspath(fname)
    if not fname.endswith('.npy'):
        fname = fname + '.npy'

    arr, shapes, shape = _validate_distributed(arr)

    with File(fname, "wb") as fd:

        offset = None
        if comm.rank == 0:
            _write_array_header(fd, npy_header(shape, arr.dtype), None)
            offset = fd.tell()
        offset = comm.bcast(offset, root=0)

        size_per_node, offset_per_node = _distributed_sizes_and_offset(shapes)
        offset += offset_per_node[comm.rank] * arr.itemsize

        fd.seek(offset)

        buffersize = max(16 * 1024 ** 2 // arr.itemsize, 1)
        flags = ['external_loop', 'buffered', 'zerosize_ok']
        for chunk in np.nditer(arr, flags=flags,
                               buffersize=buffersize, order='C'):
            fd.write(chunk.tobytes('C'))


def load(fname):
    from numpy.lib.format import read_magic, _check_version
    from numpy.lib.format import _read_array_header, _read_bytes

    BUFFER_SIZE = 2**18

    with File(fname, "rb") as fd:
        version = read_magic(fd)
        _check_version(version)
        shape, fortran_order, dtype = _read_array_header(fd, version)

        slice_ = Distributor(shape[0]).slice()
        shape = (slice_.stop - slice_.start, ) + shape[1:]
        size = np.multiply.reduce(shape, dtype=np.int64)

        offset = slice_.start * np.multiply.reduce(shape[1:], dtype=np.int64)
        fd.seek(offset * dtype.itemsize, 1)

        array = np.ndarray(size, dtype=dtype)
        if dtype.itemsize > 0:
            # If dtype.itemsize == 0 then there's nothing more to read
            max_read_count = BUFFER_SIZE // min(BUFFER_SIZE, dtype.itemsize)

            for i in range(0, size, max_read_count):
                read_count = min(max_read_count, size - i)
                read_size = int(read_count * dtype.itemsize)
                data = _read_bytes(fd, read_size, "array data")
                array[i:i + read_count] = np.frombuffer(data, dtype=dtype,
                                                        count=read_count)

    if fortran_order:
        array.shape = shape[::-1]
        array = array.transpose()
    else:
        array.shape = shape

    return array
