import time
import warnings
import threading
import heapq
from itertools import islice
from collections import defaultdict, OrderedDict

import numpy as np

from petra.util.indexing import pairwise
from petra.util import mpi
from petra.linalg import rms
from petra.integrate import IntegrationWarning, IntegrationResult


def integrate(func, a, b, max_eval=10, init_eval=1, atol=0., rtol=0.,
              verbose=False):

    if verbose:
        print('step  #eval  error_abs  error_rel')
    integrator = LocalIntegrator(func, a, b)
    integrator.divide(init_eval - len(integrator))
    if verbose:
        print('init  {:5d}  {:9.2f}  {:9.2f}'
              .format(len(integrator),
                      np.log10(integrator.abs_error),
                      np.log10(integrator.rel_error)),
              flush=True)
    integrator.refine(evaluations=max_eval, atol=atol, rtol=rtol)
    if verbose:
        print('refi  {:5d}  {:9.2f}  {:9.2f}'
              .format(len(integrator),
                      np.log10(integrator.abs_error),
                      np.log10(integrator.rel_error)),
              flush=True)

    if not integrator.converged:
        warnings.warn('Integrator did not converge, results are not accurate.',
                      IntegrationWarning)
    if verbose:
        print('converged' if integrator.converged else 'not converged',
              flush=True)

    return IntegrationResult(integrator.value,
                             integrator.abs_error,
                             integrator.converged,
                             integrator.weights,
                             integrator.x,
                             integrator.f)


def subdict(d, keys):
    return {k: d[k] for k in keys if k in d}


def _get_first(val):
    if isinstance(val, tuple):
        return val[0]
    return val


def filter_first(d):
    return {k: _get_first(d[k]) for k in d}


def split(interval, function, evaluations):
    new_evaluations = {}

    children = interval.children()
    for child in children:
        new_evaluations.update(child.evaluate(function, evaluations))

    newint = sum(child.value for child in children)
    difference = np.abs(newint - interval.value)**2
    error = np.sqrt(np.sum(difference) / np.size(difference)) / 15
    roundoff = np.abs(newint).max() * np.finfo(float).eps
    error = error / len(children) + roundoff
    for child in children:
        child.error = error

    return children, new_evaluations


class Interval:
    """An interval for adaptive integration.

    Attributes:
        value: The value of the integral of this interval.
        error: The estimated error of this interval.
        weights: The integration weights.
    """
    __slots__ = ['x', 'value', 'weights', 'error']

    def __init__(self, x):
        self.x = x
        self.value = None
        self.weights = None
        self.error = np.inf

    def children(self):
        return [Interval([a, (a + b) / 2, b])
                for a, b in pairwise(self.x)]

    def evaluate(self, function, evaluations=None):
        if evaluations is None:
            evaluations = {}
        new_evaluations = {}
        f = []
        for x in self.x:
            try:
                f.append(_get_first(evaluations[x]))
            except KeyError:
                new_evaluations[x] = function(x)
                f.append(_get_first(new_evaluations[x]))

        L = self.x[-1] - self.x[0]
        self.value, self.weights = simps(L, f)
        self.error = np.inf
        return new_evaluations

    def __lt__(self, other):
        ''' Returns gt instead to have heapq sorted
        with maximum values at the top
        '''
        return self.error > other.error

    def __repr__(self):
        return 'Interval(error={})'.format(self.error)


def simps(length, values):
    """Calculates Simpson rule over 3 points

    Arguments
    ---------
    length
        The length of the interval.
    values
        The values of the function at the start, middle and end of the interval.

    Returns
    -------
    A tuple containing: the value of the integral, the summation weights.
    """
    weights = [length / 6, length * 4 / 6, length / 6]
    integral = (weights[0] * values[0] +
                weights[1] * values[1] +
                weights[2] * values[2])
    return integral, weights


def LocalIntegrator(func, a, b):
    """Builds an adaptive Simpson integrator.

    Arguments
    ---------
    func
        The function to integrate, it should always return a tuple,
        the first element of the tuple should be the value to integrate.
    a
        The lower limit of the integration.
    b
        The upper limit of the integration.

    Returns
    -------
    A ParallelLocalIntegrator is returned when mpi is active and has
    more than one node. Otherwise, a SerialLocalIntegrator is returned.
    """
    if mpi.is_parallel():
        return ParallelLocalIntegrator(func, a, b)
    return SerialLocalIntegrator(func, a, b)


class _LocalIntegratorBase:
    """A base class for the adaptive Simpson integration algorithm.

    It sets up the initial interval and data structures but does not implement
    any of the adaptive refining of the evaluation intervals.

    Attributes
    ----------
    func
        The function to integrate.
    L
        The length of the integration interval.
    x
        The arguments at which the function has been evaluated.
    f
        The function values corresponding to func(x).
    intervals
        The Simpson intervals that have been determined.
    a
        The lower limit of the integration.
    b
        The upper limit of the integration.
    """
    def __init__(self, func, a, b):
        self.func = func
        self.L = b - a
        self.evaluations = OrderedDict()
        self.intervals = []
        self.running_intervals = set()
        self.converged = False
        self.atol = 0.
        self.rtol = 0.

        interval = Interval((a, (a + b) / 2, b))
        self.evaluations.update(interval.evaluate(self.func))
        heapq.heappush(self.intervals, interval)

    def worst_interval(self):
        while not self.converged:
            if len(self.intervals) == 0:
                yield None
                continue
            interval = heapq.heappop(self.intervals)
            self.running_intervals.add(interval)
            yield interval

    def all_intervals(self):
        intervals = list(self.intervals)
        self.running_intervals.update(intervals)
        self.intervals = []
        return intervals

    def replace_interval(self, interval, intervals):
        self.running_intervals.remove(interval)
        for interval in intervals:
            heapq.heappush(self.intervals, interval)

        max_error = self.intervals[0].error
        if max_error < self.atol and self.abs_error < self.atol:
            new_tol = self.rtol * self.rms_value
            if new_tol < self.atol:
                self.atol = new_tol
            else:
                self.converged = True

    def __len__(self):
        return len(self.evaluations)

    @property
    def abs_error(self):
        err = sum(interval.error for interval in self.intervals)
        err += sum(interval.error for interval in self.running_intervals)
        return err

    @property
    def rel_error(self):
        return self.abs_error / self.rms_value

    @property
    def rms_value(self):
        value = sum(interval.value for interval in self.intervals)
        value += sum(interval.value for interval in self.running_intervals)
        return rms(value)

    @property
    def x(self):
        return np.array(list(self.evaluations))

    @property
    def f(self):
        return list(self.evaluations.values())

    @property
    def errors(self):
        errors = defaultdict(float)
        for interval in self.intervals:
            for x in interval.x:
                errors[x] += interval.error
        return np.array([errors[x] for x in self.x])

    @property
    def weights(self):
        weights = defaultdict(float)
        for interval in self.intervals:
            for x, weight in zip(interval.x, interval.weights):
                weights[x] += weight
        return np.array([weights[x] for x in self.x])

    @property
    def value(self):
        return sum(interval.value for interval in self.intervals)


class SerialLocalIntegrator(_LocalIntegratorBase):
    """A serial implementation of the adaptive Simpson integration algorithm.

    Attributes
    ----------
    func
        The function to integrate.
    L
        The length of the integration interval.
    x
        The arguments at which the function has been evaluated.
    f
        The function values corresponding to func(x).
    intervals
        The Simpson intervals that have been determined.
    roundoff
        True if roundoff error has occurred in the calculation.
    a
        The lower limit of the integration.
    b
        The upper limit of the integration.
    """

    def process(self, interval):
        subintervals, new_evals = split(interval, self.func, self.evaluations)
        self.evaluations.update(new_evals)
        self.replace_interval(interval, subintervals)

    def refine(self, iterations=None, evaluations=None,
               atol=0.0, rtol=1e-5):
        self.converged = False
        self.atol = atol
        self.rtol = rtol

        if evaluations is not None:
            iterations = evaluations // 2
        for interval in islice(self.worst_interval(), iterations):
            self.process(interval)
        return self.converged

    def divide(self, evaluations=10):
        self.converged = False
        self.atol = 0.
        self.rtol = 0.
        eval0 = len(self.intervals)

        while len(self.intervals) - eval0 < evaluations:
            for interval in self.all_intervals():
                self.process(interval)


class Worker:
    __slots__ = ['comm', 'id', 'data', 'occupied']

    def __init__(self, comm, id_):
        self.id = id_
        self.data = None
        self.occupied = False

    def send(self, data):
        self.occupied = True
        self.data = data
        mpi.comm.send(data, dest=self.id, tag=11)

    def receive(self):
        data = mpi.comm.recv(source=self.id, tag=12)
        self.occupied = False
        self.data = None
        return data

    @property
    def done(self):
        if not self.occupied:
            return False
        return mpi.comm.iprobe(source=self.id, tag=12)


class ParallelLocalIntegrator(_LocalIntegratorBase):
    """A parallel implementation of the adaptive Simpson integration algorithm.

    Attributes
    ----------
    func
        The function to integrate.
    L
        The length of the integration interval.
    x
        The arguments at which the function has been evaluated.
    f
        The function values corresponding to func(x).
    intervals
        The Simpson intervals that have been determined.
    a
        The lower limit of the integration.
    b
        The upper limit of the integration.
    """

    def __init__(self, func, a, b, poll=1e-3):
        super().__init__(func, a, b)
        self.poll = poll

    def submit(self, interval, worker):
        evals = filter_first(subdict(self.evaluations, interval.x))
        worker.send((interval, evals))

    def receive(self, worker, block=False):
        if not (block and worker.occupied):
            if not worker.done:
                return
        interval, _ = worker.data
        subintervals, new_evals = worker.receive()
        self.evaluations.update(new_evals)
        self.replace_interval(interval, subintervals)

    def wait_for_completed_work(self, workers):
        if any(worker.occupied for worker in workers):
            # wait by polling every self.poll sec
            while not mpi.comm.iprobe(tag=12):
                time.sleep(self.poll)

    def start_workers(self, treading=True):
        if not mpi.is_root():
            self.worker_loop()
            return None

        thread = threading.Thread(target=self.worker_loop)
        thread.start()
        return [Worker(mpi.comm, ii) for ii in range(mpi.comm.size)]

    def stop_workers(self, workers):
        if workers is not None:
            for worker in workers:
                self.receive(worker, block=True)
                # stop worker
                worker.send(None)

        # sync state across processes
        self.evaluations = mpi.comm.bcast(self.evaluations, root=0)
        self.intervals = mpi.comm.bcast(self.intervals, root=0)
        self.converged = mpi.comm.bcast(self.converged, root=0)

    def worker_loop(self):
        while True:
            data = mpi.comm.recv(source=0, tag=11)
            if data is None:
                break
            interval, evals = data
            ret_data = split(interval, self.func, evals)
            mpi.comm.send(ret_data, dest=0, tag=12)
            # wait by polling every self.poll sec
            while not mpi.comm.iprobe(source=0, tag=11):
                time.sleep(self.poll)

    def refine(self, iterations=None, evaluations=None,
               atol=0.0, rtol=1e-5):
        """Refine the integration

        Arguments
        ---------
        iterations
            The maximum number of iterations.
        evaluations
            The maximum number of evaluations.
        atol
            The absolute tolerance (default: 0).
        rtol
            The relative tolerance (default: 1e-5).

        Notes
        -----
        The relative tolerance is only activated once the absolute
        tolerance criterion has been met. By default, the absolute
        tolerance is 0.0 and the refinement will not converge even though
        a relative tolerance is set.
        """
        self.converged = False
        self.atol = atol
        self.rtol = rtol

        if evaluations is not None:
            iterations = evaluations // 2

        workers = self.start_workers()

        if workers is not None:

            intervals = self.worst_interval()
            while not self.converged and iterations > 0:
                for worker in workers:
                    self.receive(worker)

                    if not worker.occupied:
                        interval = next(intervals, None)
                        if interval is None:
                            continue
                        self.submit(interval, worker)
                        iterations -= 1

                self.wait_for_completed_work(workers)

        self.stop_workers(workers)

        return self.converged

    def divide(self, evaluations=1):
        self.converged = False
        self.atol = 0.
        self.rtol = 0.

        eval0 = len(self.intervals)
        workers = self.start_workers()

        if workers is not None:

            while True:
                if len(self.intervals) - eval0 >= evaluations:
                    break
                intervals = iter(self.all_intervals())
                try:
                    while True:
                        for worker in workers:
                            if not worker.occupied:
                                self.submit(next(intervals), worker)
                        for worker in workers:
                            self.receive(worker)

                    self.wait_for_completed_work(workers)

                except StopIteration:
                    pass

                for worker in workers:
                    self.receive(worker, block=True)

        self.stop_workers(workers)
