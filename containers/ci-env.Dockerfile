FROM centos:8

ENV LANG=C.UTF-8 LC_ALL=C.UTF-8
ENV MAMBA_ROOT_PREFIX="/opt/conda"
ENV PATH "$MAMBA_ROOT_PREFIX/bin:$PATH"

RUN dnf install -y bzip2 make git && \
    dnf clean all && \
    curl -L https://micromamba.snakepit.net/api/micromamba/linux-64/latest | \
    tar -xj -C /usr bin/micromamba && \
    mkdir -p "$MAMBA_ROOT_PREFIX" && \
    chmod -R a+rwx "$MAMBA_ROOT_PREFIX"

COPY environment.yml environment.yml

RUN micromamba install -y -n base -f environment.yml && \
    micromamba clean --all --yes

WORKDIR /mnt