import numpy as np


def flat(x, nodes):
    if len(nodes) <= 1:
        return x[None, :]**0, x[None, :] * 0
    shape = np.array([
        x**0,
        x**0
    ])
    dshape = np.array([x * 0., x * 0.])
    return shape, dshape


def wannier(x, nodes):
    if len(nodes) <= 1:
        return x[None, :]**0, x[None, :] * 0
    shape = (1 + np.exp(1j * np.pi * (x[None, :] - nodes[:, None]))) / 2
    dshape = .5j * np.pi * np.exp(1j * np.pi * (x[None, :] - nodes[:, None]))
    return shape, dshape


def hermite(x, nodes):
    if len(nodes) <= 1:
        return x[None, :]**0, x[None, :] * 0
    shape = np.array([1 - 3 * x**2 + 2 * x**3, 3 * x**2 - 2 * x**3])
    dshape = np.array([- 6 * x + 6 * x**2, 6 * x - 6 * x**2])
    return shape, dshape


def hat(x, nodes):
    if len(nodes) <= 1:
        return x[None, :]**0, x[None, :] * 0
    dnode = nodes[1] - nodes[0]
    # calculate shape functions
    shape = np.empty(nodes.shape + x.shape)
    for i, node in enumerate(nodes):
        shape[i] = np.clip(1 - np.abs((x - node) / dnode), 0, 1)
    dshape = np.empty(nodes.shape + x.shape)
    for i, node in enumerate(nodes):
        dshape[i] = np.sign(node - x) * (shape[i] != 0.0) / dnode
    return shape, dshape


def lagrange(x, nodes):
    # calculate shape functions
    shape = np.empty(nodes.shape + x.shape)
    for i, node in enumerate(nodes):
        others = nodes != node
        shape[i] = np.prod(x[None, :] - nodes[others, None], axis=0)
        shape[i] /= np.prod(node - nodes[others])
    # calculate derivatives
    dshape = np.empty(nodes.shape + x.shape)
    for i, node in enumerate(nodes):
        xdiff = x[None, :] - nodes[nodes != node, None]
        dshape[i] = np.sum(shape[None, i] / xdiff, axis=0)
    return shape, dshape


def sin2cos2(x, nodes):
    # calculate shape functions
    if nodes.size > 2:
        raise ValueError('The sin2cos2 basis supports a maximum of 2 nodes.')
    if nodes.size == 1:
        ones = np.ones(nodes.shape + x.shape)
        return x, nodes, ones, 0 * ones
    L = nodes.max() - nodes.min()
    shape = np.empty(nodes.shape + x.shape)
    for i, node in enumerate(nodes):
        shape[i, :] = np.cos(np.pi * (x - node) / (2 * L))**2
    dshape = np.empty(nodes.shape + x.shape)
    for i, node in enumerate(nodes):
        dshape[i, :] = -np.pi * np.sin(np.pi * (x - node) / L) / (2 * L)
    return shape, dshape


def combine(shapes):
    return np.prod(np.meshgrid(*shapes, indexing='ij'), axis=0)


def generate_unit_1d(npos, nnodes, x0=0, x1=1, func=lagrange):
    '''Calculates shape functions and their derivatives
    to the order matching the number of nodes.

    Args:
        npos: The number of points on the grid.
        nnodes: The number of nodes and the order of the grid.
        x0, x1: The start and end-points of the grid.

    '''
    nodes = np.linspace(x0, x1, nnodes)
    x = np.linspace(0, 1, npos, endpoint=False)
    x += x[1] / 2

    shape, dshape = func(x, nodes)

    return x, nodes, shape, dshape


def generate_unit(npos_per_dim, nnodes_per_dim):
    import itertools
    nnodes_per_dim = np.array(nnodes_per_dim)
    npos_per_dim = np.array(npos_per_dim)
    ndims = nnodes_per_dim.size
    if ndims != npos_per_dim.size:
        msg = "The number of dimensions for positions and nodes is not equal"
        raise ValueError(msg)
    x_1d, nodes_1d, shapes_1d, dshapes_1d = [], [], [], []
    for nnodes, npos in zip(nnodes_per_dim, npos_per_dim):
        unit_1d = generate_unit_1d(npos, nnodes)
        _x, _nodes, _shapes, _dshapes = unit_1d
        x_1d.append(_x)
        nodes_1d.append(_nodes)
        shapes_1d.append(_shapes)
        dshapes_1d.append(_dshapes)
    x_nd = np.array(np.meshgrid(*x_1d, indexing='ij'))
    nodes_nd = np.array([coords for coords in itertools.product(*nodes_1d)])
    # the generation of these indices relies on the lexographic ordering
    # of itertools.product
    indices = np.r_[:nodes_nd.shape[0]].reshape(nnodes_per_dim)
    shapes_nd = np.array([combine(shapes)
                          for shapes in itertools.product(*shapes_1d)])
    dshapes_nd = np.empty(nodes_nd.shape + shapes_nd.shape[1:],
                          dtype=shapes_nd.dtype)
    for ii, dshapes in enumerate(dshapes_1d):
        dshapes_combination = shapes_1d.copy()
        dshapes_combination[ii] = dshapes
        dshapes_iter = itertools.product(*dshapes_combination)
        dshapes_nd[:, ii, ...] = np.array([combine(dshapes)
                                           for dshapes in dshapes_iter])
    return x_nd, nodes_nd, shapes_nd, dshapes_nd, indices


def transform(a, x, nodes, shapes, dshapes, indices):
    tx = np.tensordot(a, x, axes=(1, 0))
    tnodes = np.tensordot(a, nodes, axes=(1, 1)).T
    absa = np.sqrt(np.sum(a**2, 1))
    selection = (None, slice(None)) + (None,) * (len(dshapes.shape) - 2)
    tdshapes = dshapes / absa[selection]
    return tx, tnodes, shapes, tdshapes, indices


def generate(a, npos_per_dim, nnodes_per_dim):
    return transform(a, *generate_unit(npos_per_dim, nnodes_per_dim))


def plot_shapes_2d(npos_per_dim, nnodes_per_dim, a=None):
    if len(npos_per_dim) != 2:
        raise ValueError("Showing of shapes only works in 2D space")
    unit = generate_unit(npos_per_dim, nnodes_per_dim)
    x, nodes, shapes, dshapes, indices = unit
    if a is not None:
        transformed = transform(a, *unit)
        x, nodes, shapes, dshapes, indices = transformed
    import matplotlib.pyplot as plt
    plt.figure()
    for jj, (shape, node, dshape) in enumerate(zip(shapes, nodes, dshapes)):
        plt.subplot(1 + dshape.shape[0], nodes.shape[0], jj + 1)
        plt.contourf(*x, shape, 100, vmin=-1.5, vmax=1.5, cmap='seismic')
        plt.scatter(node[0:1], node[1:])
        for ii, idshape in enumerate(dshape):
            cols = 1 + dshape.shape[0]
            rows = nodes.shape[0]
            plt.subplot(cols, rows, (1 + ii) * nodes.shape[0] + jj + 1)
            plt.contourf(*x, idshape, 100, vmin=-1.5, vmax=1.5, cmap='seismic')
            plt.scatter(node[0:1], node[1:])
