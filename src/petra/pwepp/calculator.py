from functools import wraps

import numpy as np
from scipy import optimize

import ase.io.ulm as ulm
from ase.calculators.calculator import Calculator
from ase.io.trajectory import read_atoms, write_atoms
from ase.dft.kpoints import BandPath

from petra.util import log, mpi
from petra.distributions import fermi_dirac
from petra.periodiccell import PeriodicCell
from petra import constants as c
from petra.pwepp.kpoints import KPoints
from petra.pwepp.pseudopotential import Pseudopotential, Pseudopotentials
from petra.pwepp import hamiltonian
from petra.pwepp import states
from petra.pwepp import basis

logger = log.getLogger(__name__)


def run_initialized(func):
    @wraps(func)
    def wrapped(self, *args, **kwargs):
        self.initialize()
        return func(self, *args, **kwargs)
    return wrapped


def mode(mode, **kwargs):
    return mode, kwargs


def PW(antialias=2, Ecut=None):
    return dict(
        hamiltonian=mode('pw'),
        basis=mode('pw',
                   antialias=antialias,
                   closest_fft=False,
                   Ecut=Ecut),
    )


def FFT(antialias=2, closest_fft=True, Ecut=None):
    return dict(
        hamiltonian=mode('fft'),
        basis=mode('pw',
                   antialias=antialias,
                   closest_fft=closest_fft,
                   Ecut=Ecut),
    )


def FD(antialias=2, closest_fft=True, h=0.5):
    return dict(
        hamiltonian=mode('fd', order=3),
        basis=mode('grid', h=h),
    )


class PWEPP(Calculator):

    implemented_properties = ['states', 'vg', 'fermi', 'occupation', 'density']

    default_parameters = {
        **FFT(),
        'kpts': [(0.0, 0.0, 0.0)],
        'kweights': [1],
        'nbands': None,
        'pseudopotentials': {},
        'external_potential': None,
        'cutoff': None,
        'Etol': 0,
        'Efield': None,
        'smear': 300 * c.K,
        'gspin': 2,
    }

    def __init__(self, restart=None, label=None, atoms=None, **kwargs):

        self.cell = None
        self.basis = None
        self.wfs = None

        Calculator.__init__(self, restart=restart, label=label, atoms=atoms, **kwargs)

        if atoms is not None:
            self.initialize(atoms)

    @mpi.only_root
    def write(self, name, mode='w', tag='PWEPP'):
        if name.endswith('.pwepp'):
            name = name[:-6]
        fname = name + '.pwepp'

        writer = ulm.Writer(fname, mode=mode, tag=tag)
        self._write(writer, mode)
        writer.close()

        self.states.write(name)

        logger.info('Written to {}'.format(fname))

    def _write(self, writer, mode):
        write_atoms(writer.child('atoms'), self.atoms)
        writer.child('parameters').write(**self.todict())
        writer.child('results').write(**self.results)

        return writer

    def _set_atoms(self, atoms):
        self.atoms = atoms
        self.spos_ia = atoms.get_scaled_positions()
        self.cell_av = atoms.get_cell() * c.Ang
        self.symb_i = atoms.get_chemical_symbols()

        # self.initialized = False

    def read(self, name):
        if name.endswith('.pwepp'):
            name = name[:-6]
        fname = name + '.pwepp'

        reader = ulm.open(fname)
        self._read(reader)

        self.states = states.read(self.basis, name)

        return reader

    def _read(self, reader):
        atoms = read_atoms(reader.atoms)
        self._set_atoms(atoms)

        self.parameters = self.get_default_parameters()
        self.parameters.update(reader.parameters.asdict())

        # parse paseudopotentials
        for key in self.parameters.pseudopotentials:
            pp_dict = self.parameters.pseudopotentials[key]
            pp = Pseudopotential.fromdict(pp_dict)
            self.parameters.pseudopotentials[key] = pp

        self.results = reader.results.asdict()

        self.initialize()

    def calculate(self, atoms=None, properties=[],
                  system_changes=['cell']):
        """Calculates the requested properties.

        Parameters
        ----------
        atoms : ase.Atoms, optional
            The atoms to calculate this property for. (defaults previous atoms)
        properties: array or string, optional
            A list of properties, a single property or 'all', see Notes.
            (defaults to [])

        Notes
        -----
        Implmented properties are:
            - vg: Group velocities
            - fermi: Fermi level
            - occupation: Occupation of each state
            - density: Integrated electron density
        """
        Calculator.calculate(self, atoms, properties, system_changes)
        atoms = self.atoms

        if system_changes:
            logger.info('Detected system change, PWEPP has been reset.')
            for system_change in system_changes:
                logger.info('System change: ' + system_change)

            if 'positions' in system_changes:
                self.set_positions(atoms)

        if not self.initialized:
            self.initialize()

        if properties == 'all':
            properties = ['vg', 'density', 'occupation', 'fermi']
        elif isinstance(properties, str):
            properties = [properties]

        properties = [prop for prop in set(properties) if prop not in self.results]

        if self.states is None:
            self.calculate_states()

        if 'vg' in properties:
            self._calculate_group_velocity()

        if 'fermi' in properties or 'occupation' in properties:
            self._calculate_fermi()

        if 'density' in properties:
            self._calculate_density()

    def invalidate(self, prop):
        invalid = {
            'states': ['vg', 'fermi', 'occupation', 'density'],
            'vg': ['vg'],
            'fermi': ['fermi', 'occupation'],
            'occupation': ['fermi', 'occupation'],
            'density': ['density'],
        }[prop]
        for result in invalid:
            if self.results.pop(result, None) is not None:
                logger.info('invalidating result: ' + result)

    def set(self, **kwargs):
        for key in kwargs:
            if key not in self.default_parameters:
                raise ValueError('Unknown parameter {}'.format(key))

        if 'kpts' in kwargs:
            if isinstance(kwargs['kpts'], KPoints):
                kwargs['kweights'] = kwargs['kpts'].weight_k
                kwargs['kpts'] = kwargs['kpts'].k_ka
            elif isinstance(kwargs['kpts'], BandPath):
                kwargs['kpts'] = kwargs['kpts'].kpts
            if 'kweights' not in kwargs:
                kwargs['kweights'] = np.ones(len(kwargs['kpts'])) / len(kwargs['kpts'])
            kwargs['kpts'] = np.asarray(kwargs['kpts'])
            kwargs['kweights'] = np.asarray(kwargs['kweights'])

        changed_parameters = Calculator.set(self, **kwargs)

        if not changed_parameters:
            return

        for key in changed_parameters:
            logger.info('changed parameter: ' + key)
            if key in ['nbands', 'kpts', 'cutoff', 'Etol', 'gspin']:
                self.states = None
                self.initialized = False
            if key in ['smear']:
                self.invalidate('occupation')

        self.parameters.update(changed_parameters)

    def initialize_positions(self, atoms=None):
        if atoms is None:
            atoms = self.atoms
        else:
            atoms = atoms.copy()
            self._set_atoms(atoms)

    def set_positions(self, atoms=None):
        self.initialize_positions(atoms)

    def initialize(self, atoms=None):
        if atoms is None:
            atoms = self.atoms
        else:
            atoms = atoms.copy()
            self._set_atoms(atoms)

        self.cell = PeriodicCell(self.cell_av)

        self.pseudopotentials = Pseudopotentials(self.spos_ia, self.symb_i,
                                                 self.parameters.pseudopotentials)

        basis_mode, basis_args = self.parameters.basis
        if 'Ecut' in basis_args and not basis_args['Ecut']:
            basis_args['Ecut'] = self.pseudopotentials.E_max

        self.basis = basis.create(basis_mode, self.cell, basis_args)

        hamiltonian_mode, hamiltonian_args = self.parameters.hamiltonian
        self.hamiltonian = hamiltonian.create(hamiltonian_mode, self.basis, hamiltonian_args)
        self.hamiltonian.set_atomic_potential(self.pseudopotentials)
        if self.parameters.external_potential is not None:
            V_xyz = self.parameters.external_potential(self.basis.to_grid().r_xyzv)
            self.hamiltonian.set_external_potential(V_xyz)

        self.states = None

        self.initialized = True

    def get_bz_k_points(self):
        kpts = self.parameters.kpts
        if isinstance(kpts, KPoints):
            return np.asarray(kpts.k_full_ka)
        return np.asarray(kpts)

    def get_ibz_k_points(self):
        kpts = self.parameters.kpts
        if isinstance(kpts, KPoints):
            return np.asarray(kpts.k_ka)
        return np.asarray(kpts)

    def get_k_point_weights(self):
        if self.states is not None:
            return self.states.weight_k
        weight_k = self.parameters.kweights
        return weight_k / weight_k.sum()

    def get_number_of_bands(self):
        nbands = self.parameters.nbands
        if nbands is None:
            nbands = (self.pseudopotentials.Nv + len(self.atoms)) // self.parameters.gspin
        if nbands < 0:
            nbands = int(self.pseudopotentials.Nv / self.parameters.gspin) - nbands
        return nbands

    def get_number_of_spins(self):
        return 2 // self.parameters.gspin

    def get_effective_potential(self):
        return self.hamiltonian.V_total

    def get_eigenvalues(self, kpt=None, atoms=None):
        if self.states is None:
            self.calculate_states()
        E = self.states.E_kn
        if kpt is not None:
            return E[kpt]
        return E

    def get_group_velocity(self, kpt=None, atoms=None):
        vg = self.get_property('vg', atoms=atoms)
        if kpt is not None:
            return vg[kpt]
        return vg

    def get_fermi_level(self, atoms=None):
        return self.get_property('fermi', atoms=atoms)

    def get_occupation_numbers(self, atoms=None):
        return self.get_property('occupation', atoms=atoms)

    def get_pseudo_density(self, atoms=None):
        return self.get_property('density', atoms=atoms)

    def get_pseudo_wavefunction(self, kpt=0, band=0, **kwargs):
        '''Convenience function to access wfs functions.'''
        return self.states.psi(kpt, band, **kwargs)

    def get_xc_functional(self):
        return NotImplemented('EPP don\'t include an exchange functional.')

    @logger.block('calculating eigenstates')
    def calculate_states(self):
        k_ka = self.get_ibz_k_points()
        weight_k = self.parameters.kweights / np.sum(self.parameters.kweights)
        nspin = 2 // self.parameters.gspin

        if np.any(k_ka[:, ~np.array(self.atoms.pbc)] != 0):
            raise ValueError('A non-zero wavevector has been specified in a '
                             'non-periodic direction.')
        nbands = self.get_number_of_bands()

        self.states = states.create(self.basis, k_ka, nbands, nspin=nspin, weight_k=weight_k)
        self.states.solve(self.hamiltonian, tol=self.parameters.Etol)

    def _calculate_group_velocity(self):
        logger.begin('Calculating group velocities')

        v_knv = self.states.expectation(self.hamiltonian.dHdk)
        self.results['vg'] = v_knv

        logger.end('Calculating group velocities')

    def _calculate_fermi(self):
        smear = self.parameters.smear

        E_kn = self.states.E_kn

        if self.pseudopotentials.Nv == 0:
            self.results['fermi'] = E_kn.min()
            self.results['occupation'] = np.zeros_like(E_kn)
            return

        nbands = self.get_number_of_bands()
        if self.pseudopotentials.Nv > nbands * self.parameters.gspin:
            raise ValueError('Too few bands for the number of electrons')

        E_kn = self.states.E_kn
        weight_kn = self.states.weight_k[:, None]
        gspin = self.states.gspin

        logger.begin('Calculating occupation')

        def n(mu):
            nel = gspin * np.sum(weight_kn * fermi_dirac(E_kn, mu, smear))
            return nel - self.pseudopotentials.Nv

        fermi = optimize.brentq(n, E_kn.min(), E_kn.max())
        occupation_kn = weight_kn * fermi_dirac(E_kn, fermi, smear)
        self.results['fermi'] = fermi
        self.results['occupation'] = occupation_kn
        logger.end('Calculating occupation')

    def _calculate_density(self):
        occupation_kn = self.get_property('occupation')

        logger.begin('Calculating density')
        rho = 0.
        for (ik, occupation_n) in enumerate(occupation_kn):
            for (n, occupation) in enumerate(occupation_n):
                psi = self.states.psi(ik, n, real=True)
                rho += occupation * np.abs(psi)**2
        self.results['density'] = rho
        logger.end('Calculating density')
