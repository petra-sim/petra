import os

from . import _version
__version__ = _version.get_versions()['version']

nthreads = None


def set_threads(num=None):
    """Sets the number of threads that are used by the numerical libraries
    """
    global nthreads
    if num is None:
        try:
            nthreads = int(os.environ["PETRA_NUM_THREADS"])
        except KeyError:
            from petra.util.threading import optimal_threads
            nthreads = optimal_threads()
    os.environ["MKL_NUM_THREADS"] = str(nthreads)
    os.environ["NUMEXPR_NUM_THREADS"] = str(nthreads)
    os.environ["OMP_NUM_THREADS"] = str(nthreads)


try:
    set_threads()
except ModuleNotFoundError:
    pass
