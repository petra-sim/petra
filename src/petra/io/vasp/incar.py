from itertools import chain
from collections.abc import MutableMapping

from petra.io.vasp.util import CommonFile, parse_variable


class INCAR(MutableMapping, CommonFile):

    def __init__(self, path='', read=True):
        self._data = {}
        CommonFile.__init__(self, path, read)

    def _read(self, f):
        self._data = {}

        for line in chain.from_iterable(l.split(';') for l in f):
            line = line.strip()
            if line.startswith('#'):
                continue

            var, value = parse_variable(line)
            if var is not None:
                self._data[var] = value

    def _write(self, f):
        for var, value in self._data.items():
            svar = var.upper()
            svalue = str(value)
            print(f'{svar} = {svalue}', file=f)

    def __len__(self):
        return len(self._data)

    def __iter__(self):
        return iter(self._data)

    def __delitem__(self, key):
        if key in self._data:
            del self._data[key]

    def __setitem__(self, key, value):
        if value is None:
            self.__delitem__(key)
        else:
            self._data[key] = value

    def __getitem__(self, key):
        return self._data.get(key, None)

    def __setattr__(self, name, value):
        if name in ('_data', 'path'):
            super().__setattr__(name, value)
        else:
            self[name] = value

    def __getattr__(self, name):
        if name in ('_data', 'path', 'fpath'):
            return object.__getattr__(self, name)
        return self[name]

    # convenience functions

    def relaxation(self, positions=True, cell_shape=True, volume=True,
                   method='rmm-diis'):

        isif_lookup = {
            # (pos, cell, volume)
            (True, False, False): 2,
            (True, True, True): 3,
            (True, True, False): 4,
            (False, True, False): 5,
            (False, True, True): 6,
            (False, False, True): 7,
        }

        ibrion_lookup = {
            'rmm-diis': 1,
            'cg': 1,
            'md': 1
        }

        self.ediff = self.ediff or 1e-6
        self.ediffg = self.ediffg or 1e-5
        self.nsw = self.nsw or 30
        self.ismear = self.ismear or -5

        self.istart = 0
        try:
            self.ibrion = ibrion_lookup[method]
        except KeyError:
            raise ValueError('method should be "rmm-diis", "cg" or "md".')
        try:
            self.isif = isif_lookup[(positions, cell_shape, volume)]
        except KeyError:
            raise ValueError('combination of constraints is not supported.')
