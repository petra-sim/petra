import numpy as np


def test_matel_kernels():
    shape = (2, 10, 6, 7, 8)
    bra_inxyz = np.random.random(shape) + 1j * np.random.random(shape)
    ket_inxyz = np.random.random(shape) + 1j * np.random.random(shape)

    from petra.transport.block import _matel_fast_iinn, _matel_lowmem_iinn

    assert(np.allclose(_matel_fast_iinn(bra_inxyz, ket_inxyz, 1.),
                       _matel_lowmem_iinn(bra_inxyz, ket_inxyz, 1.), atol=1e-9))
