import numpy as np
import numpy.linalg as la

from ase import Atoms
from petra.constants import Ang


class PeriodicCell:
    """A periodic cell, defined by lattice vectors a0, a1, a2

    Attributes
    ----------
    a : np.ndarray
        lattice vectors [a0, a1, a2]
    areas : np.ndarray
        area spanned by lattice vectors [1-2, 0-2, 0-1]
    volume : float
        volume of unit cell
    """

    def __init__(self, a, reciprocal=None):
        if isinstance(a, Atoms):
            a = a.cell * Ang
        self.a = a

        self.areas = [np.sqrt(la.det(a[span] @ a[span].T))
                      for span in ([1, 2], [0, 2], [0, 1])]
        self.volume = la.det(a)
        if self.volume < 0:
            raise ValueError('The volume is negative, '
                             'please interchange lattice vectors.')
        self.a_inv = la.inv(self.a)
        if reciprocal is None:
            reciprocal = PeriodicCell(2 * np.pi * self.a_inv.T,
                                      reciprocal=self)
        self.reciprocal = reciprocal
        self.a_len = la.norm(self.a, axis=1)

    def direct_to_cartesian(self, vector):
        return np.array(vector) @ self.a
    to_cartesian = direct_to_cartesian

    def cartesian_to_direct(self, vector):
        return np.array(vector) @ self.a_inv
    to_direct = cartesian_to_direct

    def is_orthorhombic(self):
        return (np.isclose(self.a[0] @ self.a[1], 0) and
                np.isclose(self.a[1] @ self.a[2], 0) and
                np.isclose(self.a[2] @ self.a[0], 0))

    def grid(self, shape, direct=False):
        if direct:
            return np.moveaxis(np.indices(shape), 0, -1) / np.asarray(shape)
        return self.direct_to_cartesian(self.grid(shape, direct=True))

    def scaled(self, scale):
        return PeriodicCell(self.a * scale[:, None])


class Grid:
    def __init__(self, cell, n=None, h=None):
        assert((n is None) != (h is None))

        if not isinstance(cell, PeriodicCell):
            cell = PeriodicCell(cell)
        self.cell = cell

        if n is not None:
            self.shape = tuple(n * np.ones(3, dtype=int))
        if h is not None:
            self.shape = tuple(np.ceil(cell.a_len / h).astype(int))
        self.size = np.prod(self.shape)

        self.r_xyza = np.moveaxis(np.indices(self.shape), 0, -1) / np.asarray(self.shape)
        self.r_xyzv = self.cell.direct_to_cartesian(self.r_xyza)

    @classmethod
    def from_pw(cls, pw):
        return cls(pw.cell, n=pw.nG)

    def to_pw(self):
        return PlaneWaves.from_grid(self)

    def to_grid(self):
        return self

    def __eq__(self, other):
        return (self.cell == other.cell) and (self.shape == other.shape)


class PlaneWaves:
    '''Implements transformation and indexing operations in a plane wave basis.

    Parameters
    ----------
    antialias : int, optional
        0: no antialiassing
        n: anti-aliassing with n times G-cutoff (default: n=1)
    closest_fft : bool, optional
        True: use the closest larger optimal FFT grid
        False: do not change the grid (default)
    order : str, optional
        Ordering of the plane-waves in the cutoff:
        'C': C style (x, y, z). (default)
        'F': Fortran style (z, y, x)
        Use this to interoperate with FORTRAN codes such as VASP,
        otherwise it should be of little concern to the end user.
    '''
    def __init__(self, cell, nG, order='C'):

        if not isinstance(cell, PeriodicCell):
            cell = PeriodicCell(cell)
        self.cell = cell
        self.order = order

        self.nG = np.asarray(nG)
        i_xyza = np.moveaxis(np.indices(nG), 0, -1)

        self.G_xyza = np.mod(i_xyza + self.nG // 2, self.nG) - self.nG // 2
        self.G_xyzv = self.cell.reciprocal.direct_to_cartesian(self.G_xyza)

        self.shape = tuple(self.nG)
        self.size = np.prod(self.shape)

    @classmethod
    def from_grid(cls, grid):
        return cls(grid.cell, nG=grid.shape)

    def to_grid(self):
        return Grid.from_pw(self)

    def to_pw(self):
        return self

    def __eq__(self, other):
        return all([
            (self.cell == other.cell),
            (self.shape == other.shape),
            (self.order == other.order),
        ])

    def is_compatible(self, other):
        return all([
            (self.cell == other.cell),
            (self.shape == other.shape),
        ])

    def similar(self, nG=None, order=None):
        return PlaneWaves(
            self.cell,
            self.nG if nG is None else nG,
            self.order if order is None else order,
        )

    def on_bragg_plane(self, k_a, tol=1e-6):
        if np.abs(np.sum(k_a**2)) < tol:
            return True
        G_gv = self.G_gv[np.any(self.G != 0., axis=0)]
        k_v = self.cell.reciprocal.to_cartesian(k_a)
        distance = 2 * (G_gv @ k_v) - np.sum(G_gv**2, axis=1)
        return np.any(np.abs(distance) <= tol)
