# PETRA

[![Status](https://gitlab.com/petra-sim/petra/badges/master/pipeline.svg)](https://gitlab.com/petra-sim/petra/-/commits/master)
[![Test coverage](https://gitlab.com/petra-sim/petra/badges/master/coverage.svg)](https://petra-sim.gitlab.io/petra/coverage/nosetests)
[![Type coverage](https://gitlab.com/petra-sim/petra/badges/master/coverage.svg)](https://petra-sim.gitlab.io/petra/coverage/mypy)

The PETRA simulation package features advanced solvers for the study of
electronic transport through nanostructures and low dimensional materials
at the atomistic scale.

The code is hosted at <https://gitlab.com/petra-sim/petra> and documentation can be found at
<https://petra-sim.gitlab.io/petra>.
