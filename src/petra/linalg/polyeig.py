""" Linearization and solution of polynomial eigenvalues
Includes both symmetric and companion linearizations.
The latter is more commonly used but this implementaiton defaults to the more
stable symmetric linearization. [1]_

References
----------
.. [1] Symmetric Linearizations for Matrix Polynomials
   N.J. Higham, D.S. Mackey, N. Mackey, and F. Tisseur
   SIAM Journal on Matrix Analysis and Applications 2007 29:1, 143-159
"""
import numpy as np
import scipy.linalg as la


def linearize_companion(Apoly):
    N = Apoly[0].shape[0]

    def block(i, j):
        return (slice(N * i, (N * (i + 1)) or None),
                slice(N * j, (N * (j + 1)) or None))

    exponents = list(Apoly)
    m = max(exponents) - min(exponents)
    A = np.zeros((N * m, N * m), dtype=complex)
    B = np.zeros((N * m, N * m), dtype=complex)
    eye = np.eye(N, dtype=complex)

    for exponent in range(min(exponents), max(exponents) - 1):
        A[block(exponent, exponent + 1)] = eye  # I a^i+1 v
        B[block(exponent, exponent)] = eye  # I a a^i v

    for exponent in Apoly:
        a = Apoly[exponent]
        if exponent != max(exponents):
            A[block(max(exponents) - 1, exponent)] = a
        else:
            B[block(max(exponents) - 1, exponent - 1)] = -a

    return A, B, slice(0, N)


def linearize_symmetric(Apoly):
    N = Apoly[0].shape[0]

    zero = np.zeros((N, N), dtype=complex)

    exponents = sorted(list(Apoly))
    mats = [Apoly[exp] for exp in exponents]
    M = len(mats) - 1

    A = np.block([[mats[i + j - M + 1] if i + j - M + 1 >= 0 else zero
                  for j in range(M)]
                 for i in range(M)])
    B = np.block([[mats[i + j - M + 2]
                   if i + j - M + 2 >= 0 and i < M - 1 and j < M - 1 else zero
                  for j in range(M)]
                 for i in range(M)])
    B[-N:, -N:] = -mats[-1]
    return A, B, slice(0, N)


linearize = linearize_symmetric


def polyeig_accurate(Apoly, linearization_method=None, full=False):
    if linearization_method is None:
        linearization_method = linearize
    A, B, select = linearization_method(Apoly)
    eigvals, eigvects = la.eig(A, B, overwrite_a=True, overwrite_b=True)
    if full:
        return eigvals, eigvects, select
    return eigvals, eigvects[select, :]


def polyeig_fast(Apoly, linearization_method=None, k=10, full=False):
    if linearization_method is None:
        linearization_method = linearize
    A, B, select = linearization_method(Apoly)
    Z = la.solve(A, B)
    eigvals, eigvects = la.eig(Z)
    if full:
        return eigvals, eigvects, select
    return 1 / eigvals, eigvects[select, :]


polyeig = polyeig_accurate
