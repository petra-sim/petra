import numpy as np


def barrier(x, L, V0):
    V = np.zeros(x.shape)
    V[np.abs(x) < L / 2] = V0
    return V


def pn_junction(x, L, V0):
    V1 = V0 / (2 * (L / 2)**2)
    V = np.zeros(x.shape)
    V += (x + L / 2)**2 * (-L / 2 < x) * (x <= 0) * V1
    V += - (x - L / 2)**2 * (0 < x) * (x <= L / 2) * V1
    V += (0 < x) * V0
    return V


def smooth_barrier(x, L, V0, dx):
    return pn_junction(x + L, dx, V0) + pn_junction(x - L, dx, -V0)
