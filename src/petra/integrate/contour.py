from collections import namedtuple

import numpy as np
from scipy.special import roots_legendre

Path = namedtuple("Path", ['path', 'dpath'])


def _points(n):
    return roots_legendre(n, mu=False)


def ellipse(Emin, Emax, stretch=1.):
    r = (Emax + Emin) / 2
    x = (Emax - Emin) / 2
    factor = np.pi / 2

    def E(z):
        return r + x * (np.sin(factor * z) +
                        1j * stretch * np.cos(factor * z))

    def dEdz(z):
        return x * factor * (np.cos(factor * z) -
                             1j * stretch * np.sin(factor * z))

    return Path(E, dEdz)


def positive_circle(Emin, Emax):
    return ellipse(Emin, Emax, stretch=1.)


def negative_circle(Emin, Emax):
    return ellipse(Emin, Emax, stretch=-1.)


def straight(Emin, Emax):
    r = (Emax + Emin) / 2
    x = (Emax - Emin) / 2

    def E(z):
        return r + x * z

    def dEdz(z):
        return x

    return Path(E, dEdz)


line = straight


def integral(path, n):
    zi, wi = _points(n)
    Ei = path.path(zi)
    wi = path.dpath(zi) * wi
    return Ei, wi
