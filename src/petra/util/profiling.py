# coding: utf-8
import time


class tictoc(object):
    def __init__(self, m):
        self.m = m

    def __enter__(self):
        self.tic = time.time()

    def __exit__(self, type, value, traceback):
        print(u"⏱ {}: {:.2f} sec".format(self.m, time.time() - self.tic))


class flamegraph:
    def __init__(self, fname):
        self.fname = fname

    def __enter__(self):
        import flamegraph as fg
        self.fd = open(self.fname, "w")
        self.flame = fg.start_profile_thread(fd=self.fd)

    def __exit__(self, type, value, traceback):
        self.flame.stop()
        self.fd.close()
