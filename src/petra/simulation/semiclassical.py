from typing import NamedTuple
import time

import numpy as np
from scipy import sparse

from petra import constants as c
from petra.device import Device
from petra.distributions import dfermi_dirac, fermi_dirac
from petra.linalg import rms
from petra.linalg.diis import IterativeSubspace
from petra.poisson import Poisson
from petra.transport.element import Element
from petra.util import human_timedelta


class DriftDiffusionSolution(NamedTuple):
    converged: bool
    device: Device
    n: np.ndarray
    p: np.ndarray
    mu_n: np.ndarray
    mu_p: np.ndarray
    rho: np.ndarray
    V: np.ndarray

    def write_vtk(self, fname):
        self.device.write_vtk(fname,
                              n=self.n * c.cm3, p=self.p * c.cm3,
                              mu_n=self.mu_n / c.eV, mu_p=self.mu_p / c.eV,
                              rho=self.rho * c.cm3,
                              V=self.V / c.eV)


class SemiClassicalDensity:
    def __init__(self, structure, T, nk):
        self.structure = structure
        self.T = T

        assert(all(structure.elements[0].block is el.block for el in structure.elements))

        # TODO: Update so elements with different Hamiltonians are allowed
        ref_el = Element(structure.elements[0].block, contacted=1)
        dV = ref_el.dV
        self.bs = ref_el.bandstructure()

        k_ka = np.zeros((nk, 3))
        k_ka[:, ref_el.direction] = np.linspace(0., 1., nk, endpoint=False)

        self.Efermi = self.bs.estimate_fermi()

        Ecb = self.bs.find_cb()
        Evb = self.bs.find_vb()

        psi2p_nr, Ep_n = [], []
        psi2n_nr, En_n = [], []
        for ik, k_a in enumerate(k_ka):
            E_j, c_nj = self.bs.solve_k(k_a)

            # holes
            p_mask = (Evb - 20 * T < E_j) & (E_j <= self.Efermi)
            Ep_n.extend(E_j[p_mask])
            c_inj = self.bs.coef_per_node(k_a, c_nj[:, p_mask])
            for ij, c_ni in enumerate(c_inj.T):
                psi2_r = ref_el.psi2_r(c_ni.T)
                psi2p_nr.append(ref_el.g_spin * psi2_r / (psi2_r.sum() * dV * nk))

            # electrons
            n_mask = (self.Efermi < E_j) & (E_j < Ecb + 20 * T)
            En_n.extend(E_j[n_mask])
            c_inj = self.bs.coef_per_node(k_a, c_nj[:, n_mask])
            for ij, c_ni in enumerate(c_inj.T):
                psi2_r = ref_el.psi2_r(c_ni.T)
                psi2n_nr.append(ref_el.g_spin * psi2_r / (psi2_r.sum() * dV * nk))

        self.Ep_n = np.asarray(Ep_n)
        self.psi2p_nr = np.asarray(psi2p_nr)
        self.En_n = np.asarray(En_n)
        self.psi2n_nr = np.asarray(psi2n_nr)

    def n(self, V_r, mu_r):
        n_r = np.zeros(self.structure.ndensity)

        for element in self.structure.elements:
            points = self.structure.points_of_element[element]
            fn_nr = fermi_dirac(self.En_n[:, None] + V_r[points], mu_r[points], self.T)
            n_r[points] = np.sum(fn_nr * self.psi2n_nr, axis=0)

        return n_r

    def p(self, V_r, mu_r):
        p_r = np.zeros(self.structure.ndensity)

        for element in self.structure.elements:
            points = self.structure.points_of_element[element]
            fp_nr = 1. - fermi_dirac(self.Ep_n[:, None] + V_r[points], mu_r[points], self.T)
            p_r[points] = np.sum(fp_nr * self.psi2p_nr, axis=0)

        return p_r

    def dn(self, V_r, mu_r):
        dn_r = np.zeros(self.structure.ndensity)

        for element in self.structure.elements:
            points = self.structure.points_of_element[element]
            dfn_nr = dfermi_dirac(self.En_n[:, None] + V_r[points], mu_r[points], self.T)
            dn_r[points] = np.sum(dfn_nr * self.psi2n_nr, axis=0)

        return dn_r

    def dp(self, V_r, mu_r):
        dp_r = np.zeros(self.structure.ndensity)

        for element in self.structure.elements:
            points = self.structure.points_of_element[element]
            dfp_nr = - dfermi_dirac(self.Ep_n[:, None] + V_r[points], mu_r[points], self.T)
            dp_r[points] = np.sum(dfp_nr * self.psi2p_nr, axis=0)

        return dp_r

    def mu0(self, doping_r):
        contact = self.structure.elements.contacts[0]
        points = self.structure.points_of_element[contact]
        n_free_r = doping_r[points]
        n_free = np.sum(n_free_r) * contact.dV
        return self.bs.estimate_fermi(n_free=n_free)


class DriftDiffusionProblem:
    def __init__(self, device: Device, T=300 * c.K, nk=20):

        self.device = device

        self.density = SemiClassicalDensity(self.device.structure, T, nk)

        self.doping = self.device.doping()
        self.eps_rV = self.device.dielectric()

        self.poisson = Poisson(self.device.density_mesh.mesh)

        for where, value in self.device.gates.values():
            self.poisson.set_bc(where, value)
        self.poisson.set_eps(self.eps_rV)

    def mu(self, n_r, atol, mu_0=None):

        # the equation to be solved is of Poisson-type
        poisson = Poisson(self.device.density_mesh.mesh)

        for contact, mu in zip(self.device.structure.elements.contacts,
                               self.device.structure.occupation.mu_c):
            structure_points = self.device.density_mesh.points_of_structure[self.device.structure]
            where = np.r_[structure_points][self.device.structure.points_of_element[contact]]
            poisson.set_bc(where, mu)

        n_rV = np.zeros((self.device.density_mesh.size, 6))
        n_rV[:, (0, 1, 2)] = n_r[:, None]
        poisson.set_eps(n_rV)

        rhs_r = np.zeros(self.device.density_mesh.size)

        return poisson.solve(rhs_r, tol=atol, V0=mu_0, multigrid=True)

    def _structure_to_device(self, func, V_r, mu_r):
        values_r = np.zeros(self.device.density_mesh.size)
        points = self.device.density_mesh.points_of_structure[self.density.structure]
        values_r[points] = func(V_r[points], mu_r[points])
        return values_r

    def n(self, V_r, mu_r):
        return self._structure_to_device(self.density.n, V_r, mu_r)

    def p(self, V_r, mu_r):
        return self._structure_to_device(self.density.p, V_r, mu_r)

    def dn(self, V_r, mu_r):
        return self._structure_to_device(self.density.dn, V_r, mu_r)

    def dp(self, V_r, mu_r):
        return self._structure_to_device(self.density.dp, V_r, mu_r)

    def V0(self):
        points = self.device.density_mesh.points_of_structure[self.density.structure]
        mu0 = self.density.mu0(self.doping[points])
        return np.full(self.device.density_mesh.size, -mu0)

    def solve(self, Nit=20, atol=1e-5 * c.eV, rho_bg=1e5 / c.cm3, diis_range=5):

        self.rho_r = np.zeros(self.device.density_mesh.size)

        n_mu_r = np.full(self.device.density_mesh.size, self.device.structure.occupation.mu_c[0])
        p_mu_r = np.full(self.device.density_mesh.size, self.device.structure.occupation.mu_c[0])

        V_r = n_mu_r + self.V0()

        print('--- DRIFT-DIFFUSION SELF-CONSISTENCY ---')
        print('{:^6s}  {:>5s}  {:^10s}  {:^10s}  {:^10s}  {:^10s}  {:^10s}'
              .format('step', 'iter', 'Δ rho', 'Δ V', 'Δ mu_n', 'Δ mu_p', 'elapsed'),
              flush=True)

        V_r, n_r, p_r, rho_converged = self._self_consistent(V_r, n_mu_r, p_mu_r)

        diis_n = IterativeSubspace(diis_range)
        diis_p = IterativeSubspace(diis_range)

        n_converged = p_converged = False
        for i in range(Nit):
            tstart = time.time()

            n1_r = np.clip(n_r, rho_bg, None)
            p1_r = np.clip(p_r, rho_bg, None)

            n_mu_new_r = self.mu(n1_r, atol / 10, n_mu_r)
            p_mu_new_r = self.mu(p1_r, atol / 10, p_mu_r)

            n_mu_new_r = diis_n.update(n_mu_new_r, n_mu_new_r - n_mu_r)
            p_mu_new_r = diis_p.update(p_mu_new_r, p_mu_new_r - p_mu_r)

            R_n_mu = n_mu_new_r - n_mu_r
            R_p_mu = p_mu_new_r - p_mu_r

            r_n_mu, r_p_mu = rms(R_n_mu), rms(R_p_mu)

            n_converged = r_n_mu < atol
            p_converged = r_p_mu < atol

            print('{:<6s}  {:3d}    {:10s}  {:10s}  {:10.4e}{:1s} {:10.4e}{:1s} {:<10s}'
                  .format('QFERMI', i + 1, '', '',
                          r_n_mu / c.eV, '✓' if n_converged else '',
                          r_p_mu / c.eV, '✓' if p_converged else '',
                          human_timedelta(time.time() - tstart)),
                  flush=True)

            # update the potential to match the bias
            if i == 0:
                V_r += ((n_mu_new_r - n_mu_r) * n1_r + (p_mu_new_r - p_mu_r) * p1_r) / (n1_r + p1_r)

            n_mu_r, p_mu_r = n_mu_new_r, p_mu_new_r

            V_r, n_r, p_r, rho_converged = self._self_consistent(V_r, n_mu_r, p_mu_r)

            if n_converged and p_converged:
                break
        else:
            print('not converged!', flush=True)

        converged = n_converged and p_converged and rho_converged

        return DriftDiffusionSolution(converged, self.device,
                                      n_r, p_r, n_mu_r, p_mu_r, self.rho_r, V_r)

    def _self_consistent(self, V_r, mu_n, mu_p, Nit=20, atol=1e-5 * c.eV):
        converged = False

        for i in range(Nit):
            tstart = time.time()
            n_r = self.n(V_r, mu_n)
            p_r = self.p(V_r, mu_p)
            rho_new_r = self.doping - n_r + p_r

            R_rho = rho_new_r - self.rho_r
            self.rho_r = rho_new_r

            drho = - self.dn(V_r, mu_n) + self.dp(V_r, mu_p)
            J_rho = sparse.diags(drho)

            V_new = self.poisson.solve(self.rho_r, V0=V_r, tol=atol / 10, J_rho=J_rho,
                                       multigrid=True)

            R_V = V_new - V_r
            V_r = V_new
            converged = rms(R_V) < atol

            print('{:<6s}  {:5d}  {:10.4e}  {:10.4e}{:1s} {:^10s}  {:^10s}  {:<10s}'
                  .format('SCLASS', i + 1, rms(R_rho) * c.cm3, rms(R_V) / c.eV,
                          '✓' if converged else '', '', '',
                          human_timedelta(time.time() - tstart)),
                  flush=True)

            if converged:
                break
        else:
            print('not converged!', flush=True)

        return V_r, n_r, p_r, converged
