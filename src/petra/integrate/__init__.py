from collections import namedtuple


class IntegrationWarning(UserWarning):
    pass


IntegrationResult = namedtuple('IntegrationResult',
                               ['value', 'error', 'converged',
                                'weights', 'x', 'f'])
