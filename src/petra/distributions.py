import numpy as np


def sech(x):
    """Hyperbolic secant.
    Vectorized and protected against overflow.
    """
    x = - np.sign(x) * x  # Exploiting symmetry to protect against overflow.
    return 2 * np.exp(x) / (1 + np.exp(2 * x))


def _maxwell_boltzmann(x):
    return np.exp(-x)


def _bose_einstein(x):
    xmax = np.log(np.finfo(np.asarray(x).dtype).max)
    return np.expm1(np.minimum(x, xmax))**-1


def _fermi_dirac(x):
    return (1 - np.tanh(x / 2)) / 2


def _dfermi_dirac(x):
    return - sech(x / 2)**2 / 4


def _intfermi_dirac(x):
    return - (x / 2) + np.log(2 * np.cosh(x / 2))


def _x(E, mu, T):
    with np.errstate(divide='ignore'):
        x = (E - mu) / T
    return x


def maxwell_boltzmann(E, mu, T):
    """Maxwell-Boltzmann distribution.
    Vectorized and protected against over- and underflow.

    Parameters:
        - E: energy
        - mu: chemical potential
        - T: temperature

    Returns:
        Maxwell-Boltzmann distribution
    """

    return _maxwell_boltzmann(_x(E, mu, T))


def bose_einstein(E, mu, T):
    """Bose-Einstein distribution.
    Vectorized and protected against over- and underflow.

    Parameters:
        - E: energy
        - mu: chemical potential
        - T: temperature

    Returns:
        Bose-Einstein distribution
    """

    return _bose_einstein(_x(E, mu, T))


def fermi_dirac(E, mu, T):
    """Fermi-Dirac distribution.
    Vectorized and protected against over- and underflow.

    Parameters:
        - E: energy
        - mu: chemical potential
        - T: temperature

    Returns:
        Fermi-Dirac distribution
    """

    return _fermi_dirac(_x(E, mu, T))


def dfermi_dirac_dE(E, mu, T):
    """Derivative of the Fermi-Dirac distribution to E.
    Vectorized and protected against over- and underflow.

    Parameters:
        - E: energy
        - mu: chemical potential
        - T: temperature

    Returns:
        Derivative of Fermi-Dirac distribution to E
    """
    return _dfermi_dirac(_x(E, mu, T)) / T


dfermi_dirac = dfermi_dirac_dE


def dfermi_dirac_dmu(E, mu, T):
    """Derivative of the Fermi-Dirac distribution to mu.
    Vectorized and protected against over- and underflow.

    Parameters:
        - E: energy
        - mu: chemical potential
        - T: temperature

    Returns:
        Derivative of Fermi-Dirac distribution to mu
    """
    return -_dfermi_dirac(_x(E, mu, T)) / T


def dfermi_dirac_dT(E, mu, T):
    """Derivative of the Fermi-Dirac distribution to T.
    Vectorized and protected against over- and underflow.

    Parameters:
        - E: energy
        - mu: chemical potential
        - T: temperature

    Returns:
        Derivative of Fermi-Dirac distribution to T
    """
    x = _x(E, mu, T)
    return -_dfermi_dirac(x) * x / T


def intfermi_dirac(E, mu, T):
    """Integral of the Fermi-Dirac distribution from mu to infinity.
    Vectorized and protected against over- and underflow.

    Parameters:
        - E: energy
        - mu: chemical potential
        - T: temperature

    Returns:
        Integral of the Fermi-Dirac from mu to infinity
    """
    return _intfermi_dirac(_x(E, mu, T)) * T


def electron_hole(f_s, E_s, E_fermi_s):
    return f_s - 1.0 * (E_s < E_fermi_s)
