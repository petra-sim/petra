from abc import ABC, abstractmethod
from typing import Optional

import numpy as np
from scipy import sparse
import scipy.sparse.linalg as spla
from scipy.fft import fftn, ifftn

from petra.linalg import finite_difference as fdiff
from petra.linalg import orthogonalize_posdef
from petra.linalg import fft

from petra.pwepp.basis import PlaneWaves, Grid


def create(mode, basis, params):
    return {
        'pw': PWHamiltonian,
        'fft': FFTHamiltonian,
        'fd': FDHamiltonian,
    }[mode](basis, **params)


class Hamiltonian(ABC):
    supported_basis: Optional[type] = None

    def __init__(self, basis):
        assert(isinstance(basis, self.supported_basis))
        self.basis = basis

        self.reset()

    def reset(self):
        self.mass = 1.
        self.V_atomic = None
        self.V_external = None
        self.V_hartree = None

    def set_mass(self, mass=1.):
        self.mass = mass

    @abstractmethod
    def set_atomic_potential(self, pseudopotentials):
        pass

    def set_external_potential(self, V_external):
        self.V_external = V_external

    def set_hartree_potential(self, V_hartree):
        self.V_hartree = V_hartree

    @property
    def V_total(self):
        V_total = 0.
        if self.V_atomic is not None:
            V_total += self.V_atomic
        if self.V_external is not None:
            V_total += self.V_external
        if self.V_hartree is not None:
            V_total += self.V_hartree
        return V_total

    def __repr__(self):
        return "{}(basis={})".format(type(self).__name__, repr(self.basis))

    @abstractmethod
    def H(self, k_a):
        pass

    @abstractmethod
    def dHdk(self, k_a):
        pass

    @abstractmethod
    def p(self, k_a):
        pass

    def solve(self, k_a, nbands, tol=1e-6, maxiter=20, method='lanczos'):
        H_oo = self.H(k_a)

        if method in ('lanczos', 'krylov', 'arpack'):
            E_n, psi_on = spla.eigsh(H_oo, nbands, which='SA',
                                     tol=tol, maxiter=maxiter * H_oo.shape[0])

        elif method == 'lobpcg':
            shape = (H_oo.shape[0], nbands)
            x_on = np.random.random(shape) + 1j * np.random.random(shape)
            E_n, psi_on = spla.lobpcg(H_oo, x_on, largest=False,
                                      tol=tol, maxiter=maxiter)

        else:
            raise ValueError('method "{}" unknown'.format(method))

        psi_no = orthogonalize_posdef(psi_on).T
        idx = np.argsort(E_n)
        return E_n[idx], psi_no[idx]


class PWHamiltonian(Hamiltonian):
    supported_basis = PlaneWaves

    def set_atomic_potential(self, pseudopotentials):
        trunc_pw = self.basis.cutoff(n=1)
        self.V_atomic = pseudopotentials.VG_local_xyz(trunc_pw)

    def set_external_potential(self, V_external):
        self.V_external = fftn(V_external)

    def set_hartree_potential(self, V_hartree):
        self.V_hartree = fftn(V_hartree)

    def set_mass(self, mass=1.):
        if not np.isscalar(mass):
            raise NotImplementedError('Spatially varying mass is not supported in this mode.')
        self.mass = mass

    def H(self, k_a):
        pw_cut = self.basis.cutoff(k_a)

        # kinetic energy
        T_g = pw_cut.T_g / self.mass

        # potential energy
        V_xyz = self.V_total

        # build Hamiltonian
        G_ag = pw_cut.G_ga.T
        GG_agg = G_ag[:, :, None] - G_ag[:, None, :]
        H_gg = V_xyz[tuple(GG_agg)]
        np.fill_diagonal(H_gg, H_gg.diagonal() + T_g)

        return H_gg


class FFTHamiltonian(Hamiltonian):
    supported_basis = PlaneWaves

    def __init__(self, basis):
        super().__init__(basis)

        self.ffth = fft.helper(self.basis.nG)

    def set_atomic_potential(self, pseudopotentials):
        trunc_pw = self.basis.cutoff(n=max(self.basis.antialias, 1))
        self.V_atomic = pseudopotentials.Vr_local_xyz(trunc_pw)

    def H(self, k_a):
        pw_cut = self.basis.cutoff(k_a)

        ffth = self.ffth

        # kinetic energy / momentum pre-calculation
        if np.isscalar(self.mass):
            T_g = pw_cut.T_g / self.mass
        else:
            k_v = self.cell.reciprocal.to_cartesian(k_a)
            P_ag = (pw_cut.G_gv + k_v).T

            mass_g = pw_cut.compress(fftn(self.mass))
            Pmass_axyz = ifftn(pw_cut.uncompress(P_ag * mass_g), axes=[1, 2, 3])

        # potential energy
        V_xyz = self.V_total

        def Hpsi(psi):
            # compatibilty with [size, 1] and [size] shapes
            shape = psi.shape
            psi_g = np.squeeze(psi)

            # apply kinetic energy
            Tpsi_g = T_g * psi_g
            if not np.isscalar(self.mass):
                Ppsi_axyz = ifftn(pw_cut.uncompress(P_ag * psi_g), axis=[1, 2, 3])
                Tpsi_g += pw_cut.compress(fftn(np.tensordot(Pmass_axyz, Ppsi_axyz, axes=1)))

            # apply potential energy
            ffth.array = pw_cut.uncompress(psi_g)
            ffth.q_to_r()
            ffth.array[:] *= V_xyz
            ffth.r_to_q()
            Vpsi_g = pw_cut.compress(ffth.array)

            return (Tpsi_g + Vpsi_g).reshape(shape)

        return spla.LinearOperator((pw_cut.size, pw_cut.size), matvec=Hpsi, dtype=complex)

    def p(self, k_a):
        G_gv = self.basis.cutoff(k_a).G_gv
        k_v = self.basis.cell.reciprocal.to_cartesian(k_a)
        return tuple(sparse.diags(diag) for diag in (G_gv + k_v).T)

    def dHdk(self, k_a):
        if not np.isscalar(self.mass):
            raise NotImplementedError()

        return tuple(px / self.mass for px in self.p(k_a))


class FDHamiltonian(Hamiltonian):
    supported_basis = Grid

    def __init__(self, basis, order=3):
        if not basis.cell.is_orthorhombic():
            raise NotImplementedError(
                'The FD mode is not implemented for non-orthorhombic cells')

        super().__init__(basis)
        self.order = order

    def set_atomic_potential(self, pseudopotentials):
        self.V_atomic = pseudopotentials.Vr_local_xyz(self.basis)

    def set_mass(self, mass=1.):
        if not np.isscalar(mass):
            raise NotImplementedError('Spatially varying mass is not supported in this mode.')
        self.mass = mass

    def H(self, k_a):

        # kinetic energy
        dr_a = self.basis.r_xyza[1, 1, 1] - self.basis.r_xyza[0, 0, 0]
        phase_a = np.exp(2j * np.pi * k_a)
        bc_a = ['periodic'] * 3
        # ['periodic' if periodic else 'dirichlet' for periodic in self.atoms.pbc]

        D2_a = [fdiff.D(self.basis.shape[i],
                        derivative=2,
                        order=self.order,
                        bc=bc_a[i],
                        phase=phase_a[i]) / dr_a[i]**2
                for i in range(3)]

        phase_r = self.basis.flatten(np.exp(2j * np.pi * (self.basis.r_xyza @ k_a)))
        phase_rr = sparse.diags(phase_r)

        T_rr = - fdiff.kronsum(D2_a) / (2 * self.mass)
        T_rr = phase_rr.conj() @ T_rr @ phase_rr

        # potential energy
        V_r = self.basis.flatten(self.V_total)
        V_rr = sparse.diags(V_r)

        return (T_rr + V_rr).astype(complex)

    def dHdk(self, k_a):
        raise NotImplementedError()

    def p(self, k_a):
        raise NotImplementedError()
