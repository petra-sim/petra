import pickle


class PicklableWrapper:
    '''Allow pickling with __getattr__ reference to a child object'''
    def __setstate__(self, state):
        self.__dict__ = state

    def __getstate__(self):
        return self.__dict__


class SaveLoad:
    '''Add save and load methods with type checking (uses pickling)'''
    def save(self, fname=None):
        name = type(self).__name__
        if fname is None:
            fname = name.lower() + '.pkl'
        with open(fname, 'wb') as fd:
            pickle.dump(self, fd)
        print("{} saved to {}".format(name, fname))

    @classmethod
    def load(cls, fname=None):
        name = cls.__name__
        if fname is None:
            fname = name.lower() + '.pkl'
        with open(fname, 'rb') as fd:
            instance = pickle.load(fd)
        if isinstance(instance, cls):
            print("{} loaded from {}".format(name, fname))
            return instance
        raise ValueError('The file does not contain a valid {}'.format(name))
