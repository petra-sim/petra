from functools import cached_property

import numpy as np
import scipy.sparse as sparse
import scipy.sparse.linalg as spla

from petra.util.lazy import invalidate_cached_property
from petra.util import log
from petra.util.sparse import wipe_csr_rows
from petra.linalg import tensor
from petra import linalg as pla
from petra import constants as c

verbose = False
logger = log.getLogger(__name__)


class Poisson:
    """The Poisson equation solver.

    Attributes:
        V_bc (array): The potential containing only the boundary condition
                      terms, NaN everywhere else.

    """
    def __init__(self, mesh):
        """Initialize the Poisson equation solver

        Args:
            mesh (:obj:`petra.mesh.Mesh`): The FEM mesh.
        """
        self.mesh = mesh

        self.bc = []
        self.eps_rV = np.zeros((self.mesh.points.shape[0], 6))
        self.eps_rV[:, :3] = 1

    @property
    def bc_idx(self):
        return np.unique([np.r_[0:self.mesh.points.shape[0]][where] for where, _ in self.bc])

    @cached_property
    def M(self):
        logger.begin('building FEM M matrix')
        M = self.mesh.mass_matrix.copy()
        wipe_csr_rows(M, self.bc_idx)
        M.eliminate_zeros()
        logger.end('building FEM M matrix')
        return M

    def eps_cvv(self):
        eps_cV = self.eps_rV[self.mesh.simplices].mean(axis=1)
        return eps_cV[:, tensor.idx]

    @cached_property
    def D(self):
        logger.begin('building FEM D matrix')
        D = self.mesh.laplacian_matrix(self.eps_cvv())

        Ddiag = D.diagonal()
        Ddiag[self.bc_idx] = 1.0

        wipe_csr_rows(D, self.bc_idx)
        D.setdiag(Ddiag)
        D.eliminate_zeros()
        logger.end('building FEM D matrix')
        return D

    def initialize(self):
        self.M
        self.D

    def set_eps(self, eps_rV):
        if np.all(self.eps_rV == eps_rV):
            return
        self.eps_rV = eps_rV
        invalidate_cached_property(self, 'D')

    def set_bc(self, where, value):
        """Set a Dirichlet type boundary condition.

        Args:
            condition: Where to apply the boundary condition.
            value: The value of the boundary condition.

        """

        logger.begin('setting boundary conditions')

        if callable(where):
            where = np.vectorize(where, signature='(m)->()')
            where = self.mesh.evaluate(where)

        if callable(value):
            value = np.vectorize(value, signature='(m)->()')
            value = self.mesh.evaluate(value)

        self.bc.append((where, value))

        invalidate_cached_property(self, 'M', 'D')

        logger.end('setting boundary conditions')

    @property
    def V_bc(self):
        """The potential containing only the boundary condition terms,
           NaN everywhere else.
        """
        return self._apply_bc_rhs(np.zeros(self.D.shape[0]) * np.nan)

    def _apply_bc_rhs(self, vec):
        for idx, value in self.bc:
            vec[idx] = value
        return vec

    def _make_system(self, rho, V0=None, J_rho=None):
        if len(self.bc) == 0:
            raise ValueError("No boundary conditions set")

        A = self.D
        b = self._apply_bc_rhs(self.M @ (rho / c.eps0))
        x0 = self._apply_bc_rhs(np.zeros(self.D.shape[0])
                                if V0 is None else V0.copy())

        if J_rho is not None:
            if J_rho.ndim == 1:
                J_rho = sparse.diags(J_rho)
            J_rho = (self.M @ J_rho) / c.eps0
            A = A - J_rho
            b = b - (J_rho @ x0)

        return A, b, x0

    def solve(self,
              rho,
              V0=None,
              J_rho=None,
              multigrid=True,
              direct=False,
              tol=1e-8,
              maxiter=1000,
              method='gmres',
              preconditioner='none'):
        """Solves the Poisson equation.

        Parameters
        ----------
        rho : numpy.ndarray of double
            The charge density.
        V0 : array, optional
            The initial or previous guess for V.
        J_rho : array, optional
            The jacobian of the density for Newton iteration.
        multigrid : bool, optional
            Use algebraic multigrid if True. (defaults to True)
        direct : bool, optional
            Use a direct LU solver if True. (defaults to False)
        tol : float, optional
            The relative tolerance at which iterative solvers stop.
            (defaults to 1e-6)
        maxiter : float, optional
            Maximum number of iterations for iterative solvers.
            (defaults to 1000)
        method : str, optional
            The method to use when solving iteratively. (defaults to 'gmres')
        preconditioner : str, optional
            An optional preconditioner.

        Returns
        -------
        V : numpy.ndarray of double
            The obtained potential.

        """

        logger.begin('solving poisson')

        A, b, x0 = self._make_system(rho, V0=V0, J_rho=J_rho)

        if direct:
            V = solve_direct(A, b)
        elif multigrid:
            V = solve_amg(A, b, x0, tol=tol, maxiter=maxiter, method=method)
        else:
            V = solve_iterative(A, b, x0, tol=tol, maxiter=maxiter,
                                   method=method,
                                   preconditioner=preconditioner)

        logger.end('solving poisson')
        return V


def solve_direct(A, b):
    """Solves the matrix equation using a direct LU solver.

    This is slower than the iterative solvers and uses massive amounts of
    memory, but it is accurate to machine precision.
    """
    return pla.solve(A, b)


def solve_amg(A, b, x0, tol=1e-6, maxiter=100, method='gmres'):
    """Solves the matrix equation using algebraic multigrid.

    This is the fastest method.
    """

    import pyamg

    res = [] if verbose else None

    ml_solver = pyamg.smoothed_aggregation_solver
    ml = ml_solver(A, max_coarse=40)
    V = ml.solve(b, x0=x0, residuals=res, accel=method,
                 tol=tol, maxiter=maxiter, cycle='F')

    if verbose:
        print_cycle_history(res, ml, verbose=True, plotting=True)

    return V


def solve_iterative(A, b, x0, tol=1e-6, maxiter=1000, method='cg',
                    preconditioner='none'):
    """Solves the matrix equation using scipy's built-in iterative solvers.

    This is a fallback that is not well tested or supported.
    """

    # preconditioner
    M = None
    if preconditioner == 'jacobi':
        M = sparse.diags([A.diagonal()**-1], [0])

    elif preconditioner == 'ilu':
        ilu = spla.spilu(A, fill_factor=5, drop_tol=1e-4)

        M = spla.LinearOperator(shape=A.shape,
                                dtype=float,
                                matvec=ilu.solve)

    # iterative method
    solver = getattr(spla, method)
    V, info = solver(A, b, M=M, x0=x0, tol=tol, maxiter=maxiter)

    if info > 0:
        print('Warning: Solver is not converged')
    elif info < 0:
        raise ValueError('Iterative solver {} has invalid input'
                         ''.format(method))

    return V


def print_cycle_history(resvec, ml, verbose=False, plotting=False):
    """Shows a summary of the complexity, convergence factors, and total work
    along with a verbose mode highlighted each iteration.

    Parameters
    ----------
    resvec : array like
        Vector of residuals from a MG iteration
    ml : multilevel
        Multilevel object from AMG setup
    verbose : {True, False}
        Indicates verbose text output
    plotting : {True, False}
        Plot the residual history

    Notes
    -----
        - Factor refers to the immediate reduction factor
        - A-mean refers to the current or running arithmetic mean
        - G-mean refers to the geometric mean (current or
        - Work is the estimated total work needed to reduce the residual by a
            factor of 10, based on the geometric mean of the convergence
            factors and on the cycle complexity
    """
    import scipy
    import numpy as np

    print(ml)

    resvec = np.array(resvec)

    print('---Convergence Summary---------------------------------------')
    print('')
    avg_convergence_factor = (resvec[-1] / resvec[0])**(1.0 / len(resvec))
    print('             Levels: %d' % len(ml.levels))
    print('   Cycle Complexity: %6.3f' % ml.cycle_complexity())
    print('Operator Complexity: %6.3f' % ml.operator_complexity())
    print('    Grid Complexity: %6.3f' % ml.grid_complexity())
    print('avg geo conv factor: %6.3f' % avg_convergence_factor)
    print('               work: %6.3f' % (-ml.cycle_complexity() /
                                          scipy.log10(avg_convergence_factor)))
    print('')

    total_nnz = sum(level.A.nnz for level in ml.levels)
    print('level   unknowns     nnz')
    for n, level in enumerate(ml.levels):
        A = level.A
        print('   %-2d   %-10d   %-10d [%5.2f%%]' % (n, A.shape[1], A.nnz,
                                                     (100 * float(A.nnz) /
                                                      float(total_nnz))))

    if verbose:
        print('')
        print('---Convergence Summary (verbose)-----------------------------')
        print('%20s' % 'Factors:')
        plist = ('iter', 'Factor', 'A-Mean', 'G-Mean', 'Work')
        print('%-10s %-10s %-10s %-10s %-10s' % plist)

        for i in range(0, len(resvec)):
            if(i > 0):
                iresvec = resvec[:(i + 1)]  # running list of residuals
                ifactors = iresvec[1:] / iresvec[0:-1]
                # running list of factors

                ifactor = ifactors[-1]  # current arith mean
                aafactor = np.mean(ifactors)  # running arith mean
                gafactor = (iresvec[-1] / iresvec[0])**(1.0 / (i + 1))
                # geometric mean

                ocx = ml.cycle_complexity()
                iwork = - ocx / scipy.log10(gafactor)  # current work-per-digit

                plist = (i, ifactor, aafactor, gafactor, iwork)
                print('%-10d %-10.3f %-10.3f %-10.3f %-10.3f' % plist)
            else:
                print('%-10d' % (i))

    if plotting:
        import matplotlib.pyplot as plt
        plt.figure(1)
        plt.semilogy(resvec, 'k')
        plt.xlabel('iteration')
        plt.ylabel('residual')
        plt.show()
