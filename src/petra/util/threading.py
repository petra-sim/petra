import os
import warnings

from petra.util import mpi


def get_local_cpu_count():
    try:
        import psutil
        num = psutil.cpu_count(logical=False)
    except ImportError:
        num = os.cpu_count()
    return num


def optimal_threads():
    num_cores = get_local_cpu_count()
    if not mpi.is_parallel():
        return num_cores
    with mpi.communicator(mpi.node_comm()):
        if num_cores < mpi.comm.size and mpi.is_root():
            warnings.warn(
                'Oversubscribing the CPU cores ({}) of {} with ({}) MPI processes.'
                .format(num_cores, mpi.node_name(), mpi.comm.size))
            return 1
        dist = mpi.Distributor(num_cores)
        return dist.local_size()
