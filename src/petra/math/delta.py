import numpy as np

from scipy.special import factorial, erf
from numpy.polynomial.hermite import hermval

from petra.distributions import _dfermi_dirac


def fermi(x):
    return -_dfermi_dirac(x)


def gauss(x):
    return np.exp(- x**2 / 2) / np.sqrt(2 * np.pi)


def methfessel_paxton(order=0):
    def delta(x):
        n = np.r_[0:order + 1]
        c = np.zeros(2 * order + 1)
        c[::2] = (-1)**n / (np.sqrt(np.pi) * 4**n * factorial(n))
        return hermval(x, c) * np.exp(-x**2)
    return delta


def methfessel_paxton_step(x, order=0):
    if order == 0:
        return (1 - erf(x)) / 2

    n = np.r_[1:order + 1]
    c = np.zeros(2 * order)
    c[1::2] = (-1)**n / (np.sqrt(np.pi) * 4**n * factorial(n))
    return methfessel_paxton_step(x) + hermval(x, c) * np.exp(-x**2)


mp0 = methfessel_paxton(0)
mp1 = methfessel_paxton(1)
mp2 = methfessel_paxton(2)
