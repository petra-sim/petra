from math import factorial
from functools import cached_property

import numpy as np
import numpy.linalg as la
import scipy.sparse as sparse
from scipy.spatial import qhull, cKDTree

from petra import interpolation


class Mesh:
    '''Represents a mesh given by points, simplices and a list of neighbors.

    Mimics some of the capabilities of scipy.spatial.Delaunay
    Has FEM matrix generation built-in.
    '''
    def __init__(self, points, simplices, neighbors):
        self.points = points.astype(np.double)
        self.simplices = simplices.astype(np.intc)
        self.neighbors = neighbors.astype(np.intc)

        self.npoints, self.dim = self.points.shape

    def __eq__(self, other):
        if not np.allclose(self.points, other.points):
            return False
        if not np.all(self.simplices == other.simplices):
            return False
        if not np.all(self.neighbors == other.neighbors):
            return False
        return True

    def evaluate(self, function):
        return function(self.points)

    def integrate(self, data):
        return np.sum(self.mass_matrix @ data)

    @cached_property
    def coefficients(self):
        n_simplices, dim_simplex = self.simplices.shape
        coord = np.ones((n_simplices, dim_simplex, dim_simplex))
        coord[:, :, 1:] = self.points[self.simplices]

        det = la.det(coord)
        coef = la.inv(coord)
        grad = coef[:, 1:, :]

        return det, grad

    def _make_matrix(self, data):
        Nel, dimel = self.simplices.shape
        dimmat = dimel * dimel
        nnz = Nel * dimmat

        rowcol = self.simplices[:, np.mgrid[0:dimel, 0:dimel]]
        rows = rowcol[:, 0, ...].ravel()
        cols = rowcol[:, 1, ...].ravel()

        data.shape = (nnz,)

        return sparse.coo_matrix((data, (rows, cols)),
                                 shape=(self.npoints, self.npoints)).tocsr()

    @cached_property
    def mass_matrix(self):
        dim_simplex = self.simplices.shape[1]
        det, _ = self.coefficients

        M0 = (1. + np.eye(dim_simplex)) / factorial(dim_simplex + 1)
        M = M0[None, :, :] * det[:, None, None]

        return self._make_matrix(M)

    def laplacian_matrix(self, eps_cvv):
        dim_simplex = self.simplices.shape[1]
        det_c, grad_cvi = self.coefficients
        facdim = factorial(dim_simplex - 1)
        grad_civ = np.swapaxes(grad_cvi, 1, 2)
        D = -(grad_civ @ eps_cvv @ grad_cvi) * (det_c[:, None, None] / facdim)

        return self._make_matrix(D)

    @cached_property
    def cell_centers(self):
        cellpoints = self.points[self.simplices]
        return np.sum(cellpoints, axis=1) / self.simplices.shape[1]

    @cached_property
    def transform(self):
        simplices = self.simplices.astype('int32')
        return qhull._get_barycentric_transforms(self.points,
                                                 simplices,
                                                 np.finfo(float).eps)

    @cached_property
    def _simplex_tree(self):
        centers = self.points[self.simplices, :].sum(axis=1) / 4
        return cKDTree(centers)

    @cached_property
    def interpolator(self):
        return interpolation.interpolator_from_mesh(self)

    def interpolate(self, data, r):
        return self.interpolator(r)(data)

    def _find_directed(self, points, guesses):
        eps = np.finfo(float).eps * 10
        idx = np.r_[0:points.shape[0]]
        found = np.empty(points.shape[0], dtype=int)
        for i in range(1 + self.points.shape[0] // 4):
            deltas = points - self.transform[guesses, self.dim]
            bary = np.einsum('njk,nk->nj',
                             self.transform[guesses, :self.dim, :],
                             deltas)
            bary = np.hstack([bary, 1 - bary.sum(axis=1,
                                                 keepdims=True)])
            selfound = np.all(bary > -eps, axis=1)
            selnotfound = np.logical_not(selfound)
            found[idx[selfound]] = guesses[selfound]
            idx = idx[np.logical_not(selfound)]
            guesses = guesses[selnotfound]
            points = points[selnotfound]

            if idx.size == 0:
                break

            def find_dir(b):
                return np.where(b <= -eps)[0][0]
            directions = np.argmin(bary[selnotfound], axis=1)
            guesses = self.neighbors[guesses, directions]

            outside = (guesses == -1)
            inside = np.logical_not(outside)
            found[idx[outside]] = -1
            idx = idx[inside]
            guesses = guesses[inside]
            points = points[inside]
        found[idx] = -2
        return found

    def find_simplex(self, points):
        _, guesses = self._simplex_tree.query(points)
        return self._find_directed(points, guesses)

    def _serial_find_directed(self, point, guess):
        eps = np.finfo(float).eps * 10
        for i in range(1 + self.points.shape[0] // 4):
            delta = point - self.transform[guess, self.dim]
            bary = np.einsum('jk,k->j',
                             self.transform[guess, :self.dim, :], delta)
            bary = np.concatenate([bary, 1 - np.sum(bary, keepdims=True)])
            if np.all(bary > -eps):
                return guess
            direction = np.argmin(bary)
            guess = self.neighbors[guess, direction]
            if guess == -1:
                return guess
        return -2

    def serial_find_simplex(self, points):
        _, guesses = self._simplex_tree.query(points)
        simplices = [self._serial_find_directed(point, guess)
                     for guess, point in zip(guesses, points)]
        return np.array(simplices)
