import numpy as np

from petra import constants as c


def field(F, direction, center=None):
    def V(r_xyzv):
        nonlocal center

        if center is None:
            center = r_xyzv[..., direction].mean()
        return F * (r_xyzv[..., direction] - center)
    return V


def symmetry_breaking(atoms, direction, strength=1e-6 * c.eV):
    def V(r_xyzv):
        L = atoms.cell[direction, direction] * c.Ang
        return strength * np.sin(2 * np.pi * r_xyzv[..., direction] / L)
    return V
