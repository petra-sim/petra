import itertools

import numpy as np
import scipy.spatial
from numpy import linalg as la

import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from matplotlib import colors
from matplotlib import ticker
from mpl_toolkits.axes_grid1 import make_axes_locatable, Size
from mpl_toolkits import mplot3d  # noqa: F401
from matplotlib.ticker import MultipleLocator
from matplotlib.patches import Circle
from matplotlib.collections import PatchCollection

from ase import Atoms
import ase.dft.kpoints
import ase.spacegroup
from ase.data import covalent_radii
from ase.data.colors import jmol_colors

from petra import constants as c
from petra.periodiccell import PeriodicCell
from petra.atoms.tools import find_bond_positions


def log_colors(maximum, levels=100, dynamic_range=6):
    max_exponent = np.ceil(np.log10(maximum))
    levels = 10**(np.linspace(-dynamic_range, 0, 100) + max_exponent)
    ticks = 10**(np.r_[-dynamic_range:1] + max_exponent)
    norm = colors.LogNorm(vmin=levels.min(), vmax=levels.max())
    params = dict(levels=levels, norm=norm, extend='min')
    return params, ticks


def point_to_axis(x, y, dx, color='k'):
    plt.scatter(x, y, marker='o', s=180, edgecolors=color, facecolors='none', linewidths=2)
    plt.annotate("", xy=(x + dx, y), xytext=(x, y), color=color,
                 arrowprops=dict(width=1, headlength=6, headwidth=4, color=color, shrink=0.2))


def latex_kpoints(ticks, labels):
    uticks = []
    ulabels = []
    for tick, label in zip(ticks, labels):
        if label == 'G':
            label = r'\Gamma'
        if uticks and tick == uticks[-1]:
            if label != ulabels[-1]:
                ulabels[-1] = ulabels[-1] + ',' + label
            continue
        uticks.append(tick)
        ulabels.append(label)
    ulabels = [r'$\mathrm{{{}}}$'.format(label) for label in ulabels]
    return uticks, ulabels


def use_latex():
    from shutil import which
    if which('latex') is None:
        print('WARNING: latex not found, will plot without LaTeX.')
        return
    if which('dvipng') is None:
        print('WARNING: dvipng not found, will plot without LaTeX.')
        return
    plt.rc('text', usetex=True)
    plt.rc('font', family='serif')


def label_axes(ax, label, **kwargs):
    props = dict(xy=(0, 1), xycoords='axes fraction', va='top', ha='left',
                 xytext=(3.25, -3.25), textcoords='offset points', zorder=20,
                 bbox=dict(boxstyle="square", pad=0.3, fc="white", ec='k', lw=.5))
    props.update(kwargs)
    ax.label(label, **props)


def plot_cell(f=None, cell=None, atoms=None, direction=None, fig=None,
              integrate=False, offset=None, reciprocal=False, turn=False,
              contourprops=None, scatterprops=None):
    if fig is None:
        fig = plt.gcf()

    if isinstance(f, Atoms):
        atoms = f
        f = None

    if atoms is not None:
        if cell is None:
            cell = PeriodicCell(atoms.cell * c.Ang)

        apos = atoms.get_positions() * c.Ang
        asym = atoms.get_chemical_symbols()
    if cell is None:
        cell = PeriodicCell(np.eye(3) * c.nm)

    if f is not None:
        r = np.moveaxis(cell.grid(f.shape), -1, 0)
    else:
        r = np.moveaxis(cell.grid([1, 1, 1]), -1, 0)
    L = la.norm(cell.a, axis=0)
    unit = 1 / c.nm if reciprocal else c.nm
    r /= unit
    L /= unit
    r0 = r.min((1, 2, 3))

    axes = (0, 1, 2) if direction is None else (direction, )

    cprops = dict(cmap='viridis')
    if contourprops is not None:
        cprops = {**cprops, **contourprops}
    sprops = dict(c='k' if f is None else 'w', edgecolors='none', s=16)
    if scatterprops is not None:
        sprops = {**sprops, **scatterprops}

    if len(axes) == 1:
        gs = gridspec.GridSpec(1, 1)
    elif len(axes) == 2:
        gs = gridspec.GridSpec(1, 2)
    else:
        gs = gridspec.GridSpec(2, 2,
                               width_ratios=[L[1], L[0]],
                               height_ratios=[L[2], L[1]])

    for where, axis in enumerate(axes):
        if len(axes) == 3 and turn:
            where = [0, 2, 3][where]
        ax = plt.subplot(gs[where])

        xax, yax = [a for a in (0, 1, 2) if a != axis]
        if turn:
            xax, yax = yax, xax

        x = r[xax].mean(axis)
        y = r[yax].mean(axis)
        z = r[axis].mean((xax, yax))

        if f is not None:
            if integrate:
                data = np.trapz(f, z, axis=axis)
            else:
                data = f.mean(axis)
            if turn:
                data = data
            cnt = plt.contourf(x, y, data, 100, **cprops)

        if atoms is not None:
            for symbol in set(asym):
                pos = np.array([p for p, s in zip(apos, asym)
                                if s == symbol]) / c.nm
                if offset is not None:
                    pos -= offset[None, :] / c.nm
                plt.scatter(pos[:, xax], pos[:, yax], label=symbol,
                            marker=r'$\mathrm{{{}}}$'.format(symbol), **sprops)
        unit = r'\ \mathrm{(nm' + '^{-1}' * reciprocal + ')}$'
        plt.xlabel('$' + ['x', 'y', 'z'][xax] + unit)
        plt.ylabel('$' + ['x', 'y', 'z'][yax] + unit)
        ax.set_aspect('equal', adjustable='box')
        plt.xlim(r0[xax], r0[xax] + L[xax])
        plt.ylim(r0[yax], r0[yax] + L[yax])

        divider = make_axes_locatable(ax)
        if f is not None:
            cax = divider.append_axes("right", Size.Fixed(0.05),
                                      pad=Size.Fixed(0.05))
            cb = plt.colorbar(cnt, cax=cax)
            tick_locator = ticker.MaxNLocator(nbins=5, min_n_ticks=1)
            cb.locator = tick_locator
            cb.update_ticks()

    gs.tight_layout(fig)


def plot_k(atoms, *ks, show_box=False, show_axes=True, show_b=False,
           direct=False):
    '''Shows k-points in 1st Brillouin zone
    atoms is an ase.Atoms instance
    k-points should be provided in cartesian coordinates, or relative
    coordinates if direct is True
    '''

    unit = 1. / c.nm

    sg = ase.spacegroup.get_spacegroup(atoms)
    cell = PeriodicCell(atoms.cell * c.Ang)

    k_special_dict = ase.dft.kpoints.get_special_points(atoms.cell)
    k_special_ka = np.array(list(k_special_dict.values()))
    k_special_all_ka = (k_special_ka @ sg.rotations).reshape((-1, 3))
    k_special_all_kv = cell.reciprocal.to_cartesian(k_special_all_ka)
    hull = scipy.spatial.ConvexHull(k_special_all_kv)

    max_range = 1.2 * k_special_all_kv.max() / unit

    plt.figure(dpi=150, figsize=(10, 10))
    ax = plt.axes(projection='3d')
    ax.view_init(17, -35)
    for simplex in hull.simplices:
        for pair in itertools.permutations(simplex):
            ax.plot3D(*k_special_all_kv[pair, :].T / unit, 'k-', lw=1)

    for k in ks:
        if direct:
            k = cell.reciprocal.to_cartesian(k)
        ax.plot3D(*k.T / unit, '.', ms=2, lw=1)
        max_range = max(max_range, np.abs(k).max() / unit)

    ax.scatter3D(*k_special_all_kv.T / unit, color='k', depthshade=False)
    for klabel, k_a in k_special_dict.items():
        k_v = cell.reciprocal.to_cartesian(k_a)
        ax.text(*k_v / unit, klabel, ha='left', va='bottom')

    if show_axes:
        origin = np.zeros((3, 3))
        extent = np.eye(3) * max_range
        ax.quiver(*origin, *extent, arrow_length_ratio=.05, color='k')
        ax.text(max_range, 0, 0, 'x', ha='left', va='bottom')
        ax.text(0, max_range, 0, 'y', ha='left', va='bottom')
        ax.text(0, 0, max_range, 'z', ha='left', va='bottom')

    if show_b:
        origin = np.zeros((3, 3))
        extent = cell.reciprocal.a / unit
        ax.quiver(*origin, *extent, arrow_length_ratio=.05, color='k')
        ax.text(max_range, 0, 0, 'x', ha='left', va='bottom')
        ax.text(0, max_range, 0, 'y', ha='left', va='bottom')
        ax.text(0, 0, max_range, 'z', ha='left', va='bottom')
        max_range = max(max_range, np.abs(extent).max())

    ax.set_xlim(-max_range, max_range)
    ax.set_ylim(-max_range, max_range)
    ax.set_zlim(-max_range, max_range)

    if show_box:

        ax.set_xlabel('$k_x$ (nm$^{-1}$)')
        ax.set_ylabel('$k_y$ (nm$^{-1}$)')
        ax.set_zlabel('$k_z$ (nm$^{-1}$)')
        ax.grid(False)

        for axis in (ax.xaxis, ax.yaxis, ax.zaxis):
            axis.pane.set_edgecolor('k')
            axis.pane.set_alpha(1)
            axis.pane.set_facecolor('w')
            axis.pane.set_linewidth(1)

        [t.set_va('center') for t in ax.get_yticklabels()]
        [t.set_ha('left') for t in ax.get_yticklabels()]
        [t.set_va('center') for t in ax.get_xticklabels()]
        [t.set_ha('right') for t in ax.get_xticklabels()]
        [t.set_va('center') for t in ax.get_zticklabels()]
        [t.set_ha('left') for t in ax.get_zticklabels()]

        ax.xaxis.set_major_locator(MultipleLocator(1))
        ax.yaxis.set_major_locator(MultipleLocator(1))
        ax.zaxis.set_major_locator(MultipleLocator(1))

        def add_missing_edges():
            minx, maxx, miny, maxy, minz, maxz = ax.axes.get_w_lims()
            mins = np.array((minx, miny, minz))
            maxs = np.array((maxx, maxy, maxz))
            deltas = (maxs - mins) / 12.
            mins = mins - deltas / 4.
            maxs = maxs + deltas / 4.
            ax.plot3D([mins[0], mins[0]], [mins[1], mins[1]],
                      [mins[2], maxs[2]], 'k', lw=1, zorder=-100)
            ax.plot3D([mins[0], mins[0]], [mins[1], maxs[1]],
                      [mins[2], mins[2]], 'k', lw=1, zorder=-100)
            ax.plot3D([mins[0], mins[0]], [maxs[1], maxs[1]],
                      [mins[2], maxs[2]], 'k', lw=1, zorder=-100)

        add_missing_edges()

    else:
        ax.set_axis_off()


def annotated_imshow(ax, array, cmap='viridis'):
    ax.imshow(array, cmap=cmap, interpolation='none')
    for ii, Ti in enumerate(array):
        for jj, Tj in enumerate(Ti):
            ax.text(jj, ii, '{:.4f}'.format(Tj), va='center', ha='center')
    ax.set_yticks(range(array.shape[0]))
    ax.set_xticks(range(array.shape[1]))
    ax.set_xlim(-.5, array.shape[1] - .5)
    ax.set_ylim(-.5, array.shape[0] - .5)


def plot_atoms(ax, atoms, xaxis, yaxis, constraints=True, bonds=True, cell=True, set_aspect=True):

    # atoms
    circles = [Circle((atom.position[xaxis], atom.position[yaxis]),
                      radius=covalent_radii[atom.number] / 2,
                      color=jmol_colors[atom.number],
                      ec='k', lw=.1)
               for atom in atoms]
    ax.add_collection(PatchCollection(circles, match_original=True))

    # constraints
    if constraints:
        for constraint in atoms.constraints:
            ax.scatter(atoms.positions[constraint.get_indices(), xaxis],
                       atoms.positions[constraint.get_indices(), yaxis],
                       edgecolors='r', marker='s', s=7, facecolors='none', linewidth=.5)

    # bonds
    if bonds:
        for between in itertools.combinations(np.unique(atoms.symbols), 2):
            bonds = find_bond_positions(atoms, between=between)
            ax.plot(bonds[:, :, xaxis].T, bonds[:, :, yaxis].T, 'k-', zorder=-1)

    # cell
    ivertices = np.array([
        [[0, 0], [0, 1], [1, 1], [1, 0], [0, 0], [0, 1], [1, 1], [1, 0],
         [0, 0], [0, 0], [1, 1], [1, 1]],
        [[0, 1], [1, 1], [1, 0], [0, 0], [0, 1], [1, 1], [1, 0], [0, 0],
         [0, 0], [1, 1], [0, 0], [1, 1]],
        [[0, 0], [0, 0], [0, 0], [0, 0], [1, 1], [1, 1], [1, 1], [1, 1],
         [0, 1], [0, 1], [0, 1], [0, 1]],
    ])

    if cell:
        vertices = (ivertices.T @ atoms.cell)

        ax.plot(vertices[..., xaxis], vertices[..., yaxis], c='grey', lw=.5)

    if set_aspect:
        ax.set_aspect('equal', adjustable='datalim', share=True)
