import time

import numpy as np
from scipy import fft

try:
    import mkl_fft
except ImportError:
    mkl_fft = None

try:
    import pyfftw
    import pyfftw.interfaces
    pyfftw.interfaces.cache.enable()
except ImportError:
    pyfftw = None


def helper(dimensions, threads=None, measure=True):
    if measure:
        return best_helper(dimensions, threads)

    if mkl_fft is not None:
        return MKLFFTHelper(dimensions)
    if pyfftw is not None:
        return FFTWHelper(dimensions, threads=threads)
    else:
        return FFTHelper(dimensions)


def best_helper(dimensions, threads):
    options = [FFTHelper(dimensions)]
    if mkl_fft:
        options.append(MKLFFTHelper(dimensions))
    if pyfftw:
        options.append(FFTWHelper(dimensions, threads))

    times = []
    for option in options:
        begin = time.time()
        option.r_to_q()
        option.q_to_r()
        times.append(time.time() - begin)

    return options[np.argmin(times)]


class FFTHelperBase:
    @property
    def array(self):
        return self._array

    @array.setter
    def array(self, value):
        self._array[:] = value


class FFTWHelper(FFTHelperBase):
    def __init__(self, dimensions, threads=None, patience='FFTW_MEASURE'):
        if threads is None:
            import petra
            threads = petra.nthreads

        self._array = pyfftw.empty_aligned(dimensions, dtype=complex)
        self.fwd = pyfftw.FFTW(self._array, self._array,
                               axes=tuple(range(len(dimensions))),
                               flags=(patience,),
                               direction='FFTW_FORWARD',
                               threads=threads)
        self.bwd = pyfftw.FFTW(self._array, self._array,
                               axes=tuple(range(len(dimensions))),
                               flags=(patience,),
                               direction='FFTW_BACKWARD',
                               threads=threads)
        self.norm = np.sqrt(np.prod(dimensions))

    def r_to_q(self):
        self.fwd.execute()
        self._array /= self.norm

    def q_to_r(self):
        self.bwd.execute()
        self._array /= self.norm


class MKLFFTHelper(FFTHelperBase):
    def __init__(self, dimensions):
        self._array = np.empty(dimensions, dtype=complex)
        self.norm = np.sqrt(np.prod(dimensions))

    def r_to_q(self):
        mkl_fft.fftn(self._array, overwrite_x=True)
        self._array /= self.norm

    def q_to_r(self):
        mkl_fft.ifftn(self._array, overwrite_x=True)
        self._array *= self.norm


class FFTHelper(FFTHelperBase):
    def __init__(self, dimensions):
        self._array = np.empty(dimensions, dtype=complex)
        self.norm = np.sqrt(np.prod(dimensions))

    def r_to_q(self):
        self._array = fft.fftn(self._array, norm='ortho')

    def q_to_r(self):
        self._array = fft.ifftn(self._array, norm='ortho')


def spherical_fft(f, dk, n):
    r'''Fourier transform of a spherically symmetric function along its radial coordinate.

    This uses the projection-slice theorem of the Fourier transform for the
    evaluation. This is also known as the FHA cycle for 2D.

    '''
    k = np.r_[:n] * dk

    rmax = np.pi / dk
    r, dr = np.linspace(0, rmax, k.size, endpoint=False, retstep=True)
    dR = 2 * np.pi / dk

    scale = 2 * np.pi * dr * dR  # / (2 * np.pi)**2

    rr = np.sqrt(r[:, None]**2 + r[None, :]**2)
    frr = f(rr)
    fr = (r[None, :] * frr).sum(1)
    fk = fft.idct(fr, type=1)

    return k, scale * fk


def spherical_ifft(f, dr, n):
    r, fr = spherical_fft(f, dr, n)
    return r, fr * (2 * np.pi)**-3


def prime_factors(N, primes):
    n = N
    primes = np.asarray(primes)
    exponents = np.zeros_like(primes)
    for ii, prime in enumerate(primes):
        if n < prime:
            break
        while n % prime == 0:
            n = n // prime
            exponents[ii] += 1
    assert(np.prod(primes**exponents) * n == N)
    return exponents, n


def closest_optimal(N):
    primes = [2, 3, 5, 7, 11, 13]
    exponents, remain = prime_factors(N, primes)
    # optimal size for FFTW does not contain higher prime factors
    # and at most one of 11 or 13
    if remain > 1 or exponents[-1] + exponents[-2] > 1:
        return closest_optimal(N + 1)
    return N
