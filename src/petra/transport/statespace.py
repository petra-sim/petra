from __future__ import annotations
from functools import cached_property
from typing import Collection, Optional, TYPE_CHECKING

import numpy as np

from petra.util import mpi

if TYPE_CHECKING:
    from petra.transport.open_system import ExtendedState
    from petra.transport.structure import Structure


class StateSpace:
    def __init__(self,
                 structure: Structure,
                 weights: Collection[float],
                 states: Collection[ExtendedState],
                 occupations: Optional[Collection[float]] = None):
        self.structure = structure
        self.weights = np.asarray(weights)
        self.states = states
        self.occupations = None if occupations is None else np.asarray(occupations)

    def filter(self, Emin=-np.inf, Emax=np.inf):
        mask = [Emin <= state.E <= Emax for state in self.states]
        return StateSpace(self.structure,
                          self.weights[mask],
                          [state for state in self.states if Emin <= state.E <= Emax],
                          self.occupations[mask])

    def with_occupations(self, occupations):
        return StateSpace(self.structure, self.weights.copy(), self.states, occupations)

    def copy(self):
        return StateSpace(self.structure, self.weights.copy(), self.states, self.occupations.copy())

    @cached_property
    def wave_sI(self):
        return np.asarray([np.sqrt(state.dos) * state.c_I
                           for state in self.states])

    @cached_property
    def dmats(self):
        dmats = []
        idx = self.structure.nodes.idx
        for element in mpi.split(self.structure.elements):
            eidx = idx[[node.parent for node in element.nodes]]
            element_wave_sI = self.wave_sI[:, eidx]
            dmats.append(element.coefs_to_dmat(
                element_wave_sI, self.weights * self.occupations))
        return mpi.join(dmats)

    # Special properties, calculated using dmats for efficiency

    def density(self):
        rho_r = self.structure.dmats_to_density(self.dmats)
        return mpi.allreduce(rho_r)

    def current_density(self):
        J_r = self.structure.dmats_to_current_density(self.dmats)
        return mpi.allreduce(J_r)

    # Normal properties, calculated from the states directly

    def node_density(self):
        return self._observable(self.structure.node_density)

    def density_simple(self):
        return self._observable(self.structure.density)

    def current(self):
        return self._observable(self.structure.current)

    def spectral_density(self, E=None, smearing=None):
        return self._observable_spectral(self.structure.density,
                                         E=E, smearing=smearing)

    def ldos(self, E=None, smearing=None):
        return self._observable_spectral(self.structure.density,
                                         E=E, smearing=smearing,
                                         occupy=False)

    def spectral_node_density(self, E=None, smearing=None):
        return self._observable_spectral(self.structure.node_density,
                                         E=E, smearing=smearing)

    def node_ldos(self, E=None, smearing=None):
        return self._observable_spectral(self.structure.node_density,
                                         E=E, smearing=smearing,
                                         occupy=False)

    def spectral_current(self, E=None, smearing=None):
        return self._observable_spectral(self.structure.current,
                                         E=E, smearing=smearing)

    def spectral_T(self, E=None, smearing=None):
        return self._observable_spectral(self.structure.T,
                                         E=E, smearing=smearing,
                                         occupy=False)

    def _observable(self, operator):
        result = 0.
        for weight, state in zip(self.weights * self.occupations, self.states):
            result += weight * operator(state)
        return mpi.allreduce(result)

    def _observable_spectral(self, operator, E=None, smearing=None, occupy=True):
        if E is not None and smearing is None:
            raise ValueError('Specifying your own energy mesh is only allowed '
                             'with smearing')
        if E is None:
            Es = mpi.concatenate(np.array([state.E for state in self.states]))
            E, idx = np.unique(Es, return_inverse=True)

        # We divide by J * dos = dEdk * dos to allow both E and k integrations.
        state_results = np.array([operator(state) / np.abs(state.J * state.dos)
                                  for state in self.states])
        if occupy:
            view = state_results.T
            view *= self.occupations
        spectral_results = np.zeros(E.shape + state_results.shape[1:])

        if smearing is None:
            idx = (idx, ) + tuple(slice(None) for _ in state_results.shape[1:])
            np.add.at(spectral_results, idx, state_results)
        else:
            for weight, state, result in zip(self.weights, self.states,
                                             state_results):
                # Adaptive smearing width, works for both E and k integrations.
                dE = weight * np.abs(state.J) * state.dos
                smear = smearing((E - state.E) / dE)
                spectral_results += smear[:, None] * result[None, :]

        return E, mpi.allreduce(spectral_results)

    @cached_property
    def E_s(self):
        return np.fromiter((s.E for s in self.states), np.float64)

    @cached_property
    def J_s(self):
        return np.fromiter((s.J for s in self.states), np.float64)

    @cached_property
    def dos_s(self):
        return np.fromiter((s.dos for s in self.states), np.float64)

    @cached_property
    def icontact_s(self):
        return np.fromiter((s.icontact for s in self.states), int)

    @cached_property
    def T_sc(self):
        return np.fromiter((s.T_c for s in self.states), np.float64)
