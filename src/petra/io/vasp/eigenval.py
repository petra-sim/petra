import numpy as np

from petra.io.vasp.constants import eV
from petra.io.vasp.util import CommonFile


class EIGENVAL(CommonFile):
    def _read(self, f):
        for i in range(4):
            f.readline()

        self.name = f.readline().strip()

        self.nel, nk, nband = map(int, f.readline().strip().split())

        self.k_ka = np.zeros((nk, 3))
        self.weight_k = np.zeros(nk)
        self.E = np.zeros((nk, nband))
        self.fill = np.zeros((nk, nband))

        for ik in range(nk):
            for line in f:
                if line.strip():
                    kx, ky, kz, w = map(float, line.strip().split())
                    self.k_ka[ik] = [kx, ky, kz]
                    self.weight_k[ik] = w
                    break
            for line in f:
                if not line.strip():
                    break
                data = line.strip().split()
                if len(data) == 3:
                    iband, E, fill = data
                else:
                    iband, E = data
                    fill = np.nan

                iband = int(iband) - 1
                self.E[ik, iband] = float(E) * eV
                self.fill[ik, iband] = float(fill)

    @property
    def iconduction(self):
        return np.count_nonzero(self.fill[0] >= .5)
