import time
from collections import defaultdict
from typing import NamedTuple

import numpy as np
from scipy import sparse

from petra import constants as c
from petra.device import Device
from petra.linalg import rms
from petra.linalg.diis import IterativeSubspace
from petra.poisson import Poisson
from petra.transport.integration import node_density
from petra.transport.statespace import StateSpace
from petra.util import human_timedelta, log

logger = log.getLogger(__name__)


class Stat(NamedTuple):
    rms: float
    mean: float
    max: float


class SelfConsistentSolution(NamedTuple):
    converged: bool
    device: Device
    statespace: StateSpace
    rho_free: np.ndarray
    rho: np.ndarray
    V: np.ndarray

    def write_vtk(self, fname):
        J = self.device.density_mesh.map_structure_data(
            {self.statespace.structure: self.statespace.current_density()}
        )
        self.device.write_vtk(fname,
                              rho_free=self.rho_free * c.cm3,
                              rho=self.rho * c.cm3,
                              V=self.V / c.eV,
                              J=J / c.A * c.m**2)


class SelfConsistentProblem:

    def __init__(self, device, V0=None, progress=None):

        self.progress = None
        if progress is not None:
            self.progress = open(progress, 'a+')

        self.device = device
        self.structure = self.device.structure

        logger.begin('initializing Poisson solver')
        self.poisson = Poisson(self.device.density_mesh.mesh)

        for where, value in self.device.gates.values():
            self.poisson.set_bc(where, value)

        eps_rV = self.device.dielectric()
        self.poisson.set_eps(eps_rV)
        logger.end('initializing Poisson solver')

        logger.begin('setting initial potential')
        if V0 is not None:
            self.set_V(V0)
        logger.end('setting initial potential')

        self.stats = defaultdict(list)

        self.rho = np.inf
        self.node_rho = np.inf

    def set_V(self, value):
        self.V = value * np.ones(self.device.density_mesh.size)
        for idx, value in self.device.gates.values():
            self.V[idx] = value

        self.device.set_potential(self.V, 'Hartree')
        # update the occupations as the contact potentials might have shifted
        self.device.structure.occupation.update()

    def _statistics(self, residual, name=None):
        absR = np.abs(residual)
        stat = Stat(max=absR.max(), rms=rms(absR), mean=absR.mean())
        if name is not None:
            self.stats[name + '.mean'].append(stat.mean)
            self.stats[name + '.rms'].append(stat.rms)
            self.stats[name + '.max'].append(stat.max)
        return stat

    def solve(self, max_iter=100, atol=1e-6 * c.eV,
              atol_rho=1e16 / c.cm3, atol_V=None, max_E_eval=1000,
              damp_until=0,
              overshoot=1. * c.eV,
              diis_from=0, diis_range=5, diis_type='V',
              poisson_direct=False,
              integration=node_density,
              scattering=None,
              callback=None,
              ):
        self.converged = False

        print('--- QUANTUM SELF-CONSISTENCY ---')
        print('{:^4s}  {:^5s}  {:^10s}  {:^10s}  {:^6s}  {:^4}  {:^7s}  {:^10s}'
              .format('iter', '#eval', 'Δ rho', 'Δ V', 'damped', 'diis', 'clipped', 'elapsed'),
              file=self.progress, flush=True)

        if atol_V is None:
            atol_V = atol / 10

        damp_until = cleanup_size(damp_until)
        max_iter = cleanup_size(max_iter)
        diis_from = cleanup_size(diis_from)

        diis_from = max(diis_from, damp_until)

        if diis_from <= max_iter:
            diis = IterativeSubspace(diis_range)

        res_V_max_last = np.inf
        doping = self.device.doping()

        xx, yy, zz, yz, xz, xy = np.r_[:6]
        for i in range(max_iter):
            tstart = time.time()
            logger.begin('iteration #{}'.format(i + 1))

            iresult, self.ss = integration(self.device.structure,
                                           init_eval=16,
                                           max_eval=max_E_eval,
                                           atol=atol_rho,
                                           return_full=True)
            self.stats['rho.converged'].append(iresult.converged)
            self.stats['rho.evaluations'].append(len(iresult.x))
            self.stats['rho.error'].append(iresult.error)

            if scattering is not None:
                from petra.paulimaster import PauliMasterEquation

                logger.begin('calculating scattering rates')
                M_ss = scattering(self.ss)
                logger.end('calculating scattering rates')

                logger.begin('solving pauli-master equation')
                rho_s = PauliMasterEquation(self.device, self.ss, M_ss).solve()
                contacts = self.device.structure.elements.contacts
                Efermi_c = np.asarray([contact.bandstructure().estimate_fermi()
                                       for contact in contacts])
                g_spin_c = np.asarray([contact.g_spin for contact in contacts])
                valence_s = self.ss.E_s < Efermi_c[self.ss.icontact_s]
                occ_s = g_spin_c[self.ss.icontact_s] * (rho_s - 1.0 * valence_s)
                self.ss = self.ss.with_occupations(occ_s)
                logger.end('solving pauli-master equation')

            node_rho = - self.ss.node_density()

            logger.begin('calculating density on 3D mesh')
            self.rho_free = self.device.density_mesh.map_structure_data(
                {self.ss.structure: -self.ss.density()}
            )
            rho_new = self.rho_free + doping
            logger.end('calculating density on 3D mesh')

            logger.begin('calculating Jacobian drho/dE')
            docc_s = list(map(self.device.structure.occupation.d, self.ss.states))
            drho = self.device.density_mesh.map_structure_data(
                {self.ss.structure: -self.ss.with_occupations(docc_s).density()}
            )
            J_rho = sparse.diags(drho)
            logger.end('calculating Jacobian drho/dE')

            self.stats['rho.range'].append((rho_new.min() * c.cm3, rho_new.max() * c.cm3))

            self._statistics(node_rho - self.node_rho, name='rho_node.residual')

            R_rho = rho_new - self.rho

            self._statistics(R_rho, name='rho.residual.0')

            # DIIS for the density
            if i and i >= diis_from and diis_type == 'rho':
                rho_diis = diis.update(rho_new, R_rho)
                R_rho = rho_diis - self.rho
                self.stats['rho.diis'].append(len(diis))
            else:
                self.stats['rho.diis'].append(False)

            res_rho = self._statistics(R_rho, name='rho.residual.diis')

            # set a new tolerance for the density simulator
            atol_rho = min(res_rho.rms / 10, atol_rho)

            self.rho = rho_new
            self.node_rho = node_rho

            V_newton = self.poisson.solve(self.rho, V0=self.V, tol=atol_V, J_rho=J_rho,
                                          direct=poisson_direct, multigrid=True)

            R_V = V_newton - self.V

            self._statistics(R_V, name='V.residual.0')

            # DIIS for the potential
            if i >= diis_from and diis_type == 'V':
                V_diis = diis.update(V_newton, R_V)
                R_V = V_diis - self.V
                self.stats['V.diis'].append(len(diis))
            else:
                self.stats['V.diis'].append(False)

            res_V = self._statistics(R_V, name='V.residual.diis')

            # damping (half step size)
            if i < damp_until or res_V.rms >= res_V_max_last:
                res_V_max_last = res_V.max
                R_V /= 2
                self.stats['V.damping'].append(True)
            else:
                self.stats['V.damping'].append(False)

            # overshoot protection
            if overshoot is not None:
                R_V_new = sigmoid(R_V, overshoot)
                overshot = np.abs(R_V - R_V_new).max() > atol * 10
                if overshot:
                    diis.reset()
                R_V = R_V_new
                self.stats['V.overshoot'].append(overshot)
            else:
                self.stats['V.overshoot'].append(False)

            res_V = self._statistics(R_V, name='V.residual')

            self.set_V(self.V + R_V)

            self.stats['V.range'].append((self.V.min(), self.V.max()))

            if callback is not None:
                callback(i, self)

            logger.info('residual rho = {:.2e} cm^-3'.format(res_rho.rms * c.cm3))
            logger.info('residual V = {:.2e} eV'.format(res_V.rms / c.eV))
            logger.end('iteration #{}'.format(i + 1))

            self.converged = res_V.rms <= atol
            print('{:4d}  {:5d}  {:10.4e}  {:10.4e}{:1s} {:^6s}  {:>4s}  {:^7s}  {:<10s}'
                  .format(
                      i + 1,
                      len(iresult.x),
                      res_rho.rms * c.cm3 if res_rho.rms > 0 else -np.inf,
                      res_V.rms / c.eV if res_V.rms > 0 else -np.inf,
                      '✓' if self.converged else '',
                      '*' if self.stats['V.damping'][-1] else '',
                      str(self.stats['V.diis'][-1] or self.stats['rho.diis'][-1] or ''),
                      '*' if self.stats['V.overshoot'][-1] else '',
                      human_timedelta(time.time() - tstart)
                  ), file=self.progress, flush=True)

            if self.converged:
                print('converged', file=self.progress, flush=True)
                logger.info('Converged after {} iterations.'.format(i + 1))
                break
        else:
            print('not converged', file=self.progress, flush=True)
            logger.info('NOT converged after {} iterations.'.format(i + 1))

        return SelfConsistentSolution(self.converged, self.device, self.ss,
                                      self.rho_free, self.rho, self.V)

    def plot_convergence(self, save=None, loglog=False, detail=False):
        import matplotlib.pyplot as plt

        fig, lax = plt.subplots(dpi=150, constrained_layout=True)
        rax = lax.twinx()

        lax.set_title("Convergence")
        lax.set_xlabel('iterations')
        lax.set_ylabel(r'residual $V$ (eV)')
        rax.set_ylabel(r'residual $\rho$ (cm$^{-3}$)')

        R_V = np.array(self.stats['V.residual.rms']) / c.eV
        if detail:
            R_V0 = np.array(self.stats['V.residual.0.rms']) / c.eV
            R_Vdiis = np.array(self.stats['V.residual.diis.rms']) / c.eV
        R_rho0 = np.array(self.stats['rho.residual.0.rms']) * c.cm3
        R_rhodiis = np.array(self.stats['rho.residual.diis.rms']) * c.cm3
        i = np.r_[:len(R_V)] + 1

        handles = []
        if loglog:
            handles += lax.loglog(i, R_V, 'kx-', label=r'$V$')
            if detail:
                handles += lax.loglog(i, R_V0, 'kx:', lw=1, label=r'$V$ (Newton)')
                handles += lax.loglog(i, R_Vdiis, 'kx--', lw=1, label=r'$V$ (DIIS)')
            if detail:
                handles += rax.loglog(i, R_rho0, 'rx-', label=r'$\rho$ (Newton)')
                handles += rax.loglog(i, R_rhodiis, 'rx--', label=r'$\rho$ (DIIS)')
            else:
                handles += rax.loglog(i, R_rhodiis, 'rx--', label=r'$\rho$')
        else:
            handles += lax.semilogy(i, R_V, 'kx-', label=r'$V$')
            if detail:
                handles += lax.semilogy(i, R_V0, 'kx:', lw=1, label=r'$V (Newton)$')
                handles += lax.semilogy(i, R_Vdiis, 'kx--', lw=1, label=r'$V (DIIS)$')
            if detail:
                handles += rax.semilogy(i, R_rho0, 'rx-', label=r'$\rho$ (Newton)')
                handles += rax.semilogy(i, R_rhodiis, 'rx--', label=r'$\rho$ (DIIS)')
            else:
                handles += rax.semilogy(i, R_rhodiis, 'rx--', label=r'$\rho$')

        lax.legend(handles=handles)

        if save is not None:
            plt.savefig(save, dpi=300)
            plt.close()


def sigmoid(x, limit):
    return limit * np.tanh(x / limit)


def cleanup_size(num):
    if num < 0:
        return np.inf
    return num
