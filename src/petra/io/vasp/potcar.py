import subprocess
from io import StringIO
from pathlib import Path
from itertools import chain

import numpy as np

from petra.io.vasp.util import read_array, parse_variable
from petra.io.vasp.util import CommonFile


class POTCAR(CommonFile):
    '''VASP Pseudopotential File

    Attributes
    ----------
    valence
        no. valence electrons
    psctr
        the PSCTR generation header variables
    psgmax
        maximal G for local potential
    psmaxn
        maximal G for non local potential
    psrmax
        maximal r for non local contribution
        (note that rmax = psrmax * (npsnl-1) / npsnl)
    psdmax
        maximal r for augmentation charge
        (note that rmax = psdmax * (npsnl-1) / npsnl)
    r
        radial grid
    psp
        local pseudopotential in rec. space
    psp_q
        coordinate for psp
    pspcor
        partial core in rec. space
    psptau
        partial kinetic energy density in rec. space
    psptauval
        kinetic energy density of valence electrons in rec. space
    psprho
        atomic pseudo charge density in rec. space
    lps
        L quantum number for each PP
    nlpro
        unused
    ndep
        number of augmentation channels per ll'
    dion
        non-local strength
    pspnl
        non-local projectors reciprocal space
    pspnl0
        0-index of pspnl
    psprnl
        non-local projectors real space
    psprnl_r
        radial coordinate for psprnl
    qion
        spherical augmentation charge
    qdep
        L-dependent augmentation charges on regular grid
    qpaw
        integrated augmentation charge (wae*wae - wps*wps r^l)
    qtot
        total charge in each channel
    qato
        initial occupancies (in atom)
    rhoae
        frozen core charge rho(r)r^2 on r-grid
    rhops
        frozen pseudo partial core charge rho(r)r^2 on r-grid
    tauae
        kinetic energy density of core
    taups
        kinetic energy density of partial core
    potae
        frozen core AE potential on r-grid for atomic reference configuration (valence only)
    potps
        local PP on r-grid for atomic reference configuration (valence only)
    potpsc
        local PP on r-grid (core only), V_H[\\tilde{n}_Zc]
    wae
        AE valence wavefunction on r-grid
    wps
        pseudo valence wavefunction on r-grid
    '''

    # static numbers defined in 'pseudo.F'
    nekerr = 100
    npspts = 1000
    npsnl = 100
    npsrnl = 100

    def _read(self, f):
        self.comment = f.readline().strip()
        self.valence = float(f.readline().strip())
        self.parse_psctr(f)
        self.parse_local(f)
        self.parse_non_local(f)

    def parse_psctr(self, f):
        line = f.readline().strip()

        assert(line.startswith('p'))
        self.psctr = {}
        for line in chain.from_iterable(l.split(';') for l in f):
            line = line.strip()
            if line.lower().startswith('end'):
                break

            var, value = parse_variable(line)
            if var is not None:
                self.psctr[var] = value

    def parse_local(self, f):
        header = f.readline().strip()

        assert(header.startswith('l'))  # local part
        self.psgmax = float(f.readline().strip())
        self.psp = read_array(f, self.npspts)
        self.psp_q = self.psgmax * np.r_[:self.npspts] / self.npspts
        header = f.readline().strip()

        # gradient info (ignored, in ptscr info)
        if header.startswith('g'):
            f.readline()  # ignore
            header = f.readline().strip()

        # core charge-density (partial)
        if header.startswith('c'):
            self.pspcor = read_array(f, self.npspts)
            header = f.readline().strip()

        # partial kinetic energy density
        if header.startswith('k'):
            self.psptau = read_array(f, self.npspts)
            header = f.readline().strip()

        # kinetic energy density of atom
        if header.startswith('K'):
            self.psptauval = read_array(f, self.npspts)
            header = f.readline().strip()

        assert(header.startswith('a'))  # atomic pseudo charge-density
        self.psprho = read_array(f, self.npspts)

    def parse_non_local(self, f):
        line = f.readline().strip()
        if line.startswith('E'):
            return

        psmaxn, ldum = line.split()
        psmaxn = float(psmaxn)
        ldum = self.parse_bool(ldum)
        self.psmaxn = psmaxn

        if psmaxn > 0:
            self.parse_paw(f)

    def parse_paw(self, f):
        header = f.readline().strip()

        self.lps = []
        self.nlpro = []
        self.psrmax = []
        self.dion = []
        self.pspnl = []
        self.pspnl0 = []
        self.psprnl = []
        self.psprnl_r = []

        channels = 0
        # Non local Parts
        while True:
            if header[0] in ('A', 'D', 'P'):
                break

            if header.startswith('E'):
                return

            lps, nlpro, psrmax = f.readline().split()
            lps, nlpro, psrmax = int(lps), int(nlpro), float(psrmax)
            self.lps.append(lps)
            self.nlpro.append(nlpro)
            self.psrmax.append(psrmax)

            self.dion = read_array(f, (nlpro, nlpro))

            for ii in range(nlpro):
                header = f.readline().strip()
                assert(header.startswith('Reciprocal'))
                pspnl = read_array(f, self.npsnl)
                if lps % 2 == 0:
                    pspnl0 = pspnl[1]
                else:
                    pspnl0 = -pspnl[1]
                self.pspnl.append(pspnl)
                self.pspnl0.append(pspnl0)

                header = f.readline().strip()
                assert(header.startswith('Real'))
                self.psprnl.append(read_array(f, self.npsrnl))
                psprnl_r = np.r_[:self.npsrnl] * psrmax / self.npsrnl
                self.psprnl_r.append(psprnl_r)

                channels += 1

            header = f.readline().strip()

        self.lps = np.array(self.lps)
        self.nlpro = np.array(self.nlpro)
        self.psrmax = np.array(self.psrmax)
        self.dion = np.array(self.dion)
        self.pspnl = np.array(self.pspnl)
        self.pspnl0 = np.array(self.pspnl0)
        self.psprnl = np.array(self.psprnl)
        self.psprnl_r = np.array(self.psprnl_r)

        # Depletion charges
        if header[0] in ('A', 'D'):

            self.qion = np.zeros((channels, channels))
            self.qdep = []
            self.ndep = []

            for l in range(channels):
                for lp in range(l, channels):
                    data = f.readline().strip().split()
                    if header.startswith('D'):
                        idumm1, idumm2, qion, psdmax = data
                        ndep = -1
                    else:  # header.startswith('A')
                        idumm1, idumm2, idumm3, ndep, qion, psdmax = data
                        ndep = int(ndep)
                        assert(idumm1 == self.lps[l])
                        assert(idumm2 == self.lps[lp])
                    self.ndep.append(ndep)
                    self.qion[l, lp] = float(qion)
                    self.qion[lp, l] = float(qion)

                    if ndep != 0:
                        qdep = read_array(f, self.npsrnl)
                        qdep *= np.r_[:self.npsrnl] / self.npsrnl
                        self.qdep.append(qdep)

                    f.readline().strip()

            self.qdep = np.array(self.qdep)

        # PAW data
        if header.startswith('P'):
            nmax, psdmax = f.readline().strip().split()
            nmax, psdmax = int(nmax), float(psdmax)
            self.psdmax = psdmax
            f.readline().strip()  # fread form of data
            f.readline()  # augmantation charges
            self.qpaw = read_array(f, (channels, channels))
            header = f.readline().strip()

            if header.startswith('t'):
                self.qtot = read_array(f, (channels, channels))
                header = f.readline()
            else:
                self.qtot = np.eye(channels)

            self.qato = read_array(f, (channels, channels))
            header = f.readline().strip()

            assert(header.startswith('g'))  # grid
            self.r = read_array(f, nmax)
            header = f.readline().strip()

            assert(header.startswith('a'))  # aepotential
            self.potae = read_array(f, nmax)
            header = f.readline().strip()

            assert(header.startswith('c'))  # core charge-density
            self.rhoae = read_array(f, nmax)
            header = f.readline().strip()

            if header.startswith('k'):  # kinetic energy-density
                self.tauae = read_array(f, nmax)
                header = f.readline().strip()

            if header.startswith('m'):  # m???????
                self.taups = read_array(f, nmax)
                header = f.readline().strip()

            if header.startswith('l'):  # l???????
                self.potpsc = read_array(f, nmax)
                header = f.readline().strip()

            assert(header.startswith('p'))  # pspotential
            self.potps = read_array(f, nmax)
            header = f.readline().strip()

            assert(header.startswith('c'))  # core charge-density (pseudized)
            self.rhops = read_array(f, nmax)
            header = f.readline().strip()

            self.wps, self.wae = [], []
            for i in range(channels):

                assert(header.startswith('p'))  # pseudo wavefunction
                self.wps.append(read_array(f, nmax))
                header = f.readline().strip()

                assert(header.startswith('a'))  # ae wavefunction
                self.wae.append(read_array(f, nmax))
                header = f.readline().strip()

            self.wps = np.array(self.wps)
            self.wae = np.array(self.wae)

            # end of dataset

    @staticmethod
    def parse_bool(value):
        return value.lower().lstrip('.').startswith('t')


class POTCARLib:
    def __init__(self, path):
        self.path = Path(path)

        self.symbols = {path.parent.name: path
                        for path in self.path.glob('*/POTCAR*')}

        if not self.symbols:
            raise ValueError('No POTCARs found in given directory')

    def path_to(self, symbol):
        if symbol not in self.symbols:
            raise ValueError(f'Unknown symbol {symbol}')
        path = self.path / symbol / 'POTCAR'
        if path.exists():
            return path
        return self.path / symbol / 'POTCAR.Z'

    def lookup(self, atoms, specific=None):
        if specific is None:
            specific = {}

        last_symbol = None
        paths = []
        for symbol in atoms.symbols:
            if symbol == last_symbol:
                continue
            last_symbol = symbol
            symbol = specific.get(symbol, symbol)
            paths.append(self.path_to(symbol))
        return paths

    def generate(self, atoms, specific=None, path=''):
        path = Path(path)
        if path.is_dir():
            path = path / 'POTCAR'
        with path.open('w') as f:
            for part in self.lookup(atoms, specific):
                f.write(open_z(part).read())


def lookup(atoms, specific=None):
    import os
    path = os.environ.get('VASP_PP_PATH')
    return POTCARLib(path).lookup(atoms, specific)


def open_z(path):
    if path.suffix == '.Z':
        # ATTTENTION: We open with gunzip since these are actually made by the
        # "compress" utility, not by gzip, and cannot be read by any built-in
        # Python library such as zlib, gzip or zipfile.
        proc = subprocess.run(['gunzip'],
                              stdin=path.open('r'),
                              stdout=subprocess.PIPE)
        return StringIO(proc.stdout.decode())
    return path.open()
