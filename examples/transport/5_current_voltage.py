import os

import numpy as np
import matplotlib.pyplot as plt

from petra import constants as c
from petra.util import log
from petra.transport import Block, SimpleHomoStructure, EquilibriumElectronHoleOccupation, current
from petra.device import Device
from petra import geometry
from petra.simulation import DriftDiffusionProblem, SelfConsistentProblem

base_dir = '5_current_voltage'
os.makedirs(base_dir, exist_ok=True)
log.to_file(base_dir + '/main.log', clear=True)

block = Block.from_calc('7agnr')

L = 35 * c.nm
Lgate = 5 * c.nm
Lsource = (L - Lgate) / 2
Ldrain = Lsource

# a gate oxide thickness based on EOT
EOT = 1. * c.nm
eps_graphene = 6.9 * 2.2 / 3.34
eps_SiO2 = 3.9
eps_ox_in = 6.93  # hBN in-plane
eps_ox_out = 3.76  # hBN out-of-plane
t_oxide = EOT / eps_SiO2 * eps_ox_out

# layer thickness
t = 3.34 * c.Ang  # layer thickness of graphene = interlayer separation of bilayer

structure = SimpleHomoStructure(block, L=L)
occupation = EquilibriumElectronHoleOccupation(structure, mu_c=(0., 0.), T=300 * c.K)
structure.set_occupation(occupation)

channel = geometry.slab(Lsource, L - Ldrain, 'z')
source_drain = geometry.invert(channel)
graphene_ribbon = geometry.inside(structure.atoms, t / 2)

gate = geometry.intersect(channel, geometry.outside(structure.atoms, t / 2 + t_oxide))

doping_source_drain = geometry.intersect(graphene_ribbon, source_drain)
doping_channel = geometry.intersect(graphene_ribbon, channel)

dielectric_graphene = graphene_ribbon
dielectric_oxide = geometry.invert(graphene_ribbon)

device = Device(structure)
device.define_gate(gate, name='gate')
device.define_doping(doping_channel, -1e20 / c.cm3)  # p doping
device.define_doping(doping_source_drain, 1e20 / c.cm3)  # n doping
device.define_dielectric(dielectric_graphene, [1, 1, eps_graphene])
device.define_dielectric(dielectric_oxide, [eps_ox_in, eps_ox_out, eps_ox_in])

# set the workfunction of the metal equal to the bottom of the conduction band of the left contact
work_function = - structure.elements.contacts[0].bandstructure().find_cb()

# Voltage Sweep

Vds = 0.1 * c.V
Vg_sweep = np.linspace(-0.6, 0.2, 9) * c.V

device.structure.occupation.mu_c = (0.0, -Vds)
currents = np.zeros(Vg_sweep.size)
sc = None
for ii, Vg in enumerate(Vg_sweep):
    print('configuration {}/{}'.format(ii + 1, len(Vg_sweep)), flush=True)
    base_name = base_dir + '/{:03d}'.format(ii)
    with open(base_name + '_setup.txt', 'w') as f:
        print('Vg = {:.3f} V'.format(Vg / c.eV), file=f)
        print('Vds = {:.3f} V'.format(Vds / c.eV), file=f)

    device.set_gate('gate', work_function - Vg)

    if sc is None:
        semi = DriftDiffusionProblem(device).solve()
        V0 = semi.V
    else:
        V0 = sc.V

    scprob = SelfConsistentProblem(device, V0=V0)
    sc = scprob.solve(max_iter=15, atol=1e-6 * c.eV, max_E_eval=1000, damp_until=2)

    scprob.plot_convergence()
    plt.savefig(base_name + '_convergence.pdf')
    plt.close()

    sc.write_vtk(base_name + '_results.vtu')
    J, _ = current(structure, max_eval=2000, atol=1e-12 * c.A)
    currents[ii] = J[0, 1]

np.savez(f'{base_dir}/iv.npz', Vg=Vg_sweep, Vds=Vds, I=currents)
