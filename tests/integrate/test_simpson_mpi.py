import unittest
import numpy as np

from petra.integrate import simpson
from petra.util import mpi

reason = "Needs to be run with more than 1 MPI node, skipping tests"
ifmpi = unittest.skipIf(not mpi.is_parallel(), reason)


@ifmpi
def test_integration_pole():

    def f(x):
        return np.piecewise(x, [x <= 0, x > 0],
                            [0.0, lambda x: 1.0 / np.sqrt(x)]),
    xmin, xmax = -1, 1
    analytic = 2.0
    integrate(f, xmin, xmax, analytic)


@ifmpi
def test_integration_accuracy():

    def f(x):
        return np.sin(x) / x + np.exp(-(x - 4)**2), np.sin(x)
    xmin, xmax = 1, 10
    analytic = 2.484698797563970262861816781878156908423370264215762388394
    integrate(f, xmin, xmax, analytic)


def integrate(f, xmin, xmax, analytic):
    w = simpson.SimpsonIntegrator(f, xmin, xmax)

    assert(isinstance(w, simpson.ParallelSimpsonIntegrator))

    w.refine(iterations=1000, atol=1e-8)

    print(len(w))
    assert(len(w) <= 2003)
    assert(w.error < 1e-8)
    assert(len(w.running_intervals) == 0)

    reconstruct = np.sum(np.array(w.f)[:, 0] * w.weights)
    assert(np.allclose(reconstruct, w.value))

    error = np.abs(w.value - analytic)
    assert(error < 1e-8)

    xlin, dx = np.linspace(xmin, xmax, len(w.x), retstep=True)
    flin = f(xlin)[0]
    trapz = np.trapz(flin, dx=dx)
    trapz_error = np.abs(trapz - analytic)
    assert(error < trapz_error)
