#!/usr/bin/env python3
import glob
import json
import os
import subprocess
from urllib.request import urlopen, Request

root_url = os.environ['CI_PAGES_URL']
job_branch = os.environ['CI_COMMIT_REF_NAME']
api_url = os.environ['CI_API_V4_URL']
api_token = os.environ.get('CI_JOB_TOKEN')
project_id = os.environ['CI_PROJECT_ID']

branches = ['master', 'develop']


def list_tagged():
    url = f"{api_url}/projects/{project_id}/packages"
    request = Request(url, headers={'JOB-TOKEN': api_token})
    with urlopen(request) as response:
        response = json.loads(response.read().decode())
    return [package['version'] for package in response
            if package['name'] == 'docs' and package['version'] not in branches]


def alias(ref):
    return 'stable' if ref == 'master' else ref


def download(ref):
    print(f'Downloading {ref} docs...')
    url = f"{api_url}/projects/{project_id}/packages/generic/docs/{ref}/public.tar.gz"
    cmd = f'curl --header "JOB-TOKEN: {api_token}" {url} | tar -xz'
    status = subprocess.call(cmd, shell=True)
    if status == 0:
        print(f'Added public/{ref} docs at {root_url}/{ref}')
        return (f'public/{ref}', f'{root_url}/{ref}')
    print('FAILED.')
    return None


os.makedirs('public')

current_versions = {alias(branch): download(branch) for branch in branches}
released_versions = {version: download(version) for version in list_tagged()}
all_versions = {**current_versions, **released_versions}


def link(versions):
    return '\n'.join(f'      <dd><a href="{url}">{version}</a></dd>'
                     for version, (_, url) in versions.items())


version_html = f'''
<div class="rst-versions" data-toggle="rst-versions" role="note" aria-label="versions">
  <span class="rst-current-version" data-toggle="rst-current-version">
    <span class="fa fa-book"> Versions</span>
    v: {{version}}
    <span class="fa fa-caret-down"></span>
  </span>
  <div class="rst-other-versions" id="petra-versions">
    <dl>
     <dt>Current</dt>
{link(current_versions)}
    </dl>
    <dl>
     <dt>Releases</dt>
{link(released_versions)}
    </dl>
  </div>
</div>
'''


def modify_html(version, path):
    pattern = f'{path}/**/*.html'
    fnames = glob.glob(pattern, recursive=True)
    for fname in fnames:
        with open(fname) as f:
            html = f.read()
        html = html.replace(
            '</body>',
            version_html.format(version=version) + '</body>'
        )
        with open(fname, 'w') as f:
            f.write(html)
    return len(fnames)


print('Adding version info to html files...')
for version, (path, _) in all_versions.items():
    nfiles = modify_html(version, path)
    print(f' - {path}: {nfiles} files changed.')
print('done.')


index_url = 'master'
index_html = f'''<!doctype html>
<html lang="en">
<head>
  <title>PETRA Documentation</title>
  <meta http-equiv="refresh" content="0; URL={index_url}" />
</head>
<body>
  Redirecting to the <a href="{index_url}">documentation.</a>
</body>
</html>
'''

print('Writing index.html...')
with open('public/index.html', 'w') as f:
    f.write(index_html)
print('success!')
