from abc import ABC, abstractmethod
from typing import Tuple, TYPE_CHECKING

import numpy as np

from petra import distributions
from petra import constants as c

if TYPE_CHECKING:
    from petra.transport.structure import Structure


class Occupation(ABC):

    def __init__(self, structure: 'Structure'):
        self.structure = structure

    @abstractmethod
    def update(self) -> None:
        pass

    @abstractmethod
    def Erange(self, cutoff=1e-5) -> Tuple[float, float]:
        pass

    @abstractmethod
    def __call__(self, state) -> float:
        pass

    @abstractmethod
    def d(self, state) -> float:
        pass


class EquilibriumElectronOccupation(Occupation):

    def __init__(self, structure: 'Structure', mu_c: Tuple[float, float], T: float):
        self.structure = structure
        self.mu_c = mu_c
        self.T = T

        self.update()

    def update(self) -> None:
        contacts = self.structure.elements.contacts
        self.g_spin_c = np.asarray([contact.g_spin for contact in contacts])

        bs_c = [contact.bandstructure() for contact in contacts]
        self.Ecb = np.array([bs.find_cb() for bs in bs_c])
        self.Emin = np.array([bs.find_Emin() for bs in bs_c])

        V = self.structure.node_V
        self.V_contact = np.array([np.mean([V[self.structure.nodes.index(node.parent)]
                                            for node in contact.nodes])
                                   for contact in contacts])
        self.Vmin, self.Vmax = V.min(), V.max()

    def Erange(self, cutoff=1e-5) -> Tuple[float, float]:
        """Provides an energy range for integrations"""
        tail_E = -2 * np.log10(cutoff) * c.k_B * self.T

        Emax = max(self.Ecb + self.Vmax - self.V_contact) + tail_E
        Emin = self.Emin - (Emax - self.Emin) * 1e-3  # including some margin

        return Emin, Emax

    def __call__(self, state) -> float:
        """Fermi-Dirac distribution for electrons"""
        stat = distributions.fermi_dirac(state.E, self.mu_c[state.icontact], self.T)
        return self.g_spin_c[state.icontact] * stat

    def d(self, state) -> float:
        """Derivative of the Fermi-Dirac distribution for electrons"""
        stat = distributions.dfermi_dirac(state.E, self.mu_c[state.icontact], self.T)
        return self.g_spin_c[state.icontact] * stat


class EquilibriumElectronHoleOccupation(EquilibriumElectronOccupation):

    def update(self) -> None:
        contacts = self.structure.elements.contacts
        self.g_spin_c = np.asarray([contact.g_spin for contact in contacts])

        bs_c = [contact.bandstructure() for contact in contacts]
        self.Ecb = np.array([bs.find_cb() for bs in bs_c])
        self.Evb = np.array([bs.find_vb() for bs in bs_c])
        self.Efermi_c = np.asarray([bs.estimate_fermi() for bs in bs_c])

        V = self.structure.node_V
        self.V_contact = np.array([np.mean([V[self.structure.nodes.index(node.parent)]
                                            for node in contact.nodes])
                                   for contact in contacts])
        self.Vmin, self.Vmax = V.min(), V.max()

    def Erange(self, cutoff=1e-5) -> Tuple[float, float]:
        """Provides an energy range for integrations"""
        tail_E = -2 * np.log10(cutoff) * c.k_B * self.T

        Emin = min(self.Evb + self.Vmin - self.V_contact) - tail_E
        Emax = max(self.Ecb + self.Vmax - self.V_contact) + tail_E

        return Emin, Emax

    def __call__(self, state) -> float:
        """
        Fermi-Dirac distribution for electrons in the conduction band (+)
        and holes in the valence band (-)
        """
        stat = distributions.fermi_dirac(state.E, self.mu_c[state.icontact], self.T)
        stat -= 1.0 * (state.E < self.Efermi_c[state.icontact])
        return self.g_spin_c[state.icontact] * stat
