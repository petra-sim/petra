import numpy as np
from scipy.spatial import Delaunay
import meshpy
import meshpy.tet

from petra.mesh import Mesh


def generate_convex_tetgen(boundary_points, internal_points, a):
    ninternal = internal_points.shape[0]

    tri = Delaunay(boundary_points)
    ipoints = np.concatenate([internal_points, tri.points], axis=0)
    ifacets = (ninternal + tri.convex_hull).tolist()
    mesh_info = meshpy.tet.MeshInfo()
    mesh_info.set_points(ipoints)
    mesh_info.set_facets(ifacets)

    options = meshpy.tet.Options("pq", neighout=True)
    return meshpy.tet.build(mesh_info, options=options, max_volume=a**3)


def generate_convex(boundary_points, internal_points=None, a=1.0):
    ninternal = internal_points.shape[0]

    tmesh = generate_convex_tetgen(boundary_points, internal_points, a)

    points = np.array(tmesh.points)
    simplices = np.array(tmesh.elements)
    neighbors = np.array(tmesh.neighbors)

    assert(np.all(points[:ninternal] == internal_points))

    return Mesh(points, simplices, neighbors)
