from pathlib import Path

import ase.io.vasp

from petra.io.vasp.constants import Ang
from petra.io.vasp.util import CommonFilePath
from petra import constants as c


class POSCAR(CommonFilePath):
    def __init__(self, path='', read=True):
        self.path = Path(path)
        self._atoms = None
        if read:
            self.read()

    def read(self, fix_units=True):
        '''Read a POSCAR.
        Correctly accounts for the different units of VASP'''
        self.atoms = ase.io.vasp.read_vasp(self.fname)
        if fix_units:
            cell = self.atoms.get_cell() * Ang / c.Ang
            self.atoms.set_cell(cell, scale_atoms=True)
        return self

    def write(self, path=None, direct=True, fix_units=True):
        if path is not None:
            self.path = Path(path)

        atoms = self.atoms.copy()
        if fix_units:
            cell = atoms.get_cell() * c.Ang / Ang
            atoms.set_cell(cell, scale_atoms=True)
        ase.io.vasp.write_vasp(self.fname, atoms,
                               direct=direct, sort=True, vasp5=True)
        return self

    @classmethod
    def create(cls, atoms, path='POSCAR'):
        poscar = cls(path=path, read=False)
        poscar.atoms = atoms
        return poscar

    @property
    def atoms(self):
        return self._atoms

    @atoms.setter
    def atoms(self, atoms):
        self._atoms = atoms
        self.a = atoms.get_cell() * Ang

    @property
    def positions(self):
        '''Atomic positions'''
        return self.atoms.get_positions() * Ang

    @property
    def species(self):
        '''Atomic species'''
        return self.atoms.get_chemical_symbols()
