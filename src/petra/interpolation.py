from functools import partial

import numpy as np
import scipy.spatial.qhull as qhull


def interpolator_from_points(xyz):
    tri = qhull.Delaunay(xyz)
    return partial(interpolation_weights, tri)


def interpolator_from_mesh(tri):
    return partial(interpolation_weights, tri)


def interpolation_weights(tri, uvw):
    dim = uvw.shape[-1]
    simplex = tri.find_simplex(uvw.reshape(-1, dim)).reshape(uvw.shape[:-1])
    vertices = np.take(tri.simplices, simplex, axis=0)
    temp = np.take(tri.transform, simplex, axis=0)
    delta = uvw - temp[..., dim, :]
    bary = np.einsum('...jk,...k->...j', temp[..., :dim, :], delta)
    weights = np.concatenate((bary, 1 - bary.sum(axis=-1, keepdims=True)), -1)
    return partial(interpolate, vertices, weights)


def interpolate(vertices, weights, values):
    return np.einsum('...j,...j->...', np.take(values, vertices), weights)
