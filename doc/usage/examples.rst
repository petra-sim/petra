Examples
========

The PETRA repository contains example jupyter notebooks in the source directory :source:`examples/`:

- :source:`examples/pwepp/` demonstrates the use of the plane wave empirical
  pseudopotential solver.
- :source:`examples/transport/` demonstrates the use of the transport code in steps, starting from
  an empirical pseudopotential calculation to the generation of transfer characteristics (IV).


If you have cloned the Petra repository, it is advised not to run the jupyter notebooks from the
source tree. Rather, copy the :source:`examples` directory and run the examples there. Running in
the source tree will result in merge conflicts when updating Petra.
