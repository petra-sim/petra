from collections import defaultdict
from functools import partial, cached_property

import numpy as np
import scipy.optimize as opt
import scipy.linalg as la

from petra.util import log
from petra.linalg import polyeig
from petra.distributions import fermi_dirac
from petra import constants as c

logger = log.getLogger(__name__)


def k_to_alpha(k_ak):
    """Calculates phase factors from the wavevector in reciprocal coordinates.

    Parameters
    ----------
    k_ak : :obj:`list` of complex
        Wavevector.

    Returns
    -------
    alpha_ak : :obj:`list` of complex
        Phase factors.
    """
    return np.exp(2j * np.pi * k_ak.astype(complex))


def alpha_to_k(alpha_ak):
    """Calculate wavevector in reciprocal coordinates from the phase factors.

    Parameters
    ----------
    alpha_ak : :obj:`list` of complex
        Phase factors.

    Returns
    -------
    k_ak : :obj:`list` of complex
        Wavevector.
    """
    return np.log(alpha_ak.astype(complex)) / (2j * np.pi)


class Bandstructure:
    """Tools for solving the bandstructure of an element.
    """

    def __init__(self, element):
        """Initialize the bandstructure with an element.

        Parameters
        ----------
        element : :obj:`petra.element.Element`
        """
        self.element = element

        idx = self.element.nodes.interior.idx
        zero_factory = partial(np.zeros, (idx.size, idx.size), dtype=complex)

        self.H_poly = defaultdict(zero_factory)
        self.M_poly = defaultdict(zero_factory)

        for n0, n1, h, m, p in self.element.HM_periodic:
            i0, i1 = idx[n0], idx[n1]
            p = tuple(p.tolist())
            self.H_poly[p][i0, i1] += h
            self.M_poly[p][i0, i1] += m

    def H(self, alpha):
        """The Hamiltonian matrix.

        Parameters
        ----------
        alpha : :obj:`list` of complex
            The phase factors.

        Returns
        -------
        H : :obj:`numpy.ndarray` of complex
            The Hamiltonian
        """
        return sum(np.prod(alpha**p) * Hi
                   for p, Hi in self.H_poly.items())

    def M(self, alpha):
        """The Hamiltonian matrix.

        Parameters
        ----------
        alpha : :obj:`list` of complex
            The phase factors.

        Returns
        -------
        M : :obj:`numpy.ndarray` of complex
            The mass matrix
        """
        return sum(np.prod(alpha**p) * Mi
                   for p, Mi in self.M_poly.items())

    def HM(self, alpha):
        """The Hamiltonian and Mass matrix.

        Parameters
        ----------
        alpha : :obj:`list` of complex
            The phase factors.

        Returns
        -------
        H : :obj:`numpy.ndarray` of complex
            The Hamiltonian
        M : :obj:`numpy.ndarray` of complex
            The mass matrix
        """
        return self.H(alpha), self.M(alpha)

    def dHMdk(self, alpha):
        """The derivative of the Hamiltonian and the Mass matrix with respect to k.

        Parameters
        ----------
        alpha : :obj:`list` of complex
            The phase factors.

        Returns
        -------
        dHdk : :obj:`numpy.ndarray` of complex
            The derivative of the Hamiltonian
        dMdk : :obj:`numpy.ndarray` of complex
            The derivative of the mass matrix
        """
        a = self.element.cell.a
        dM = sum(np.prod(alpha**p) *
                 (1j * (a @ p))[:, None, None] * Mi[None, :, :]
                 for p, Mi in self.M_poly.items())
        dH = sum(np.prod(alpha**p) *
                 (1j * (a @ p))[:, None, None] * Hi[None, :, :]
                 for p, Hi in self.H_poly.items())
        return dH, dM

    def poly_E(self, E, alpha=(1, 1, 1)):
        """The polynomial eigenvalue equation at a specified energy.

        Parameters
        ----------
        E : float
            The energy.
        alpha : :obj:`list` of complex
            The phase factors.

        Returns
        -------
        dict of :obj:`numpy.ndarray` of complex
            A dictionary with the exponents of the polynomial (keys) and
            associated matrices (values).
        """
        mask = np.ones(3)
        mask[self.element.direction] = 0

        return {p[self.element.direction]:
                (self.H_poly[p] - E * self.M_poly[p]) *
                np.prod(alpha**(p * mask))
                for p in self.H_poly}

    def solve_k(self, k_a, retwave=True):
        """Solve the bandstructure for a given wavevector.

        Parameters
        ----------
        k_a : :obj:`list` of complex
            Wavevector in reciprocal coordinates.
        retwave : bool, optional
            Return the wavefunctions or not. (defaults to True).

        Returns
        -------
        E : :obj:`numpy.ndarray` of float
            The energies.
        coef : :obj:`numpy.ndarray` of complex
            The coefficients of the wavefunction solutions.
        """
        alpha = k_to_alpha(k_a)
        H, M = self.HM(alpha)

        result = la.eigh(H, M, eigvals_only=(not retwave))
        if not retwave:
            return result
        E_i, c_ni = result
        return E_i, self._normalize(c_ni, M)

    def coef_per_node(self, k_a, c_ni):
        node_idx = self.element.nodes.interior.idx
        k_v = self.element.cell.reciprocal.to_cartesian(k_a)

        c_jni = []
        for inode, node in enumerate(self.element.nodes):
            origin = getattr(node, 'origin', node)
            delta_v = getattr(node, 'period', np.zeros(3))
            c_jni.append(c_ni[node_idx[origin]] * np.exp(1j * (k_v @ delta_v)))

        return np.stack(c_jni)

    def _normalize(self, c_is, M_ii):
        ''' Delta normalization of the modes
        '''
        norm_i = np.abs(np.einsum('is,ij,js->s', c_is.conj(), M_ii, c_is))
        return c_is / np.sqrt(self.element.dk * norm_i)

    def solve_E(self, E, alpha_a=None, k_a=None):
        """Solve the bandstructure for a given energy.

        Parameters
        ----------
        E : float
            The energy.
        alpha_a : :obj:`list` of complex, optional
            The phase factors. (defaults to [1, 1, 1])
        k_a : :obj:`list` of complex, optional
            The wavevectors. (defaults to using alpha)

        Returns
        -------
        ComplexModes
            Solution data and tools.
        """
        if alpha_a is None:
            if k_a is not None:
                alpha_a = k_to_alpha(k_a)
            else:
                alpha_a = np.ones(3)
        poly = self.poly_E(E, alpha_a)
        alpha_n, v, select = polyeig.polyeig(poly, full=True)

        psi_in = v[select]

        alpha_na = np.ones((alpha_n.shape[0], 3), dtype=complex)
        alpha_na *= alpha_a
        alpha_na[:, self.element.direction] = alpha_n

        return ComplexModes(self, E, alpha_na, psi_in, self.element.direction)

    def estimate_fermi(self, kpts=20, T=300 * c.K, n_free=0.0):
        """Estimates the Fermi-Level .

        Parameters
        ----------
        kpts : int, optional
            Number of k-points used to sample the 1st Brillouin zone.
            (defaults to 20)
        T : float, optional
            The temperature. (defaults to 300 K)
        n_free : float, optional
            The number of free electrons. (defaults to 0.0)

        Returns
        -------
        float
            Fermi level
        """
        k_ka = np.zeros((kpts, 3))
        k_ka[:, self.element.direction] = np.linspace(0., 1., kpts, endpoint=False)
        E = np.array([self.solve_k(k_a, retwave=False) for k_a in k_ka])
        n_valence = self.element.n_valence

        if n_valence == 0:
            return E.min()

        def delta_n(mu):
            n = fermi_dirac(E, mu, T).sum() / kpts
            n *= self.element.g_spin
            return n - (n_valence + n_free)

        return opt.brentq(delta_n, E.min(), E.max())

    def dEdk(self, psi_in, *, alpha=None, k=None):
        """The derivation of energy with respect to k.

        Parameters
        ----------
        psi_in : :obj:`numpy.ndarray` of complex
            The coefficients of the wavefunction solutions.
        alpha_v : :obj:`list` of complex
            The phase factors.
        k_v : :obj:`list` of complex
            The wavevector.

        Returns
        -------
        :obj:`list` of complex
            The gradient of energy.
        """
        if alpha is None and k is None:
            raise ValueError('Either alpha or k need to be provided')
        if alpha is not None and k is not None:
            raise ValueError('Only provide one of alpha or k')
        if alpha is None:
            alpha = k_to_alpha(k)
        H_ii, M_ii = self.HM(alpha)
        dHdk_vii, dMdk_vii = self.dHMdk(alpha)
        psi_conj_in = psi_in.conj()
        contract = 'i...,ij,j...->...'
        expect_H_n = np.real(np.einsum(contract, psi_conj_in, H_ii, psi_in))
        expect_M_n = np.real(np.einsum(contract, psi_conj_in, M_ii, psi_in))
        contract = 'i...,vij,j...->v...'
        expect_dHdk_vn = np.real(np.einsum(contract,
                                           psi_conj_in, dHdk_vii, psi_in))
        expect_dMdk_vn = np.real(np.einsum(contract,
                                           psi_conj_in, dMdk_vii, psi_in))
        return ((expect_M_n * expect_dHdk_vn - expect_H_n * expect_dMdk_vn) /
                expect_M_n**2)

    def velocity(self, psi_in, *, alpha=None, k=None):
        dEdk_vn = self.dEdk(psi_in, alpha=alpha, k=k)
        return np.sign(self.element.period_v) @ dEdk_vn

    def find_edge(self, iband, which='top', direction=None):
        """Find the edge of a band.

        Parameters
        ----------
        iband : int
            The band index.
        which : str, optional
            Which edge to find, can be 'min'/'bottom' or 'max'/'top'.
            (defaults to 'min')
        direction : int, optional
            The direction to search. (defaults to searching all directions)

        Returns
        -------
        float
            The edge's energy.
        """
        if which not in ('top', 'bottom', 'min', 'max'):
            raise ValueError('which needs to be top, bottom, min or max')
        sign = 1 if which in ('min', 'bottom') else -1

        def energy_to_min(phase):
            if direction is not None:
                phase = np.array([phase if d == direction else 1.
                                  for d in range(3)])
            k = alpha_to_k(np.exp(1j * phase))
            E = self.solve_k(k, retwave=False)
            return sign * E[iband]

        if direction is None:
            optimum = opt.minimize(energy_to_min, np.zeros(3),
                                   bounds=[(-np.pi, np.pi),
                                           (-np.pi, np.pi),
                                           (-np.pi, np.pi)]).fun
        else:
            optimum = opt.minimize_scalar(energy_to_min).fun

        return sign * optimum

    def find_Emin(self, direction=None):
        """Find the energy minimum of the spectrum

        Parameters
        ----------
        direction : int, optional
            The direction to search. (defaults to searching all directions)

        Returns
        -------
        float
            The energy of the minimum.
        """
        return self.find_edge(0, which='bottom', direction=direction)

    def find_vb(self, direction=None):
        """Find valence band maximum

        Parameters
        ----------
        direction : int, optional
            The direction to search. (defaults to searching all directions)

        Returns
        -------
        float
            The energy of the valence band maximum.
        """
        E = 0
        Efermi = self.estimate_fermi()
        for iband in range(self.element.nbasis):
            Ei = self.find_edge(iband, which='top', direction=direction)
            if Ei > Efermi:
                break
            E = Ei
        return E

    def find_cb(self, direction=None):
        """Find conduction band minimum

        Parameters
        ----------
        direction : int, optional
            The direction to search. (defaults to searching all directions)

        Returns
        -------
        float
            The energy of the conduction band minimum.
        """
        E = 0
        Efermi = self.estimate_fermi()
        for iband in range(self.element.nbasis):
            Ei = self.find_edge(iband, which='bottom', direction=direction)
            if Ei >= Efermi:
                E = Ei
                break
        return E

    def find_bandgap(self, direction=None):
        """Find bandgap

        Parameters
        ----------
        direction : int, optional
            The direction to search. (defaults to searching all directions)

        Returns
        -------
        float
            The bandgap size.
        """
        return (self.find_cb(direction=direction) -
                self.find_vb(direction=direction))

    def find_midgap(self, direction=None):
        """Find mid-gap energy

        Parameters
        ----------
        direction : int, optional
            The direction to search. (defaults to searching all directions)

        Returns
        -------
        float
            The energy of the middle of the bandgap.
        """
        return (self.find_vb(direction=direction) +
                self.find_cb(direction=direction)) / 2


class ComplexModes:
    '''The eigenmodes of a complex bandstructure.

    Attributes
    ----------
    bandstructure : petra.bandstructure.Bandstructure
        The bandstructure object that produced these results
    element : petra.element.Element
        The element of which these are the solutions
    E : float
        The energy of the modes
    alpha : np.ndarray of complex
        The phase-factors of the modes
    c : (N_basis, N_modes) np.ndarray of complex
        The coeficients of the eigenmodes
    direction : int
        The direction of the eigenvalues (wave vectors)
    '''
    def __init__(self, bandstructure, E_i, alpha_i, c_ni, direction):
        self.bandstructure = bandstructure
        self.element = bandstructure.element
        self.E = E_i
        self.alpha = alpha_i
        self.c_ni = c_ni
        self.direction = direction

        self.sense = self.element.sense_v[direction]

        self._identify()
        self._normalize()

    def _identify(self):

        test = -np.real(np.log(self.alpha_t)) + self.velocity

        self.all_in = test * self.sense > 0
        self.all_out = test * self.sense < 0

        Nin = np.count_nonzero(self.all_in)
        Nout = np.count_nonzero(self.all_out)
        N = self.c_ni.shape[0]
        if Nin != N or Nout != N:
            Nunid = np.count_nonzero(~(self.all_in | self.all_out))
            logger.error("FAILED TO INDENTIFY IN: {}, OUT: {}, "
                         "UNIDENTIFIED: {} for E={}eV"
                         "".format(Nin, Nout, Nunid, self.E / c.eV))
            idx = np.argsort(test)
            self.all_in = np.zeros(2 * N, dtype=bool)
            self.all_in[idx[:N]] = True
            self.all_out = np.zeros(2 * N, dtype=bool)
            self.all_out[idx[N:]] = True

        test = np.abs(self.alpha_t) + np.abs(1. / self.alpha_t)

        eps = np.finfo(float).eps
        self.running = (test <= 2 * (1 + eps)) & (np.abs(self.velocity) > 0)
        self.decaying = ~self.running
        self.running_in = self.all_in & self.running
        self.running_out = self.all_out & self.running
        self.decaying_in = self.all_in & self.decaying
        self.decaying_out = self.all_out & self.decaying

    def _normalize(self):
        ''' Delta normalization of the modes
        '''
        for i in range(self.c_ni.shape[1]):
            c_n = self.c_ni[:, i]
            M = self.bandstructure.M(self.alpha[i])
            norm = np.abs(np.vdot(c_n, M @ c_n)) * self.element.dk
            self.c_ni[:, i] /= np.sqrt(norm)

    @property
    def psi(self):
        return self.c_ni

    @cached_property
    def velocity(self):
        N = self.alpha.shape[0]
        test = np.abs(self.alpha_t) + np.abs(1. / self.alpha_t) - 2
        v_group = np.zeros(N)
        tol = 1e-3 * np.finfo(float).eps
        for i in np.r_[0:N][test <= tol]:
            dEdk = self.bandstructure.dEdk(self.c_ni[:, i], alpha=self.alpha[i])
            v_group[i] = dEdk[self.direction]
        v_group.setflags(write=False)
        return v_group

    @property
    def alpha_t(self):
        '''The phase factors in the injection direction.'''
        return self.alpha[:, self.direction]

    @cached_property
    def k_na(self):
        '''Wave vector in reciprocal lattice coordinates'''
        return np.array([alpha_to_k(ialpha) for ialpha in self.alpha])

    @property
    def alpha_out(self):
        '''The phase factors of the out-flowing modes.'''
        return self.alpha_t[self.all_out]

    @property
    def alpha_in(self):
        '''The phase factors of the in-flowing modes.'''
        return self.alpha_t[self.all_in]

    @property
    def psi_out(self):
        '''The wavefunction coeficients of the out-flowing modes.'''
        return self.c_ni[:, self.all_out]

    @property
    def psi_in(self):
        '''The wavefunction coegficients of the in-flowing modes.'''
        return self.c_ni[:, self.all_in]

    def __repr__(self):
        from petra import constants as c
        rep = ['ComplexModes(',
               '  E: {} eV'.format(self.E / c.eV),
               '  # running: {} in, {} out, {} total'.format(
                   np.sum(self.running_in), np.sum(self.running_out),
                   np.sum(self.running)),
               '  # decaying: {} in, {} out, {} total'.format(
                   np.sum(self.decaying_in), np.sum(self.decaying_out),
                   np.sum(self.decaying)),
               ')']
        return '\n'.join(rep)

    def plot_alpha(self, ax=None):
        '''Plot the phase factors of all the modes.'''
        import matplotlib.pyplot as plt

        if ax is None:
            ax = plt.gca()
        theta = np.linspace(0, 2 * np.pi, 1000)
        ax.plot(np.sin(theta), np.cos(theta), 'k-')
        ax.plot(np.real(self.alpha_t),
                np.imag(self.alpha_t),
                'k.')
        ax.plot(np.real(self.alpha_t[self.running_in]),
                np.imag(self.alpha_t[self.running_in]),
                'b>')
        ax.plot(np.real(self.alpha_t[self.running_out]),
                np.imag(self.alpha_t[self.running_out]),
                'r<')
        ax.plot(np.real(self.alpha_t[self.decaying_in]),
                np.imag(self.alpha_t[self.decaying_in]),
                'r4')
        ax.plot(np.real(self.alpha_t[self.decaying_out]),
                np.imag(self.alpha_t[self.decaying_out]),
                'b3')

    def plot_k(self, ax=None):
        '''Plot the complex wavevectors of all the modes.

        Parameters
        ----------
        ax : matplotlib.axes.Axes, optional
            Axes to plot in. (defaults to current axes)
        '''
        import matplotlib.pyplot as plt

        if ax is None:
            ax = plt.gca()
        ax.axhline(0.0, color='k')
        ax.plot(np.real(self.k_na),
                np.imag(self.k_na),
                'k.')
        ax.plot(np.real(self.k_na[self.running_in]),
                np.imag(self.k_na[self.running_in]),
                'b>')
        ax.plot(np.real(self.k_na[self.running_out]),
                np.imag(self.k_na[self.running_out]),
                'r<')
        ax.plot(np.real(self.k_na[self.decaying_in]),
                np.imag(self.k_na[self.decaying_in]),
                'r4')
        ax.plot(np.real(self.k_na[self.decaying_out]),
                np.imag(self.k_na[self.decaying_out]),
                'b3')

        ticks = [-1, -.5, 0, .5, 1]
        labels = [r'$-1$', '$-1/2$', '$0$', '$1/2$', r'$1$']
        ax.set_xticks(ticks)
        ax.set_xticklabels(labels)
        ax.set_yticks(ticks)
        ax.set_yticklabels(labels)
        ax.grid(True)
