About
=====

.. _label-publications:

Publications
------------

The core PETRA paper, detailing the Bloch wave method:

   | `M.L. Van de Put`_, `M.V. Fischetti`_, and `W.G. Vandenberghe`_
   | *Scalable atomistic simulations of quantum electron transport using empirical pseudopotentials*
   | Computer Physics Communications **244**, 156-169 (2019), :doi:`10.1016/j.cpc.2019.06.009`

Functionality and applications

   | A.A. Laturia, `M.L. Van de Put`_, and `W.G. Vandenberghe`_
   | *Generation of empirical pseudopotentials for transport applications and their application to
     group IV materials*
   | Journal of Applied Physics **128** (3), 034306 (2020), :doi:`10.1063/5.0009838`

   | S. Chen, `M.L. Van de Put`_, and `M.V. Fischetti`_
   | *Quantum Transport Simulations of graphene-nanoribbon field-effect transistors with defects*
   | Journal of Computational Electronics, S.I.: Two-Dimensional Materials (2020),
   | :doi:`10.1007/s10825-020-01588-1`

.. _label-funding:

Funding
-------
Initial development of PETRA was funded by the National Science Foundation (NSF),
`Grant #1710066 <https://nsf.gov/awardsearch/showAward?AWD_ID=1710066>`_.


.. _`The University of Texas at Dallas`: https://utdallas.edu
.. _`M.L. Van de Put`: https://mvandeput.gitlab.io
.. _`W.G. Vandenberghe`: https://williamvandenberghe.com
.. _`M.V. Fischetti`:  https://profiles.utdallas.edu/max.fischetti
