import functools
import operator

import numpy as np
import numpy.linalg as la
from scipy import spatial

from ase.atoms import Atoms
from petra import constants as c


_axes = {
    'x': (1, 0, 0),
    'y': (0, 1, 0),
    'z': (0, 0, 1),
}


def inside(positions, distance, p=2):
    if isinstance(positions, Atoms):
        positions = positions.positions * c.Ang
    atree = spatial.cKDTree(positions)

    def inside_filter(x):
        xtree = spatial.cKDTree(x.reshape(-1, x.shape[-1]))
        idx = np.concatenate(
            [i for i in atree.query_ball_tree(xtree, distance, p=p) if i])
        shape = x.shape[:-1]
        mask = np.zeros(np.prod(shape), dtype=bool)
        mask[idx] = True
        return mask.reshape(shape)
    return inside_filter


within = inside


def outside(positions, distance, p=2):
    return invert(inside(positions, distance, p=p))


def halfspace(r0, axis='z'):
    axis = np.array(_axes.get(axis, axis), dtype=float)

    r0 = np.atleast_1d(r0)
    if r0.ndim > 1:
        raise ValueError()
    if r0.shape[0] == 1:
        r0 = r0 * axis

    unitnormal = axis / la.norm(axis)

    def halfspace_filter(x):
        y = (x - r0) @ unitnormal
        return (0.0 <= y)
    return halfspace_filter


def slab(r0, r1, axis=None):
    if axis is None:
        axis = r1 - r0
        axis /= la.norm(axis)
    return intersect(halfspace(r0, axis),
                     invert(halfspace(r1, axis)))


def invert(filt):
    if hasattr(filt, 'inverse'):
        return filt.inverse

    def invert_(x):
        return ~filt(x)
    setattr(invert_, 'inverse', filt)
    return invert_


def intersect(*filters):
    def intersect_(x):
        return functools.reduce(operator.and_, (f(x) for f in filters))
    return intersect_


def union(*filters):
    def union_(x):
        return functools.reduce(operator.or_, (f(x) for f in filters))
    return union_
