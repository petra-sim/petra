{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 2. Bandstructure Reconstruction\n",
    "\n",
    "In this part, we will introduce the Bloch basis, obtained from the PWEPP results, and reconstruct the bandstructure from it."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%load_ext autoreload\n",
    "%autoreload 2"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib inline\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "\n",
    "from petra import constants as c\n",
    "from petra.plot import use_latex\n",
    "from petra.util import log\n",
    "\n",
    "log.to_stdout()\n",
    "use_latex()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 2.1. Selecting a Basis and building a Block\n",
    "\n",
    "We begin by creating a `Block`, the basic building-block of the device structure.\n",
    "It is the supercell in which the Bloch waves of our basis are defined.\n",
    "\n",
    "We make a block directly from a PWEPP calculator with the appropriate k-points. We load it first."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from petra.pwepp import PWEPP\n",
    "\n",
    "calc = PWEPP('7agnr.pwepp')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next, we'll build a `Block` using Bloch-basis.\n",
    "\n",
    "The basis set $\\phi_{ink}(r)= u_{nk}(r) e^{ik(r-r_i)}$ is built from the Bloch waves, $u_{nk}(r)$, in the PWEPP calculator.\n",
    "The wavefunction is expanded on this basis as\n",
    "$$\\psi(r) = \\sum_{ink}\\, c_{ink}\\, f_i(r)\\, \\phi_{ink}(r)$$\n",
    "The wavevectors $k$ and band indices $n$ can be chosen manually or, as in this case, are automatically selected.\n",
    "\n",
    "Internally, this generates the matrix elements:\n",
    "$$ M_{ink,i'n'k'} = \\int \\mathrm{d}^3r\\ f_{i}(r)\\, \\phi^*_{ink}(r) f_{i'}(r)\\, \\phi_{i'n'k'}(r)$$\n",
    "$$ T_{ink,i'n'k'} = \\int \\mathrm{d}^3r\\ [\\nabla f_{i}(r)]\\, \\phi^*_{ink}(r) [\\nabla f_{i'}(r)]\\, \\phi_{i'n'k'}(r)$$\n",
    "$$ P_{ink,i'n'k'} = \\int \\mathrm{d}^3r\\ [\\nabla f_{i}(r)]\\, \\phi^*_{ink}(r) f_{i'}(r)\\, [\\nabla \\phi_{i'n'k'}(r)]$$\n",
    "$$ \\dots $$ \n",
    "Where the $f_i(r)$ are the linear shape functions associated with two nodes per `Block` in the $z$-direction.\n",
    "\n",
    "Note that this step takes a relatively long time to compute because the integrals are quite large."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from petra.transport import Block\n",
    "block = Block.from_calc(calc)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can verify some of the contents of the block by plotting the absolute squared basis-functions in real-space"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from petra.plot import plot_cell\n",
    "\n",
    "wave2 = np.abs(block.U_nxyz[0])**2\n",
    "\n",
    "plt.figure(dpi=150)\n",
    "plot_cell(wave2, atoms=block.atoms, offset=block.r_xyzv[0, 0, 0])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 2.2. Reconstructing the Bandstructure\n",
    "\n",
    "To verify our expansion of the wavefunction, we calculate the bandstructure using our new basis in the `Block` and compare it to the bandstructure in the VASP data.\n",
    "\n",
    "Calculating the bandstructure involves building a periodic Hamiltonian from the `Block` and solving it.\n",
    "Because calculating a `Block` takes considerable time, we wish to reuse it as much as possible, therefore, it does not contain any position information and is unaware of the concept of periodicity. It can be considered as dumb storage for the basis functions and their matrix-elements.\n",
    "\n",
    "To support positioning and periodicity we introduce the `Element` and `Node`. An `Element` is essentially a specialized `Block` and exposes some convenient functions. Upon creation, an `Element` creates a `Node` for every shape function $f_i(r)$ of the `Block`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from petra.transport.element import Element\n",
    "\n",
    "element = Element(block, contacted=1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "bs = element.bandstructure()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now that we have a periodic `Element`, we can calculate its bandstructure using the `petra.bandstructure` module. For convenience, we expose it through a bandstructure property on the `Element`: `element.bandstructure`.\n",
    "\n",
    "In particular, we use the `element.bandstructure.solve_k` method, which solves \n",
    "$$ ( H_{-1} \\alpha^{-1} + H_0 + H_{-1}\\alpha )\\, \\mathbf{c} = E\\, ( M_{-1} \\alpha^{-1} + M_0 + M_{-1}\\alpha )\\, \\mathbf{c} $$\n",
    "for $E$, where $\\alpha=e^{ik\\Delta r}$, and $\\Delta r$ is the distance between periodic nodes.\n",
    "\n",
    "Selecting the first $k$ of the PWEPP calculator, we can verify the reconstruction of its eigen energies (up to the number of Bloch waves taken per $k$)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "k0_a = calc.states.k_ka[0]\n",
    "E0_n = calc.states.E_kn[0]\n",
    "\n",
    "E_n, psi_nm = bs.solve_k(k0_a)\n",
    "\n",
    "plt.figure(dpi=150)\n",
    "ax = plt.subplot(111)\n",
    "plt.bar(np.r_[:E_n.size//2], np.abs(E_n[:E_n.size//2] - E0_n[:E_n.size//2])/c.meV, color='k')\n",
    "plt.xlabel(r'band index $n$')\n",
    "plt.ylabel(r'$|E_n^\\mathrm{Bloch} - E_n^0|\\ \\mathrm{(meV)}$');"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`element.bandstructure`  has even more important functions that we will explore. It has a built-in detection of the band edges, with convenience functions for the conduction band minimum and valence band maximum."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Evb = bs.find_vb() / c.eV\n",
    "Ecb = bs.find_cb() / c.eV\n",
    "\n",
    "print('Ecb = {:.2f} eV'.format(Ecb))\n",
    "print('Evb = {:.2f} eV'.format(Evb))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We now turn to calculating the bandstructure along a line in reciprocal space. We generate the $k$-points using the `petra.util.linspace` function, which is similar to `numpy.linspace`, but it operates on vectors.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from petra import util\n",
    "\n",
    "kmax_a = np.abs(calc.states.k_ka[-1])\n",
    "k_ka = util.linspace(-kmax_a, kmax_a, 201)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Because calculating the bandstructure for a number of $k$-points is a common operation, we provide the `petra.simulation.bandstructure` module. (not to be confused with `petra.bandstructure`) This is a common theme in petra, whenever a simulation is done often, chances are you'll find it in the `petra.simulation` module."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from petra.simulation import bandstructure as bandsim\n",
    "bandstruct = bandsim.calculate(element, k_ka, retwave=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that we asked to calculate the wave functions too (`retwave=True`), this is so we can sort them using the built-in `petra.simulation.bandstructure.sort` function. This traces the bands on a reciprocal path using wavefunction overlaps."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "bandstruct = bandsim.sort(bandstruct)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally, we can plot the resulting bands in blue (note that crossings are correctly traced). We show the wavecar bandstructure as black dots for reference, and indicate the basis functions with crosses. The band-edges and Fermi level are indicated in grey."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.figure(figsize=(5, 6), dpi=150)\n",
    "\n",
    "bandsim.plot(bandstruct, element=element, direction=2, lw=1)\n",
    "\n",
    "plt.plot(calc.states.k_ka[:,2], calc.states.E_kn/c.eV, 'k.', ms=3)\n",
    "\n",
    "plt.axhline(block.Efermi / c.eV, c='k', ls='--', alpha=0.3, lw=.5)\n",
    "plt.axhline(Evb, color='k', alpha=0.3, lw=.5)\n",
    "plt.axhline(Ecb, color='k', alpha=0.3, lw=.5)\n",
    "\n",
    "plt.ylabel('$E$ (eV)')\n",
    "plt.xlabel('$k$')\n",
    "plt.ylim(calc.states.E_kn.min()/c.eV - 1, calc.states.E_kn.max()/c.eV + 1)\n",
    "plt.xlim(-.5, .5)\n",
    "plt.xticks([-.5, -.25, 0, .25, .5], [r'$-\\pi/a$', r'$-\\pi/2a$', r'$0$', r'$\\pi/2a$', r'$\\pi/a$']);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that the bandstructure contains as many bands as there are basis functions, but that only the first half are actually meaningful.\n",
    "\n",
    "We can also calculate the velocity of the bands, and plot them (here near the Fermi-level):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.figure(figsize=(6, 4), dpi=150)\n",
    "\n",
    "v_kn = np.array([bs.velocity(psi_nn, k=k_a) for k_a, psi_nn in zip(bandstruct.k_ka, bandstruct.psi_knn)])\n",
    "\n",
    "plt.scatter(bandstruct.k_ka[:,2, None]*bandstruct.E_kn**0,\n",
    "            bandstruct.E_kn/c.eV, c=v_kn, s=.5, marker='o',\n",
    "            cmap='coolwarm', vmin=-1, vmax=1)\n",
    "plt.colorbar()\n",
    "\n",
    "plt.axhline(Evb, color='k', alpha=0.3, lw=.5)\n",
    "plt.axhline(Ecb, color='k', alpha=0.3, lw=.5)\n",
    "\n",
    "plt.ylabel(r'$E\\ [\\mathrm{eV}]$')\n",
    "plt.xlabel('$k$')\n",
    "plt.ylim(Evb - 2, Ecb + 2)\n",
    "plt.xlim(-.5, .5)\n",
    "plt.xticks([-.5, -.25, 0, .25, .5], [r'$-\\pi/a$', r'$-\\pi/2a$', r'$0$', r'$\\pi/2a$', r'$\\pi/a$']);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 2.3. Complex Bandstructure\n",
    "\n",
    "We calculate the complex bandstructure.\n",
    "While this is of limited practical use, it is a major component in determining the injection of waves in our open system as these solutions form the contact self-energies.\n",
    "\n",
    "We call `bandstructure.solve_E(...)`, which internally solves the polynomial eigenvalue problem:\n",
    "$$ ( H_{-1} \\alpha^{-1} + H_0 + H_{-1}\\alpha )\\, \\mathbf{c} = E\\, ( M_{-1} \\alpha^{-1} + M_0 + M_{-1}\\alpha )\\, \\mathbf{c} $$\n",
    "for $\\alpha=e^{ik\\Delta r}$\n",
    "\n",
    "#### 2.3.1 Solution at a single energy\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "E_single = -1 * c.eV"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We first calculate the solution with a periodic element having positive sense. Here the meaning of the sense becomes clear: all running waves with a velocity in the sense of the sense of the periodic element are indicated with blue triangles, while running waves with opposite velocity are indicated with red triangles. Decaying waves, with a non-zero imaginary part in $k$, are similarly colored blue if they decay in the same sense as the element and red otherwise."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "element = Element(block, contacted=+1)\n",
    "Lsol = element.bandstructure().solve_E(E_single)\n",
    "\n",
    "fig, ax = plt.subplots(1, 2, figsize=(5, 2.7), dpi=150, constrained_layout=True)\n",
    "ax[0].set_title(r'$\\alpha_i = \\mathrm{e}^{\\mathrm{i}k_i R_i}$')\n",
    "Lsol.plot_alpha(ax[0])\n",
    "ax[0].axis('equal')\n",
    "ax[0].set_xlim(-3, 3)\n",
    "ax[0].set_ylim(-3, 3)\n",
    "ax[1].set_title('$k_i$')\n",
    "Lsol.plot_k(ax[1]);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Doing the same thing with a periodic element having negative sense inverts the recognised direction of the waves"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "element = Element(block, contacted=-1)\n",
    "Rsol = element.bandstructure().solve_E(E_single)\n",
    "\n",
    "fig, ax = plt.subplots(1, 2, figsize=(5, 2.7), dpi=150, constrained_layout=True)\n",
    "ax[0].set_title(r'$\\alpha_i = \\mathrm{e}^{\\mathrm{i}k_i R_i}$')\n",
    "Rsol.plot_alpha(ax[0])\n",
    "ax[0].axis('equal')\n",
    "ax[0].set_xlim(-3, 3)\n",
    "ax[0].set_ylim(-3, 3)\n",
    "ax[1].set_title('$k_i$')\n",
    "Rsol.plot_k(ax[1]);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### 2.3.2 Solution over an energy range\n",
    "\n",
    "We solve the bandstructure over an energy range and plot only the running waves (real $k$) overlayed on top of the normal (real) bandstructure."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "E_range = np.linspace(Evb - 2, Ecb + 2, 500) * c.eV"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "First for the positive sense element:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "element = Element(block, contacted=+1)\n",
    "bs = element.bandstructure()\n",
    "\n",
    "plt.figure(dpi=150)\n",
    "bandsim.plot(bandstruct, element=element, c='k', lw=1)\n",
    "\n",
    "for Ei in E_range:\n",
    "    sol = bs.solve_E(Ei)\n",
    "    kin = np.real(sol.k_na[sol.running_in, 2])\n",
    "    lin, = plt.plot(kin, Ei*kin**0/c.eV, 'r.', label='in', ms=2)\n",
    "    kout = np.real(sol.k_na[sol.running_out, 2])\n",
    "    lout, = plt.plot(kout, Ei*kout**0/c.eV, 'b.', label='out', ms=2)\n",
    "    \n",
    "plt.legend(handles=[lin, lout], loc=0)\n",
    "plt.ylim(E_range.min()/c.eV, E_range.max()/c.eV)\n",
    "plt.ylabel(r\"$E\\ \\mathrm{[eV]}$\")\n",
    "plt.xlabel(r\"$k$\")\n",
    "plt.xticks([-.5, -.25, 0, .25, .5], [r'$-\\pi/a$', r'$-\\pi/2a$', r'$0$', r'$\\pi/2a$', r'$\\pi/a$']);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Then for the negative sense:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "element = Element(block, contacted=-1)\n",
    "bs = element.bandstructure()\n",
    "\n",
    "plt.figure(dpi=150)\n",
    "bandsim.plot(bandstruct, element=element, c='k', lw=1)\n",
    "\n",
    "for Ei in E_range:\n",
    "    sol = bs.solve_E(Ei)\n",
    "    kin = np.real(sol.k_na[sol.running_in, 2])\n",
    "    lin, = plt.plot(kin, Ei*kin**0/c.eV, 'r.', label='in', ms=2)\n",
    "    kout = np.real(sol.k_na[sol.running_out, 2])\n",
    "    lout, = plt.plot(kout, Ei*kout**0/c.eV, 'b.', label='out', ms=2)\n",
    "    \n",
    "plt.legend(handles=[lin, lout], loc=0)\n",
    "plt.ylabel(r\"$E\\ \\mathrm{[eV]}$\")\n",
    "plt.xlabel(r\"$k$\")\n",
    "plt.ylim(E_range.min()/c.eV, E_range.max()/c.eV)\n",
    "plt.xticks([-.5, -.25, 0, .25, .5], [r'$-\\pi/a$', r'$-\\pi/2a$', r'$0$', r'$\\pi/2a$', r'$\\pi/a$']);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the next part we'll look at building a `Structure` and studying ballistic transport."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.12"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
