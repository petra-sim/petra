import numpy as np


def rmesh(*axes):
    return np.stack(np.meshgrid(*axes, indexing='ij'), -1)
