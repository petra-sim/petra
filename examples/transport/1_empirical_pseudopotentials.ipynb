{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 1.  Empirical Pseudopotentials\n",
    "\n",
    "In this first notebook, we use he empirical pseudopotentials method to determine the electronic structure of an armchair-edged graphene nanoribbon."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We import the needed modules. Note that, apart from the scientific python stack, we also rely on the \"Atomic Simulation Environement\" (ase)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib inline\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "\n",
    "import ase.build\n",
    "from ase.dft.kpoints import bandpath\n",
    "\n",
    "from petra import constants as c\n",
    "from petra import pwepp\n",
    "from petra.plot import plot_cell, use_latex, latex_kpoints\n",
    "\n",
    "use_latex()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 1.1. Empirical Pseudopotentials\n",
    "\n",
    "The `petra_data` package contains well known, and tested empirical pseudopotentials. We load the potentials for carbon (C) and the matching passivating pseudopotential for hydrogen (H)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "C = pwepp.pp.library['kurokawa']['C']\n",
    "H_C = pwepp.pp.library['kurokawa']['H']"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We plot the radial empirical pseudopotentials in reciprocal space:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "q = np.linspace(0., 125, 1000) / c.nm\n",
    "\n",
    "plt.figure(dpi=150)\n",
    "plt.plot(q*c.nm, C.local(q)/c.eV, 'k-', lw=1, label='C')\n",
    "plt.plot(q*c.nm, H_C.local(q)/c.eV, 'k--', lw=1, label='H on C')\n",
    "plt.legend()\n",
    "plt.xlabel(r'$q\\ \\mathrm{[nm^{-1}]}$')\n",
    "plt.ylabel(r'$V(q)\\ \\mathrm{[eV]}$')\n",
    "plt.xlim(0, 125)\n",
    "pass"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "and in real space:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "r = np.linspace(0, 1*c.nm, 5000)\n",
    "\n",
    "plt.figure(dpi=150)\n",
    "plt.plot(r/c.nm, C.radial(r)/c.eV, 'k-', lw=1, label='C')\n",
    "plt.plot(r/c.nm, H_C.radial(r)/c.eV, 'k--', lw=1, label='H on C')\n",
    "plt.legend()\n",
    "plt.xlabel(r'$r\\ \\mathrm{[nm]}$')\n",
    "plt.ylabel(r'$V(r)\\ \\mathrm{[eV]}$')\n",
    "plt.xlim(0, 0.3)\n",
    "pass"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 1.2. Atomic Structure\n",
    "\n",
    "To obtain the atomic structure for a 7 atom wide armchair graphene nanoribbon, we use the `ase` package. \n",
    "Note that 7 atoms correspond to three and a half unit cells, hence the 3.5 in the line below."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "atoms = ase.build.graphene_nanoribbon(3.5, 1, type='armchair', saturated=True, vacuum=14)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.figure(dpi=150)\n",
    "plot_cell(atoms=atoms)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 1.3. The PWEPP Calculator\n",
    "\n",
    "Using the atomic structure and pseudopotentials, we can build a PWEPP calculator:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "calc = pwepp.PWEPP(atoms=atoms, pseudopotentials={'C': C, 'H': H_C})\n",
    "# in case you already saved a previous calculation, use:\n",
    "#calc = pwepp.PWEPP('7agnr')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And plot the effective potential in real space"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Vr = calc.get_effective_potential()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.figure(dpi=150)\n",
    "plot_cell(Vr / c.eV, atoms=calc.atoms)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 1.4. Bandstructure Calculation\n",
    "\n",
    "We use `ase` to set the kpoints in the PWEPP calculator to a bandpath from $\\Gamma$ (G) to X in the first Brillouin zone.  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "bpath = bandpath('GX', atoms.cell, 20)\n",
    "calc.set(kpts=bpath)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We then have the PWEPP calculator calculate the default set of properties (energies and wavefunctions). This step is optional, since any request for a property will calculate it. However, it allows us to time the execution and save the data to a file if we wish. (See later)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%time\n",
    "calc.calculate()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The pwepp package includes a sorting algorithm for the bands that can track 1D bandstructures across bandcrossings, resulting in nicer plots."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%time\n",
    "E, idx = calc.states.sorted_bands()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally, we plot the bandstructure. We set E=0 to be the Fermi-level, as calculated by the PWEPP calculator:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Efermi = calc.get_fermi_level()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x, xticks, xlabels = bpath.get_linear_kpoint_axis()\n",
    "xticks, xlabels = latex_kpoints(xticks, xlabels)\n",
    "Eplt = (E - Efermi) / c.eV\n",
    "\n",
    "fig, ax = plt.subplots(dpi=150, figsize=(4, 5))\n",
    "ax.plot(x, Eplt, c='k', lw=1)\n",
    "ax.axhline(0., color='k', ls='--', lw=1)\n",
    "ax.set_xticks(xticks)\n",
    "ax.set_xticklabels(xlabels)\n",
    "ax.set_ylabel('$E$ (eV)')\n",
    "ax.set_xlim(x.min(), x.max())\n",
    "ax.set_ylim(-16, 2);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 1.5. Group Velocity and Wavefunctions\n",
    "\n",
    "In addition to the energies, we can also access the group velocities (calculated using the Hellmann-Feynman theorem) and wavefunctions."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "vg_knv = np.real(calc.get_group_velocity())"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, ax = plt.subplots(dpi=150, figsize=(4, 5))\n",
    "\n",
    "# the normal bandstructure\n",
    "ax.plot(x, Eplt, c='k', lw=1)\n",
    "ax.axhline(0., color='k', ls='--', lw=1)\n",
    "\n",
    "# get k, E and speed on the same grid\n",
    "x_kn = x[:, None] * Eplt**0\n",
    "vgz_kn = vg_knv[idx][:, :, 2]\n",
    "\n",
    "# plot triangles in the right direction\n",
    "s = 10 * np.sqrt(np.abs(vgz_kn) / max(vg_knv.max(), -vgz_kn.min()))\n",
    "ax.scatter(x_kn[vgz_kn > 0], Eplt[vgz_kn > 0], c='b', s=s[vgz_kn > 0], marker='>', zorder=10)\n",
    "ax.scatter(x_kn[vgz_kn < 0], Eplt[vgz_kn < 0], c='r', s=s[vgz_kn < 0], marker='<', zorder=10)\n",
    "ax.set_xticks(xticks)\n",
    "ax.set_xticklabels(xlabels)\n",
    "ax.set_ylabel('$E$ (eV)')\n",
    "ax.set_xlim(x.min(), x.max())\n",
    "ax.set_ylim(-16, 2);"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "wave = calc.states.psi(0, 0, real=True)\n",
    "\n",
    "plt.figure(dpi=150)\n",
    "plot_cell(np.abs(wave)**2, atoms=calc.atoms)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 1.6. Storing PWEPP Calculation\n",
    "\n",
    "We can easily store the entire PWEPP Calculator to disk. The following statement will write a file `7agnr.pwepp`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "calc.write('7agnr')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The entire calculator can be reconstructed from disk, e.g., in a different session, by intantiating `PWEPP('7agnr')`."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.12"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
