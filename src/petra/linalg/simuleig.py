import numpy as np
import numpy.linalg as la

from petra.util import find_degenerate


def simuleig(op, evals, evecs, tol=1e-6, ret_R=False):
    """Simultaneous eigendecomposition.

    Arguments
    ---------
    op
        Operator to diagonalize (is a function or matrix-like).
    evals : numpy.ndarray
        Eigenvalues.
    evecs : numpy.ndarray
        Eigenvectors.
    ret_R : bool, optional
        Return the rotation matrix, defaults to False.

    Returns
    -------
    evals
        The eigenvalues of the operator
    evecs
        The simultaneous eigenvectors of the operators
    R
        The rotation of the input to the output evecs (if ret_R is True)

    Example
    -------
    Given operators A and B, this can be used as follows,

    evals_A, evecs = np.eig(A)
    evals_B, evecs = simuleig(B, evals_A, evecs)

    where evecs are the simultaneous eigenvectors.

    """
    groups = find_degenerate(evals, tol)
    N = evals.size
    R = np.eye(N, dtype=complex)
    evals_op = np.zeros(N, dtype=complex)

    for idx in groups:
        subspace = evecs[:, idx]
        if callable(op):
            subspace_op = subspace.conj().T @ op(subspace)
        else:
            subspace_op = subspace.conj().T @ op @ subspace
        subspace_eval, subspace_R = la.eig(subspace_op)
        R[np.ix_(idx, idx)] = subspace_R
        evals_op[idx] = subspace_eval

    evecs_op = evecs @ R

    if ret_R:
        return evals_op, evecs_op, R
    return evals_op, evecs_op
