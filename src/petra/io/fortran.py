import numpy as np
from struct import unpack


class FortranError(IOError):
    pass


def read_record(fd, endian='<', force=False):
    raw = fd.read(4)
    if len(raw) != 4:
        raise EOFError
    reclen = int(unpack(endian + 'i', raw)[0])
    raw = fd.read(reclen)
    bkscp = int(unpack(endian + 'i', fd.read(4))[0])
    if not force and reclen != bkscp:
        raise FortranError('Record not closed correctly')
    return raw


def write_records(fd, data, endian='<'):
    shape = (data.shape[0],)
    data_dtype = data.dtype.newbyteorder(endian)
    dtype = [('reclen', endian + 'i4'),
             ('data', str(data_dtype), data.shape[1]),
             ('bkscp', endian + 'i4')]
    reclen = data_dtype.itemsize * data.shape[1]
    mm = np.memmap(fd, mode='w+', dtype=dtype, shape=shape)
    mm['reclen'][:] = reclen
    mm['data'][:] = data
    mm['bkscp'][:] = reclen
    mm.flush()
    pass
