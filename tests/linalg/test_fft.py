import numpy as np


def test_fftw_fft():
    from petra.linalg.fft import FFTHelper, FFTWHelper
    A = np.random.random((20, 10, 8))
    hw = FFTWHelper(A.shape)
    h = FFTHelper(A.shape)
    h.array = A
    hw.array = A
    h.r_to_q()
    hw.r_to_q()
    assert(np.allclose(hw.array, h.array))
    h.array = A
    hw.array = A
    h.q_to_r()
    hw.q_to_r()
    assert(np.allclose(hw.array, h.array))


def test_fftw_inversion():
    from petra.linalg.fft import FFTWHelper
    A = np.random.random((20, 10, 8))
    h = FFTWHelper(A.shape)
    h.array = A
    h.r_to_q()
    assert(not np.allclose(A, h.array))
    h.q_to_r()
    assert(np.allclose(A, h.array))


def test_fft_inversion():
    from petra.linalg.fft import FFTHelper
    A = np.random.random((20, 10, 8))
    h = FFTHelper(A.shape)
    h.array = A
    h.r_to_q()
    assert(not np.allclose(A, h.array))
    h.q_to_r()
    assert(np.allclose(A, h.array))
