from __future__ import annotations
from typing import TYPE_CHECKING, Dict, List
from functools import cached_property

import numpy as np
import numpy.linalg as la

from petra.transport.node import Node, NodeList, PeriodicNode
from petra.transport.bandstructure import Bandstructure
from petra.util import flatten_last
from petra.util.lazy import invalidate_cached_property
from petra import constants as c

if TYPE_CHECKING:
    from petra.transport.block import Block


class Element:

    def __init__(self, block: Block, contacted=None, position=None):
        self.block = block
        self.dof = self.block.nbasis
        self._position = np.zeros(3)
        if position is not None:
            self.position = position

        self.nodes = NodeList(Node(self, self.position + pos)
                              for pos in self.block.node_pos)

        if contacted is not None:
            for source, dest in block.periodic_pairs(contacted):
                self.nodes[dest] = PeriodicNode(self,
                                                self.nodes[dest].position,
                                                origin=self.nodes[source])

        self.V_dxyz: Dict[str, np.ndarray] = {}

    def __getattr__(self, name):
        return getattr(self.block, name)

    def theta(self, R):
        x = la.solve(self.cell.a.T, R)
        rx = np.round(x).astype(int)
        if not np.allclose(x, rx, atol=1e-12, rtol=0):
            raise ValueError("The periodicity has not been recogised")
        return rx

    @property
    def period_v(self):
        if not any(self.nodes.periodic):
            raise ValueError('This element has no periodicity')
        periods = np.unique([node.period for node in self.nodes.periodic],
                            axis=0)
        if periods.shape[0] != 1:
            raise ValueError('This element\'s periodicity makes no sense.')
        return periods[0]

    @property
    def sense_v(self):
        return np.sign(self.period_v)

    @property
    def HM_periodic(self):
        zero3 = np.zeros(3)

        for i0, node0 in enumerate(self.nodes):
            delta0 = getattr(node0, 'period', zero3)
            node0 = getattr(node0, 'origin', node0)

            for i1, node1 in enumerate(self.nodes):
                delta1 = getattr(node1, 'period', zero3)
                node1 = getattr(node1, 'origin', node1)

                theta = self.theta(delta0 - delta1)

                yield (node0, node1, self.H_iinn[i0, i1], self.M_iinn[i0, i1], theta)

    @property
    def HM(self):
        for i0, node0 in enumerate(self.nodes):
            for i1, node1 in enumerate(self.nodes):
                yield (node0, node1, self.H_iinn[i0, i1], self.M_iinn[i0, i1])

        # add the contribution of the periodic Element to every periodic Node
        for node0 in self.nodes.periodic:
            i0 = self.nodes.index(node0.origin)
            for node1 in self.nodes.periodic:
                i1 = self.nodes.index(node1.origin)
                yield (node0, node1, self.H_iinn[i0, i1], self.M_iinn[i0, i1])

    @property
    def HM0(self):
        for i0, node0 in enumerate(self.nodes):
            for i1, node1 in enumerate(self.nodes):
                yield (node0, node1, self.H_iinn[i0, i1], self.M_iinn[i0, i1])

    def iter_V(self):
        for i0, node0 in enumerate(self.nodes):
            for i1, node1 in enumerate(self.nodes):
                yield (node0, node1, self.V_iinn[i0, i1])

        # add the contribution of the periodic Element to every periodic Node
        for node0 in self.nodes.periodic:
            i0 = self.nodes.index(node0.origin)
            for node1 in self.nodes.periodic:
                i1 = self.nodes.index(node1.origin)
                yield (node0, node1, self.V_iinn[i0, i1])

    @property
    def HM_contact(self):
        exclude = set(node.origin for node in self.nodes.periodic)

        for node0 in self.nodes.periodic:
            i0 = self.nodes.index(node0.origin)
            delta = node0.period
            theta = self.theta(delta)

            for i1, node1 in enumerate(self.nodes):
                if node1 in exclude:
                    continue
                yield (node0, node1, self.H_iinn[i0, i1], self.M_iinn[i0, i1], theta)

    @property
    def has_edge(self):
        return any(self.nodes.edge)

    @property
    def position(self):
        return self._position

    @position.setter
    def position(self, newpos):
        self._position = newpos
        for node, relpos in zip(self.nodes, self.block.node_pos):
            node.position = newpos + relpos

    @property
    def atoms(self):
        atoms = self.block.atoms.copy()
        atoms.positions += self.position / c.Ang
        return atoms

    def V_xyz(self):
        if not self.V_dxyz:
            return None
        return sum(V for V in self.V_dxyz.values())

    @cached_property
    def V_iinn(self):
        V_xyz = self.V_xyz()
        if V_xyz is None:
            return np.zeros_like(self.block.H_iinn)
        return self.block.scalar_field_matrix(V_xyz)

    @property
    def H_iinn(self):
        return self.block.H_iinn + self.V_iinn

    @property
    def r_xyzv(self):
        return self.block.r_xyzv + self.position

    @property
    def r_rv(self):
        return self.r_xyzv.reshape((-1, 3))

    @property
    def r_density_rv(self):
        return self.block.r_density_rv + self.position

    def set_potential(self, field, name='default'):
        if callable(field):
            field = field(self.r_xyzv)
        self.V_dxyz[name] = field.reshape(self.r_xyzv.shape[:3])
        self.invalidate_potential()

    def invalidate_potential(self):
        invalidate_cached_property(self, 'V_iinn')

    def clear_potential(self, name):
        if name == '*':
            self.V_dxyz = {}
        else:
            del self.V_dxyz[name]
        self.invalidate_potential()

    def __repr__(self):
        title = '{}(nodes='.format(self.__class__.__name__)
        space = ' ' * len(title)
        nodes = ('\n' + space).join(repr(self.nodes).split('\n'))
        return title + nodes + ')'

    def bandstructure(self):
        return Bandstructure(self)

    @property
    def r_boundary_rv(self):
        o = self.position
        x, y, z = self.cell.a
        return [o, o + x, o + y, o + x + y,
                o + z, o + x + z, o + y + z, o + x + y + z]

    def psi_xyz(self, c_in):
        ''' Calculates $\\psi(x,y,z)$ from the element coeficients.
        '''
        phi_inxyz = self.block.phi_inxyz
        return np.tensordot(c_in, phi_inxyz, 2)

    def psi_r(self, c_in, full=False):
        ''' Calculates $\\psi(r)$ from the element coeficients.
        '''
        if full:
            phi_inr = flatten_last(self.block.phi_inxyz, 3)
        else:
            phi_inr = self.block.phi_density_inr
        return np.tensordot(c_in, phi_inr, 2)

    def dpsi_vr(self, c_in, full=False):
        ''' Calculates $\\partial_r \\psi(r)$ from the element coeficients.
        '''
        if full:
            phi_vinr = flatten_last(self.block.dphi_vinxyz, 3)
        else:
            phi_vinr = self.block.dphi_density_vinr
        return np.tensordot(c_in, phi_vinr, 2)

    def psi2_xyz(self, c_in):
        ''' Calculates $|\\psi(r)|^2$ from the element coeficients.
        '''
        psi_xyz = self.psi_xyz(c_in)
        return np.real(psi_xyz.conj() * psi_xyz)

    def psi2_r(self, c_in, full=False):
        ''' Calculates $|\\psi(r)|^2$ from the element coeficients.
        '''
        psi_r = self.psi_r(c_in, full=full)
        return np.real(psi_r.conj() * psi_r)

    def probability_current_vr(self, c_in, full=False):
        ''' Calculates the probability current from the element coeficients.
        '''
        psi_r = self.psi_r(c_in, full=full)
        dpsi_vr = self.dpsi_vr(c_in, full=full)
        return np.imag(psi_r.conj() * dpsi_vr)

    def coefs_to_dmat(self, coef_sn, weight_s):
        return (coef_sn.conj().T * weight_s) @ coef_sn

    def coef_to_dmat(self, coef_n):
        return coef_n[:, None].conj() @ coef_n[None, :]

    def dmat_to_current_density(self, dmat_nn):
        ''' Calculates the current density from the element density matrix.

        The density matrix has dimensions [#nodes $\\times$ #basis, #nodes $\\times$ #bases]
        '''
        r_shape = self.block.phi_density_inr.shape[-1]
        phi_nr = self.block.phi_density_inr.reshape((-1, r_shape))
        dphi_vnr = self.block.dphi_density_vinr.reshape((3, -1, r_shape))
        dJ_vr = np.sum(phi_nr.conj() * (dmat_nn @ dphi_vnr), axis=1)
        return np.imag(dJ_vr)

    def dmat_to_density(self, dmat_nn):
        ''' Calculates the density from the element density matrix.

        The density matrix has dimensions [#nodes $\\times$ #basis, #nodes $\\times$ #bases]
        '''
        r_shape = self.block.phi_density_inr.shape[-1]
        phi_nr = self.block.phi_density_inr.reshape((-1, r_shape))
        rho_r = np.sum(phi_nr.conj() * (dmat_nn @ phi_nr), axis=0)
        return np.real(rho_r)


class ElementList(List[Element]):

    @property
    def contacts(self):
        return ElementList([el for el in self if el.has_edge])

    def __repr__(self):
        title = 'ElementList(['
        space = ' ' * len(title)
        return title + ('\n' + space).join(
            [line for el in self for line in repr(el).split('\n')]) + '])'
