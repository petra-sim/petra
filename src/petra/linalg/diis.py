import warnings
from typing import Optional
from collections import deque

import numpy as np
import scipy.linalg as la

from petra.util import log

logger = log.getLogger(__name__)


def diis(x, r, accurate=True):
    '''The DIIS method

    Note: keep accuract=True unless you know what you are doing!
    See _diis_accurate and _diis_direct for details.

    References:
        - P. Pulay
          Journal of Computational Chemistry, 3 (4), 556-560 (1982)
        - R. Shepard and M. Minkoff
          Molecular Physics, 105, 19-22 (2007)
          doi: 10.1080/00268970701691611
        - H.H.B. Sorensen and O. Osterby
          AIP Conference Proceedings 1168, 468 (2009)
          doi: 10.1063/1.3241499
    '''

    if len(x) == 1:
        return x

    X, R = np.asarray(x), np.asarray(r)

    # don't bother if last solution is perfect
    if la.norm(R[-1]) == 0:
        return X[-1]

    if accurate:
        c = _diis_accurate(R)
    else:
        c = _diis_direct(R)

    maxc = np.abs(c).max()
    if np.any(maxc > 2):
        logger.warning(f'DIIS coefficient high: {maxc}')

    return c @ X


def _diis_accurate(R):
    '''A numerically accurate implementation of DIIS

    Solving the minimization of the error vectors
        min || \\sum_i c_i R_i || with \\sum_i c_i = 1
    using least squares without numerical roundoff errors.

    For details, see:
        R. Shepard and M. Minkoff
        Molecular Physics, 105, 19-22 (2007)
        doi: 10.1080/00268970701691611
    '''

    E = R[:-1] - R[-1, None, :]

    c, _, _, _ = la.lstsq(E.T, -R[-1], lapack_driver='gelsy')
    return np.concatenate([c, [1 - np.sum(c)]])


def _diis_direct(R):
    '''A faster implementation of DIIS

    Solving the minimization of the error vectors
        min || \\sum_i c_i R_i || with \\sum_i c_i = 1
    in the subspace R^\\dagger R with Lagrange multipliers.

    This method tries direct solution first, but falls back to least
    squares when RR is rank-deficient, i.e. when there is linear dependence
    in the set of error vectors {R_i}.

    Note that, while fast, this method suffers from numerical roundoff errors.
    Its numerical accuracy is only the square root of the machine epsilon.

    For details on accuracy, see:
        R. Shepard and M. Minkoff
        Molecular Physics, 105, 19-22 (2007)
        doi: 10.1080/00268970701691611
    '''

    RR = R.conj() @ R.T

    # add Lagrange multiplier
    b = np.block([np.zeros(RR.shape[0]), 1])
    A = np.block([[RR, np.ones((RR.shape[0], 1))],
                  [np.ones((1, RR.shape[1])), 0.]])

    with warnings.catch_warnings():
        warnings.filterwarnings('error')
        try:
            c = la.solve(A, b)

        except RuntimeWarning:  # probably linearly dependent
            # try solving using least squares (SVD)
            logger.warning("DIIS fallback to SVD")

            c, _, rank, s = la.lstsq(A, b, rcond=None)

            if rank < RR.shape[0] + 1:
                logger.info(f'DIIS (SVD) rank deficient: '
                            f'{rank - 1} < {RR.shape[0]}')
                logger.info(f'DIIS (SVD) singular values: '
                            f'{s}')
    return c[:-1]


class IterativeSubspace:
    def __init__(self, size: Optional[int]):
        self.size = size
        self.reset()

    def reset(self):
        self.X = deque([], maxlen=self.size)
        self.R = deque([], maxlen=self.size)

    def update(self, x, r):
        self.X.append(x)
        self.R.append(r)

        if len(self.X) <= 1:
            return self.X[-1]
        x = diis(self.X, self.R)
        if x is None:  # restart
            x = self.X[-1]
            self.X.clear()
            self.R.clear()
        return x

    def __len__(self):
        return len(self.X)
