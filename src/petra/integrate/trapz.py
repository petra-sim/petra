import warnings

import numpy as np

from petra.util import mpi
from petra.linalg import rms
from petra.integrate import IntegrationWarning, IntegrationResult


def _get_first(val):
    if isinstance(val, tuple):
        return val[0]
    return val


def integrate(func, a, b, max_eval=10, init_eval=1, atol=0., rtol=0.,
              verbose=False):
    integral = 0.
    x, f = [], []

    max_iter = int(np.log2(max_eval / init_eval)) + 1
    N = mpi.comm.size * init_eval

    print('iter  #eval  error_abs  error_rel', flush=True)

    abs_err = np.inf
    converged = False
    for step in range(max_iter):
        dx = (b - a) / N
        iglobal = np.r_[1:N:2] if step > 0 else np.r_[:N]
        ilocal = iglobal[mpi.comm.rank::mpi.comm.size]
        xlocal = a + dx * ilocal

        local_change = - integral / (2 * mpi.comm.size)
        for xi in xlocal:
            fi = func(xi)
            local_change += dx * _get_first(fi)

            x.append(xi)
            f.append(fi)

        global_change = mpi.allreduce(np.atleast_1d(local_change))
        integral += global_change

        abs_err = rms(global_change)
        rel_err = abs_err / rms(integral)

        if verbose:
            print('{:4d}  {:5d}  {:9.2f}  {:9.2f}'
                  .format(step, len(x), np.log10(abs_err), np.log10(rel_err)),
                  flush=True)

        if step and abs_err <= atol and rel_err <= rtol:
            converged = True
            break

        N *= 2

    if not converged:
        warnings.warn('Integrator did not converge, results are not accurate.',
                      IntegrationWarning)
    if verbose:
        print('converged' if converged else 'not converged', flush=True)

    weights = np.full(len(x), dx)

    return IntegrationResult(integral, abs_err, converged,
                             weights, x, f)
