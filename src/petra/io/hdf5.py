from typing import Any, Dict, NamedTuple, Callable, Optional
import warnings


not_stored_msg = "attribute '{}' on '{}' object will not be stored"


class H5Warning(UserWarning):
    pass


class H5Store:
    _h5_tag: Optional[str] = None
    _h5_attrs: Dict[str, Any] = {}
    _h5_policy: str = 'warn'

    @classmethod
    def _h5_iter_attrs(cls):
        for name, attr in cls._h5_attrs.items():
            if not isinstance(attr, H5Attr):
                raise ValueError(
                    'HDF5 Attribute definition for {} is not recognised'
                    .format(name))
            if attr.store:
                yield name, attr

    @classmethod
    def _h5_verify(cls, group):
        if cls._h5_tag is None:
            return
        if group.attr.get('tag') != cls._h5_tag:
            raise IOError('HDF5 group does not have the correct tag {}'
                          .format(cls._h5_tag))

    def _write(self, group):
        if self._h5_tag is not None:
            group.attr['tag'] = self._h5_tag

        for name, attr in self._h5_iter_attrs():
            if attr.required:
                value = getattr(self, name)
            else:
                value = getattr(self, name, None)
            attr.write(group, name, value)

    @classmethod
    def _read(cls, group):
        cls._h5_verify(group)

        obj = cls.__new__(cls)
        for name, attr in cls._h5_iter_attrs():
            if attr.required:
                value = attr.read(group, name)
                setattr(obj, name, value)
            else:
                value = attr.read(group, name, optional=True)
                if value is not None:
                    setattr(obj, name, value)

        obj._post_read()
        return obj

    def _post_read(self):
        pass

    def __setattr__(self, name, value):
        if self._h5_policy != 'ignore' and name not in self._h5_attrs:
            msg = not_stored_msg.format(name, type(self).__name__)
            if self._h5_policy == 'warn':
                warnings.warn(msg, H5Warning)
            else:
                raise AttributeError(msg)
        super().__setattr__(name, value)


def _generic_write(group, name, value):
    if isinstance(value, H5Store):
        vgroup = group.create_group(name)
        value._write(vgroup)
    else:
        group[name] = value


def _generic_read(group, name, optional=False):
    if optional:
        return group.get(name, None)
    return group[name]


class H5Attr(NamedTuple):
    store: bool = True
    required: bool = True
    read: Optional[Callable] = _generic_read
    write: Optional[Callable] = _generic_write


Store = H5Store
attr = H5Attr

ignore = H5Attr(store=False, required=False, read=None, write=None)


def write_atoms(group, name, atoms):
    from ase.io.jsonio import encode

    group = group.create_group(name)

    group['positions'] = atoms.get_positions()
    group['cell'] = atoms.get_cell()
    group['pbc'] = atoms.pbc
    group['numbers'] = atoms.numbers

    if atoms.constraints:
        if all(hasattr(c, 'todict') for c in atoms.constraints):
            group['constraints'] = encode(atoms.constraints)

    if atoms.has('masses'):
        group['masses'] = atoms.get_masses()
    if atoms.has('tags'):
        group['tags'] = atoms.get_tags()
    if atoms.has('momenta'):
        group['momenta'] = atoms.get_momenta()
    if atoms.has('initial_magmoms'):
        group['magmoms'] = atoms.get_initial_magnetic_moments()
    if atoms.has('initial_charges'):
        group['charges'] = atoms.get_initial_charges()


def read_atoms(group, name, optional=False):
    from ase import Atoms
    from ase.io.jsonio import decode
    from ase.constraints import dict2constraint

    if optional:
        group = group.get(name, None)
        if group is None:
            return None
    else:
        group = group[name]

    constraints = decode(group.get('constraints', '[]'))

    return Atoms(
        positions=group['positions'],
        numbers=group['numbers'],
        cell=group['cell'],
        masses=group.get('masses'),
        pbc=group['pbc'],
        info=group.get('info'),
        constraint=[dict2constraint(d) for d in constraints],
        momenta=group.get('momenta'),
        magmoms=group.get('magmoms'),
        charges=group.get('charges'),
        tags=group.get('tags'),
    )


def atoms_attr(store=True, required=True):
    return attr(store, required, read_atoms, write_atoms)
