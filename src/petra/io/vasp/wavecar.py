from pathlib import Path
from struct import unpack

import numpy as np
import scipy.linalg as la
from scipy import fft

from petra.io.vasp.constants import eV, Ang
from petra.io.vasp.util import CommonFilePath
from petra.periodiccell import PeriodicCell


def vasp_version(version):
    if version == 53300:  # VASP.5.3
        precision = 'single'
        span_records = True
    elif version == 53310:  # VASP.5.3 double precision
        precision = 'double'
        span_records = True
    elif version == 45200:
        precision = 'single'
        span_records = False
    else:  # double precision
        precision = 'double'
        span_records = False
    return precision, span_records


class VaspPW:
    '''VASP Plane Wave implementation

    '''
    def __init__(self, cell, Ecut):
        self.cell = cell
        self.Ecut = Ecut
        self.set_antialiassing_nG()

    def set_nG(self, nG):
        ig = [np.mod(np.arange(dim) + dim // 2, dim) - dim // 2 for dim in nG]
        ig = np.meshgrid(*ig, indexing='ij')

        self.iG = np.reshape(ig, (3, -1), order='F').T
        self.G = self.cell.reciprocal.direct_to_cartesian(self.iG)
        self.nG = tuple(nG)
        self.shape = self.nG

    def set_smallest_nG(self):
        self.set_nG(self.smallest_nG())

    def set_antialiassing_nG(self):
        self.set_nG(self.antialiassing_nG())

    def smallest_nG(self):
        Gcut = np.sqrt(2 * self.Ecut)
        L = la.norm(self.cell.reciprocal.a_inv, axis=0)
        nbmax = 2 * np.array(np.ceil(Gcut * L), dtype=int)
        return nbmax

    def antialiassing_nG(self):
        return 2 * self.smallest_nG()

    def Tcut(self, kb):
        '''Kinetic energy'''
        k = self.cell.reciprocal.to_cartesian(kb)
        return np.sum((self.Gcut(kb) + k[None, :])**2, axis=1) / 2

    def T(self, kb):
        '''Kinetic energy'''
        k = self.cell.reciprocal.to_cartesian(kb)
        return np.sum((self.G + k[None, :])**2, axis=1) / 2

    def mask(self, kb):
        '''Mask is True if G vector is within the cutoff'''
        return self.T(kb) < self.Ecut

    def Gcut(self, kb):
        return self.G[self.mask(kb)]

    def nGcut(self, kb):
        return np.sum(self.mask(kb))

    def to_flat(self, full):
        return np.reshape(full, full.shape[:-3] + (-1,), order='F')

    def to_3D(self, full):
        return np.reshape(full, full.shape[:-1] + self.nG, order='F')

    @property
    def G_xyzv(self):
        G_vxyz = self.to_3D(self.G.T)
        return np.moveaxis(G_vxyz, 0, -1)

    def mask_indices(self, kb):
        return tuple(self.iG[self.mask(kb)].T)

    def to_full(self, kb):
        iG = self.mask_indices(kb)

        def transform(coefs_xg):
            shape = coefs_xg.shape[:-1]
            full = np.zeros(shape + self.nG, dtype=complex)
            full[(...,) + iG] = coefs_xg
            return full
        return transform

    def to_cutoff(self, kb):
        iG = self.mask_indices(kb)

        def transform(full_xuvw):
            return full_xuvw[(...,) + iG]
        return transform

    def change_cutoff(self, kbi, kbf):
        maski = self.mask(kbi)
        maskf = self.mask(kbf)
        N = self.nGcut(kbf)

        def transform(psii):
            psif = np.zeros(N, dtype=complex)
            psif[maski[maskf]] = psii[maskf[maski]]
            return psif
        return transform


class WAVECAR(CommonFilePath):

    def __init__(self, path='', mode='r', endian=None, read=True):
        self.path = Path(path)
        self.mode = mode
        self.endian = endian
        if read:
            self.read()

    def read(self):
        self.read_header()
        self.read_data()

    def read_header(self):
        if self.endian is None:
            self.endian = self.find_endian()

        data = self.memmap(0, 3)
        self.reclen = int(data[0])
        self.spin = int(data[1])
        self.version = int(data[2])

    def read_data(self):
        self.precision, self.span_records = vasp_version(self.version)

        if self.spin != 1:
            raise IOError('Cannot read WAVECAR that has spin '
                          'set to {} (can only read spin == 1)'
                          ''.format(self.spin))
        if self.span_records:
            raise IOError('Cannot read WAVECAR of VASP >= 5.3 '
                          'because SPAN_RECORDS is true')

        _real = self.endian + 'f8'
        _complex = self.endian + 'c16'
        _complex_size = {'single': 8, 'double': 16}[self.precision]
        _complex_prec = self.endian + 'c{}'.format(_complex_size)

        offset = self.reclen
        dtype = [('nk', _real),
                 ('nband', _real),
                 ('Ecut', _real),
                 ('a', _real, (3, 3)),
                 ('Efermi', _real)]
        nk, nband, Ecut, a, Efermi = self.memmap(offset, 1, dtype=dtype)[0]
        self.nk = int(nk)
        self.nband = int(nband)
        self.Ecut = Ecut * eV
        self.a = a * Ang
        self.Efermi = Efermi * eV

        offset = 2 * self.reclen
        self.max_nG = self.reclen // _complex_size
        dtype = [('nG', _real),
                 ('k', _real, (3,)),
                 ('bands', [('E', _complex),
                            ('fill', _real)], (self.nband,)),
                 ('empty', _real, (self.reclen // 8 -
                                   (1 + 3 + self.nband * 3))),
                 ('wave', _complex_prec, (self.nband, self.max_nG))]

        padding = self.reclen - _complex_size * self.max_nG
        if padding:
            raise ValueError('The record length is not a multiple'
                             ' of the precision')
        self.data = None
        if self.nk > 0:
            self.data = self.memmap(offset, self.nk, dtype=dtype)

        if np.any(self.a):
            self.cell = PeriodicCell(self.a)
            self.basis = VaspPW(self.cell, self.Ecut)

        return self

    def memmap(self, offset, shape, dtype=None):
        if dtype is None:
            dtype = self.endian + 'f8'
        return np.memmap(self.fname, mode=self.mode, offset=offset,
                         dtype=dtype, shape=shape, order='F')

    @classmethod
    def create(cls, nG_guess, a, Ecut,
               spin=1, version=45200, nk=0, nband=0, Efermi=0.,
               path='', endian=''):

        wavecar = cls(path, mode='r+', endian=endian, read=False)

        precision, span_records = vasp_version(version)

        reclen = int(nG_guess * 1.2) * {'single': 8,
                                        'double': 16}[precision]

        write_values(wavecar.fname, 0, [reclen, spin, version], endian)

        wavecar.read_header()
        wavecar.set(a=a, nk=nk, nband=nband, Ecut=Ecut, Efermi=Efermi)
        return wavecar

    def set(self, a=None, nk=None, nband=None, Ecut=None, Efermi=None,
            read=True):
        assert('w' in self.mode or '+' in self.mode)
        alist = ((self.a if a is None else a) / Ang).flatten('C').tolist()
        values = ([self.nk if nk is None else nk,
                   self.nband if nband is None else nband,
                   (self.Ecut if Ecut is None else Ecut) / eV] +
                  alist +
                  [(self.Efermi if Efermi is None else Efermi) / eV])
        write_values(self.fname, self.reclen, values, endianness=self.endian)
        if read:
            return self.read()

    def add(self, ik, k, E, wave=None, fill=0.):
        if self.mode == 'r':
            raise IOError('Cannot write a read only WAVECAR,'
                          ' reopen with mode=\'r+\'')

        if wave is not None:
            assert(wave.shape[0] == E.size)
            assert(wave.shape[1] <= self.max_nG)
        if self.nk == 0 and self.nband == 0:
            self.set(nk=ik + 1, nband=E.size)
        assert(E.size == self.nband)
        if (ik >= self.nk):
            self.set(nk=ik + 1)

        self.data['nG'][ik] = float(wave.shape[1])
        self.data['k_a'][ik] = self.a @ k / (2 * np.pi)
        self.data['bands']['E'][ik, :] = E / eV
        self.data['bands']['fill'][ik, :] = fill
        if wave is not None:
            self.data['wave'][ik, :, :wave.shape[1]] = wave
        self.data.flush()

    def find_endian(self):
        '''Endianness used in the WAVECAR'''
        with open(self.fname, 'rb') as f:
            breclen = f.read(8)
        for endian in ('>', '<'):
            reclen = unpack(endian + 'd', breclen)[0]
            if reclen - int(reclen) == 0.0:
                return endian
        raise IOError('Could not determine endianness of WAVECAR.')

    @property
    def nGcut(self):
        '''Number of plane waves of wavefunctions'''
        return np.array(self.data['nG'], dtype=int)

    @property
    def k_ka(self):
        '''Wave vector k in direct coordinates (reciprocal basis)'''
        return self.data['k']

    @property
    def k_kv(self):
        '''Wave vector k in cartesian coordinates'''
        return self.cell.reciprocal.to_cartesian(self.k_ka)

    @property
    def E_complex(self):
        '''Complex eigen energies'''
        return self.data['bands']['E'] * eV

    @property
    def E_kn(self):
        '''(Real part of) eigen energies'''
        return np.real(self.E_complex)

    @property
    def fill(self):
        '''The occupation level of the band'''
        return self.data['bands']['fill']

    def wave(self, ik, iband):
        '''Plane wave components of the wavefunction as stored'''
        return self.data['wave'][ik, iband, :self.nGcut[ik]]

    # convenient access to basis properties

    @property
    def b_av(self):
        '''Reciprocal lattice basis vectors'''
        return self.cell.reciprocal.a

    @property
    def iG(self):
        '''Indices of the G vectors'''
        return self.basis.iG

    @property
    def nG(self):
        '''Total number of G vectors'''
        return self.basis.nG

    @property
    def G(self):
        '''Reciprocal lattice G vectors'''
        return self.basis.G

    def T(self, ik):
        '''Kinetic energy'''
        return self.basis.T(self.k_ka[ik])

    def Gmask(self, ik):
        '''Mask is True if G vector is within the cutoff'''
        return self.basis.mask(self.k_ka[ik])

    # calculated properties

    def fullwave(self, ik, iband, operator=None, offset=None,
                 envelope=True, fixnorm=False):
        '''All plane wave components of the wavefunction.

        Properties:
         - Includes zero-padding for anti-aliassing.
         - Can be used to convert to FFT.
         - Are of uniform size for different wave vectors k'''

        derivatives = ('dx', 'dy', 'dz')
        if operator not in (None, 'T', 'kinetic') + derivatives:
            err = 'Operator {} is not implemented'
            raise NotImplementedError(err.format(operator))

        wave = self.wave(ik, iband)
        if fixnorm:
            wave = wave / np.sqrt(np.vdot(wave, wave))

        k_v = self.k_kv[ik]
        k_a = self.k_ka[ik]

        mask = self.Gmask(ik)
        iG = self.iG[mask]
        if iG.shape[0] != wave.size:
            raise IOError('The size of the WAVECAR wavefunction ({}) is'
                          ' different from the expected size ({}).'
                          ' The calculated cutoff-sphere must be wrong.'
                          ''.format(wave.size, iG.shape[0]))

        if offset is not None:
            G = self.G[mask]
            wave = wave * np.exp(1j * G @ offset)

        if operator == 'kinetic' or operator == 'T':
            wave = wave * self.T(ik)[mask]
        elif operator in derivatives:
            idx = derivatives.index(operator)
            q = self.G[mask, idx] + envelope * k_v[idx]
            wave = wave * 1j * q

        return self.basis.to_full(k_a)(wave)

    def realwave(self, ik, iband, operator=None, envelope=True,
                 offset=None, replicate=None, fixnorm=False):
        '''Wavefunctions in real space basis psi(r),
        or periodic part of u(r) if envelope is set to False'''

        waveG_xyz = self.fullwave(ik, iband,
                                  operator=operator,
                                  offset=offset,
                                  envelope=envelope,
                                  fixnorm=fixnorm)
        wave_xyz = fft.ifftn(waveG_xyz) * np.sqrt(waveG_xyz.size)

        if replicate is not None:
            wave_xyz = np.tile(wave_xyz, replicate)
        if envelope:
            wave_xyz *= self.envelope(ik, offset, replicate)
        return wave_xyz

    def r(self, offset=None, replicate=None):
        offset = np.array([0., 0., 0.] if offset is None else offset)
        replicate = np.array([1, 1, 1] if replicate is None else replicate)
        cell = self.cell.scaled(replicate)
        shape = self.nG * replicate
        return cell.grid(shape) + offset[None, None, None, :]

    def envelope(self, ik, offset=None, replicate=None):
        '''Envelope function f(r)=exp(ik.r)'''
        r = self.r(offset=offset, replicate=replicate)
        return np.exp(1j * r @ self.k_kv[ik])

    def dHdk(self, ik):
        '''Derivative of the Hamiltonian to wave vector k'''
        return self.G[self.Gmask(ik)] + self.k_kv[ik, None, :]

    def dEdk(self):
        '''Derivative of energy E to wave vector k using Hellmann-Feynman'''
        dEdk = np.empty((self.nk, self.nband, 3), dtype=float)
        for ik in range(self.nk):
            dHdk = self.dHdk(ik)
            for iband in range(self.nband):
                wave = self.wave(ik, iband)
                Gk_wave = dHdk * wave[:, None]
                norm = np.real(wave.conj() @ wave)
                dEdk[ik, iband, :] = np.real(wave.conj() @ Gk_wave) / norm
        return dEdk


def write_values(fname, offset, values, endianness):
    shape = len(values)
    values = np.array(values, dtype='f8')
    try:
        data = np.memmap(fname, mode='r+', offset=offset,
                         dtype=endianness + 'f8',
                         shape=shape, order='F')
    except FileNotFoundError:
        data = np.memmap(fname, mode='w+', offset=offset,
                         dtype=endianness + 'f8',
                         shape=shape, order='F')
    data[:] = values
    data.flush()
    del data
