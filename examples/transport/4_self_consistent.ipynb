{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 4. Self-consistent Schrödinger-Poisson"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib inline\n",
    "\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "\n",
    "from petra import constants as c\n",
    "from petra.plot import use_latex\n",
    "from petra.util import log\n",
    "from petra.transport import Block, SimpleHomoStructure\n",
    "\n",
    "use_latex()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 2.1. Initialize Logging\n",
    "\n",
    "In this part, we will use the logging capabilities that are implemented in petra. This allows us to follow what the code is doing and is a good way to catch and diagnose a problem early on. There are three main ways of logging in petra:\n",
    "\n",
    "- We can log to the terminal/screen by calling `petra.log.to_stdout()` or `petra.log.to_stderr()`. When running in a terminal or notebook (as done here), this method is probably the easiest to use as we can follow each step in real time.\n",
    "- We can log to memory by calling `petra.log.to_memory()`. This method is provided as a convenience when running in a terminal or notebook. It returns a file-like object that allows you to view the entire log and post process it at will, just like file-based logging, but without saving anything to disk.\n",
    "- We can log to a file by calling `petra.log.to_file('myfile.log')`. This is most suited for unattended / headless runs where we can't connect a terminal.\n",
    "\n",
    "In this introduction, we will use logging a file so you can follow the progress there."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "log.to_file('4_self_consistent.log', clear=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 4.2. Defining the Device\n",
    "\n",
    "#### 4.2.1. Build the Structure\n",
    "\n",
    "As in the previous parts, we build our `Block` and `Structure`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "block = Block.from_calc('7agnr.pwepp')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "structure = SimpleHomoStructure(block, L=35*c.nm)\n",
    "structure.write_atoms('4_atoms.vtu')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We specify the occupation of the contacts as before:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from petra.transport import EquilibriumElectronHoleOccupation\n",
    "\n",
    "occupation = EquilibriumElectronHoleOccupation(structure,\n",
    "                                               mu_c=(0.0 * c.eV, -0.1 * c.eV),\n",
    "                                               T=300 * c.K)\n",
    "structure.set_occupation(occupation)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To couple the quantum system to electrostatics, we build a device"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from petra.device import Device\n",
    "\n",
    "device = Device(structure)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### 4.2.1 Define Regions\n",
    "\n",
    "Instead of defining an arbitrary potential, we will define metal contacts (gate) and doping profiles. We first define some useful dimensions:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "L = device.structure.r_boundary_rv[:, 2].max()\n",
    "\n",
    "Lgate = 5 * c.nm\n",
    "Lsource = (L - Lgate) / 2\n",
    "Ldrain = Lsource\n",
    "\n",
    "# a gate oxide thickness based on EOT\n",
    "EOT = 1. * c.nm\n",
    "eps_graphene = 6.9 * 2.2 / 3.34\n",
    "eps_SiO2 = 3.9 \n",
    "eps_ox_in = 6.93 # hBN in-plane\n",
    "eps_ox_out = 3.76 # hBN out-of-plane\n",
    "t_oxide = EOT / eps_SiO2 * eps_ox_out\n",
    "\n",
    "# layer thickness for doping\n",
    "t = 3.34 * c.Ang # layer thickness of graphene = interlayer separation of bilayer"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next, we specify special regions using the `petra.geomety` module. In particular, we specify a source, drain, channel and gate region as follows:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from petra import geometry\n",
    "\n",
    "channel = geometry.slab(Lsource, L - Ldrain, 'z')\n",
    "source_drain = geometry.invert(channel)\n",
    "graphene_ribbon = geometry.inside(structure.atoms, t / 2)\n",
    "\n",
    "gate = geometry.intersect(channel, geometry.outside(structure.atoms, t / 2 + t_oxide))\n",
    "\n",
    "doping_source_drain = geometry.intersect(graphene_ribbon, source_drain)\n",
    "doping_channel = geometry.intersect(graphene_ribbon, channel)\n",
    "\n",
    "dielectric_graphene = graphene_ribbon\n",
    "dielectric_oxide = geometry.invert(graphene_ribbon)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### 4.2.2. Define the Metal Contacts: Gate\n",
    "\n",
    "The gate is defined using its region and a potential. We specify a work-function for convenience."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "work_function = -structure.elements.contacts[0].bandstructure().find_cb()\n",
    "device.define_gate(gate, work_function + 0.1 * c.eV)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### 4.2.3. Apply doping\n",
    "\n",
    "The doping is similarly defined by associating regions with doping concentrations. General doping profiles are also possible by specifying a function of position instead of a static number."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "device.define_doping(doping_channel, -1e20 / c.cm3) # p doping\n",
    "device.define_doping(doping_source_drain, 1e20 / c.cm3) # n doping"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### 4.2.4. Set dielectric constants"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "device.define_dielectric(dielectric_graphene, [1, 1, eps_graphene])\n",
    "device.define_dielectric(dielectric_oxide, [eps_ox_in, eps_ox_out, eps_ox_in])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Having specified the device, we can verify it is correctly built by saving it to a `.vtu` file and inspecting in ParaView."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "device.write_vtk('4_device.vtu')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 4.3. Self-consistent iteration"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To start a self-consistent iteration, we have to define an initial potential. To this end, we first find the self-consistent semi-classical potential:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%time\n",
    "from petra.simulation import DriftDiffusionProblem\n",
    "\n",
    "driftdiffusionproblem = DriftDiffusionProblem(device)\n",
    "driftdiffusion = driftdiffusionproblem.solve()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "driftdiffusion.write_vtk('4_driftdiffusion.vtu')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next, we initialize the self-consistent quantum solver. This initialization builds the density mesh, sets up the FEM Poisson solver and resolves the doping profile, as can be seen from the output of the logger below:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from petra.simulation import SelfConsistentProblem\n",
    "\n",
    "selfconsistentproblem = SelfConsistentProblem(device, V0=driftdiffusion.V)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It is now time to start the self-consistent iterations. Petra has several optimization built in such as acceleration of convergence using the DIIS method and the solution of Poisson using algebraic multigrid."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%time\n",
    "selfconsistent = selfconsistentproblem.solve(10, atol=1e-6*c.eV)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Check marks indicate convergence. Plotting the convergence behavior is also useful."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.figure(dpi=150)\n",
    "selfconsistentproblem.plot_convergence(detail=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We show a couple of ways to plot the results directly in python.\n",
    "We start with the energy-resolved density, also called the spectral density."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ss = selfconsistent.statespace.filter(-0.1*c.eV, 0.3*c.eV)\n",
    "E_srho, srho = ss.spectral_node_density()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from petra.plot import log_colors\n",
    "params, ticks = log_colors(srho.max(), dynamic_range=7)\n",
    "\n",
    "V_plt = structure.node_V - structure.node_V[0] + structure.elements.contacts[0].bandstructure().find_cb()\n",
    "\n",
    "fig, ax = plt.subplots(figsize=(6, 3), dpi=300, sharey=True, constrained_layout=True)\n",
    "cnt = ax.contourf(structure.nodes.position[:, 2]/c.nm, E_srho/c.eV, srho, **params)\n",
    "ax.plot(structure.nodes.position[:, 2]/c.nm, V_plt/c.eV, 'r-', lw=1)\n",
    "cbar = fig.colorbar(cnt, ax=ax, label='Spectral density')\n",
    "cbar.set_ticks(ticks)\n",
    "\n",
    "ax.set_xlabel('$z$ (nm)')\n",
    "ax.set_ylabel('$E$ (eV)')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next, we plot the density integrated over the out-of-plane y-direction. To do this, we first interpolate the result from the FEM mesh to a rectilinear mesh:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from petra.mesh.rect import rmesh"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x0 = (device.density_mesh.r_rv[:, 0].max() + device.density_mesh.r_rv[:, 0].min()) / 2\n",
    "y0 = (device.density_mesh.r_rv[:, 1].max() + device.density_mesh.r_rv[:, 1].min()) / 2\n",
    "\n",
    "x = x0 + np.linspace(-1.1, 1.1, 100) * c.nm\n",
    "y = y0 + np.linspace(-t, t, 20)\n",
    "z = np.linspace(0, device.density_mesh.r_rv[:, 2].max(), 1000)\n",
    "\n",
    "r_xyzv = rmesh(x, y, z)\n",
    "\n",
    "n_xyz = device.density_mesh.mesh.interpolate(-selfconsistent.rho_free, r_xyzv)\n",
    "n_xz = np.trapz(n_xyz, y, axis=1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "apos = structure.atoms.positions / 10\n",
    "\n",
    "fig, ax = plt.subplots(dpi=150, figsize=(20, 2.5), constrained_layout=True)\n",
    "levels = np.linspace(0, 1e13, 101)\n",
    "cont = ax.contourf(z/c.nm, x/c.nm, n_xz * c.cm**2, levels=levels, cmap='Reds', extend='max')\n",
    "fig.colorbar(cont, ax=ax)\n",
    "ax.scatter(apos[:, 2], apos[:, 0], s=structure.atoms.get_atomic_numbers(), c='k')\n",
    "ax.axis('square')\n",
    "ax.set(xlim=(z.min()/c.nm, z.max()/c.nm), ylim=(x.min()/c.nm, x.max()/c.nm));"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For more advanced visualizations using paravies, we save the results in a `.vtu` file."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "selfconsistent.write_vtk('4_self_consistent.vtu')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Extra: By enabling logging, we were able to follow the progress of the code real-time.\n",
    "For looking at the logs after running the code, the `petra.log` module contains a parser, which detects the hierarchy of the program and outputs a tree of the events with durations if available. This is a simple way to see where the time has been spent in the simulation. We can post-process the file log:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "with open('4_self_consistent.log') as filelog:\n",
    "    log.parse_time(filelog)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "With the complete calculation of the self-consistent Schroedinger-Poisson system , we have correctly described fully ballistic transport in our system under study. All that is left now is to vary the parameters of our system and solve the system for each.\n",
    "\n",
    "In the next part we will demonstrate changing the gate potential and plot a current-voltage (IV) characteristic of this device. We will also highlight the limitations of the ballistic transport problem."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.12"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
