"""Linear algebra helper functions
"""

import numpy as np
import numpy.linalg as la
import scipy.sparse.linalg as spla
from scipy.sparse.linalg.dsolve import linsolve

from petra.linalg import pardiso


def solve(A, b):
    if pardiso.use_pardiso:
        # pardiso is fast, but needs Intel MKL
        solve = pardiso.factorized(A)
        c = solve(b)

    elif linsolve.useUmfpack:
        # umfpack is faster than SuperLU, RHS must be a vector
        solve = spla.factorized(A)
        c = np.empty_like(b)
        for ii, bi in enumerate(b.T):
            c[:, ii] = solve(bi)

    else:
        # fallback to SuperLU
        c = spla.spsolve(A, b, permc_spec='NATURAL')
        c.shape = b.shape  # fix to keep index of size 1

    return c


def orthogonalize_posdef(v):
    overlap = v.conj().T @ v
    R = orthogonalize_cholesky(overlap)
    return v @ R


def orthogonalize_cholesky(A):
    L = la.cholesky(A)
    return la.inv(L.conj().T)


def rms(v):
    return np.sqrt((v**2).mean())
