from collections import defaultdict
from functools import reduce
import math

import numpy as np


def human_timedelta(delta):
    delta *= 1000
    delta, milliseconds = divmod(delta, 1000)
    delta, seconds = divmod(delta, 60)
    delta, minutes = divmod(delta, 60)
    days, hours = divmod(delta, 24)

    if days > 0:
        return '{:.0f}days, {:.0f}h'.format(days, hours)
    if hours > 0:
        return '{:.0f}h, {:.0f}min'.format(hours, minutes)
    if minutes > 0:
        return '{:.0f}min, {:.0f}s'.format(minutes, seconds)
    return '{:.0f}s, {:.0f}ms'.format(seconds, milliseconds)


def gcd(array, axis=0):
    return np.gcd.reduce(array, axis=axis)


def lcm(array):
    return reduce(lambda a, b: a * b // math.gcd(a, b), array)


def flatten_last(array, n):
    return array.reshape(*array.shape[:-n], -1)


def linspace(v0, v1, *args, **kwargs):
    v0, v1 = np.asarray(v0), np.asarray(v1)
    shape = (-1,) + tuple(1 for i in v0.shape)
    v = v1 - v0
    z = np.linspace(0, 1, *args, **kwargs)
    if 'retstep' in kwargs:
        z, step = z
        step *= v
    z = z.reshape(shape)
    result = v0[None, ...] + z * v[None, ...]

    if 'retstep' in kwargs:
        return result, step
    return result


def merge_dicts_of_lists(dicts, func=None):
    d = defaultdict(list)
    for di in dicts:
        for k, v in di.items():
            d[k].extend(v)
    if func is None:
        return dict(d)
    else:
        return {k: func(v) for k, v in d.items()}


def add_periodic_boundaries(array):
    shape = tuple(dim + 1 for dim in array.shape)
    slices = tuple(slice(-1) for dim in array.shape)
    out = np.empty(shape, dtype=array.dtype)
    out[slices] = array
    for axis in range(len(shape)):
        swapped = out.swapaxes(0, axis)
        swapped[-1] = swapped[0]
    return out


def find_degenerate(evals, tolerance):
    idx = np.argsort(evals)
    r_idx = np.r_[:evals.size][idx]
    devals = np.diff(evals[idx])
    groups = [[r_idx[0]]]
    for deval, ii in zip(devals, r_idx[1:]):
        if deval > tolerance:
            groups.append([])
        groups[-1].append(ii)
    return groups
