import numpy as np
import numpy.linalg as la

from petra import constants as c
from petra.math import delta


def adaptive_smearing(states, dEdk_knv, dk, E_dos, a=1, delta=delta.mp0, safe=True):
    dE = a * la.norm(dEdk_knv, axis=-1) * dk
    if safe:
        dE = np.maximum(dE, a * np.diff(E_dos).mean())
    return smearing(states, E_dos, dE=dE, delta=delta)


def smearing(states, E_dos, dE=300 * c.K, delta=delta.mp0):
    weight_k = states.gspin * states.weight_k / states.weight_k.sum()
    return np.array([weight_k @ np.sum(delta((states.E_kn - E) / dE) / dE, axis=1)
                     for E in E_dos])


def binning(E, E_dos, weights=None):
    if weights is None:
        weights = np.ones(E.shape[0])

    dE = E_dos[1] - E_dos[0]

    mask = (E_dos.min() - dE / 2 < E) & (E < E_dos.max() + dE / 2)

    cat = np.round((E[mask] - E_dos[0]) / dE).astype(int)
    weight = np.tile(weights[:, None], (1, E.shape[1]))[mask] / np.sum(weights)
    return 2 * np.bincount(cat, weight, minlength=E_dos.size) / dE


def linear_tetrahedral(cell, nk, E, E_dos):
    import ase.dft.dos

    a = cell.a / c.Ang

    return 2 * ase.dft.dos.ltidos(a, E.reshape(nk + (E.shape[-1],)), E_dos)


def vt_finite_difference(nk, E):
    E_k = E.reshape(tuple(nk) + E.shape[-1:])

    vt = np.empty(E.shape + (3,))
    for ii in range(3):
        ix = np.r_[:E_k.shape[ii]]
        dE = (E_k.take(ix + 1, ii, mode='wrap') -
              E_k.take(ix - 1, ii, mode='wrap')) / 2
        vt[..., ii] = dE.reshape(E.shape)
    return vt


def gilat_raubenheimer(cell, nk, E, E_dos, vg=None, weights=None):
    '''The Generalized Gilat-Raubenheimer method
    '''

    if weights is None:
        weights = np.ones(E.shape[0])
    weights = np.tile(weights[:, None], (1, E.shape[1]))

    b = 1 / 2
    B = cell.reciprocal.a / np.array(nk)[:, None]

    if vg is not None:
        vt = vg @ B.T
    else:
        vt = vt_finite_difference(nk, E)
    vt = np.sort(np.abs(vt))[:, :, ::-1]

    E1 = b * np.abs(vt @ np.array([1, -1, -1]))
    E2 = b * (vt @ np.array([1, -1, 1]))
    E3 = b * (vt @ np.array([1, 1, -1]))
    E4 = b * (vt @ np.array([1, 1, 1]))

    vtnorm = la.norm(vt, axis=-1)
    vtprod = np.prod(vt, axis=-1)

    dos = np.zeros(E_dos.shape)

    for ii, Ei in enumerate(E_dos):
        dE = np.abs(Ei - E)

        mask = (E3 <= dE) & (dE < E4)
        dos[ii] += np.sum(
            weights[mask] *
            (E4[mask] - dE[mask])**2 / (2 * vtprod[mask])
        )

        mask = (E2 <= dE) & (dE < E3)
        dos[ii] += np.sum(
            weights[mask] *
            2 * (b**2 * (vt[mask, 0] + vt[mask, 1]) - b * dE[mask]) /
            (vt[mask, 0] * vt[mask, 1])
        )

        mask = (E1 <= dE) & (dE < E2)
        dos[ii] += np.sum(
            weights[mask] *
            (b**2 * (vt[mask, 0] * vt[mask, 1] +
                     3 * vt[mask, 1] * vt[mask, 2] +
                     vt[mask, 2] * vt[mask, 0]) -
             b * dE[mask] * (vt[mask] @ np.array([-1, 1, 1])) -
             (dE[mask]**2 + (vtnorm[mask] * b)**2) / 2) /
            vtprod[mask]
        )

        mask = (dE < E1) & (vt[:, :, 0] >= vt[:, :, 1] + vt[:, :, 2])
        dos[ii] += np.sum(
            weights[mask] *
            4 * b**2 / vt[mask, 0]
        )

        mask = (dE < E1) & (vt[:, :, 0] < vt[:, :, 1] + vt[:, :, 2])
        dos[ii] += np.sum(
            weights[mask] *
            (2 * b**2 * (vt[mask, 0] * vt[mask, 1] +
                         vt[mask, 1] * vt[mask, 2] +
                         vt[mask, 2] * vt[mask, 0]) -
             (dE[mask]**2 + (vtnorm[mask] * b)**2)) /
            vtprod[mask]
        )

    return 2 * dos / np.prod(nk)
