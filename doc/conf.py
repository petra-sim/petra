import os
import sys
import shutil
import pathlib

import sphinx_rtd_theme

doc_path = pathlib.Path(__file__).absolute().parent
project_path = doc_path.parent
build_path = doc_path / 'build'
html_build_path = build_path / 'html'
src_path = project_path / 'src'
examples_path = project_path / 'examples'

print(f'\u001b[32mAdding to path: {src_path}\u001b[0m')
sys.path.insert(0, str(src_path))

from petra import __version__  # noqa E402
__version__ = os.environ.get('DOCS_VERSION', __version__)

if 'BASE_URL' in os.environ:
    base_url = os.environ['BASE_URL']
else:
    html_examples_path = html_build_path / 'examples'
    print(f'\u001b[32mCopying {examples_path} to {html_examples_path}\u001b[0m')
    ignore = shutil.ignore_patterns("*_run.ipynb", "WAVECAR", "POSCAR", "*.npz", 'Makefile',
                                    "*.vtu", "*.pwepp", "*.pdf", "*.txt", '.*', '*.log')
    shutil.copytree(examples_path, html_examples_path, ignore=ignore)

    html_src_path = html_build_path / 'src'
    print(f'\u001b[32mCopying {src_path} to {html_src_path}\u001b[0m')
    shutil.copytree(src_path, html_src_path)

    base_url = ''

source_url = base_url + '/src'

print(f'\u001b[32mSource URL: {source_url}\u001b[0m')


def linkcode_resolve(domain, info):
    if domain != 'py':
        print('INPUT', domain, info)
        return None
    if not info['module']:
        print('INPUT', domain, info)
        return None
    filename = info['module'].replace('.', '/')
    if (src_path / filename).is_dir():
        filename += '/__init__'
    filename += '.py'
    if not (src_path / filename).exists():
        print('Could not find the source file for {info["module"]}.{info["fullname"]}')
        exit(-1)
    return f'{source_url}/{filename}'


project = 'petra'
title = 'PETRA Documentation'
copyright = '2021, Maarten L. Van de Put'
author = 'Maarten L. Van de Put'

version = __version__
release = __version__

master_doc = 'index'
templates_path = ['templates']
source_suffix = '.rst'
exclude_patterns = ['build']
pygments_style = 'sphinx'

# extensions

extensions = [
    'sphinx.ext.napoleon',
    'sphinx.ext.autodoc',
    'sphinx.ext.doctest',
    'sphinx.ext.mathjax',
    'sphinx.ext.linkcode',
    'sphinx.ext.autosummary',
    'sphinx.ext.extlinks',
]

extlinks = {
    'source': (f'{base_url}/%s', ''),
    'doi': ('https://doi.org/%s', 'doi:'),
}

napoleon_use_rtype = False
napoleon_google_docstring = False
napoleon_use_param = False
napoleon_use_ivar = True

autosummary_generate = True

# HTML output

html_theme = 'sphinx_rtd_theme'
html_theme_path = [sphinx_rtd_theme.get_html_theme_path()]
html_theme_options = {'logo_only': True}
html_logo = 'logo.png'
html_favicon = 'favicon.ico'

# LaTeX output

latex_elements = {
    'papersize': 'letterpaper',
    'pointsize': '11pt',
}
latex_documents = [
    (master_doc, 'petra.tex', title, author, 'manual'),
]

man_pages = [
    (master_doc, project, title, [author], 1)
]

texinfo_documents = [
    (master_doc, project, title, author,
     'petra', 'PETRA Quantum Transport Package', 'Miscellaneous'),
]
