import io
import sys
import logging
from collections import defaultdict
from contextlib import contextmanager
from dateutil.parser import parse

import numpy as np

from petra.util import mpi


if mpi.is_parallel():
    fmt = '%(asctime)s - %(name)s - %(levelname)s - mpi[{rank}]: %(message)s'
    fmt = fmt.format(rank=mpi.comm.rank)
else:
    fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'


class BlockLogger(logging.Logger):
    def begin(self, message):
        self.info('[begin] ' + message)

    def end(self, message):
        self.info('[end] ' + message)

    @contextmanager
    def block(self, message):
        self.begin(message)
        try:
            yield
        finally:
            self.end(message)


logging.setLoggerClass(BlockLogger)


def getLogger(name='petra'):
    return logging.getLogger(name)


def to_stdout(level=logging.INFO):
    logger = to_stream(sys.stdout, level=level)
    logger.info("started logging to stdout")


def to_memory(level=logging.INFO):
    tempio = io.StringIO()
    logger = to_stream(tempio, level=level)
    logger.info("started logging to memory")
    return tempio


def to_file(fname, clear=False, level=logging.INFO):
    if not fname.endswith('.log'):
        raise ValueError('Refusing to log to files not ending in .log, '
                         'this is for your own protection.')
    logger = getLogger()
    logger.setLevel(level)
    handler = logging.FileHandler(fname, mode='w' if clear else 'a')
    handler.setLevel(level)
    formatter = logging.Formatter(fmt)
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    logger.info("started logging to file {}".format(fname))


def to_stream(stream, level=logging.INFO):
    logger = getLogger()
    for handler in logger.handlers:
        if handler.stream is stream:
            return logger
    logger.setLevel(level)
    handler = logging.StreamHandler(stream)
    handler.setLevel(level)
    formatter = logging.Formatter(fmt)
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    return logger


def analyse(log):
    stack = []
    indent = 0
    log.seek(0)
    for line in log:
        parts = line.strip().split(' - ')
        time_string, module_name, log_level = parts[:3]
        time = parse(time_string)
        message = ' - '.join(parts[3:])
        if message.startswith('[begin]'):
            message = message[7:].strip()
            stack.append((indent, log_level, module_name, message, time))
            indent += 1
        elif message.startswith('[end]'):
            message = message[5:].strip()
            index = [ii for ii, item in enumerate(stack)
                     if item[3] == message][-1]
            duration = time - stack[index][4]
            stack[index] = stack[index][:4] + (duration,)
            indent -= 1
        else:
            stack.append((indent, log_level, module_name, message, None))
    return stack


def parse_tree(log):
    stack = analyse(log)
    for indent, level, module, message, duration in stack:
        print('  ' * indent, end='')
        level = '' if level == 'INFO' else level + ': '
        print('{}{}: {}'.format(level, module, message), end='')
        if duration is not None:
            try:
                print(' ({:.2f} sec)'.format(duration.total_seconds()), end='')
            except AttributeError:
                pass
        print()


def cumulate(stack):
    events = defaultdict(lambda: 0.0)
    counts = defaultdict(lambda: 0)

    for indent, level, module, message, duration in stack:
        if duration is not None:
            level = '' if level == 'INFO' else level + ': '
            process = '{}{}: {}'.format(level, module, message)
            try:
                events[process] += duration.total_seconds()
                counts[process] += 1
            except AttributeError:
                pass
    return dict(events), dict(counts)


def parse_time(log):
    stack = analyse(log)
    events, counts = cumulate(stack)

    print('total time (count) - message')
    print('----------------------------')
    for process in sorted(events, key=events.get, reverse=True):
        print('{:10.2f} ({:5d}) - {}'.format(events[process],
                                             counts[process],
                                             process))


def parse_compare(logs):
    stacks = [analyse(log) for log in logs]
    eventss, countss = zip(*(cumulate(stack) for stack in stacks))

    processes = set().union(*(events.keys() for events in eventss))
    all_events = {process: max(events.get(process, 0) for events in eventss)
                  for process in processes}

    for ii, log in enumerate(logs):
        print('{:5d}: {:s}'.format(ii, log.name))
    print('')

    header = []
    header += ['{:>10s}'.format('t' + str(ii)) for ii, log in enumerate(logs)]
    header += ['|']
    header += ['{:>5s}'.format('N' + str(ii)) for ii, log in enumerate(logs)]
    header += ['|', 'message']
    header = ' '.join(header)
    print(header)
    print('-' * (len(header) - 7 + max(len(process) for process in processes)))
    for process in sorted(all_events, key=all_events.get, reverse=True):
        data = []
        for events in eventss:
            try:
                data.append('{:10.2f}'.format(events[process]))
            except KeyError:
                data.append('_' * 10)
        data.append('|')
        for counts in countss:
            try:
                data.append('{:5d}'.format(counts[process]))
            except KeyError:
                data.append('_' * 5)
        data.append('|')
        data.append(process)
        data = ' '.join(data)
        print(data)


def parse_plot(logs, pdf=False):
    import matplotlib.pyplot as plt
    from matplotlib.ticker import AutoMinorLocator
    import tempfile
    import webbrowser

    stacks = [analyse(log) for log in logs]
    eventss, countss = zip(*(cumulate(stack) for stack in stacks))

    processes = set().union(*(events.keys() for events in eventss))
    all_events = {process: max(events.get(process, 0) for events in eventss)
                  for process in processes}

    plt.figure(dpi=300, figsize=(10, 7))
    ax = plt.subplot(111)
    dy = .9 / len(logs)
    y = np.r_[:len(processes)] + dy * len(logs) / 2
    for ii, (events, log) in enumerate(zip(eventss, logs)):
        label = log.name
        time = []
        yticks = []
        for process in sorted(all_events, key=all_events.get):
            yticks.append(process)
            time.append(events.get(process, np.nan) / 60)
        plt.barh(y - ii * dy, np.array(time), label=label, height=dy, lw=0)
    plt.yticks(range(len(yticks)), yticks)
    ax.xaxis.set_minor_locator(AutoMinorLocator())
    plt.grid(True, axis='x', which='both')
    ax.set_axisbelow(True)
    plt.xlabel('time (min)')
    plt.legend(loc='lower right', fancybox=False, framealpha=1)
    plt.tight_layout()
    if pdf:
        _, fname = tempfile.mkstemp('.pdf')
        plt.savefig(fname)
        webbrowser.open_new(fname)
    else:
        plt.show()


if __name__ == '__main__':
    usage = """Petra logging

    Usage:
        petra.log tree <file>
        petra.log time <file>
        petra.log compare <files>...
        petra.log plot [--pdf] <files>...
    """
    import docopt
    args = docopt.docopt(usage)
    if args['tree']:
        with open(args['<file>']) as fd:
            parse_tree(fd)
    elif args['time']:
        with open(args['<file>']) as fd:
            parse_time(fd)
    elif args['compare']:
        fds = [open(fname) for fname in args['<files>']]
        parse_compare(fds)
        for fd in fds:
            fd.close()
    elif args['plot']:
        fds = [open(fname) for fname in args['<files>']]
        parse_plot(fds, pdf=args['--pdf'])
        for fd in fds:
            fd.close()
