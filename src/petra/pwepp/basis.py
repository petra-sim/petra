import numpy as np
import numpy.linalg as la

from petra.linalg import fft
from petra import periodiccell as pc


def create(mode, cell, params):
    return {
        'grid': Grid,
        'pw': PlaneWaves,
    }[mode](cell, **params)


class Grid(pc.Grid):
    def flatten(self, data_xyz):
        '''xyz -> r
        '''
        shape = data_xyz.shape[:-3] + (self.size,)
        return data_xyz.reshape(shape)

    def expand(self, data_r):
        '''r -> xyz
        '''
        shape = data_r.shape[:-1] + self.shape
        return np.reshape(data_r, shape)

    @classmethod
    def from_pw(cls, pw):
        return cls(pw.cell, n=pw.nG)

    def to_pw(self):
        return PlaneWaves.from_grid(self)

    def to_grid(self):
        return self

    def __eq__(self, other):
        return (self.cell == other.cell) and (self.shape == other.shape)


def find_nG(cell, E_cut, antialias=1):
    ''' Find the number of plane-waves based on the cutoff energy

    Parameters
    ----------
    E_cut : float, optional
    antialias : int, optional
        0: no antialiassing
        n: anti-aliassing with n times G-cutoff (default: n=2)
    '''
    assert(antialias >= 0)

    Gcut = np.sqrt(2 * E_cut)

    L_a = la.norm(cell.reciprocal.a_inv, axis=0)
    nbmax_a = np.array(np.ceil(Gcut * L_a), dtype=int)
    nG_a = (2 + antialias) * nbmax_a
    return nG_a


class PlaneWaves(pc.PlaneWaves):
    def __init__(self, cell, Ecut=None, nG=None, antialias=1, closest_fft=False, order='C'):

        if not isinstance(cell, pc.PeriodicCell):
            cell = pc.PeriodicCell(cell)

        assert(np.isfinite(Ecut) or nG is not None)
        assert(antialias >= 0)

        if Ecut is None:
            Ecut = np.inf
        elif np.isfinite(Ecut):
            nG = find_nG(cell, Ecut, antialias)

        if closest_fft:
            nG = [fft.closest_optimal(nGi) for nGi in nG]

        super().__init__(cell, nG, order=order)

        self.Ecut = Ecut
        self.antialias = antialias
        self.closest_fft = closest_fft

    def is_consistent(self):
        nG = find_nG(self.cell, self.Ecut, self.antialias)
        return self.nG == nG

    @classmethod
    def from_grid(cls, grid):
        return cls(grid.cell, nG=grid.shape, antialias=0, closest_fft=False)

    def to_grid(self):
        return Grid.from_pw(self)

    def __eq__(self, other):
        return all([
            (self.cell == other.cell),
            (self.shape == other.shape),
            (self.Ecut == other.Ecut),
            (self.antialias == other.antialias),
            (self.closest_fft == other.closest_fft),
            (self.order == other.order),
        ])

    def is_compatible(self, other):
        return all([
            (self.cell == other.cell),
            (self.shape == other.shape),
            (self.Ecut == other.Ecut),
        ])

    def similar(self, Ecut=None, nG=None,
                antialias=None, closest_fft=False, order=None):
        return PlaneWaves(
            self.cell,
            self.Ecut if Ecut is None else Ecut,
            self.nG if nG is None else nG,
            self.antialias if antialias is None else antialias,
            self.closest_fft if closest_fft is None else closest_fft,
            self.order if order is None else order,
        )

    def flatten(self, data_xyz):
        '''xyz -> g
        '''
        shape = data_xyz.shape[:-3] + (self.size,)
        return data_xyz.reshape(shape, order=self.order)

    def expand(self, data_g):
        '''g -> xyz
        '''
        shape = data_g.shape[:-1] + self.shape
        return np.reshape(data_g, shape, order=self.order)

    def cutoff(self, k_a=None, n=1):
        if k_a is None:
            k_a = np.zeros(3)
        return TruncatedPlaneWaves(self, k_a, n=n)

    def T_xyz(self, k_a):
        k_v = self.cell.reciprocal.to_cartesian(k_a)
        return np.sum((self.G_xyzv + k_v)**2, axis=3) / 2

    @property
    def Gmax_a(self):
        return self.G_xyza.max((0, 1, 2))

    @property
    def Gmin_a(self):
        return self.G_xyza.min((0, 1, 2))

    def to(self, other):
        assert(isinstance(other, type(self)))

        if other.size < self.size:
            mask_xyz = np.all((other.Gmin_a <= self.Gxyza) & (self.G_xyza <= other.Gmax), -1)

            def transfer(psi_oxyz):
                return psi_oxyz[..., mask_xyz]

        elif self.size < other.size:
            mask_xyz = np.all((self.Gmin_a <= other.Gxyza) & (other.G_xyza <= self.Gmax), -1)

            def transfer(psi_oxyz):
                out_oxyz = np.zeros(psi_oxyz.shape[:-3] + other.shape, dtype=psi_oxyz.dtype)
                out_oxyz[..., mask_xyz] = psi_oxyz
                return out_oxyz

        else:
            def transfer(psi_oxyz):
                return psi_oxyz

        return transfer


class TruncatedPlaneWaves:
    def __init__(self, pw, k_a, n=1):
        self.pw = pw
        self.k_a = k_a

        T_xyz = self.pw.T_xyz(self.k_a)
        self._mask_xyz = T_xyz < self.pw.Ecut * n**2
        self.mask_g = self.pw.flatten(self._mask_xyz)

        self.G_ga = self.compress(np.moveaxis(self.pw.G_xyza, [0, 1, 2], [-3, -2, -1])).T
        self.G_gv = self.compress(np.moveaxis(self.pw.G_xyzv, [0, 1, 2], [-3, -2, -1])).T
        self.T_g = self.compress(T_xyz)

        self.nG = self.size = np.count_nonzero(self.mask_g)

    def compress(self, data_xyz):
        data_g = self.pw.flatten(data_xyz)
        return data_g[..., self.mask_g]

    def uncompress(self, data_g):
        shape = data_g.shape[:-1] + (self.pw.size,)
        full_g = np.zeros(shape, dtype=complex)
        full_g[..., self.mask_g] = data_g
        return self.pw.expand(full_g)

    def to(self, other):
        assert(isinstance(other, type(self)))

        i_to_f = self.pw.flatten(self._mask_xyz[other.pw.to(self.pw)(other._mask_xyz)])
        f_to_i = other.pw.flatten(other._mask_xyz[self.pw.to(other.pw)(self._mask_xyz)])

        def transfer(psi_og):
            out_og = np.zeros(psi_og.shape[:-1] + (other.size,), dtype=psi_og.dtype)
            out_og[..., i_to_f] = psi_og[..., f_to_i]
            return out_og

        return transfer
