import numpy as np


def stable_marriages(suitor_preference, suitee_preference):
    '''
    An implementation of the Gale-Shapley algorithm
    for solving the stable marriages problem
    '''
    suitor_preference = np.copy(suitor_preference)
    free = set(range(suitor_preference.shape[0]))
    partner = {}
    while len(free) > 0:
        suitor = min(free)
        preferred_mate = suitor_preference[suitor, :].argmax()
        # only propose once
        suitor_preference[suitor, preferred_mate] = -np.inf
        if preferred_mate not in partner:
            partner[preferred_mate] = suitor
            free.discard(suitor)
        else:
            current = partner[preferred_mate]
            current_preference = suitee_preference[preferred_mate, current]
            if suitee_preference[preferred_mate, suitor] > current_preference:
                free.add(current)
                partner[preferred_mate] = suitor
                free.discard(suitor)
    return sorted(partner, key=lambda k: partner[k])
