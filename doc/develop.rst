Developer Instructions
======================

Development in PETRA starts from the :code:`develop` branch. The :code:`develop` branch should not
be considered stable for general use. Use this only when you know what you're doing.

Installation
------------

1. Clone the :code:`develop` branch of the repository:

   .. code-block:: shell

      git clone -b develop git@gitlab.com:petra-sim/petra.git

2. Change directory to the petra folder

   .. code-block:: console

      cd petra

3. Create and activate the conda environment:

   .. code-block:: console

      conda env create
      conda activate petra


4. After activation, install PETRA using pip in development mode:

   .. code-block:: console

      pip install -e .


5. After installation, activate the petra environment whenever you want to use it

   .. code-block:: console

      conda activate petra


Feature Branch
--------------

The PETRA project uses the git-flow branching model.
If you are developing a new feature, you should setup a new branch
:code:`<your-feature>` to implement the feature you're working on:

.. code-block:: shell

   git branch feature/<your-feature>
   git checkout feature/<your-feature>

When your feature is complete, you can make a merge request for it to be merged with the develop
branch through gitlab.

Testing
-------

On Gitlab, Continuous Integration (CI) automatically runs unit-tests on the :code:`develop` and
:code:`master` branches. All CI actions are done in a Docker container containing an up-to-date
conda environment, as specified in :code:`environment.yml`.

To ensure the CI pipeline does not fail on the servers, you can (and should) run the test suite
locally before pushing or merging your feature. Since your local development environment is
potentially dirty, it is advised to use the Docker container used by Gitlab CI. If Docker is
properly installed on your local machine, all tests can be run using:

.. code-block:: shell

   make build-ci-docker
   make ci-test
   make ci-docs

If any errors show up, they should be fixed before pushing or merging into :code:`develop` or
:code:`master`. Once everything looks fine, you can clean the tests and docs using:

.. code-block:: shell

   make clean-test
   make clean-docs


Code Style
----------

Code contributions should closely follow the Python style guide PEP8_ with the following exceptions:

- The line length is 100 characters instead of 80.
- Ambiguous variable names (like "x") are allowed if they have physical meaning.

While coding, you should use a linter to enforce the style. A flake8 config is included in setup.cfg
and should be used automatically.

Documentation follows the `numpy documentation standards`_.

Array naming
------------

Where possible, array variable names are suffixed with identifiers for each dimension.

.. highlight:: python

Example: A wavefunction array :code:`psi`, with mode index (:code:`s`) as the
first dimension and position (:code:`r`) as the second, is denoted as :code:`psi_sr`.

.. table:: List of commonly used index identifiers.

   +----------------+-----------------------------------+
   | notation       | description                       |
   +================+===================================+
   | :code:`x_r`    | position                          |
   +----------------+-----------------------------------+
   | :code:`x_xyz`  | position (x, y, z)                |
   +----------------+-----------------------------------+
   | :code:`x_k`    | wavevector                        |
   +----------------+-----------------------------------+
   | :code:`x_G`    | reciprocal lattice coordinate     |
   +----------------+-----------------------------------+
   | :code:`x_s`    | wavefunction mode index           |
   +----------------+-----------------------------------+
   | :code:`x_n`    | Bloch wave band index             |
   +----------------+-----------------------------------+
   | :code:`x_i`    | node index                        |
   +----------------+-----------------------------------+


.. _PEP8: https://www.python.org/dev/peps/pep-0008/
.. _`numpy documentation standards`: https://numpydoc.readthedocs.io/en/latest/format.htm
