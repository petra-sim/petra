import ase.io.vasp

from petra.io.vasp.constants import eV
from petra.io.vasp.util import read_array, CommonFile


class _VASPArray(CommonFile):
    unit = 1.

    def _read(self, f):
        self.atoms = ase.io.vasp.read_vasp(self.fname)

        for i in range(len(self.atoms) + 9):
            f.readline()
        shape = tuple(map(int, f.readline().split()))
        self.data = read_array(f, shape) * self.unit


class LOCPOT(_VASPArray):
    unit = eV


class CHG(_VASPArray):
    pass


class CHGCAR(_VASPArray):

    def _read(self, f):
        self.atoms = ase.io.vasp.read_vasp(self.fname)

        for i in range(len(self.atoms) + 9):
            f.readline()
        shape = tuple(map(int, f.readline().split()))
        self.rho = read_array(f, shape)

        self.rho_aug = {}

        header = f.readline().strip().lower()
        while header.startswith('augmentation'):
            ni, nelements = map(int, header.split()[-2:])
            ni -= 1
            data = read_array(f, nelements)
            rho_ni = self.rho_aug.get(ni, 0.0)
            if 'imaginary part' in header:
                self.rho_aug[ni] = rho_ni + 1j * data
            else:
                self.rho_aug[ni] = rho_ni + data

            header = f.readline().strip().lower()
