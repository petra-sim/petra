#!/usr/bin/env pvpython
import sys
from typing import Any, Dict

import numpy as np

import paraview
import paraview.simple as pv

usage = 'average.py (x|y|z) <input> <output> [<N>]'

sdirection = sys.argv[1]
try:
    direction = {'x': 0, 'y': 1, 'z': 2}[sdirection]
except KeyError:
    print(usage)
    exit(-1)

N = 1000
if len(sys.argv) == 5:
    N = int(sys.argv[4])

vtk_file = sys.argv[2]
if not vtk_file.endswith('.vtu'):
    print(usage)
    exit(-1)

npz_file = sys.argv[3]
if not npz_file.endswith('.npz'):
    print(usage)
    exit(-1)

inp = pv.XMLUnstructuredGridReader(FileName=vtk_file)

myslice = pv.Slice(Input=inp)
myslice.SliceType = 'Plane'
myslice.SliceOffsetValues = [0.0]
normal = [0., 0., 0.]
normal[direction] = 1.
myslice.SliceType.Normal = normal  # type: ignore
myslice.Triangulatetheslice = 1

integrate = pv.IntegrateVariables(Input=myslice)

data: Dict[str, Any] = {value: [] for value in inp.PointData.keys()}

bounds = inp.GetDataInformation().DataInformation.GetBounds()
xmin, xmax = bounds[2 * direction:2 * direction + 2]
eps = np.finfo(float).eps

x = np.linspace(xmin + eps, xmax - eps, N)

for ii, xi in enumerate(x):
    origin = [0., 0., 0.]
    origin[direction] = xi
    myslice.SliceType.Origin = origin  # type: ignore
    integrate.UpdatePipeline()
    DataSliceFile = paraview.servermanager.Fetch(integrate)
    for key in data.keys():
        value = DataSliceFile.GetPointData().GetArray(key).GetValue(0)
        data[key].append(value)
    print('\r{: 3.0%}'.format((ii + 1.) / N), end='')
    sys.stdout.flush()
print('done')

data[sdirection] = x
np.savez(npz_file, **data)
