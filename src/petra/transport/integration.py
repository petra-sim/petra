import warnings
from functools import partial
from typing import List

import numpy as np

from petra import constants as c
from petra import distributions
from petra.integrate import simpson, trapz
from petra.transport import open_system
from petra.transport.statespace import StateSpace
from petra.transport.structure import Structure
from petra.util import log

logger = log.getLogger(__name__)


def integration(observable, structure: Structure,
                mode='k', refine='local',
                Emin=None, Emax=None, init_eval=16, max_eval=100,
                atol=0., rtol=1e-5, verbose=False,
                return_statespace=True, return_full=False):

    if mode not in ('k', 'E'):
        raise ValueError('Parameter mode has to be either \'E\' or \'k\'.')

    if refine not in ('local', 'global'):
        raise ValueError('Parameter refine has to be either '
                         '\'local\' or \'global\'.')

    if structure.occupation is None:
        raise ValueError('structure.occupation must be set for integrations of observables.')

    structure.occupation.update()
    natural_Emin, natural_Emax = structure.occupation.Erange(cutoff=rtol)

    if Emin is None:
        Emin = natural_Emin
    elif natural_Emin < Emin:
        warnings.warn('The minimum energy for the integration is too high for the requested '
                      'tolerance. I hope you know what you are doing!')

    if Emax is None:
        Emax = natural_Emax
    elif natural_Emax > Emax:
        warnings.warn('The maximum energy for the integration is too low for the requested '
                      'tolerance. I hope you know what you are doing!')

    assert(Emin < Emax)

    logger.begin('integrating {}'.format(observable.__name__))

    if mode == 'E':
        def integrand_and_states(x):
            states = open_system.solve_E(structure, x)
            result = 0.
            for state in states:
                result += structure.occupation(state) * observable(structure, state)
            return result, states

        xmin, xmax = Emin, Emax

    else:  # mode == 'k'
        def integrand_and_states(x):
            states = []
            for contact in structure.elements.contacts:
                states += open_system.solve_k(structure, contact,
                                              np.array([0, 0, x]),
                                              Emin=Emin, Emax=Emax)

            result = 0.
            for state in states:
                result += structure.occupation(state) * observable(structure, state)

            return result, states

        xmin, xmax = 0, 1

    integrate = trapz.integrate if refine == 'global' else simpson.integrate

    result = integrate(integrand_and_states, xmin, xmax, atol=atol, rtol=rtol,
                       init_eval=init_eval, max_eval=max_eval, verbose=verbose)

    logger.end('integrating {}'.format(observable.__name__))
    logger.info('Accuracy = {:.2e} cm-3'.format(result.error * c.cm3))
    logger.info('# evaluations = {:d}'.format(len(result.x)))

    rval = result if return_full else result.value

    if not return_statespace:
        return rval

    ss_weights: List[float] = []
    ss_states: List[open_system.ExtendedState] = []
    ss_occupations: List[float] = []
    for weight, (_, states) in zip(result.weights, result.f):
        ss_weights.extend([weight] * len(states))
        ss_states.extend(states)
        ss_occupations.extend(map(structure.occupation, states))
    ss = StateSpace(structure, ss_weights, ss_states, ss_occupations)

    return rval, ss


node_density = partial(integration, Structure.node_density, mode='E')
density = partial(integration, Structure.density, mode='E')
current = partial(integration, Structure.current, mode='E')


def charge_neutral(element, doping, T=300 * c.K, g_spin=2, dE=2 * c.eV):
    from scipy.optimize import brentq

    bs = element.bandstructure()
    cb = bs.find_cb()
    vb = bs.find_vb()

    def net_charge(mu):
        return charge(element, mu, T=T, g_spin=g_spin) + doping

    mu = brentq(net_charge, vb - dE, cb + dE)

    return mu


def charge(element, mu, T=300 * c.K, g_spin=2):
    bs = element.bandstructure()
    fermi = bs.estimate_fermi()

    def occupation(E):
        return distributions.fermi_dirac(E, mu, T) - 1.0 * (E < fermi)

    def charge(k):
        k_a = np.array([0, 0, 1]) * k
        E = bs.solve_k(k_a, retwave=False)
        return g_spin * occupation(E).sum()

    result = simpson.integrate(charge, 0, 1, atol=1e-5, rtol=1e-5,
                               init_eval=5, max_eval=1000, verbose=False)

    return result.value
