import warnings

from itertools import accumulate, chain, tee
import numpy as np
from scipy.sparse import lil_matrix


def pairwise(iterable):
    "s -> (s0,s1), (s1,s2), (s2, s3), ..."
    a, b = tee(iterable)
    next(b, None)
    return zip(a, b)


def reduceby(array, idx, size, axis=0):
    uidx = np.r_[:size]
    shape = list(array.shape)
    shape[axis] = size
    reduced = np.zeros(shape, dtype=array.dtype)
    array_view = np.moveaxis(array, axis, 0)
    reduced_view = np.moveaxis(reduced, axis, 0)
    for ii in uidx:
        reduced_view[ii] = np.sum(array_view[idx == ii], axis=0)
    return reduced


def reduce_slices(array, slices, ufunc=np.add, axis=0):
    '''Performs reduction for numpy ufuncs based on slices

    This function should eventually be replaced or reimplemented once numpy
    decides on how to fix 'reduceat'.
    (see: https://github.com/numpy/numpy/issues/834)

    Arguments
    ---------
    array
        Input array to reduce.
    slices
        A list of slice objects indicating the reduction ranges.
    ufunc
        A numpy ufunc (default: np.add).
    axis
        The axis along which to perform the reduction (default: 0).

    Returns
    -------
    An array similar to the input array where the specified axis has been
    replaced by the results of a reduction over each slice. The shape of
    the output follows from the number of slices:
    out.shape[axis] == len(slices)
    '''
    # choose the fast path if possible
    if all(slice0.start < slice0.stop for slice0 in slices):
        if all(slice0.stop == slice1.start
               for slice0, slice1 in pairwise(slices)):
            indices = [slice_.start for slice_ in slices]
            return ufunc.reduceat(array, indices, axis=axis)
    # if we are in an edge case that reduceat does not handle well
    shape = list(array.shape)
    shape[axis] = len(slices)
    shape = tuple(shape)
    out = np.empty(shape, dtype=array.dtype)
    if axis != 0:
        array = array.swapaxes(0, axis)
        out = out.swapaxes(0, axis)
    for i, slice_ in enumerate(slices):
        ufunc.reduce(array[slice_], out=out[i], axis=axis)
    if axis != 0:
        out = out.swapaxes(0, axis)
    return out


def next_slice(current, size):
    return slice(current.stop, current.stop + size)


def slices(sizes):
    bootstrapped = chain((slice(0, sizes[0]),), sizes[1:])
    slices = accumulate(bootstrapped, next_slice)
    return tuple(slices)


def slices_to_indices(slices):
    return tuple(np.r_[slice_] for slice_ in slices)


class Indexer(object):
    __slots__ = ('slices', 'indices', 'lookup', 'size', '_idxmat')

    def __init__(self, records):
        keys, sizes = zip(*records)
        self.size = np.sum(sizes)
        self.slices = slices(sizes)
        self.indices = slices_to_indices(self.slices)
        ordered = zip(self.slices, self.indices)
        self.lookup = dict(zip(keys, ordered))
        # a lookup with None yields an empty slice and no indices
        self.lookup[None] = (slice(0, 0), tuple())
        self._idxmat = None

    def split(self, array, axis=0):
        return [array.swapaxes(0, axis)[slice_].swapaxes(0, axis)
                for slice_ in self.slices]

    def reduce(self, array, ufunc=np.add, axis=0):
        return reduce_slices(array, self.slices, ufunc=ufunc, axis=axis)

    @property
    def matrix(self):
        if self._idxmat is None:
            idxmat = lil_matrix((len(self.slices), self.size), dtype=bool)
            for ii, slice_ in enumerate(self.slices):
                idxmat[ii, slice_] = True
            self._idxmat = idxmat.tocsr()
        return self._idxmat

    def __getitem__(self, key):
        # TODO: optimize lookup of consecutive keys by returning a slice
        if isinstance(key, int):
            return self.slices[key]
        if isinstance(key, slice):
            return list(chain(*self.indices[key]))
        try:
            return self.lookup[key][0]
        except (KeyError, TypeError):
            try:
                return list(chain(*(self.indices[subkey]
                                    for subkey in key)))
            except (KeyError, TypeError):
                return list(chain(*(self.lookup[subkey][1]
                                    for subkey in key)))


def overlapping_chunks(a, n, overlap=1):
    assert(n >= 0)
    assert(overlap < n)
    if n == 1:
        return a[..., None]
    shape = list(a.shape) + [n]
    shape[-2] -= overlap
    shape[-2] //= (n - overlap)
    strides = list(a.strides) + [a.strides[-1]]
    strides[-2] *= (n - overlap)
    if (a.shape[-1] - overlap) % (n - overlap) != 0:
        warnings.warn('Returning a truncating view, '
                      'not all data is accessible.')
    return np.lib.stride_tricks.as_strided(a, shape=shape, strides=strides,
                                           writeable=False)
