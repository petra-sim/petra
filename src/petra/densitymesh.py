from __future__ import annotations
from functools import cached_property
from typing import TYPE_CHECKING, List, Optional

import numpy as np

from petra.util import log
from petra.util.indexing import Indexer
from petra.mesh import tetgen

if TYPE_CHECKING:
    from petra.transport.structure import Structure
    from petra.mesh import Mesh

logger = log.getLogger(__name__)


class DensityMesh:
    def __init__(self,
                 structures: List[Structure],
                 mesh: Optional[Mesh] = None,
                 r_boundary_rv: Optional[np.ndarray] = None):
        self.structures = structures

        self.points_of_structure = Indexer((s, s.ndensity) for s in self.structures)
        self.ndensity = self.points_of_structure.size

        self.r_density_rv = np.concatenate([s.r_density_rv for s in self.structures])

        if mesh is None:
            self.r_boundary_rv = np.concatenate([s.r_boundary_rv for s in self.structures])
            if r_boundary_rv is not None:
                self.r_boundary_rv = np.concatenate([r_boundary_rv, self.r_boundary_rv])

            logger.begin('generating tetrahedral mesh')
            self.mesh = tetgen.generate_convex(self.r_boundary_rv, self.r_density_rv)
            logger.end('generating tetrahedral mesh')
        else:
            for structure in self.structures:
                if not np.allclose(self.mesh.points[:self.ndensity], self.r_density_rv):
                    raise ValueError('The provided mesh does not match the structures provided')
            self.mesh = mesh

        self.r_rv = self.mesh.points
        self.size = self.mesh.points.shape[0]

    def integrate(self, data):
        return self.mesh.integrate(data)

    @cached_property
    def _interpolators(self):
        logger.begin('building interpolators')
        ip = self.mesh.interpolator
        ips = []
        for structure in self.structures:
            ips.append([])
            for element in structure.elements:
                mask_xyz = element.density_mask_xyz
                imask_xyz = np.logical_not(mask_xyz)
                ips[-1].append(ip(element.r_xyzv[imask_xyz]))
        logger.end('building interpolators')
        return ips

    def set_potential(self, V_r, name='default'):
        logger.begin(f'setting potential: {name}')
        for structure, ips in zip(self.structures, self._interpolators):
            structure.invalidate_potential()

            points_of_structure = self.points_of_structure[structure]
            for element, ip in zip(structure.elements, ips):
                points_of_element = structure.points_of_element[element]
                mask_r = element.density_mask_xyz.ravel()
                imask_r = np.logical_not(mask_r)
                V_element = np.empty(mask_r.shape)
                V_element[mask_r] = V_r[points_of_structure][points_of_element]
                V_element[imask_r] = ip(V_r)
                element.set_potential(V_element)
        logger.end(f'setting potential: {name}')

    def map_structure_data(self, structure_data):
        ref = next(iter(structure_data.values()))
        shape = (self.size,) + ref.shape[1:]
        device_data = np.zeros(shape, ref.dtype)
        for structure, data in structure_data.items():
            points = self.points_of_structure[structure]
            device_data[points] += data
        return device_data

    def scatter_structure_data(self, *data):
        structure_data = {}
        for structure in self.structures:
            points = self.points_of_structure[structure]
            structure_data[structure] = [d[points] for d in data]
        return structure_data

    def write_vtk(self, fname, field_data=None, **point_data):
        from petra.io.vtk import save_mesh
        save_mesh(fname, self.mesh, point_data=point_data, field_data=field_data)
