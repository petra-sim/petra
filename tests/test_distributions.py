import numpy as np

from petra.distributions import fermi_dirac, bose_einstein, dfermi_dirac


def test_fermi_dirac():
    E = np.linspace(-10, 10, 100)
    assert np.allclose(fermi_dirac(E, 0, .1), 1 / (1 + np.exp(E / .1)))
    assert np.allclose(fermi_dirac(E - 4, 0, .1), fermi_dirac(E, 4, .1))
    assert np.allclose(fermi_dirac(E + 4, 0, .1), fermi_dirac(E, -4, .1))
    assert np.isclose(fermi_dirac(-1e50, 0, .1), 1.)
    assert np.isclose(fermi_dirac(1e50, 0, .1), 0.)
    assert np.isclose(fermi_dirac(-1000, 0, .1), 1.)
    assert np.isclose(fermi_dirac(1000, 0, .1), 0.)


def test_bose_einstein():
    E = np.linspace(.1, 10, 100)
    assert np.allclose(bose_einstein(E, 0, .1), 1 / (np.exp(E / .1) - 1))
    assert np.allclose(bose_einstein(E + 4, 0, .1), bose_einstein(E, -4, .1))
    assert np.isclose(fermi_dirac(1e50, 0, .1), 0.)
    assert np.isclose(fermi_dirac(1000, 0, .1), 0.)


def test_dfermi_dirac():
    E = np.linspace(-2, 2, 10000)
    assert np.allclose(dfermi_dirac(E, 0., 1.), np.gradient(fermi_dirac(E, 0., 1.), E), rtol=1e-3)
