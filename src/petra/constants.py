'''Physical constants and units in Hartree atomic units
ref: CODATA 2010, NIST, http://physics.nist.gov/cuu/Constants/index.html
'''
from math import pi

# mathematical constant
degree = pi / 180

# fixed physical constants
q_e = 1.0  # elementary charge
m_e = 1.0  # electron mass
hbar = 1.0  # reduced Planck constant
ħ = hbar
h = 2 * pi * hbar  # Planck constant
k_B = 1.0  # Boltzmann constant
eps0 = 0.25 / pi  # vacuum permittivity
ε0 = eps0
mu_B = 0.5  # Bohr magneton
μ_B = 0.5
alpha = 7.2973525693e-3
c = 1 / alpha  # speed of light

# charge
C = 1.0 / 1.602176565e-19  # Coulomb

# current
A = 1.0 / 6.62361795e-3  # Ampere

# mass
kg = 1.0 / 9.10938291e-31  # kilogram

# energy
Ry = 0.5             # Rydbergs
eV = 3.674932379e-2  # electron-volt
meV = 1e-3 * eV      # milli electron-volt
J = 1.0 / 4.35974434e-18  # joule

# electrostatic potential
V = eV  # Volt
mV = meV

# temperature
K = 8.6173324e-5 * eV  # kelvin

# force
N = 1.0 / 8.23872278e-8  # newton

# length
Bohr = 1.0  # Bohr radius of H
m = 1.0 / 0.52917721092e-10  # meter
cm = 1e-2 * m
um = 1e-6 * m
µm = um
micron = um
nm = 1e-9 * m
Ang = 1e-10 * m  # Angstrom
Å = Ang

# volume
m3 = m**3    # cube meter
cm3 = cm**3  # cube centimeter

# magnetic flux
T = 1.0 / 2.350517464e5  # tesla

# time
s = 1.0 / 2.418884326502e-17  # second
ps = 1e-12 * s
fs = 1e-15 * s

# frequency
Hz = 1.0 / s  # Hertz
kHz = 1e3 * Hz
MHz = 1e6 * Hz
GHz = 1e9 * Hz
THz = 1e12 * Hz

# resistance and conductance
Ohm = V / A  # ohm
Ω = Ohm
S = A / V  # Siemens

# Avogadro

N_Avogadro = 6.02214076e23
mol = N_Avogadro
