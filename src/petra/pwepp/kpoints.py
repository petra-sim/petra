from collections import namedtuple

import numpy as np

from petra.periodiccell import PeriodicCell


KPoints = namedtuple('KPoints', ('nk', 'k_ka', 'weight_k', 'k_full_ka', 'map_k'))


def map_to_smallest(k_ka):
    return ((k_ka + .5) % -1.) + .5


def monkhorst_pack(atoms, nk, gamma=True):
    import spglib

    nk = tuple(nk)
    cell = atoms.get_cell()

    if len(nk) == 1:
        b = PeriodicCell(cell).reciprocal.a_len
        delta = np.mean(b)
        nk = tuple(np.ceil(nk * bi / delta) for bi in b)

    spg_cell = (cell[::-1],
                atoms.get_scaled_positions()[:, ::-1],
                atoms.get_atomic_numbers())
    shift = np.full(3, not gamma) & (np.array(nk) > 1)
    mapping, mesh = spglib.get_ir_reciprocal_mesh(nk[::-1], spg_cell, is_shift=shift[::-1])

    unique = np.unique(mapping)

    k_full_ka = map_to_smallest((mesh[:, ::-1] + shift * 0.5) / np.array(nk))
    k_ka = k_full_ka[unique]
    weight_k = np.bincount(mapping)[unique]

    map_to_full = np.zeros_like(mapping)
    for ii, idx in enumerate(unique):
        map_to_full[mapping == idx] = ii

    return KPoints(nk, k_ka, weight_k, k_full_ka, map_to_full)
