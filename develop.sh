#!/usr/bin/env bash
set -e
cd conda
conda env create
cd ..
conda activate petra
pip install -e .
