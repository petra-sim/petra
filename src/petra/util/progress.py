import time


def progress(sequence, size=None, name='Items', bar=False):
    try:
        size = len(sequence)
    except TypeError:
        pass
    t0 = tic = time.time()

    for index, record in enumerate(sequence, 1):
        if size is None:
            print('\r{}: {}'.format(name, index), end='')
        else:
            if index < 2:
                print('\r{}: {}/{}'.format(name, index, size), end='')
            else:
                toc = time.time() - tic
                eta = int(round(toc / (index - 1) * (size - index + 1)))
                print('\r{}: {}/{}, eta {}s   '.format(name, index, size, eta),
                      end='')
        yield record
    print('\r{}: {}/{}, finished in {}s'.format(name, index, size, time.time() - t0))
