IMAGE_TAG ?= petra-ci-env:latest

DOCKER_BUILD ?= docker build
DOCKER_RUN = docker run --rm -ti -v .:/mnt
DOCKER_RUN_CI = ${DOCKER_RUN} ${IMAGE_TAG}
SERVE_PORT ?= 8000
DOCKER_RUN_CI_SERVE = ${DOCKER_RUN} -p ${SERVE_PORT}:${SERVE_PORT} ${IMAGE_TAG}

SINGULARITY_BUILD ?= singularity build --fakeroot --force
SINGULARITY_KEY ?= ${HOME}/singularity/sypgp/pgp-private

.PHONY: clean
clean:
	find . -name '*.pyc' -exec rm -f {} +
	find . -name '*.pyo' -exec rm -f {} +
	find . -name '*~' -exec rm -f {} +
	find . -name '__pycache__' -exec rm -fr {} +

.PHONY: clean-build
clean-build:
	rm -fr build/
	rm -fr dist/
	rm -fr .eggs/
	find . -name '*.egg-info' -exec rm -fr {} +
	find . -name '*.egg' -exec rm -f {} +

.PHONY: clean-test
clean-test:
	rm -rf nosetests
	rm -rf mypy

.PHONY: clean-docs
clean-docs:
	cd doc && ${MAKE} clean

.PHONY: clean-containers
clean-containers:
	rm -f containers/*.sif

.PHONY: sdist
sdist: clean
	python3 setup.py sdist
	rm -f dist/petra-latest.tar.gz
	ln -s `python3 setup.py --fullname`.tar.gz dist/petra-latest.tar.gz
	@echo "sdist available at dist/petra-latest.tar.gz"

.PHONY: test
test: clean clean-test
	flake8
	mypy -p petra
	nosetests 

.PHONY: docs
docs: clean clean-docs
	doc8 doc/
	cd doc && ${MAKE} html

.PHONY: serve-docs
serve-docs:
	python3 -m http.server ${SERVE_PORT} --directory doc/build/html/

.PHONY: build-ci-docker
build-ci-docker: containers/ci-env.Dockerfile
	${DOCKER_BUILD} -t $(IMAGE_TAG) -f containers/ci-env.Dockerfile .
	@echo "Docker container $(IMAGE_TAG) built"

.PHONY: ci-test
ci-test:
	${DOCKER_RUN_CI} ${MAKE} test

.PHONY: ci-docs
ci-docs:
	${DOCKER_RUN_CI} ${MAKE} docs

.PHONY: ci-all
ci-all: ci-test ci-docs

.PHONY: ci-test
ci-serve-docs:
	${DOCKER_RUN_CI_SERVE} ${MAKE} serve-docs; true

containers/%.sif: containers/%.def sdist
	${SINGULARITY_BUILD} $@ $<
