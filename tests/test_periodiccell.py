import pytest

import numpy as np

from petra.periodiccell import PeriodicCell

good_cells = [
    np.array([[1., 0., 0.],
              [0., 1., 0.],
              [0., 0., 1.]]),
    np.array([[0., 1., 1.],
              [1., 0., 1.],
              [1., 1., 0.]]) * 2.715,
    np.array([[2.25, -3.9, 0.],
              [2.25, 3.9, 0.],
              [0., 0., 6.0]]),
    np.array([[2.45, 0., 0.],
              [-1.225, 2.1217622392718751, 0.],
              [0., 0., 20.]])
]

bad_cell = np.array([[-1., 0., 0.],
                     [0., -1., 0.],
                     [0., 0., -1.]])


def test_negative_volume():

    with pytest.raises(ValueError):
        PeriodicCell(bad_cell)


def test_direct_cartesian_conversion():

    for a in good_cells:
        pc = PeriodicCell(a)

        for i in range(10):
            R = (np.random.random(3) - 0.5) * 10
            r = pc.direct_to_cartesian(R)
            r_reconstruct = sum([Ri * ai for Ri, ai in zip(R, pc.a)])
            R_reconstruct = pc.cartesian_to_direct(r_reconstruct)
            assert(np.abs(r - r_reconstruct).max() < 1e-12)
            assert(np.abs(R - R_reconstruct).max() < 1e-12)


def test_lattice_lengths():

    for a in good_cells:
        pc = PeriodicCell(a)

        for a_len, ai in zip(pc.a_len, a):
            a_len_reconstruct = np.sqrt((ai**2).sum())
            assert(np.abs(a_len - a_len_reconstruct) < 1e-12)

        for i in range(10):
            R = (np.random.random(3) - 0.5) * 10
            r = pc.direct_to_cartesian(R)
            r_reconstruct = sum([Ri * ai for Ri, ai in zip(R, pc.a)])
            R_reconstruct = pc.cartesian_to_direct(r_reconstruct)
            assert(np.abs(r - r_reconstruct).max() < 1e-12)
            assert(np.abs(R - R_reconstruct).max() < 1e-12)
