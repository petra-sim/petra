from typing import Dict
from math import sqrt

import numpy as np
import numpy.linalg as la
from scipy import fft
from scipy import interpolate

from petra import constants as c
from petra.linalg.fft import spherical_ifft
from petra.pwepp.basis import Grid, PlaneWaves, TruncatedPlaneWaves

forms: Dict = {}


class Pseudopotential:
    def __init__(self, symbol, E_max, volume, form, params=None,
                 Nv=None, Nc=None):
        self.symbol = symbol
        self.E_max = E_max
        self.volume = volume
        self.Nv = Nv
        self.Nc = Nc
        self.form = form
        self.params = params

        if not callable(self.form):
            self.form = forms[self.form]

        if params is None:
            self.local = self.form
        else:
            try:
                self.local = self.form(**self.params)
            except TypeError:
                self.local = self.form(*self.params)

    @classmethod
    def read(cls, reader):
        data = reader.asdict()
        return cls.fromdict(data)

    def write(self, writer):
        writer.write(**self.todict())

    @classmethod
    def fromdict(cls, data):
        import dill
        from base64 import b64decode
        sform = b64decode(data['form'].encode('ascii'))
        data['form'] = dill.loads(sform)
        return cls(**data)

    def todict(self):
        import dill
        from base64 import b64encode
        sform = b64encode(dill.dumps(self.form)).decode('ascii')
        return dict(symbol=self.symbol, E_max=self.E_max,
                    volume=self.volume, Nc=self.Nc, Nv=self.Nv,
                    form=sform, params=self.params)

    @property
    def radial(self):
        return radial(self)

    def __repr__(self):
        return 'Pseudopotential({})'.format(self.symbol)


class Pseudopotentials:
    def __init__(self, positions_ia, symbols_i, pseudopotentials):
        self.positions_ia = np.asarray(positions_ia)
        self.symbols_i = np.asarray(symbols_i)
        self.pseudopotentials = pseudopotentials

    @property
    def _definition(self):
        return {self.pseudopotentials[sym]: self.positions_ia[sym == self.symbols_i]
                for sym in np.unique(self.symbols_i)}

    @property
    def Nv(self):
        return sum(self.pseudopotentials[symbol].Nv for symbol in self.symbols_i)

    @property
    def Nc(self):
        return sum(self.pseudopotentials[symbol].Nc for symbol in self.symbols_i)

    @property
    def E_max(self):
        return max(pp.E_max for pp in self.pseudopotentials.values())

    def VG_local_g(self, tpw):
        V_g = np.zeros(tpw.size, dtype=complex)
        Gnorm_g = la.norm(tpw.G_gv, axis=-1)

        cell_volume = tpw.pw.cell.volume

        for pp, spos_ia in self._definition.items():
            Vatom_g = pp.local(Gnorm_g) * pp.volume / cell_volume

            phase_g = sum(np.exp(2j * np.pi * tpw.G_ga @ spos_a)
                          for spos_a in spos_ia)

            V_g += Vatom_g * phase_g

        return V_g

    def VG_local_xyz(self, tpw):

        # if we are not given TruncatedPlaneWaves, generate them
        if not isinstance(tpw, TruncatedPlaneWaves):
            tpw = tpw.cutoff(n=np.inf)

        V_g = self.VG_local_g(tpw)

        return tpw.uncompress(V_g)

    def Vr_local_xyz(self, basis):
        if isinstance(basis, PlaneWaves) or isinstance(basis, TruncatedPlaneWaves):
            pw = basis
        elif isinstance(basis, Grid):
            pw = PlaneWaves.from_grid(basis)
        else:
            raise ValueError('basis has to be an instance of PlaneWaves or Grid')

        VG_xyz = self.VG_local_xyz(pw)

        return np.real(fft.fftn(VG_xyz))

    def __repr__(self):
        atoms = ', '.join('{}_{}'.format(pp.symbol, len(positions))
                          for pp, positions in self._definition.items())
        return '{}({})'.format(self.__class__.__name__, atoms)


# tools

def radial(pp, kind='cubic', eps=.05, n=1000):
    G_cut = np.sqrt(2 * pp.E_max)
    dr = eps * (2 * np.pi) / (4 * G_cut)
    r, V_r = radial_r(pp, dr, n)
    V = interpolate.interp1d(r, V_r, kind=kind, fill_value=0., bounds_error=False)
    return lambda x: V(np.abs(x))


def radial_r(pp, dr, n=1000):
    r, Vr = spherical_ifft(pp.local, dr, n)
    return r, pp.volume * Vr


def register(*names):
    def wrapper(function):
        for name in names:
            forms[name] = function
        return function
    return wrapper


# default forms

@register('function')
def function(V):
    return V


@register('interpolated')
def interpolated(method, q, Vq):
    return interpolate.interp1d(q, Vq, kind=method)


@register('dct')
def dct(Vi, qmax):
    r'''DCT Pseudopotential form. [1]_

    Parameters
    ----------
    Vi : array of float
        Pseudopotential at equidistant wavevectors
    qmax : float
        Maximum wavevector in pseudopotential

    References
    ----------
    .. [1] A.A. Laturia, M.L. Van de Put, W.G. Vandenberghe,
       "Generation of empirical pseudopotentials for transport applications and their
       application to group IV materials",
       J. Appl. Phys., vol. 128, pp. 034306 (2020).
       doi: 10.1063/5.0009838
    '''

    from scipy import fftpack
    params = np.concatenate([Vi, [0.]]) * c.eV
    a = fftpack.dct(params, type=1)
    a /= (2 * (len(a) - 1))
    a[1:-1] *= 2
    n = np.r_[0:len(a)]

    def V(q):
        V = np.cos(np.pi * q[..., None] / qmax * n) @ a
        V[np.abs(q) > qmax] = 0.0
        return V

    return V


@register('kurokawa')
def Kurokawa(lambda_, chi, mu, nu):
    '''Pseudopotential form for carbon. [1]_

    .. math ::
       V(q) = \\lambda (\\mu q^2 - \\chi)
            / (\\mathrm{e}^{\\mu q^2 - \\nu} + 1)

    Parameters
    ----------
    lambda_ : float
        :math:`\\lambda` in Rydbergs
    chi : float
        :math:`\\chi` in a.u.
    mu : float
        :math:`\\mu` in a.u.
    nu : float
        :math:`\\nu` in a.u.

    References
    ----------
    .. [1] Y. Kurokawa, S. Nomura, T. Takemori, and Y.Aoyagi,
       "Large-scale calculation of optical dielectric functions of diamond nanocrystallites",
       Phys. Rev. B, vol. 61 (19), pp. 12616 (2000).
       doi: 10.1103/PhysRevB.61.12616
    '''
    from petra.distributions import _fermi_dirac

    def V(q):
        mu_q2 = mu * q * q
        return lambda_ * (mu_q2 - chi) * _fermi_dirac(mu_q2 - nu) * c.Ry
    return V


@register('zhang')
def Zhang(b1, b2, b3, b4):
    '''Pseudopotential form used for silicon. [1]_ [2]_

    .. math ::
       V(q) = b_1 (q^2 - b_2) / (b_3 e^{b_4 q^2} - 1).

    Parameters
    ----------
    b1 : float
        :math:`b_1` in Rydbergs (a.u.)^-2
    b2 : float
        :math:`b_2` in (a.u.)^-2
    b3 : float
        :math:`b_3` unitless
    b4 : float
        :math:`b_4` in (a.u.)^2

    References
    ----------
    .. [1] S. B. Zhang, C.-Y. Yeh, and A. Zunger,
       "Electronic structure of semiconductor quantum films",
       Phys. Rev. B, vol 48 (15), pp. 11204 (1993).
       doi: 10.1103/PhysRevB.48.11204
    .. [2] L.-W. Wang, A. Zunger,
       "Solving Schrödinger's equation around a desired energy: Application to silicon quantum
       dots",
       J. Chem. Phys., vol. 100 (3), pp. 2394 (1994).
       doi: 10.1063/1.1367277
    '''
    from petra.distributions import _bose_einstein

    def V(q):
        q2 = q * q
        x = np.log(b3) + b4 * q2
        return b1 * (q2 - b2) * _bose_einstein(x) * c.Ry
    return V


@register('ballaiche')
def Bellaiche(al, bl, cl):
    '''Pseudopotential form for select zincblende III-V materials. [1]_

    .. math ::
       V(q) = \\sum_{l=1}^4 [a_l e^{-b_l (q - c_l)^2}]

    Parameters
    ----------
    al : (4,) array of float
        :math:`[a_1, a_2, a_3, a_4]` (in Rydbergs)
    bl : (4,) array of float
        :math:`[b_1, b_2, b_3, b_4]` (in 1/a.u.)
    cl : (4,) array of float
        :math:`[c_1, c_2, c_3, c_4]` (in a.u.^2)

    References
    ----------
    .. [1] L. Bellaiche, S.-H. Wei, and A. Zunger,
       "Localization and percolation in semiconductor alloys: GaAsN vs GaAsP",
       Phys. Rev. B, vol. 54 (24), pp. 17568 (1996).
       doi: 10.1103/PhysRevB.54.17568
    '''
    def V(q):
        return sum(ai * np.exp(-bi * (np.abs(q) - ci)**2)
                   for ai, bi, ci in zip(al, bl, cl)) * c.Ry
    return V


@register('wang')
def Wang(b0, b1, b2, b3, b4, b5, b6, b7):
    '''Pseudopotential form for hydrogen termination. [1]_ [2]_

    .. math ::
       V(q) = b_0 + b_1 q + b_2 q^2 + b_3 q^3
       \\quad \\text{for } q \\leq 2 \\\\
       V(q) = b_4 / q + b_5 / q^2 + b_6 / q^3 + b_7 / q^4
       \\quad \\text{for } q > 2

    Parameters
    ----------
    b0 : float
        :math:`b_0` in Rydbergs
    b1 : float
        :math:`b_1` in Rydbergs a.u.
    b2 : float
        :math:`b_2` in Rydbergs (a.u.)^2
    b3 : float
        :math:`b_3` in Rydbergs (a.u.)^3
    b4 : float
        :math:`b_4` in Rydbergs (a.u.)^-1
    b5 : float
        :math:`b_5` in Rydbergs (a.u.)^-2
    b6 : float
        :math:`b_6` in Rydbergs (a.u.)^-3
    b7 : float
        :math:`b_7` in Rydbergs (a.u.)^-4

    References
    ----------
    .. [1] L.-W. Wang, A. Zunger,
       "Solving Schrödinger's equation around a desired energy: Application to
       silicon quantum dots",
       J. Chem. Phys., vol. 100 (3) pp. 2394 (1994).
       doi: 10.1063/1.1367277
    .. [2] Y. Kurokawa, S. Nomura, T. Takemori, and Y.Aoyagi,
       "Large-scale calculation of optical dielectric functions of diamond
       nanocrystallites",
       Phys. Rev. B, vol. 61, 19, pp. 12616 (2000).
       doi: 10.1103/PhysRevB.61.12616
    '''
    def V1(q):
        return b0 + b1 * q + b2 * q**2 + b3 * q**3

    def V2(q):
        return b4 / q + b5 / q**2 + b6 / q**3 + b7 / q**4

    def V(q):
        q = np.abs(q)
        Vq = np.piecewise(q, [q <= 2.], [V1, V2]) * c.Ry
        Vq[q > 4.5] = 0.0  # hard E_max at ~ 275 eV
        return Vq

    return V


@register('friedel')
def Friedel(b1, b2, b3, b4, b5, b6):
    '''Friedel Pseudopotential form. [1]_

    .. math ::
       V(q) = b_1 / 2 (q^2 - b_2) / (1 + e^(b_3 (q^2 - b_4)))
       \\times (\\tanh((b_5 - q^2) / b_6) + 1)$.

    Parameters
    ----------
    b1 : float
        in Rydbergs
    b2 : float
        in a.u.
    b3 : float
        in a.u.
    b4 : float
        in a.u.
    b5 : float
        in a.u.
    b6 : float
        in a.u.

    References
    ----------
    .. [1] P. Friedel, M.S. Hybertsen, M. Schluter,
       "Local empirical pseudopotential approach th the optical properties of Si/Ge
       superlattices",
       Phys. Rev. B, Vol. 39, page 7974 (1989).
       doi: 10.1103/PhysRevB.39.7974
    '''
    def V(q):
        q2 = np.array(q)**2
        V0 = b1 / 2 * (q2 - b2) / (1 + np.exp(b3 * (q2 - b4)))
        return V0 * (1 + np.tanh((b5 - q2) / b6)) * c.Ry
    return V


# some default pseudopotentials

library: Dict[str, Dict[str, Pseudopotential]] = {
    'kurokawa': {
        'C': Pseudopotential(
            symbol='C',
            form='kurokawa',
            params=[1.781, 1.424, 0.354, 0.938],
            E_max=250 * c.eV,
            volume=5.672 * c.Ang**3,
            Nv=4,
        ),
        'H': Pseudopotential(
            symbol='H',
            form='wang',
            params=[-0.397, 0.0275, 0.1745, -0.0531,
                    0.0811, -1.086, 2.71, -2.86],
            E_max=100 * c.eV,
            volume=5.6741 * c.Ang**3,
            Nv=1,
        ),
    },
    'zhang': {
        'Si': Pseudopotential(
            symbol='Si',
            form='zhang',
            params=[0.53706, 2.19104, 2.05716, 0.48716],
            E_max=125 * c.eV,
            volume=20.01 * c.Ang**3,
            Nv=4,
        ),
        'H': Pseudopotential(
            symbol='H',
            form='wang',
            params=[-0.1416, 0.009802, 0.06231, -0.01895,
                    0.02898, -0.3877, 0.9692, -1.022],
            E_max=100 * c.eV,
            volume=40.02 * c.Ang**3,
            Nv=1,
        ),
    },
    'laturia': {
        'C': Pseudopotential(
            symbol='C',
            form='dct',
            params=[
                [-9.4315, -6.9199, 10.9118, 11.2437, 10.0083],
                sqrt(2 * 360 * c.eV)
            ],
            E_max=720 * c.eV,
            volume=(3.567 * c.Ang)**3 / 8,
            Nv=4,
        ),
        'Si': Pseudopotential(
            symbol='Si',
            form='dct',
            params=[
                [-15.1731, -6.1226, 0.5034, 1.0068, -0.2503],
                sqrt(2 * 225 * c.eV)
            ],
            E_max=225 * c.eV,
            volume=(5.431 * c.Ang)**3 / 8,
            Nv=4,
        ),
        'Ge': Pseudopotential(
            symbol='Ge',
            form='dct',
            params=[
                [-13.7417, -5.0123, 0.4816, 1.0313, 0.0408],
                sqrt(2 * 250 * c.eV)
            ],
            E_max=250 * c.eV,
            volume=(5.658 * c.Ang)**3 / 8,
            Nv=4,
        ),
        '4H-SiC.C': Pseudopotential(
            symbol='C',
            form='dct',
            params=[
                [-19.0425, -11.5757, 1.3851, 3.6926, 0.5361],
                sqrt(2 * 400 * c.eV)
            ],
            E_max=600 * c.eV,
            volume=(3.567 * c.Ang)**3 / 8,
            Nv=4,
        ),
        '3C-SiC.C': Pseudopotential(
            symbol='C',
            form='dct',
            params=[
                [-18.1881, -11.2410, 1.9266, 3.5810, 0.5870],
                sqrt(2 * 400 * c.eV)
            ],
            E_max=600 * c.eV,
            volume=(3.567 * c.Ang)**3 / 8,
            Nv=4,
        ),
        'SiH.H': Pseudopotential(
            symbol='H',
            form='dct',
            params=[
                [-3.4123, -2.3266, -0.7592, -0.5415, -0.5061],
                sqrt(2 * 200 * c.eV)
            ],
            E_max=200 * c.eV,
            volume=(5.431 * c.Ang)**3 / 8,
            Nv=1,
        ),
        'GeH.H': Pseudopotential(
            symbol='H',
            form='dct',
            params=[
                [-2.7157, -1.6871, -0.9442, -0.5361, -0.0735],
                sqrt(2 * 250 * c.eV)
            ],
            E_max=250 * c.eV,
            volume=(3.567 * c.Ang)**3 / 8,
            Nv=1,
        ),
    }
}
