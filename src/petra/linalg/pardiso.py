"""
Code inspired by: https://github.com/haasad/PyPardisoProject
"""

import ctypes
import ctypes.util
import functools
import hashlib
import warnings

import numpy as np
from numpy import ctypeslib
import scipy.sparse as sp
from scipy.sparse import SparseEfficiencyWarning

use_pardiso = False
libmkl = None
libmkl_path = ctypes.util.find_library('mkl_rt')

if libmkl_path is not None:
    try:
        libmkl = ctypes.CDLL(libmkl_path)
        libmkl.pardiso.restype = None
        use_pardiso = True

    except OSError:
        pass


def mkl_version():
    version_buf = ctypes.create_string_buffer(198)
    libmkl.mkl_get_version_string.restype = None
    libmkl.mkl_get_version_string.argtypes = [ctypes.c_char_p, ctypes.c_int]
    libmkl.mkl_get_version_string(version_buf, 198)
    return version_buf.value.decode('ascii').strip()


def factorized(A):
    if A.dtype == np.float64:
        solver = PardisoSolver(mtype=11)
    elif A.dtype == np.complex128:
        solver = PardisoSolver(mtype=13)
    else:
        raise NotImplementedError()

    return solver.factorize(A)


def spsolve(A, b):
    return factorized(A).solve(b)


class PardisoSolver:
    """Python interface to the Intel MKL PARDISO library for solving large sparse
    linear systems of equations Ax=b.

    Pardiso documentation: https://software.intel.com/en-us/node/470282

    - Use the "solve(A, b)" method to solve Ax=b for x, where A is a sparse CSR
      or CSC matrix and b is a numpy array
    - Use the "factorize(A)" method first, if you intend to solve the system
      more than once for different right-hand sides,
      the factorization will be reused automatically afterwards

    Notes
    -----
    - The Pardiso solver writes statistical info to the C stdout if desired
    - You can control the number of threads by setting the MKL_NUM_THREADS
      environment variable
    - remove_stored_factorization() can be used to delete the wrapper's copy of
      matrix A
    - free_memory() releases the internal memory of the solver
    """

    def __init__(self, mtype=11, size_limit_storage=5e7):

        self._mkl_pardiso = libmkl.pardiso

        self.mtype = mtype
        if self.mtype == 11:
            self.dtype = np.float64
        elif self.mtype == 13:
            self.dtype = np.complex128
        else:
            raise NotImplementedError()

        fcont = 'F_CONTIGUOUS'

        if ctypes.sizeof(ctypes.c_void_p) == 8:
            self.ptype = np.int64
        else:
            self.ptype = np.int32

        self.argtypes = [
            ctypeslib.ndpointer(dtype=self.ptype, ndim=1, flags=fcont),  # pt
            ctypes.POINTER(ctypes.c_int32),      # maxfct
            ctypes.POINTER(ctypes.c_int32),      # mnum
            ctypes.POINTER(ctypes.c_int32),      # mtype
            ctypes.POINTER(ctypes.c_int32),      # phase
            ctypes.POINTER(ctypes.c_int32),      # n
            ctypeslib.ndpointer(dtype=self.dtype, ndim=1, flags=fcont),  # a
            ctypeslib.ndpointer(dtype=np.intc, ndim=1, flags=fcont),  # ia
            ctypeslib.ndpointer(dtype=np.intc, ndim=1, flags=fcont),  # ja
            ctypeslib.ndpointer(dtype=np.intc, ndim=1, flags=fcont),  # perm
            ctypes.POINTER(ctypes.c_int32),      # nrhs
            ctypeslib.ndpointer(dtype=np.intc, ndim=1, flags=fcont),  # iparm
            ctypes.POINTER(ctypes.c_int32),      # msglvl
            ctypeslib.ndpointer(dtype=self.dtype, ndim=2, flags=fcont),  # b
            ctypeslib.ndpointer(dtype=self.dtype, ndim=2, flags=fcont),  # x
            ctypes.POINTER(ctypes.c_int32),      # error
        ]

        self.pt = np.zeros(64, dtype=self.ptype)
        self.iparm = np.zeros(64, dtype=np.intc)

        self.msglvl = False

        self.factorized_A = sp.csr_matrix((0, 0), dtype=self.dtype)
        self.size_limit_storage = size_limit_storage
        self._solve_transposed = False

    def factorize(self, A):
        """
        Factorize the matrix A, the factorization will automatically be used if
        the same matrix A is passed to the solve method. This will drastically
        increase the speed of solve, if solve is called more than once for the
        same matrix A

        Parameters
        ----------
        A: scipy.sparse.csr.csr_matrix
        """

        self._check_A(A)

        if A.nnz > self.size_limit_storage:
            self.factorized_A = self._hash_csr_matrix(A)
        else:
            self.factorized_A = A.copy()

        b = np.zeros((A.shape[0], 1), dtype=self.dtype)

        self._call_pardiso(A, b, phase=12)

        return functools.partial(self.solve, A)

    def solve(self, A, b):
        """
        solve Ax=b

        Parameters
        ----------
        A : scipy.sparse.csr.csr_matrix

        b : np.ndarray
            right-hand side(s), b.shape[0] needs to be the same as A.shape[0]

        Returns
        -------
        x: np.ndarray
            solution of the system of linear equations, same shape as input b
        """
        shape = b.shape

        self._check_A(A)
        b = self._check_b(A, b)

        phase = 33 if self._is_already_factorized(A) else 13

        x = self._call_pardiso(A, b, phase=phase)

        return x.reshape(shape)

    def _is_already_factorized(self, A):
        if type(self.factorized_A) == str:
            return self._hash_csr_matrix(A) == self.factorized_A
        else:
            return self._csr_matrix_equal(A, self.factorized_A)

    def _csr_matrix_equal(self, a1, a2):
        return all((np.array_equal(a1.indptr, a2.indptr),
                    np.array_equal(a1.indices, a2.indices),
                    np.array_equal(a1.data, a2.data)))

    def _hash_csr_matrix(self, matrix):
        return (hashlib.sha1(matrix.indices).hexdigest() +
                hashlib.sha1(matrix.indptr).hexdigest() +
                hashlib.sha1(matrix.data).hexdigest())

    def _check_A(self, A):
        if A.shape[0] != A.shape[1]:
            raise ValueError('Matrix A needs to be square, but has shape: {}'
                             ''.format(A.shape))

        if sp.isspmatrix_csr(A):
            self._solve_transposed = False
            self.set_iparm(12, 0)

        elif sp.isspmatrix_csc(A):
            self._solve_transposed = True
            self.set_iparm(12, 2)
        else:
            msg = ('PyPardiso requires matrix A to be in CSR or CSC format, '
                   'but matrix A is: {}'.format(type(A)))
            raise TypeError(msg)

        # scipy allows unsorted csr-indices,
        # which lead to completely wrong pardiso results
        if not A.has_sorted_indices:
            A.sort_indices()

        # Avoid calling Pardiso with empty rows (columns for CSC).
        # This causes a segfault.
        if not np.diff(A.indptr).all():
            row_col = 'column' if self._solve_transposed else 'row'
            raise ValueError('Matrix A is singular, because it contains empty '
                             '{}(s)'.format(row_col))

        if A.dtype != self.dtype:
            raise TypeError('Problem type expects a {} matrix, but matrix A '
                            'has dtype: {}'.format(self.dtype, A.dtype))

    def _check_b(self, A, b):
        if sp.isspmatrix(b):
            warnings.warn('PyPardiso requires the right-hand side b to be a '
                          'dense array for maximum efficiency',
                          SparseEfficiencyWarning)
            b = b.todense()

        if b.ndim == 1:
            b = b[:, None]

        # Pardiso expects Fortran (column-major) order if b is a matrix
        b = np.asfortranarray(b)

        if b.shape[0] != A.shape[0]:
            raise ValueError("Dimension mismatch: Matrix A "
                             "{} and array b {}".format(A.shape, b.shape))

        if b.dtype != self.dtype:
            if b.dtype in [np.float16, np.float32, np.complex64,
                           np.int16, np.int32, np.int64]:
                warnings.warn("Array b's data type was converted from {}"
                              " to {}".format(str(b.dtype), str(self.dtype)),
                              PyPardisoWarning)
                b = b.astype(self.dtype)
            else:
                raise TypeError('dtype {} for array b is not '
                                'supported'.format(str(b.dtype)))
        return b

    def _call_pardiso(self, A, b, phase=13):

        x = np.zeros_like(b)

        pardiso_error = ctypes.c_int32(0)

        # 1-based indexing
        ia = A.indptr + 1
        ja = A.indices + 1

        N = A.shape[0]
        nrhs = b.shape[1]

        perm = np.zeros(N, dtype=np.intc)

        self._mkl_pardiso.argtypes = self.argtypes
        self._mkl_pardiso(
            self.pt,
            ctype_int(1),  # maxfct
            ctype_int(1),  # mnum
            ctype_int(self.mtype),
            ctype_int(phase),
            ctype_int(N),
            A.data, ia, ja,
            perm,
            ctype_int(nrhs),
            self.iparm,
            ctype_int(self.msglvl),
            b, x,
            ctypes.byref(pardiso_error),
        )

        if pardiso_error.value != 0:
            raise PyPardisoError(pardiso_error.value)

        # change memory-layout back from fortran to c order
        return np.ascontiguousarray(x)

    def get_iparms(self):
        """Returns a dictionary of iparms"""
        return dict(enumerate(self.iparm, 1))

    def get_iparm(self, i):
        """Returns the i-th iparm (1-based indexing)"""
        return self.iparm[i - 1]

    def set_iparm(self, i, value):
        """Set the i-th iparm to 'value' (1-based indexing)"""
        if i not in {1, 2, 4, 5, 6, 8, 10, 11, 12, 13, 18, 19, 21, 24, 25, 27,
                     28, 31, 34, 35, 36, 37, 56, 60}:
            warnings.warn('{} is no input iparm. See the Pardiso '
                          'documentation.'.format(value), PyPardisoWarning)
        self.iparm[i - 1] = value

    def set_statistical_info_on(self):
        """Display statistical info"""
        self.msglvl = 1

    def set_statistical_info_off(self):
        """Turns statistical info off"""
        self.msglvl = 0

    def remove_stored_factorization(self):
        """Removes the stored factorization.
        This will free the memory in python, but the factorization in pardiso
        is still accessible with a direct call to self._call_pardiso(A, b, phase=33)
        """
        self.factorized_A = sp.csr_matrix((0, 0), dtype=self.dtype)

    def free_memory(self, all=True):
        """Release internal PARDISO memory.
        Only the factorization, i.e., the LU-decomposition if all==False
        All of the internal memory if all==True (default)
        """
        self.remove_stored_factorization()
        A = sp.csr_matrix((0, 0), dtype=self.dtype)
        b = np.zeros((0, 0), dtype=self.dtype)
        phase = -1 if all else 0
        self._call_pardiso(A, b, phase=phase)

    def __del__(self):
        self.free_memory()


class PyPardisoWarning(UserWarning):
    pass


class PyPardisoError(Exception):

    def __init__(self, value):
        self.value = value

    def __str__(self):
        return ('The Pardiso solver failed with error code {}. '
                'See Pardiso documentation for details.'.format(self.value))


def ctype_int(value):
    return ctypes.byref(ctypes.c_int32(value))
