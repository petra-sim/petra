import numpy as np

from petra.integrate.simpson import LocalIntegrator


def test_integration_pole():

    def f(x):
        return np.piecewise(x, [x <= 0, x > 0],
                            [0.0, lambda x: 1.0 / np.sqrt(x)]),
    xmin, xmax = -1, 1
    analytic = 2.0
    integrate(f, xmin, xmax, analytic)


def test_integration_accuracy():

    def f(x):
        return np.sin(x) / x + np.exp(-(x - 4)**2), np.sin(x)
    xmin, xmax = 1, 10
    analytic = 2.484698797563970262861816781878156908423370264215762388394
    integrate(f, xmin, xmax, analytic)


def integrate(f, xmin, xmax, analytic):
    w = LocalIntegrator(f, xmin, xmax)

    w.refine(iterations=1000, atol=1e-8)

    assert(len(w) < 1000)
    assert(w.abs_error < 1e-8)

    reconstruct = np.sum(np.array(w.f)[:, 0] * w.weights)
    assert(np.allclose(reconstruct, w.value))

    error = np.abs(w.value - analytic)
    assert(error < 1e-8)

    xlin, dx = np.linspace(xmin, xmax, len(w.x), retstep=True)
    flin = f(xlin)[0]
    trapz = np.trapz(flin, dx=dx)
    trapz_error = np.abs(trapz - analytic)
    assert(error < trapz_error)
