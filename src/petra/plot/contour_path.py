import numpy as np
import matplotlib.pyplot as plt


def plot_direction(x, y, color):
    center = len(x) // 2
    dx = x[center + 2] - x[center - 2]
    dy = y[center + 2] - y[center - 2]
    plt.arrow(x[center + 2], y[center + 2], dx, dy, color=color,
              shape='full', lw=0, length_includes_head=True,
              head_width=1 * (dx + dy),
              head_length=2 * (dx + dy))


def plot(z, *args, **kwargs):
    line, = plt.plot(np.real(z), np.imag(z), *args, **kwargs)
    plot_direction(np.real(z), np.imag(z), color=line.get_c())
