from functools import cached_property


def invalidate_cached_property(obj, *names):
    '''Invalidates cached properies defined using functools.cached_property

    Arguments
    ---------
    obj
        The object to which the properties belong
    *names
        The names of the properties to invalidate
    '''
    for name in names:
        if not isinstance(getattr(type(obj), name), cached_property):
            raise AttributeError(f'{name} is not a cached_property')

        try:
            delattr(obj, name)
        except AttributeError:
            pass
