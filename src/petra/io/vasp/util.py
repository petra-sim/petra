from pathlib import Path

import numpy as np


class CommonFilePath:

    @property
    def fpath(self):
        if self.path.is_dir():
            return self.path / self.__class__.__name__
        return self.path

    @property
    def fname(self):
        return str(self.fpath)


class CommonFile(CommonFilePath):

    def __init__(self, path='', read=True):
        self.path = Path(path)
        if read:
            self.read()

    @classmethod
    def create(cls, path=''):
        return cls(path, read=False)

    def read(self):
        with self.fpath.open('r') as f:
            self._read(f)

    def write(self, path=None):
        if path is not None:
            self.path = Path(path)

        with self.fpath.open('w') as f:
            self._write(f)

    def _read(sef, f):
        raise NotImplementedError()

    def _write(sef, f):
        raise NotImplementedError()


def read_array(f, size):
    array = []
    while len(array) < np.prod(size):
        line = f.readline().strip().replace('D', 'E')
        array.extend(map(float, line.split()))
    return np.array(array).reshape(size, order='F')


def parse_variable(line):
    var, equal, value = line.strip().partition('=')
    if not equal:
        return None, None

    var = var.strip().lower()
    value = value.strip().split()[0]

    if value.lower().strip('.') in ('t', 'true', 'on'):
        pvalue = True
    elif value.lower().strip('.') in ('f', 'false', 'off'):
        pvalue = False
    else:
        pvalue = value

    try:
        pvalue = float(value)
        pvalue = int(value)
    except ValueError:
        pass

    return var, pvalue
