'''Known compounds

Sources:
    tmds: J. Phys. Chem. C, 119, 13169−13183 (2015)
          [doi: 10.1021/acs.jpcc.5b02950]
    hexagonal: Phys. Rev. B, 80, 155453 (2009)
               [doi: 10.1103/PhysRevB.80.155453]
'''
import os.path
import json

import numpy as np
from ase.lattice.hexagonal import HexagonalFactory
from ase.lattice.orthorhombic import SimpleOrthorhombicFactory
from ase.build import mx2
from ase.symbols import string2symbols

materials_db = os.path.join(os.path.dirname(__file__), 'materials.json')
with open(materials_db) as f:
    materials = json.load(f)


def _match(material, **props):
    for prop in props:
        if props[prop] is None:
            continue
        if material[prop] != props[prop]:
            return False
    return True


def _alias(materials, material):
    if 'alias' in material:
        material = materials[material['alias']]
    return material


def _center(atoms, centered):
    if centered:
        atoms.center()
    return atoms


def _symbols(formula, N):
    symbols = string2symbols(formula)
    if len(symbols) == 1:
        symbols *= N
    assert(len(symbols) == N)
    return symbols


def search_2d(name=None, formula=None, type=None, kind=None,
              vacuum=15, centered=True, single=True):
    if name is not None:
        material = _alias(materials['2d'], materials['2d'][name])
        return create_2d(material, vacuum=vacuum, centered=centered)

    selected = [_alias(materials['2d'], material)
                for material in materials['2d']
                if _match(material, formula=formula, type=type, kind=kind)]
    if not selected:
        raise ValueError('no matching material found')
    if single and len(selected) > 1:
        raise ValueError('more than one matching material found')
    selected = [create_2d(material, vacuum=vacuum, centered=centered)
                for material in selected]
    return selected[0] if single else selected


def create_2d(material, vacuum=15, centered=True):
    if material['type'] == 'tmd':
        return create_2d_tmd(**material, vacuum=vacuum, centered=centered)
    if material['type'] == 'hex':
        return create_2d_hex(**material, vacuum=vacuum, centered=centered)
    if material['type'] == 'rect':
        return create_2d_rect(**material, vacuum=vacuum, centered=centered)
    raise ValueError('unknown material type')


def create_2d_tmd(formula, a, thickness, kind, vacuum=15, centered=True, **_):
    atoms = mx2(formula=formula, a=a, thickness=thickness,
                kind=kind, vacuum=vacuum)
    return _center(atoms, centered)


def create_2d_hex(formula, a, buckling=0.0, vacuum=15, centered=True, **_):
    c = buckling + vacuum
    buckling = buckling / c

    class _Factory(HexagonalFactory):
        xtal_name = formula
        bravais_basis = [[0, 0, 0],
                         [1 / 3, 2 / 3, buckling]]
        element_basis = [0, 1]

    atoms = _Factory()(symbol=_symbols(formula, 2),
                       latticeconstant={'a': a, 'c': c})
    return _center(atoms, centered)


def create_2d_rect(formula, a, b, h=0.0, bond_length=None,
                   vacuum=15, centered=True, **_):
    if bond_length is not None:
        b1 = np.sqrt(bond_length**2 - (a / 2)**2)
        b2 = b / 2 - b1
        h = np.sqrt(bond_length**2 - b2**2)
    else:
        b1 = (b**2 + 4 * h**2 - a**2) / (4 * b)

    c = h + vacuum

    class _Factory(SimpleOrthorhombicFactory):
        xtal_name = formula
        bravais_basis = [
            [0., 0., h / c],
            [.5, b1 / b, h / c],
            [.5, .5, 0],
            [0, .5 + b1 / b, 0],
        ]
        element_basis = [0, 1, 2, 3]

    atoms = _Factory()(symbol=_symbols(formula, 4),
                       latticeconstant={'a': a, 'b': b, 'c': c})
    return _center(atoms, centered)
