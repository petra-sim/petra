{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 3. Ballistic Transport"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib inline\n",
    "\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "\n",
    "from petra import constants as c\n",
    "from petra.plot import use_latex\n",
    "from petra.util import log\n",
    "\n",
    "log.to_stdout()\n",
    "use_latex()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 3.1. Building the Structure\n",
    "\n",
    "As discussed in the previous notebook, we build our `Block` by loading the PWEPP calculator. We again let the Block automatically choose the best basis and direction."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from petra.pwepp import PWEPP\n",
    "from petra.transport import Block\n",
    "\n",
    "calc = PWEPP('7agnr.pwepp')\n",
    "block = Block.from_calc(calc)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We want to calculate transport through a larger structure than a single `Block`. Therefore, we combine `Blocks` into a `Structure`. The simplest, and perhaps most useful, structure consists of a single `Block`, repeated $N$ times along its periodic axis. This structure can be realized in petra using a `SimpleHomoStructure`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from petra.transport import SimpleHomoStructure\n",
    "\n",
    "structure = SimpleHomoStructure(block, N=40)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A structure contains elements (`structure.elements`), which are built from the blocks and contain nodes. These nodes are linked to the structure nodes (`structure.nodes`) which have an assigned position.\n",
    "\n",
    "The complete set of atoms contained in the structure can be plotted:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from petra.atoms import get_color_float\n",
    "\n",
    "plt.figure(dpi=150, figsize=(10, 1))\n",
    "plt.scatter(structure.atoms.positions[:, 2] / 10, \n",
    "            structure.atoms.positions[:, 0] / 10, \n",
    "            c=get_color_float(structure.atoms.get_atomic_numbers()),\n",
    "            s=structure.atoms.get_masses() * .8,\n",
    "            edgecolors='k', linewidths=.2)\n",
    "plt.xlabel(r'$z\\ \\mathrm{(nm)}$')\n",
    "plt.ylabel(r'$x\\ \\mathrm{(nm)}$')\n",
    "plt.axis('equal');"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now that we have defined a structure, we can associate a potential energy field with it. In this part of the introduction, we specify an arbitrary potential. However, later on, we will couple the Schroedinger and Poisson equations to obtain a self-consistent, physical potential.\n",
    "\n",
    "For now though we make up the following potential:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from petra.examples.potentials import smooth_barrier, pn_junction\n",
    "\n",
    "V0 = -structure.elements.contacts[0].bandstructure().find_cb()\n",
    "center = structure.r_rv.max(axis=0) / 2  # the center of the structure\n",
    "\n",
    "def V_mosfet(r_xyzv):\n",
    "    x, y, z = np.moveaxis(r_xyzv - center, -1, 0)\n",
    "    barrier = smooth_barrier(z, 3*c.nm, 0.2*c.eV, 5*c.nm)\n",
    "    driving = pn_junction(z - 3*c.nm, 5*c.nm, -0.1*c.eV)\n",
    "    return V0 + barrier + driving"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We set the potential in the structure"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "structure.set_potential(V_mosfet)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The potential is given back as a list for each element $i$, with the corresponding positions ${r}_i$ and potential $V_i(r)$. So for element 2, we acces the $r$-coordinates as `r_V_per_element[3][0]` and the potential as `r_V_per_element[3][1]`.\n",
    "\n",
    "We plot the potential, but since it is a 4-dimensional quantity, we will use a technique of plotting random points projected onto two axis:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "\n",
    "\n",
    "plot_args = dict(cmap='coolwarm', marker='.', edgecolor='none',\n",
    "                 vmin=V0/c.eV-0.2, vmax=V0/c.eV+0.2, alpha=0.5, zorder=0)\n",
    "\n",
    "def random_index_of(array):\n",
    "    return tuple(np.random.randint(0, axis - 1, 10000) for axis in array.shape)\n",
    "\n",
    "plt.figure(figsize=(8, 2), dpi=150)\n",
    "\n",
    "r_rv = structure.r_rv\n",
    "V_r = structure.V_r()\n",
    "\n",
    "plt.subplot(121)\n",
    "idx = random_index_of(V_r)\n",
    "z_plot = r_rv[idx][:, 2]\n",
    "plt.scatter(z_plot / c.nm, V_r[idx] / c.eV, c=V_r[idx] / c.eV, **plot_args)\n",
    "plt.xlabel(r'$z\\ \\mathrm{[nm]}$')\n",
    "plt.ylabel(r'$V\\ \\mathrm{[eV]}$')\n",
    "\n",
    "plt.subplot(122)\n",
    "idx = random_index_of(V_r)\n",
    "x_plot = r_rv[idx][:, 2]\n",
    "y_plot = r_rv[idx][:, 0]\n",
    "plt.scatter(x_plot / c.nm, y_plot / c.nm, c=V_r[idx] / c.eV, **plot_args)\n",
    "plt.xlabel(r'$z\\ \\mathrm{[nm]}$')\n",
    "plt.ylabel(r'$x\\ \\mathrm{[nm]}$')\n",
    "cbar = plt.colorbar()\n",
    "cbar.set_label(r'$V\\ \\mathrm{[eV]}$')\n",
    "\n",
    "pass"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, ax = plt.subplots(dpi=150, figsize=(5.7, 2), constrained_layout=True)\n",
    "for el in structure.elements:\n",
    "    ax.plot(el.r_xyzv.mean((0, 1))[:, 2]/c.nm, el.V_xyz().mean((0, 1))/c.eV)\n",
    "ax.plot(structure.nodes.position[:, 2]/c.nm, structure.node_V/c.eV, 'k.')\n",
    "ax.set_ylabel('$E$ (eV)')\n",
    "ax.set_xlabel('$z$ (nm)');"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## QTBM: Injecting Waves\n",
    "\n",
    "Now that we have a structure with a given potential, we can begin exploring how ballistic transport is calculated in PETRA. To describe transport in our open system, we use the Quantum Transmitting Boundary Method (QTBM), which provides the most efficient way to describe __ballistic__ quantum transport.\n",
    "\n",
    "First, we look at the injection of waves at a single energy:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "E_single = structure.elements.contacts[0].bandstructure().find_cb() + 0.15 * c.eV\n",
    "print('Evaluation at E = {:.2f} eV'.format(E_single / c.eV))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "z = structure.nodes.position[:, 2] / c.nm\n",
    "E_cb = structure.elements.contacts[0].bandstructure().find_cb() + (structure.node_V - structure.node_V[0])\n",
    "\n",
    "fig, ax = plt.subplots(dpi=150, figsize=(5.7, 2), constrained_layout=True)\n",
    "ax.axhline(E_single/c.eV)\n",
    "ax.plot(structure.nodes.position[:, 2]/c.nm, E_cb/c.eV, 'k.-')\n",
    "ax.set_ylabel('$E$ (eV)')\n",
    "ax.set_xlabel('$z$ (nm)');"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We use the `petra.open_system` module to find the wavefunctions at this energy. `petra.open_system.solve_E(...)` automatically calculates the complex bandstructure to find the available waves, build the appropriate boundary conditions and solve for the extended states at the given energy."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from petra.transport import open_system\n",
    "states = open_system.solve_E(structure, E_single)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can plot the wavefunctions averaged over each node $i$ of the system, these are, roughly speaking, the envelopes of the wave. We plot the density of each mode $\\langle|\\psi(r)|^2\\rangle_i$ with respect to the position of the node $r_i$:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.figure(dpi=150)\n",
    "for ii, state in enumerate(states):\n",
    "    wave2 = structure.node_psi2(state)\n",
    "    l, = plt.plot(z, wave2, '.-', label='mode {} from {}'.format(ii, state.icontact))\n",
    "plt.legend(loc=0, ncol=2)\n",
    "plt.ylabel(r'$\\psi^2$ (arb. units)')\n",
    "plt.xlabel('$z$ (nm)')\n",
    "plt.xlim(0, z[-1]);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "From this picture, we observe that there are 4 modes being injected. Two from the left and two from the right, which decay exponentially as they hit the barrier in the middle.\n",
    "\n",
    "Next, we plot the local density of states (ldos) of the waves. The ldos is the density of states of the state weighed by the probability density of the wavefunction."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.figure(dpi=150)\n",
    "for ii, state in enumerate(states):\n",
    "    ldos = structure.node_density(state)\n",
    "    plt.plot(z, ldos / (c.nm * c.eV), '.-', label='mode {} from {}'.format(ii, state.icontact))\n",
    "plt.ylabel('ldos (eV~nm)')\n",
    "plt.xlabel('$z$ (nm)')\n",
    "plt.xlim(0, z[-1])\n",
    "plt.legend(loc=0, ncol=2)\n",
    "pass"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The last piece of information we will show is the transmission coefficient. The transmission coefficient is calculated from each mode to each contact and also between contacts. Reflection coefficients are shown as transmission to the originating contact."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from petra.plot import annotated_imshow\n",
    "\n",
    "T_sc = np.array([state.T_c for state in states])\n",
    "ncontacts = len(structure.elements.contacts)\n",
    "T_cc = np.zeros((ncontacts, ncontacts))\n",
    "for state in states:\n",
    "    T_cc[state.icontact] += state.T_c\n",
    "    \n",
    "fig, ax = plt.subplots(1, 2, dpi=150)\n",
    "\n",
    "annotated_imshow(ax[0], T_sc)\n",
    "ax[0].set_ylabel(r'from mode \\#')\n",
    "ax[0].set_xlabel(r'to contact \\#')\n",
    "\n",
    "annotated_imshow(ax[1], T_cc)\n",
    "ax[1].set_ylabel(r'from contact \\#')\n",
    "ax[1].set_xlabel(r'to contact \\#')\n",
    "pass"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Calculating Density\n",
    "\n",
    "After showing the capability of calculating the wavefunctions for a single energy, we show how to calculate the density (and current in the next section). The density and current are integrations over energy. These integrations are done using a custom adaptive Simpson integrator."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We need to define the chemical potential at the contacts and the temperature\n",
    "(room temperature) to use for the injection of electrons and holes."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from petra.transport import EquilibriumElectronHoleOccupation\n",
    "T = 300 * c.K\n",
    "mu_c = [contact.bandstructure().find_cb() + 0.025 * c.eV for contact in structure.elements.contacts]\n",
    "occupation = EquilibriumElectronHoleOccupation(structure, mu_c, T)\n",
    "structure.set_occupation(occupation)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We then perform a `node_density` simulation, which will perform an integration over energy, $E$, until the specified tolerance has been achieved, or the number of evaluations exceeds the specified limit."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from petra.transport import node_density"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%time\n",
    "node_rho, node_density_ss = node_density(structure, max_eval=1000, atol=1e14/c.cm3, mode='E', verbose=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Alternatively, we can integrate over $k$ instead of $E$ using"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%time\n",
    "knode_rho, knode_density_ss = node_density(structure, max_eval=200, atol=1e14/c.cm3, mode='k', verbose=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can visualize the energies at which the states have been calculated. We plot them together with the bandstructure of each contact as a visual aid. Note that the refinement actively picks up the regions of high density of states, i.e. near the band edges. Doing so, we don't waste effort on less important regions of energy."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from petra.simulation import bandstructure as bandsim\n",
    "from petra import util\n",
    "\n",
    "fig, ax = plt.subplots(1, 2, dpi=150, sharey=True, constrained_layout=True)\n",
    "\n",
    "node_density_E = np.unique([state.E for state in node_density_ss.states])\n",
    "knode_density_E = np.unique([state.E for state in knode_density_ss.states])\n",
    "for Ei in node_density_E:\n",
    "    ax[0].axhline(Ei / c.eV, lw=.5, alpha=.2, c='k')\n",
    "for Ei in knode_density_E:\n",
    "    ax[1].axhline(Ei / c.eV, lw=.5, alpha=.2, c='k')\n",
    "    \n",
    "colors = ('b', 'r')\n",
    "\n",
    "for ii, (mui, color) in enumerate(zip(occupation.mu_c, colors)):\n",
    "    for axi in ax:\n",
    "        axi.axhline(mui / c.eV, ls='--', color=color, lw=1)\n",
    "\n",
    "for ii, (contact, color) in enumerate(zip(structure.elements.contacts, colors)):\n",
    "    kmax_a = contact.k_na[-1]\n",
    "    k_ka = util.linspace(-kmax_a, kmax_a, 201)\n",
    "    \n",
    "    bandstruct = bandsim.calculate(contact, k_ka)\n",
    "    for axi in ax:\n",
    "        bandsim.plot(bandstruct, direction=2, ax=axi, c=color, lw=1,)\n",
    "    \n",
    "ax[0].set_ylim(node_density_E.min()/c.eV - .1, \n",
    "               node_density_E.max()/c.eV + .1)\n",
    "for axi in ax:\n",
    "    axi.set_xlabel('$k$')\n",
    "    axi.set_xticks([-.5, -.25, 0, .25, .5])\n",
    "    axi.set_xticklabels([r'$-\\pi/a$', r'$-\\pi/2a$', '$0$', r'$\\pi/2a$', r'$\\pi/a$'])\n",
    "    axi.set_xlim(-.25, .25)\n",
    "\n",
    "ax[0].set_ylabel(r'$E$ (eV)')\n",
    "ax[0].set_title('integration along E')\n",
    "ax[1].set_title('integration along k');"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "While interesting, the goal is of course the calculated density, and like with the wavefunction and ldos, we can plot the node-averaged density (irregardless of whether it was our integrand, or not):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, ax = plt.subplots(dpi=150, constrained_layout=True)\n",
    "axr = ax.twinx()\n",
    "\n",
    "l1, = ax.plot(z, node_rho * c.cm3, 'k-', label='linear')\n",
    "l2, = ax.plot(z, knode_rho * c.cm3, 'r-', label='linear', lw=1)\n",
    "l3, = axr.semilogy(z, node_rho * c.cm3, 'k--', label='log')\n",
    "l4, = axr.semilogy(z, knode_rho * c.cm3, 'r--', label='log', lw=1)\n",
    "ax.set_ylabel(r'$\\langle\\rho\\rangle_\\mathrm{node}\\ \\mathrm{[cm^{-3}]}$')\n",
    "axr.set_ylabel(r'$\\langle\\rho\\rangle_\\mathrm{node}\\ \\mathrm{[cm^{-3}]}$')\n",
    "ax.set_xlabel(r'$z\\ \\mathrm{[nm]}$')\n",
    "ax.legend(handles=[l1, l3], loc='lower left', title='integration along E')\n",
    "axr.legend(handles=[l2, l4], loc='lower right', title='integration along k')\n",
    "pass"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can reconstruct the full 3D density and current density (these will take some time)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%time\n",
    "rho = node_density_ss.density()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%time\n",
    "J = node_density_ss.current_density()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Visualizing multi-dimensional data is a specialized task, and we support output\n",
    "to the Visualization TookKit (vtk) data format. The .vtu file can be visualized\n",
    "using Paraview. Note that we specify `triangulate=True` to generate and save a\n",
    "triangulated mesh, instead of just points, making operations in Paraview easier."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "structure.write_atoms('2_atoms.vtu')\n",
    "structure.write_data('2_charge_density.vtu', triangulate=True, rho=rho * c.cm3, J=J / (c.A / c.nm**2))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Calculating Current"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Similar to the density, we can also calculate the current to a requested tolerance. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from petra.transport import current\n",
    "\n",
    "current_integ, current_ss = current(structure, max_eval=300, atol=1e-12 * c.A,  return_full=True)\n",
    "current = current_integ.value"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The reason for performing a separate current calculation should be clear from comparing the evaluated energies to those of the density calculation:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from petra.simulation import bandstructure as bandsim\n",
    "from petra import util\n",
    "\n",
    "fig, (ax, ax1) = plt.subplots(1, 2, dpi=150, sharey=True, constrained_layout=True)\n",
    "\n",
    "current_E = np.unique([state.E for state in current_ss.states])\n",
    "for Ei in current_E:\n",
    "    ax.axhline(Ei / c.eV, lw=.5, alpha=.2, c='k')\n",
    "    \n",
    "colors = ('b', 'k')\n",
    "\n",
    "for ii, (mui, color) in enumerate(zip(occupation.mu_c, colors)):\n",
    "    ax.axhline(mui / c.eV, ls='--', color=color)\n",
    "    \n",
    "for ii, (contact, color) in enumerate(zip(structure.elements.contacts, colors)):\n",
    "    kmax = contact.k_na[-1]\n",
    "    k = util.linspace(-kmax, kmax, 201)\n",
    "    \n",
    "    bandstruct = bandsim.calculate(contact, k)\n",
    "    \n",
    "    bandsim.plot(bandstruct, direction=2, c=color, ax=ax)\n",
    "    \n",
    "ax.set_ylim(current_E.min()/c.eV - .1, \n",
    "            current_E.max()/c.eV + .1)\n",
    "ax.set_ylabel(r'$E\\ \\mathrm{[eV]}$')\n",
    "\n",
    "\n",
    "ax.set_xlabel(r'$k$')\n",
    "ax.set_xticks([-.5, -.25, 0, .25, .5])\n",
    "ax.set_xticklabels(['$-\\pi/a$', '$-\\pi/2a$', '$0$', '$\\pi/2a$', '$\\pi/a$'])\n",
    "ax.set_xlim(-.25, .25)\n",
    "\n",
    "idxsort = np.argsort(current_integ.x)\n",
    "Eplt = current_integ.x[idxsort] / c.eV\n",
    "Jplt = np.array([value[0] * np.ones((2,2)) for value in current_integ.f])[idxsort]\n",
    "\n",
    "l1, = ax1.semilogx(-Jplt[:,0,1], Eplt, 'kx-', label=r'log', ms=4, lw=1)\n",
    "ax2 = ax1.twiny()\n",
    "l2, = ax2.plot(-Jplt[:,0,1], Eplt, 'rx-', label=r'linear', ms=4, lw=1)\n",
    "\n",
    "ax1.legend(handles=[l1, l2], loc=0)\n",
    "ax1.set_xlabel(r'$j(E)\\ \\mathrm{[A/eV]}$')\n",
    "ax2.set_xlim(0, None);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And, finally, we show the fully integrated current (in $\\mathrm{\\mu A}$)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, ax = plt.subplots(dpi=150)\n",
    "annotated_imshow(ax, current / (1e-6 * c.A), cmap='RdBu')\n",
    "plt.ylabel(r'from contact \\#')\n",
    "plt.xlabel(r'to contact \\#')\n",
    "plt.title('$J$ ($\\mu$A)')\n",
    "pass"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Since the potential was arbitrarily chosen, the current and density have little to do with a real physical system. In the next part, we'll setup a fully self-consistent Schroedinger-Poisson solver that will allow us to study a realistic system."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.12"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
