import numpy as np


def psi0(x, h=1):
    return np.piecewise(
        x,
        [
            (-h <= x) & (x < 0),
            (0 <= x) & (x < h)
        ],
        [
            lambda x_: (h + x_),
            lambda x_: (h - x_),
            0.
        ]) / h


def dpsi0(x, h=1):
    return np.piecewise(
        x,
        [
            (-h == x),
            (-h < x) & (x < 0),
            (0 < x) & (x < h),
            (h == x),
        ],
        [
            .5,
            1,
            -1,
            -.5,
            0.
        ]) / h


def psi1(x, h=1):
    return np.piecewise(
        x,
        [
            (-h <= x) & (x < 0),
            (0 <= x) & (x < h)],
        [
            lambda x_: (h + x_)**2 * (h - 2 * x_),
            lambda x_: (h - x_)**2 * (h + 2 * x_),
            0.
        ]) / h**3


def dpsi1(x, h=1):
    return np.piecewise(
        x,
        [
            (-h <= x) & (x < 0),
            (0 <= x) & (x < h)],
        [
            lambda x_: -6 * (h + x_) * x_,
            lambda x_: -6 * (h - x_) * x_,
            0.
        ]) / h**3


def psi2(x, h=1):
    return np.piecewise(
        x,
        [
            (-h <= x) & (x < h)
        ],
        [
            lambda x_: (1 + np.cos(np.pi * x_ / h)) / 2,
            0.
        ])


def dpsi2(x, h=1):
    return np.piecewise(
        x,
        [
            (-h <= x) & (x < h)
        ],
        [
            lambda x_: - np.pi * np.sin(np.pi * x_ / h) / (2 * h),
            0.
        ])


def psi3(x, h=1):
    return np.piecewise(
        x,
        [
            (-h <= x) & (x < 0),
            (0 <= x) & (x < h)],
        [
            lambda x_: (h + x_)**3 * (h**2 - 3 * h * x_ + 6 * x_**2),
            lambda x_: (h - x_)**3 * (h**2 + 3 * h * x_ + 6 * x_**2),
            0.
        ]) / h**5


def dpsi3(x, h=1):
    return np.piecewise(
        x,
        [
            (-h <= x) & (x < 0),
            (0 <= x) & (x < h)],
        [
            lambda x_: 30 * (h + x_)**2 * x_**2,
            lambda x_: -30 * (h - x_)**2 * x_**2,
            0.
        ]) / h**5
