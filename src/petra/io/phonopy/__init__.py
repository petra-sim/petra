from collections import namedtuple

import numpy as np
import yaml
import ase

from petra import constants as c

THz = 1e12 / c.s
twopiTHz = 2 * np.pi * THz

PhonopyResult = namedtuple('PhonopyResult', 'atoms k_ka E_kn weight_k')


def load(path):
    with open(path) as f:
        data = yaml.load(f, Loader=yaml.FullLoader)

    cell = np.asarray(data['lattice'])
    pos = np.asarray([point['coordinates'] for point in data['points']])
    sym = np.asarray([point['symbol'] for point in data['points']])
    atoms = ase.Atoms(symbols=sym, scaled_positions=pos, cell=cell)

    k_ka = np.asarray([phonon['q-position'] for phonon in data['phonon']])
    E_kn = np.asarray([[band['frequency'] for band in phonon['band']]
                       for phonon in data['phonon']]) * twopiTHz
    weight_k = np.asarray([phonon.get('weight', 1) for phonon in data['phonon']])

    return PhonopyResult(atoms, k_ka, E_kn, weight_k)


def load_dos(path):
    units = np.array([twopiTHz, twopiTHz**-1])
    return (np.loadtxt(path, skiprows=1) * units).T
