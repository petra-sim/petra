PETRA
=====

PETRA is a simulation package for calculating quantum transport solver in low-dimensional
structures at the atomic scale.

PETRA's features include

- Empirical pseudopotential calculations in a plane-wave basis.
- Ballistic quantum transport using a Bloch-wave expansion.
- Scattering using the Pauli master equation (work in progress).

If PETRA is useful in your research please cite our :ref:`label-publications`.

Documentation
-------------

.. toctree::
   :maxdepth: 1

   about
   usage/installation
   usage/examples
   develop
   modules
